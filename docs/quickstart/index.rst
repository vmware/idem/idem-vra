==========
Quickstart
==========

Get started with **idem-vra** in 5 minutes.

Idem is an Apache licensed project for managing complex cloud environments.

Idem-vra is an Idem plugin to allow you to manage many parts of the VRA cloud or on-prem instances.


Supported VRA resources
+++++++++++++++++++++++

.. grid:: 2

    .. grid-item-card:: Idem Exec Modules
        :link: /ref/exec/index
        :link-type: doc

        Exec Modules

        :bdg-info:`VRA`

    .. grid-item-card:: Idem State Modules
        :link: /ref/states/index
        :link-type: doc

        State Modules

        :bdg-info:`VRA`



The first step is to :doc:`install Idem and Idem-vra. </quickstart/install/index>`

.. toctree::
   :maxdepth: 1
   :glob:
   :hidden:

   install/index
   configure/index
   commands/index
   more/index
