State
=====

The *State* command allows you to ensure your environment is configured how you
specify. You specify your configuration in an *SLS* file, also known as a
*state* file.

State sls:

.. code-block:: sls

    AD-Integration:
        vra.iaas.integration.present:
        - integrationType: activedirectory
        - integrationProperties:
            server: ldaps://xx.xxx.xxx.xxx:xxx
            user: user@domain
            privateKey: <password>
            defaultOU: dc=vclass,dc=local
            alternativeHost: null
            connectionTimeout: 10
            dcId: onprem
        - name: AD-Integration
        - description: Idem created activedirectory integration
        - tags:
            - key: origin
            value: Idem
        - certificateInfo:
            certificate: ${exec:VC Certificate:cert}

State Example
++++++++++++++++

.. code-block:: bash

    idem state test.sls

