Configure idem-vra
==================

Credential management in Idem is handled by the Idem  **acct** module. Details
about this module can be found on the `acct gitlab repo README
<https://gitlab.com/vmware/idem/acct>`_. By default the `idem-vra` acct plugin
will use the default vra configuration that the logged in user already has
configured.

If you have already configured your environment with access to your AWS account
you can skip to the :doc:`Idem Command page.  </quickstart/commands/index>`
