.. idem-vra documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem-vra's Documentation!
====================================


.. toctree::
   :maxdepth: 3
   :glob:
   :hidden:

   topics/idem-vra
   quickstart/index
   tutorial/index
   releases/index

   ref/exec/index
   ref/states/index

..
   .. include:: _includes/reference-toc.rst

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:
   

   topics/contributing
   topics/license

   Project Repository <https://gitlab.com/vmware/idem/idem-vra/>

Indices and tables
==================

* :ref:`modindex`
