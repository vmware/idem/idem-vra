permission
==========

.. automodule:: idem_vra.states.vra.rbac.permission
   :members:
   :undoc-members:
   :show-inheritance:
