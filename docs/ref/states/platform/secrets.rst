secrets
=======

.. automodule:: idem_vra.states.vra.platform.secrets
   :members:
   :undoc-members:
   :show-inheritance:
