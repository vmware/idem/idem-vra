variables
=========

.. automodule:: idem_vra.states.vra.pipeline.variables
   :members:
   :undoc-members:
   :show-inheritance:
