executions
==========

.. automodule:: idem_vra.states.vra.pipeline.executions
   :members:
   :undoc-members:
   :show-inheritance:
