integration
===========

.. automodule:: idem_vra.states.vra.iaas.integration
   :members:
   :undoc-members:
   :show-inheritance:
