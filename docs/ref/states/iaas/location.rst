location
========

.. automodule:: idem_vra.states.vra.iaas.location
   :members:
   :undoc-members:
   :show-inheritance:
