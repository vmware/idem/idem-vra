pipelines
=========

.. automodule:: idem_vra.exec.vra.pipeline.pipelines
   :members:
   :undoc-members:
   :show-inheritance:
