variables
=========

.. automodule:: idem_vra.exec.vra.pipeline.variables
   :members:
   :undoc-members:
   :show-inheritance:
