logs
====

.. automodule:: idem_vra.exec.vra.iaas.logs
   :members:
   :undoc-members:
   :show-inheritance:
