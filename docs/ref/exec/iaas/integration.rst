integration
===========

.. automodule:: idem_vra.exec.vra.iaas.integration
   :members:
   :undoc-members:
   :show-inheritance:
