about
=====

.. automodule:: idem_vra.exec.vra.platform.about
   :members:
   :undoc-members:
   :show-inheritance:
