import asyncio
from typing import Any
from typing import Dict

import aiohttp


async def request(
    hub,
    ctx,
    method: str,
    headers: Dict[str, Any],
    path: str = "",
    data: Any = None,
    json: Dict[str, Any] = None,
    **kwargs,
):
    async def on_request_start(session, trace_config_ctx, params):
        hub.log.debug(f"=> {params.method} {params.url}")
        trace_config_ctx.start = asyncio.get_event_loop().time()

    async def on_request_end(session, trace_config_ctx, params):
        elapsed = asyncio.get_event_loop().time() - trace_config_ctx.start
        hub.log.debug(f"<= {params.method} {params.url} ({round(elapsed, 5)} seconds)")

    trace_config = aiohttp.TraceConfig()
    trace_config.on_request_start.append(on_request_start)
    trace_config.on_request_end.append(on_request_end)

    if "acct" in ctx:
        url = "/".join((ctx.acct.vra_url, path))
    else:
        url = "/".join((ctx.vra_url, path))

    async with aiohttp.ClientSession(
        trace_configs=[trace_config],
        loop=hub.pop.Loop,
        raise_for_status=False,
        trust_env=True,
    ) as session:
        async with session.request(
            method=method,
            url=url,
            headers=headers,
            data=data,
            json=json,
            params=kwargs,
            verify_ssl=False,
            allow_redirects=True,
        ) as response:
            return await handle_response(response)


async def handle_response(response: aiohttp.ClientResponse) -> Any:
    """Handle aiohttp response

    Args:
        response (aiohttp.ClientResponse): client response

    Raises:
        Exception: exception containing additional information with the status code and the response body

    Returns:
        Any: parsed response
    """

    binary_types = ["application/octet-stream", "application/zip"]
    body = None

    if response.headers.get("content-type") == "application/json":
        body = await response.json()
    elif response.headers.get("content-type") in binary_types:
        body = await response.read()
    else:
        body = await response.text()

    if response.status >= 400:
        raise Exception(f"HTTP Error {response.status}: {body}")

    return body
