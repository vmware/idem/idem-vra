import base64
import os
import tempfile
import zipfile
import zlib


def compress_string(hub, data: str) -> str:
    cmpstr = zlib.compress(data.encode("utf-8"))
    return base64.b64encode(cmpstr).decode()


def uncompress_string(hub, data: str) -> str:
    cmprdata = base64.b64decode(data)
    return zlib.decompress(cmprdata).decode()


def read_binary(hub, uri: str) -> bytes:
    scheme = uri[: uri.index(":")]
    if scheme == "file":
        filename = os.path.basename(uri)
        file_extension = os.path.splitext(filename)[1]
        if file_extension == ".zip":
            return read_zip(uri)
        else:
            return read_file(uri)
    elif scheme == "http":
        return read_http(uri)
    elif scheme == "https":
        return read_https(uri)
    elif scheme == "s3":
        return read_s3(uri)


def read_file(uri: str) -> bytes:
    file_name = os.path.abspath(uri.split("file://")[1])
    # Check if the path exists and is a file
    if os.path.isfile(file_name):
        with open(file_name, "rb") as file:
            binary_data = file.read()
            return binary_data
    else:
        raise Exception(f"{file_name} file not found or it is not file")


def read_http(uri: str) -> bytes:
    raise Exception("HTTP content is not supported yet")


def read_https(uri: str) -> bytes:
    raise Exception("HTTP content is not supported yet")


def read_s3(uri: str) -> bytes:
    raise Exception("S3 content is not supported yet")


def write_binary(hub, uri: str, byte_data: bytes, **kwargs) -> list:
    scheme = uri[: uri.index(":")]
    file_path_name = uri.split("//")[1]
    if scheme == "zip":
        extract_file = kwargs.get("extract_file", None)
        extract_all = kwargs.get("extract_all", None)
        extract_all_with_extension = kwargs.get("extract_all_with_extension", None)
        return extract_zip(
            byte_data, extract_file, extract_all, extract_all_with_extension
        )
    elif scheme == "file":
        return write_file(byte_data, file_path_name=file_path_name)
    elif scheme == "http":
        return write_http(byte_data, uri)
    elif scheme == "https":
        return write_https(byte_data, uri)
    elif scheme == "s3":
        return write_s3(byte_data, uri)


def write_file(byte_data: bytes, file_path_name) -> list:
    if not os.path.exists(file_path_name):
        try:
            os.makedirs(os.path.dirname(file_path_name))
        except OSError as e:
            raise Exception(
                f"supplied directory was not present , failed to create directory at {file_path_name} \n "
                f"more details about exception {e}"
            )
    try:
        with open(file_path_name, "wb") as file:
            return list(file_path_name)
    except OSError as e:
        raise Exception(f"unable to write to file, more details about exception {e}")


def read_zip(uri: str) -> bytes:
    file_name = os.path.abspath(uri.split("file://")[1])
    try:
        with open(file_name, "rb") as file:
            binary_data = file.read()
            return binary_data
    except Exception as e:
        raise Exception(f"unable to read file: {file_name} got Exception : {e}")


def extract_zip(
    byte_data: bytes,
    extract_file: str = None,
    extract_all: bool = False,
    extract_all_with_extension: str = None,
) -> list:
    """

    Parameters
    ----------
    byte_data
    extract_file
    extract_all
    extract_all_with_extension

    Returns
    -------

    """
    with tempfile.NamedTemporaryFile(
        mode="wb", delete=True, suffix=".zip"
    ) as temp_file:
        temp_file.write(byte_data)
        try:
            with zipfile.ZipFile(temp_file.name, mode="r") as archive:
                extracted_file_paths: list[str] = []
                if extract_all:
                    file_names = archive.namelist()
                    archive.extractall()
                    extracted_file_paths = [
                        "file://" + os.path.abspath(file_name)
                        for file_name in file_names
                    ]
                    return extracted_file_paths
                elif extract_file:
                    archive.extract(extract_file)
                    extracted_file_paths.append(
                        "file://" + os.path.abspath(extract_file)
                    )
                    return extracted_file_paths
                elif extract_all_with_extension:
                    for file in archive.namelist():
                        if file.endswith(extract_all_with_extension):
                            archive.extract(file)
                            extracted_file_paths.append(
                                "file://" + os.path.abspath(file)
                            )
                            return extracted_file_paths
        except zipfile.BadZipFile as error:
            return error


def write_http(byte_data: bytes, uri: str) -> list:
    raise Exception("HTTP content is not supported yet")


def write_https(byte_data: bytes, uri: str) -> list:
    raise Exception("HTTP content is not supported yet")


def write_s3(byte_data: bytes, uri: str) -> list:
    raise Exception("S3 content is not supported yet")
