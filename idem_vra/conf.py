import os
import sys

# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data
# In this dictionary goes all the immutable values you want to show up under hub.OPT.idem_vra
CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a configuration file onto hub.OPT.idem_vra",
    },
    "debug_clients": {
        "default": False,
        "action": "store_true",
        "subcommands": ["state", "describe"],
        "dyne": "idem",
    },
}

# The selected subcommand for your cli tool will show up under hub.SUBPARSER
# The value for a subcommand is a dictionary that will be passed as kwargs to argparse.ArgumentParser.add_subparsers
SUBCOMMANDS = {
    # "my_sub_command": {}
}

# Include keys from the CONFIG dictionary that you want to expose on the cli
# The values for these keys are a dictionaries that will be passed as kwargs to argparse.ArgumentParser.add_option
CLI_CONFIG = {
    "config": {"options": ["-c"]},
    "debug_clients": {
        "default": False,
        "help": "Enable client libraries debug logs",
        "subcommands": ["state", "describe"],
        "dyne": "idem",
    },
    # "my_option1": {"subcommands": ["A list of subcommands that exclusively extend this option"]},
    # This option will be available under all subcommands and the root command
    # "my_option2": {"subcommands": ["_global_"]},
}

# These are the namespaces that your project extends
# The hub will extend these keys with the modules listed in the values
DYNE = {"tool": ["tool"], "states": ["states"], "acct": ["acct"], "exec": ["exec"]}

sys.path.insert(0, os.path.abspath(os.path.join("..", "..")))
