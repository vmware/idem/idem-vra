# coding: utf-8

"""
    VMware Aria Automation Assembler IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API.  This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout VMware Aria Automation Assembler(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).  The APIs that list collections of resources also support OData like implementation. Below query params can be used across different IAAS API endpoints  Example: 1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.     ```/iaas/api/cloud-accounts?$orderby=name%20desc```  2. `$top` -  number of records you want to get.     ```/iaas/api/cloud-accounts?$top=20```  3. `$skip` -  number of records you want to skip.      ```/iaas/api/cloud-accounts?$skip=10```  4. `$select` - select a subset of properties to include in the response.        ```/project-service/api/projects?$top=10&$skip=2```  5. `$filter` -  filter the results by a specified predicate expression. Operators: eq, ne, and, or.\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'``` - name starts with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'``` - name contains 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'``` - name ends with 'ABC'\\     /iaas/api/projects and /iaas/api/deployments support different format for partial match:\\     ```/iaas/api/projects?$filter=startswith(name, 'ABC')``` - name starts with 'ABC'\\     ```/iaas/api/projects?$filter=substringof('ABC', name``` - name contains 'ABC'\\     ```/iaas/api/projects?$filter=endswith(name, 'ABC')``` - name ends with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'```  6. `$count` -  flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.\\     ```/iaas/api/cloud-accounts?$count=true```   # noqa: E501

    OpenAPI spec version: 2021-07-15
    Contact: sales@vmware.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class RegionResultContent(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'created_at': 'str',
        'updated_at': 'str',
        'owner': 'str',
        'owner_type': 'str',
        'org_id': 'str',
        'links': 'object',
        'external_region_id': 'str',
        'name': 'str',
        'cloud_account_id': 'str'
    }

    attribute_map = {
        'id': 'id',
        'created_at': 'createdAt',
        'updated_at': 'updatedAt',
        'owner': 'owner',
        'owner_type': 'ownerType',
        'org_id': 'orgId',
        'links': '_links',
        'external_region_id': 'externalRegionId',
        'name': 'name',
        'cloud_account_id': 'cloudAccountId'
    }

    def __init__(self, id=None, created_at=None, updated_at=None, owner=None, owner_type=None, org_id=None, links=None, external_region_id=None, name=None, cloud_account_id=None):  # noqa: E501
        """RegionResultContent - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._created_at = None
        self._updated_at = None
        self._owner = None
        self._owner_type = None
        self._org_id = None
        self._links = None
        self._external_region_id = None
        self._name = None
        self._cloud_account_id = None
        self.discriminator = None
        self.id = id
        if created_at is not None:
            self.created_at = created_at
        if updated_at is not None:
            self.updated_at = updated_at
        if owner is not None:
            self.owner = owner
        if owner_type is not None:
            self.owner_type = owner_type
        if org_id is not None:
            self.org_id = org_id
        self.links = links
        self.external_region_id = external_region_id
        if name is not None:
            self.name = name
        if cloud_account_id is not None:
            self.cloud_account_id = cloud_account_id

    @property
    def id(self):
        """Gets the id of this RegionResultContent.  # noqa: E501

        The id of this resource instance  # noqa: E501

        :return: The id of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this RegionResultContent.

        The id of this resource instance  # noqa: E501

        :param id: The id of this RegionResultContent.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def created_at(self):
        """Gets the created_at of this RegionResultContent.  # noqa: E501

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :return: The created_at of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this RegionResultContent.

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :param created_at: The created_at of this RegionResultContent.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def updated_at(self):
        """Gets the updated_at of this RegionResultContent.  # noqa: E501

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :return: The updated_at of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this RegionResultContent.

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :param updated_at: The updated_at of this RegionResultContent.  # noqa: E501
        :type: str
        """

        self._updated_at = updated_at

    @property
    def owner(self):
        """Gets the owner of this RegionResultContent.  # noqa: E501

        Email of the user or display name of the group that owns the entity.  # noqa: E501

        :return: The owner of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this RegionResultContent.

        Email of the user or display name of the group that owns the entity.  # noqa: E501

        :param owner: The owner of this RegionResultContent.  # noqa: E501
        :type: str
        """

        self._owner = owner

    @property
    def owner_type(self):
        """Gets the owner_type of this RegionResultContent.  # noqa: E501

        Type of a owner(user/ad_group) that owns the entity.  # noqa: E501

        :return: The owner_type of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._owner_type

    @owner_type.setter
    def owner_type(self, owner_type):
        """Sets the owner_type of this RegionResultContent.

        Type of a owner(user/ad_group) that owns the entity.  # noqa: E501

        :param owner_type: The owner_type of this RegionResultContent.  # noqa: E501
        :type: str
        """

        self._owner_type = owner_type

    @property
    def org_id(self):
        """Gets the org_id of this RegionResultContent.  # noqa: E501

        The id of the organization this entity belongs to.  # noqa: E501

        :return: The org_id of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this RegionResultContent.

        The id of the organization this entity belongs to.  # noqa: E501

        :param org_id: The org_id of this RegionResultContent.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def links(self):
        """Gets the links of this RegionResultContent.  # noqa: E501

        hrefs links  # noqa: E501

        :return: The links of this RegionResultContent.  # noqa: E501
        :rtype: object
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this RegionResultContent.

        hrefs links  # noqa: E501

        :param links: The links of this RegionResultContent.  # noqa: E501
        :type: object
        """
        if links is None:
            raise ValueError("Invalid value for `links`, must not be `None`")  # noqa: E501

        self._links = links

    @property
    def external_region_id(self):
        """Gets the external_region_id of this RegionResultContent.  # noqa: E501

        Unique identifier of region on the provider side.  # noqa: E501

        :return: The external_region_id of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._external_region_id

    @external_region_id.setter
    def external_region_id(self, external_region_id):
        """Sets the external_region_id of this RegionResultContent.

        Unique identifier of region on the provider side.  # noqa: E501

        :param external_region_id: The external_region_id of this RegionResultContent.  # noqa: E501
        :type: str
        """
        if external_region_id is None:
            raise ValueError("Invalid value for `external_region_id`, must not be `None`")  # noqa: E501

        self._external_region_id = external_region_id

    @property
    def name(self):
        """Gets the name of this RegionResultContent.  # noqa: E501

        Name of region on the provider side. In vSphere, the name of the region is different from its id.  # noqa: E501

        :return: The name of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this RegionResultContent.

        Name of region on the provider side. In vSphere, the name of the region is different from its id.  # noqa: E501

        :param name: The name of this RegionResultContent.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def cloud_account_id(self):
        """Gets the cloud_account_id of this RegionResultContent.  # noqa: E501

        The id of the cloud account this region belongs to. For some cloud accounts this field will be populated with the associated cloud account id  # noqa: E501

        :return: The cloud_account_id of this RegionResultContent.  # noqa: E501
        :rtype: str
        """
        return self._cloud_account_id

    @cloud_account_id.setter
    def cloud_account_id(self, cloud_account_id):
        """Sets the cloud_account_id of this RegionResultContent.

        The id of the cloud account this region belongs to. For some cloud accounts this field will be populated with the associated cloud account id  # noqa: E501

        :param cloud_account_id: The cloud_account_id of this RegionResultContent.  # noqa: E501
        :type: str
        """

        self._cloud_account_id = cloud_account_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(RegionResultContent, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RegionResultContent):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
