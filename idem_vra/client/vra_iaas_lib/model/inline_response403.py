# coding: utf-8

"""
    VMware Aria Automation Assembler IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API.  This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout VMware Aria Automation Assembler(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).  The APIs that list collections of resources also support OData like implementation. Below query params can be used across different IAAS API endpoints  Example: 1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.     ```/iaas/api/cloud-accounts?$orderby=name%20desc```  2. `$top` -  number of records you want to get.     ```/iaas/api/cloud-accounts?$top=20```  3. `$skip` -  number of records you want to skip.      ```/iaas/api/cloud-accounts?$skip=10```  4. `$select` - select a subset of properties to include in the response.        ```/project-service/api/projects?$top=10&$skip=2```  5. `$filter` -  filter the results by a specified predicate expression. Operators: eq, ne, and, or.\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'``` - name starts with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'``` - name contains 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'``` - name ends with 'ABC'\\     /iaas/api/projects and /iaas/api/deployments support different format for partial match:\\     ```/iaas/api/projects?$filter=startswith(name, 'ABC')``` - name starts with 'ABC'\\     ```/iaas/api/projects?$filter=substringof('ABC', name``` - name contains 'ABC'\\     ```/iaas/api/projects?$filter=endswith(name, 'ABC')``` - name ends with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'```  6. `$count` -  flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.\\     ```/iaas/api/cloud-accounts?$count=true```   # noqa: E501

    OpenAPI spec version: 2021-07-15
    Contact: sales@vmware.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse403(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'message': 'str',
        'message_id': 'str',
        'stack_trace': 'list[str]',
        'status_code': 'int',
        'error_code': 'int',
        'details': 'list[str]',
        'server_error_id': 'str',
        'document_kind': 'str',
        'internal_error_code': 'int'
    }

    attribute_map = {
        'message': 'message',
        'message_id': 'messageId',
        'stack_trace': 'stackTrace',
        'status_code': 'statusCode',
        'error_code': 'errorCode',
        'details': 'details',
        'server_error_id': 'serverErrorId',
        'document_kind': 'documentKind',
        'internal_error_code': 'internalErrorCode'
    }

    def __init__(self, message=None, message_id=None, stack_trace=None, status_code=None, error_code=None, details=None, server_error_id=None, document_kind=None, internal_error_code=None):  # noqa: E501
        """InlineResponse403 - a model defined in Swagger"""  # noqa: E501
        self._message = None
        self._message_id = None
        self._stack_trace = None
        self._status_code = None
        self._error_code = None
        self._details = None
        self._server_error_id = None
        self._document_kind = None
        self._internal_error_code = None
        self.discriminator = None
        if message is not None:
            self.message = message
        if message_id is not None:
            self.message_id = message_id
        if stack_trace is not None:
            self.stack_trace = stack_trace
        if status_code is not None:
            self.status_code = status_code
        if error_code is not None:
            self.error_code = error_code
        if details is not None:
            self.details = details
        if server_error_id is not None:
            self.server_error_id = server_error_id
        if document_kind is not None:
            self.document_kind = document_kind
        if internal_error_code is not None:
            self.internal_error_code = internal_error_code

    @property
    def message(self):
        """Gets the message of this InlineResponse403.  # noqa: E501


        :return: The message of this InlineResponse403.  # noqa: E501
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message):
        """Sets the message of this InlineResponse403.


        :param message: The message of this InlineResponse403.  # noqa: E501
        :type: str
        """

        self._message = message

    @property
    def message_id(self):
        """Gets the message_id of this InlineResponse403.  # noqa: E501


        :return: The message_id of this InlineResponse403.  # noqa: E501
        :rtype: str
        """
        return self._message_id

    @message_id.setter
    def message_id(self, message_id):
        """Sets the message_id of this InlineResponse403.


        :param message_id: The message_id of this InlineResponse403.  # noqa: E501
        :type: str
        """

        self._message_id = message_id

    @property
    def stack_trace(self):
        """Gets the stack_trace of this InlineResponse403.  # noqa: E501


        :return: The stack_trace of this InlineResponse403.  # noqa: E501
        :rtype: list[str]
        """
        return self._stack_trace

    @stack_trace.setter
    def stack_trace(self, stack_trace):
        """Sets the stack_trace of this InlineResponse403.


        :param stack_trace: The stack_trace of this InlineResponse403.  # noqa: E501
        :type: list[str]
        """

        self._stack_trace = stack_trace

    @property
    def status_code(self):
        """Gets the status_code of this InlineResponse403.  # noqa: E501


        :return: The status_code of this InlineResponse403.  # noqa: E501
        :rtype: int
        """
        return self._status_code

    @status_code.setter
    def status_code(self, status_code):
        """Sets the status_code of this InlineResponse403.


        :param status_code: The status_code of this InlineResponse403.  # noqa: E501
        :type: int
        """

        self._status_code = status_code

    @property
    def error_code(self):
        """Gets the error_code of this InlineResponse403.  # noqa: E501


        :return: The error_code of this InlineResponse403.  # noqa: E501
        :rtype: int
        """
        return self._error_code

    @error_code.setter
    def error_code(self, error_code):
        """Sets the error_code of this InlineResponse403.


        :param error_code: The error_code of this InlineResponse403.  # noqa: E501
        :type: int
        """

        self._error_code = error_code

    @property
    def details(self):
        """Gets the details of this InlineResponse403.  # noqa: E501


        :return: The details of this InlineResponse403.  # noqa: E501
        :rtype: list[str]
        """
        return self._details

    @details.setter
    def details(self, details):
        """Sets the details of this InlineResponse403.


        :param details: The details of this InlineResponse403.  # noqa: E501
        :type: list[str]
        """
        allowed_values = ["SHOULD_RETRY"]  # noqa: E501
        if not set(details).issubset(set(allowed_values)):
            raise ValueError(
                "Invalid values for `details` [{0}], must be a subset of [{1}]"  # noqa: E501
                .format(", ".join(map(str, set(details) - set(allowed_values))),  # noqa: E501
                        ", ".join(map(str, allowed_values)))
            )

        self._details = details

    @property
    def server_error_id(self):
        """Gets the server_error_id of this InlineResponse403.  # noqa: E501


        :return: The server_error_id of this InlineResponse403.  # noqa: E501
        :rtype: str
        """
        return self._server_error_id

    @server_error_id.setter
    def server_error_id(self, server_error_id):
        """Sets the server_error_id of this InlineResponse403.


        :param server_error_id: The server_error_id of this InlineResponse403.  # noqa: E501
        :type: str
        """

        self._server_error_id = server_error_id

    @property
    def document_kind(self):
        """Gets the document_kind of this InlineResponse403.  # noqa: E501


        :return: The document_kind of this InlineResponse403.  # noqa: E501
        :rtype: str
        """
        return self._document_kind

    @document_kind.setter
    def document_kind(self, document_kind):
        """Sets the document_kind of this InlineResponse403.


        :param document_kind: The document_kind of this InlineResponse403.  # noqa: E501
        :type: str
        """

        self._document_kind = document_kind

    @property
    def internal_error_code(self):
        """Gets the internal_error_code of this InlineResponse403.  # noqa: E501


        :return: The internal_error_code of this InlineResponse403.  # noqa: E501
        :rtype: int
        """
        return self._internal_error_code

    @internal_error_code.setter
    def internal_error_code(self, internal_error_code):
        """Sets the internal_error_code of this InlineResponse403.


        :param internal_error_code: The internal_error_code of this InlineResponse403.  # noqa: E501
        :type: int
        """

        self._internal_error_code = internal_error_code

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse403, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse403):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
