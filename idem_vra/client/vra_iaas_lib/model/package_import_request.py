# coding: utf-8

"""
    VMware Aria Automation Assembler IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API.  This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout VMware Aria Automation Assembler(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).  The APIs that list collections of resources also support OData like implementation. Below query params can be used across different IAAS API endpoints  Example: 1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.     ```/iaas/api/cloud-accounts?$orderby=name%20desc```  2. `$top` -  number of records you want to get.     ```/iaas/api/cloud-accounts?$top=20```  3. `$skip` -  number of records you want to skip.      ```/iaas/api/cloud-accounts?$skip=10```  4. `$select` - select a subset of properties to include in the response.        ```/project-service/api/projects?$top=10&$skip=2```  5. `$filter` -  filter the results by a specified predicate expression. Operators: eq, ne, and, or.\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'``` - name starts with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'``` - name contains 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'``` - name ends with 'ABC'\\     /iaas/api/projects and /iaas/api/deployments support different format for partial match:\\     ```/iaas/api/projects?$filter=startswith(name, 'ABC')``` - name starts with 'ABC'\\     ```/iaas/api/projects?$filter=substringof('ABC', name``` - name contains 'ABC'\\     ```/iaas/api/projects?$filter=endswith(name, 'ABC')``` - name ends with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'```  6. `$count` -  flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.\\     ```/iaas/api/cloud-accounts?$count=true```   # noqa: E501

    OpenAPI spec version: 2021-07-15
    Contact: sales@vmware.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PackageImportRequest(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'compressed_bundle': 'list[str]',
        'bundle_id': 'str',
        'option': 'str',
        'properties': 'dict(str, str)'
    }

    attribute_map = {
        'compressed_bundle': 'compressedBundle',
        'bundle_id': 'bundleId',
        'option': 'option',
        'properties': 'properties'
    }

    def __init__(self, compressed_bundle=None, bundle_id=None, option=None, properties=None):  # noqa: E501
        """PackageImportRequest - a model defined in Swagger"""  # noqa: E501
        self._compressed_bundle = None
        self._bundle_id = None
        self._option = None
        self._properties = None
        self.discriminator = None
        if compressed_bundle is not None:
            self.compressed_bundle = compressed_bundle
        if bundle_id is not None:
            self.bundle_id = bundle_id
        if option is not None:
            self.option = option
        if properties is not None:
            self.properties = properties

    @property
    def compressed_bundle(self):
        """Gets the compressed_bundle of this PackageImportRequest.  # noqa: E501


        :return: The compressed_bundle of this PackageImportRequest.  # noqa: E501
        :rtype: list[str]
        """
        return self._compressed_bundle

    @compressed_bundle.setter
    def compressed_bundle(self, compressed_bundle):
        """Sets the compressed_bundle of this PackageImportRequest.


        :param compressed_bundle: The compressed_bundle of this PackageImportRequest.  # noqa: E501
        :type: list[str]
        """

        self._compressed_bundle = compressed_bundle

    @property
    def bundle_id(self):
        """Gets the bundle_id of this PackageImportRequest.  # noqa: E501


        :return: The bundle_id of this PackageImportRequest.  # noqa: E501
        :rtype: str
        """
        return self._bundle_id

    @bundle_id.setter
    def bundle_id(self, bundle_id):
        """Sets the bundle_id of this PackageImportRequest.


        :param bundle_id: The bundle_id of this PackageImportRequest.  # noqa: E501
        :type: str
        """

        self._bundle_id = bundle_id

    @property
    def option(self):
        """Gets the option of this PackageImportRequest.  # noqa: E501


        :return: The option of this PackageImportRequest.  # noqa: E501
        :rtype: str
        """
        return self._option

    @option.setter
    def option(self, option):
        """Sets the option of this PackageImportRequest.


        :param option: The option of this PackageImportRequest.  # noqa: E501
        :type: str
        """
        allowed_values = ["FAIL", "OVERWRITE"]  # noqa: E501
        if option not in allowed_values:
            raise ValueError(
                "Invalid value for `option` ({0}), must be one of {1}"  # noqa: E501
                .format(option, allowed_values)
            )

        self._option = option

    @property
    def properties(self):
        """Gets the properties of this PackageImportRequest.  # noqa: E501


        :return: The properties of this PackageImportRequest.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._properties

    @properties.setter
    def properties(self, properties):
        """Sets the properties of this PackageImportRequest.


        :param properties: The properties of this PackageImportRequest.  # noqa: E501
        :type: dict(str, str)
        """

        self._properties = properties

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PackageImportRequest, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PackageImportRequest):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
