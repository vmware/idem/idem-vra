# coding: utf-8

"""
    VMware Aria Automation Assembler IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API.  This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout VMware Aria Automation Assembler(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).  The APIs that list collections of resources also support OData like implementation. Below query params can be used across different IAAS API endpoints  Example: 1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.     ```/iaas/api/cloud-accounts?$orderby=name%20desc```  2. `$top` -  number of records you want to get.     ```/iaas/api/cloud-accounts?$top=20```  3. `$skip` -  number of records you want to skip.      ```/iaas/api/cloud-accounts?$skip=10```  4. `$select` - select a subset of properties to include in the response.        ```/project-service/api/projects?$top=10&$skip=2```  5. `$filter` -  filter the results by a specified predicate expression. Operators: eq, ne, and, or.\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'``` - name starts with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'``` - name contains 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'``` - name ends with 'ABC'\\     /iaas/api/projects and /iaas/api/deployments support different format for partial match:\\     ```/iaas/api/projects?$filter=startswith(name, 'ABC')``` - name starts with 'ABC'\\     ```/iaas/api/projects?$filter=substringof('ABC', name``` - name contains 'ABC'\\     ```/iaas/api/projects?$filter=endswith(name, 'ABC')``` - name ends with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'```  6. `$count` -  flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.\\     ```/iaas/api/cloud-accounts?$count=true```   # noqa: E501

    OpenAPI spec version: 2021-07-15
    Contact: sales@vmware.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class NetworkIPAddressResultContent(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'created_at': 'str',
        'updated_at': 'str',
        'owner': 'str',
        'owner_type': 'str',
        'org_id': 'str',
        'links': 'object',
        'name': 'str',
        'description': 'str',
        'external_id': 'str',
        'ip_address': 'str',
        'ip_address_decimal_value': 'int',
        'ip_version': 'str',
        'ip_address_status': 'str',
        'ip_allocation_type': 'str'
    }

    attribute_map = {
        'id': 'id',
        'created_at': 'createdAt',
        'updated_at': 'updatedAt',
        'owner': 'owner',
        'owner_type': 'ownerType',
        'org_id': 'orgId',
        'links': '_links',
        'name': 'name',
        'description': 'description',
        'external_id': 'externalId',
        'ip_address': 'ipAddress',
        'ip_address_decimal_value': 'ipAddressDecimalValue',
        'ip_version': 'ipVersion',
        'ip_address_status': 'ipAddressStatus',
        'ip_allocation_type': 'ipAllocationType'
    }

    def __init__(self, id=None, created_at=None, updated_at=None, owner=None, owner_type=None, org_id=None, links=None, name=None, description=None, external_id=None, ip_address=None, ip_address_decimal_value=None, ip_version=None, ip_address_status=None, ip_allocation_type=None):  # noqa: E501
        """NetworkIPAddressResultContent - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._created_at = None
        self._updated_at = None
        self._owner = None
        self._owner_type = None
        self._org_id = None
        self._links = None
        self._name = None
        self._description = None
        self._external_id = None
        self._ip_address = None
        self._ip_address_decimal_value = None
        self._ip_version = None
        self._ip_address_status = None
        self._ip_allocation_type = None
        self.discriminator = None
        self.id = id
        if created_at is not None:
            self.created_at = created_at
        if updated_at is not None:
            self.updated_at = updated_at
        if owner is not None:
            self.owner = owner
        if owner_type is not None:
            self.owner_type = owner_type
        if org_id is not None:
            self.org_id = org_id
        self.links = links
        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if external_id is not None:
            self.external_id = external_id
        self.ip_address = ip_address
        self.ip_address_decimal_value = ip_address_decimal_value
        if ip_version is not None:
            self.ip_version = ip_version
        if ip_address_status is not None:
            self.ip_address_status = ip_address_status
        if ip_allocation_type is not None:
            self.ip_allocation_type = ip_allocation_type

    @property
    def id(self):
        """Gets the id of this NetworkIPAddressResultContent.  # noqa: E501

        The id of this resource instance  # noqa: E501

        :return: The id of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this NetworkIPAddressResultContent.

        The id of this resource instance  # noqa: E501

        :param id: The id of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def created_at(self):
        """Gets the created_at of this NetworkIPAddressResultContent.  # noqa: E501

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :return: The created_at of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this NetworkIPAddressResultContent.

        Date when the entity was created. The date is in ISO 8601 and UTC.  # noqa: E501

        :param created_at: The created_at of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def updated_at(self):
        """Gets the updated_at of this NetworkIPAddressResultContent.  # noqa: E501

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :return: The updated_at of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this NetworkIPAddressResultContent.

        Date when the entity was last updated. The date is ISO 8601 and UTC.  # noqa: E501

        :param updated_at: The updated_at of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._updated_at = updated_at

    @property
    def owner(self):
        """Gets the owner of this NetworkIPAddressResultContent.  # noqa: E501

        Email of the user or display name of the group that owns the entity.  # noqa: E501

        :return: The owner of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this NetworkIPAddressResultContent.

        Email of the user or display name of the group that owns the entity.  # noqa: E501

        :param owner: The owner of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._owner = owner

    @property
    def owner_type(self):
        """Gets the owner_type of this NetworkIPAddressResultContent.  # noqa: E501

        Type of a owner(user/ad_group) that owns the entity.  # noqa: E501

        :return: The owner_type of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._owner_type

    @owner_type.setter
    def owner_type(self, owner_type):
        """Sets the owner_type of this NetworkIPAddressResultContent.

        Type of a owner(user/ad_group) that owns the entity.  # noqa: E501

        :param owner_type: The owner_type of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._owner_type = owner_type

    @property
    def org_id(self):
        """Gets the org_id of this NetworkIPAddressResultContent.  # noqa: E501

        The id of the organization this entity belongs to.  # noqa: E501

        :return: The org_id of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this NetworkIPAddressResultContent.

        The id of the organization this entity belongs to.  # noqa: E501

        :param org_id: The org_id of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def links(self):
        """Gets the links of this NetworkIPAddressResultContent.  # noqa: E501

        hrefs links  # noqa: E501

        :return: The links of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: object
        """
        return self._links

    @links.setter
    def links(self, links):
        """Sets the links of this NetworkIPAddressResultContent.

        hrefs links  # noqa: E501

        :param links: The links of this NetworkIPAddressResultContent.  # noqa: E501
        :type: object
        """
        if links is None:
            raise ValueError("Invalid value for `links`, must not be `None`")  # noqa: E501

        self._links = links

    @property
    def name(self):
        """Gets the name of this NetworkIPAddressResultContent.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :return: The name of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this NetworkIPAddressResultContent.

        A human-friendly name used as an identifier in APIs that support this option.  # noqa: E501

        :param name: The name of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def description(self):
        """Gets the description of this NetworkIPAddressResultContent.  # noqa: E501

        A human-friendly description.  # noqa: E501

        :return: The description of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this NetworkIPAddressResultContent.

        A human-friendly description.  # noqa: E501

        :param description: The description of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def external_id(self):
        """Gets the external_id of this NetworkIPAddressResultContent.  # noqa: E501

        External entity Id on the provider side.  # noqa: E501

        :return: The external_id of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._external_id

    @external_id.setter
    def external_id(self, external_id):
        """Sets the external_id of this NetworkIPAddressResultContent.

        External entity Id on the provider side.  # noqa: E501

        :param external_id: The external_id of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """

        self._external_id = external_id

    @property
    def ip_address(self):
        """Gets the ip_address of this NetworkIPAddressResultContent.  # noqa: E501

        IP address.  # noqa: E501

        :return: The ip_address of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._ip_address

    @ip_address.setter
    def ip_address(self, ip_address):
        """Sets the ip_address of this NetworkIPAddressResultContent.

        IP address.  # noqa: E501

        :param ip_address: The ip_address of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """
        if ip_address is None:
            raise ValueError("Invalid value for `ip_address`, must not be `None`")  # noqa: E501

        self._ip_address = ip_address

    @property
    def ip_address_decimal_value(self):
        """Gets the ip_address_decimal_value of this NetworkIPAddressResultContent.  # noqa: E501

        Decimal value of the ip address.  # noqa: E501

        :return: The ip_address_decimal_value of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: int
        """
        return self._ip_address_decimal_value

    @ip_address_decimal_value.setter
    def ip_address_decimal_value(self, ip_address_decimal_value):
        """Sets the ip_address_decimal_value of this NetworkIPAddressResultContent.

        Decimal value of the ip address.  # noqa: E501

        :param ip_address_decimal_value: The ip_address_decimal_value of this NetworkIPAddressResultContent.  # noqa: E501
        :type: int
        """
        if ip_address_decimal_value is None:
            raise ValueError("Invalid value for `ip_address_decimal_value`, must not be `None`")  # noqa: E501

        self._ip_address_decimal_value = ip_address_decimal_value

    @property
    def ip_version(self):
        """Gets the ip_version of this NetworkIPAddressResultContent.  # noqa: E501

        IP address version: IPv4 or IPv6. Default: IPv4.  # noqa: E501

        :return: The ip_version of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._ip_version

    @ip_version.setter
    def ip_version(self, ip_version):
        """Sets the ip_version of this NetworkIPAddressResultContent.

        IP address version: IPv4 or IPv6. Default: IPv4.  # noqa: E501

        :param ip_version: The ip_version of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """
        allowed_values = ["IPv4", "IPv6"]  # noqa: E501
        if ip_version not in allowed_values:
            raise ValueError(
                "Invalid value for `ip_version` ({0}), must be one of {1}"  # noqa: E501
                .format(ip_version, allowed_values)
            )

        self._ip_version = ip_version

    @property
    def ip_address_status(self):
        """Gets the ip_address_status of this NetworkIPAddressResultContent.  # noqa: E501

        IP address status  # noqa: E501

        :return: The ip_address_status of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._ip_address_status

    @ip_address_status.setter
    def ip_address_status(self, ip_address_status):
        """Sets the ip_address_status of this NetworkIPAddressResultContent.

        IP address status  # noqa: E501

        :param ip_address_status: The ip_address_status of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """
        allowed_values = ["ALLOCATED", "RELEASED", "AVAILABLE", "UNREGISTERED"]  # noqa: E501
        if ip_address_status not in allowed_values:
            raise ValueError(
                "Invalid value for `ip_address_status` ({0}), must be one of {1}"  # noqa: E501
                .format(ip_address_status, allowed_values)
            )

        self._ip_address_status = ip_address_status

    @property
    def ip_allocation_type(self):
        """Gets the ip_allocation_type of this NetworkIPAddressResultContent.  # noqa: E501

        IP Allocation type  # noqa: E501

        :return: The ip_allocation_type of this NetworkIPAddressResultContent.  # noqa: E501
        :rtype: str
        """
        return self._ip_allocation_type

    @ip_allocation_type.setter
    def ip_allocation_type(self, ip_allocation_type):
        """Sets the ip_allocation_type of this NetworkIPAddressResultContent.

        IP Allocation type  # noqa: E501

        :param ip_allocation_type: The ip_allocation_type of this NetworkIPAddressResultContent.  # noqa: E501
        :type: str
        """
        allowed_values = ["SYSTEM", "USER", "NONE"]  # noqa: E501
        if ip_allocation_type not in allowed_values:
            raise ValueError(
                "Invalid value for `ip_allocation_type` ({0}), must be one of {1}"  # noqa: E501
                .format(ip_allocation_type, allowed_values)
            )

        self._ip_allocation_type = ip_allocation_type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(NetworkIPAddressResultContent, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, NetworkIPAddressResultContent):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
