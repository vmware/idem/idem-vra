# coding: utf-8

"""
    VMware Aria Automation Assembler IaaS API

    IAAS API is a RESTful service, which allows users to execute provisioning related UI actions via an API.  This page describes the RESTful APIs for IAAS API. The APIs facilitate CRUD operations on the various resources and entities used throughout VMware Aria Automation Assembler(Cloud Accounts, Cloud zones, Computes, Mappings, etc.) and allows operations on them (creating a cloud account, patching a machine, creating image progile, etc.).  The APIs that list collections of resources also support OData like implementation. Below query params can be used across different IAAS API endpoints  Example: 1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.     ```/iaas/api/cloud-accounts?$orderby=name%20desc```  2. `$top` -  number of records you want to get.     ```/iaas/api/cloud-accounts?$top=20```  3. `$skip` -  number of records you want to skip.      ```/iaas/api/cloud-accounts?$skip=10```  4. `$select` - select a subset of properties to include in the response.        ```/project-service/api/projects?$top=10&$skip=2```  5. `$filter` -  filter the results by a specified predicate expression. Operators: eq, ne, and, or.\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'ABC*'``` - name starts with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC*'``` - name contains 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20eq%20'*ABC'``` - name ends with 'ABC'\\     /iaas/api/projects and /iaas/api/deployments support different format for partial match:\\     ```/iaas/api/projects?$filter=startswith(name, 'ABC')``` - name starts with 'ABC'\\     ```/iaas/api/projects?$filter=substringof('ABC', name``` - name contains 'ABC'\\     ```/iaas/api/projects?$filter=endswith(name, 'ABC')``` - name ends with 'ABC'\\     ```/iaas/api/cloud-accounts?$filter=name%20ne%20'example-cloud-account'%20or%20customProperties.isExternal%20eq%20'false'```  6. `$count` -  flag which when specified, regardless of the assigned value, shows the total number of records. If the collection has a filter it shows the number of records matching the filter.\\     ```/iaas/api/cloud-accounts?$count=true```   # noqa: E501

    OpenAPI spec version: 2021-07-15
    Contact: sales@vmware.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class CloudAccountVsphereRegionEnumerationSpecification(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'host_name': 'str',
        'accept_self_signed_certificate': 'bool',
        'dcid': 'str',
        'username': 'str',
        'password': 'str',
        'cloud_account_id': 'str',
        'certificate_info': 'IaasapiintegrationsCertificateInfo',
        'environment': 'str'
    }

    attribute_map = {
        'host_name': 'hostName',
        'accept_self_signed_certificate': 'acceptSelfSignedCertificate',
        'dcid': 'dcid',
        'username': 'username',
        'password': 'password',
        'cloud_account_id': 'cloudAccountId',
        'certificate_info': 'certificateInfo',
        'environment': 'environment'
    }

    def __init__(self, host_name=None, accept_self_signed_certificate=None, dcid=None, username=None, password=None, cloud_account_id=None, certificate_info=None, environment=None):  # noqa: E501
        """CloudAccountVsphereRegionEnumerationSpecification - a model defined in Swagger"""  # noqa: E501
        self._host_name = None
        self._accept_self_signed_certificate = None
        self._dcid = None
        self._username = None
        self._password = None
        self._cloud_account_id = None
        self._certificate_info = None
        self._environment = None
        self.discriminator = None
        if host_name is not None:
            self.host_name = host_name
        if accept_self_signed_certificate is not None:
            self.accept_self_signed_certificate = accept_self_signed_certificate
        if dcid is not None:
            self.dcid = dcid
        if username is not None:
            self.username = username
        if password is not None:
            self.password = password
        if cloud_account_id is not None:
            self.cloud_account_id = cloud_account_id
        if certificate_info is not None:
            self.certificate_info = certificate_info
        if environment is not None:
            self.environment = environment

    @property
    def host_name(self):
        """Gets the host_name of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501

        Host name for the vSphere endpoint. Either provide hostName or provide a cloudAccountId of an existing account.  # noqa: E501

        :return: The host_name of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: str
        """
        return self._host_name

    @host_name.setter
    def host_name(self, host_name):
        """Sets the host_name of this CloudAccountVsphereRegionEnumerationSpecification.

        Host name for the vSphere endpoint. Either provide hostName or provide a cloudAccountId of an existing account.  # noqa: E501

        :param host_name: The host_name of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: str
        """

        self._host_name = host_name

    @property
    def accept_self_signed_certificate(self):
        """Gets the accept_self_signed_certificate of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501

        Accept self signed certificate when connecting to vSphere  # noqa: E501

        :return: The accept_self_signed_certificate of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: bool
        """
        return self._accept_self_signed_certificate

    @accept_self_signed_certificate.setter
    def accept_self_signed_certificate(self, accept_self_signed_certificate):
        """Sets the accept_self_signed_certificate of this CloudAccountVsphereRegionEnumerationSpecification.

        Accept self signed certificate when connecting to vSphere  # noqa: E501

        :param accept_self_signed_certificate: The accept_self_signed_certificate of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: bool
        """

        self._accept_self_signed_certificate = accept_self_signed_certificate

    @property
    def dcid(self):
        """Gets the dcid of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501

        Identifier of a data collector vm deployed in the on premise infrastructure. Refer to the data-collector API to create or list data collectors. Note: Data collector endpoints are not available in VMware Aria Automation (on-prem) release.  # noqa: E501

        :return: The dcid of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: str
        """
        return self._dcid

    @dcid.setter
    def dcid(self, dcid):
        """Sets the dcid of this CloudAccountVsphereRegionEnumerationSpecification.

        Identifier of a data collector vm deployed in the on premise infrastructure. Refer to the data-collector API to create or list data collectors. Note: Data collector endpoints are not available in VMware Aria Automation (on-prem) release.  # noqa: E501

        :param dcid: The dcid of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: str
        """

        self._dcid = dcid

    @property
    def username(self):
        """Gets the username of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501

        Username to authenticate with the cloud account. Either provide username or provide a cloudAccountId of an existing account.  # noqa: E501

        :return: The username of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: str
        """
        return self._username

    @username.setter
    def username(self, username):
        """Sets the username of this CloudAccountVsphereRegionEnumerationSpecification.

        Username to authenticate with the cloud account. Either provide username or provide a cloudAccountId of an existing account.  # noqa: E501

        :param username: The username of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: str
        """

        self._username = username

    @property
    def password(self):
        """Gets the password of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501

        Password for the user used to authenticate with the cloud Account. Either provide password or provide a cloudAccountId of an existing account.  # noqa: E501

        :return: The password of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: str
        """
        return self._password

    @password.setter
    def password(self, password):
        """Sets the password of this CloudAccountVsphereRegionEnumerationSpecification.

        Password for the user used to authenticate with the cloud Account. Either provide password or provide a cloudAccountId of an existing account.  # noqa: E501

        :param password: The password of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: str
        """

        self._password = password

    @property
    def cloud_account_id(self):
        """Gets the cloud_account_id of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501

        Existing cloud account id. Either provide existing cloud account Id, or hostName, username, password.  # noqa: E501

        :return: The cloud_account_id of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: str
        """
        return self._cloud_account_id

    @cloud_account_id.setter
    def cloud_account_id(self, cloud_account_id):
        """Sets the cloud_account_id of this CloudAccountVsphereRegionEnumerationSpecification.

        Existing cloud account id. Either provide existing cloud account Id, or hostName, username, password.  # noqa: E501

        :param cloud_account_id: The cloud_account_id of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: str
        """

        self._cloud_account_id = cloud_account_id

    @property
    def certificate_info(self):
        """Gets the certificate_info of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501


        :return: The certificate_info of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: IaasapiintegrationsCertificateInfo
        """
        return self._certificate_info

    @certificate_info.setter
    def certificate_info(self, certificate_info):
        """Sets the certificate_info of this CloudAccountVsphereRegionEnumerationSpecification.


        :param certificate_info: The certificate_info of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: IaasapiintegrationsCertificateInfo
        """

        self._certificate_info = certificate_info

    @property
    def environment(self):
        """Gets the environment of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501

        The environment where data collectors are deployed. When the data collectors are deployed on a cloud gateway appliance, use \"aap\".  # noqa: E501

        :return: The environment of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :rtype: str
        """
        return self._environment

    @environment.setter
    def environment(self, environment):
        """Sets the environment of this CloudAccountVsphereRegionEnumerationSpecification.

        The environment where data collectors are deployed. When the data collectors are deployed on a cloud gateway appliance, use \"aap\".  # noqa: E501

        :param environment: The environment of this CloudAccountVsphereRegionEnumerationSpecification.  # noqa: E501
        :type: str
        """

        self._environment = environment

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(CloudAccountVsphereRegionEnumerationSpecification, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CloudAccountVsphereRegionEnumerationSpecification):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
