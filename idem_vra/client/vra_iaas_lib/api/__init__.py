from __future__ import absolute_import

# flake8: noqa

# import apis into api package
from idem_vra.client.vra_iaas_lib.api.about_api import AboutApi
from idem_vra.client.vra_iaas_lib.api.certificates_api import CertificatesApi
from idem_vra.client.vra_iaas_lib.api.cloud_account_api import CloudAccountApi
from idem_vra.client.vra_iaas_lib.api.compute_api import ComputeApi
from idem_vra.client.vra_iaas_lib.api.compute_gateway_api import ComputeGatewayApi
from idem_vra.client.vra_iaas_lib.api.compute_nat_api import ComputeNatApi
from idem_vra.client.vra_iaas_lib.api.custom_naming_api import CustomNamingApi
from idem_vra.client.vra_iaas_lib.api.data_collector_api import DataCollectorApi
from idem_vra.client.vra_iaas_lib.api.deployment_api import DeploymentApi
from idem_vra.client.vra_iaas_lib.api.disk_api import DiskApi
from idem_vra.client.vra_iaas_lib.api.fabric_aws_volume_types_api import FabricAWSVolumeTypesApi
from idem_vra.client.vra_iaas_lib.api.fabric_azure_disk_encryption_sets_api import FabricAzureDiskEncryptionSetsApi
from idem_vra.client.vra_iaas_lib.api.fabric_azure_storage_account_api import FabricAzureStorageAccountApi
from idem_vra.client.vra_iaas_lib.api.fabric_compute_api import FabricComputeApi
from idem_vra.client.vra_iaas_lib.api.fabric_flavors_api import FabricFlavorsApi
from idem_vra.client.vra_iaas_lib.api.fabric_images_api import FabricImagesApi
from idem_vra.client.vra_iaas_lib.api.fabric_network_api import FabricNetworkApi
from idem_vra.client.vra_iaas_lib.api.fabric_v_sphere_datastore_api import FabricVSphereDatastoreApi
from idem_vra.client.vra_iaas_lib.api.fabric_v_sphere_storage_policies_api import FabricVSphereStoragePoliciesApi
from idem_vra.client.vra_iaas_lib.api.flavor_profile_api import FlavorProfileApi
from idem_vra.client.vra_iaas_lib.api.flavors_api import FlavorsApi
from idem_vra.client.vra_iaas_lib.api.folders_api import FoldersApi
from idem_vra.client.vra_iaas_lib.api.image_profile_api import ImageProfileApi
from idem_vra.client.vra_iaas_lib.api.images_api import ImagesApi
from idem_vra.client.vra_iaas_lib.api.integration_api import IntegrationApi
from idem_vra.client.vra_iaas_lib.api.load_balancer_api import LoadBalancerApi
from idem_vra.client.vra_iaas_lib.api.location_api import LocationApi
from idem_vra.client.vra_iaas_lib.api.login_api import LoginApi
from idem_vra.client.vra_iaas_lib.api.logs_api import LogsApi
from idem_vra.client.vra_iaas_lib.api.network_api import NetworkApi
from idem_vra.client.vra_iaas_lib.api.network_ip_range_api import NetworkIPRangeApi
from idem_vra.client.vra_iaas_lib.api.network_profile_api import NetworkProfileApi
from idem_vra.client.vra_iaas_lib.api.package_import_api import PackageImportApi
from idem_vra.client.vra_iaas_lib.api.project_api import ProjectApi
from idem_vra.client.vra_iaas_lib.api.property_api import PropertyApi
from idem_vra.client.vra_iaas_lib.api.request_api import RequestApi
from idem_vra.client.vra_iaas_lib.api.security_group_api import SecurityGroupApi
from idem_vra.client.vra_iaas_lib.api.storage_profile_api import StorageProfileApi
from idem_vra.client.vra_iaas_lib.api.tags_api import TagsApi
