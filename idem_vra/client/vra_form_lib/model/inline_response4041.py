# coding: utf-8

"""
    Form Service API

    A list of form and Xaas APIs.  This page describes the RESTful APIs for Form Service. The APIs facilitate CRUD operations on the various resources and entities used throughout Form service (Custom Resource , Form Definition etc.) and allow operations on them (creating custom resource type , fetch form status etc.).  Some of the APIs that list collections of resources also support OData like implementation. Below query params can be used across Form and XaaS entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/form-service/api/custom/resource-types?$orderby=name%20DESC```  2. `page`, `size` - page used in conjunction with `size` helps in pagination of resources.      ```/form-service/api/custom/resource-types?page=0&size=20```   # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse4041(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'name': 'str',
        'display_name': 'str',
        'description': 'str',
        'operations': 'list[str]',
        'schema': 'FormserviceapicustomresourcetypesProperties',
        'provider_id': 'str',
        'org_id': 'str',
        'project_id': 'str',
        'restrict_to_single_instance': 'bool'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'display_name': 'displayName',
        'description': 'description',
        'operations': 'operations',
        'schema': 'schema',
        'provider_id': 'providerId',
        'org_id': 'orgId',
        'project_id': 'projectId',
        'restrict_to_single_instance': 'restrictToSingleInstance'
    }

    def __init__(self, id=None, name=None, display_name=None, description=None, operations=None, schema=None, provider_id=None, org_id=None, project_id=None, restrict_to_single_instance=None):  # noqa: E501
        """InlineResponse4041 - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._name = None
        self._display_name = None
        self._description = None
        self._operations = None
        self._schema = None
        self._provider_id = None
        self._org_id = None
        self._project_id = None
        self._restrict_to_single_instance = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if display_name is not None:
            self.display_name = display_name
        if description is not None:
            self.description = description
        if operations is not None:
            self.operations = operations
        if schema is not None:
            self.schema = schema
        if provider_id is not None:
            self.provider_id = provider_id
        if org_id is not None:
            self.org_id = org_id
        if project_id is not None:
            self.project_id = project_id
        if restrict_to_single_instance is not None:
            self.restrict_to_single_instance = restrict_to_single_instance

    @property
    def id(self):
        """Gets the id of this InlineResponse4041.  # noqa: E501


        :return: The id of this InlineResponse4041.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse4041.


        :param id: The id of this InlineResponse4041.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this InlineResponse4041.  # noqa: E501


        :return: The name of this InlineResponse4041.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse4041.


        :param name: The name of this InlineResponse4041.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def display_name(self):
        """Gets the display_name of this InlineResponse4041.  # noqa: E501


        :return: The display_name of this InlineResponse4041.  # noqa: E501
        :rtype: str
        """
        return self._display_name

    @display_name.setter
    def display_name(self, display_name):
        """Sets the display_name of this InlineResponse4041.


        :param display_name: The display_name of this InlineResponse4041.  # noqa: E501
        :type: str
        """

        self._display_name = display_name

    @property
    def description(self):
        """Gets the description of this InlineResponse4041.  # noqa: E501


        :return: The description of this InlineResponse4041.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineResponse4041.


        :param description: The description of this InlineResponse4041.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def operations(self):
        """Gets the operations of this InlineResponse4041.  # noqa: E501


        :return: The operations of this InlineResponse4041.  # noqa: E501
        :rtype: list[str]
        """
        return self._operations

    @operations.setter
    def operations(self, operations):
        """Sets the operations of this InlineResponse4041.


        :param operations: The operations of this InlineResponse4041.  # noqa: E501
        :type: list[str]
        """
        allowed_values = ["validate", "allocate", "create", "read", "update", "delete", "action", "cancel"]  # noqa: E501
        if not set(operations).issubset(set(allowed_values)):
            raise ValueError(
                "Invalid values for `operations` [{0}], must be a subset of [{1}]"  # noqa: E501
                .format(", ".join(map(str, set(operations) - set(allowed_values))),  # noqa: E501
                        ", ".join(map(str, allowed_values)))
            )

        self._operations = operations

    @property
    def schema(self):
        """Gets the schema of this InlineResponse4041.  # noqa: E501


        :return: The schema of this InlineResponse4041.  # noqa: E501
        :rtype: FormserviceapicustomresourcetypesProperties
        """
        return self._schema

    @schema.setter
    def schema(self, schema):
        """Sets the schema of this InlineResponse4041.


        :param schema: The schema of this InlineResponse4041.  # noqa: E501
        :type: FormserviceapicustomresourcetypesProperties
        """

        self._schema = schema

    @property
    def provider_id(self):
        """Gets the provider_id of this InlineResponse4041.  # noqa: E501


        :return: The provider_id of this InlineResponse4041.  # noqa: E501
        :rtype: str
        """
        return self._provider_id

    @provider_id.setter
    def provider_id(self, provider_id):
        """Sets the provider_id of this InlineResponse4041.


        :param provider_id: The provider_id of this InlineResponse4041.  # noqa: E501
        :type: str
        """

        self._provider_id = provider_id

    @property
    def org_id(self):
        """Gets the org_id of this InlineResponse4041.  # noqa: E501


        :return: The org_id of this InlineResponse4041.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this InlineResponse4041.


        :param org_id: The org_id of this InlineResponse4041.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def project_id(self):
        """Gets the project_id of this InlineResponse4041.  # noqa: E501


        :return: The project_id of this InlineResponse4041.  # noqa: E501
        :rtype: str
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this InlineResponse4041.


        :param project_id: The project_id of this InlineResponse4041.  # noqa: E501
        :type: str
        """

        self._project_id = project_id

    @property
    def restrict_to_single_instance(self):
        """Gets the restrict_to_single_instance of this InlineResponse4041.  # noqa: E501


        :return: The restrict_to_single_instance of this InlineResponse4041.  # noqa: E501
        :rtype: bool
        """
        return self._restrict_to_single_instance

    @restrict_to_single_instance.setter
    def restrict_to_single_instance(self, restrict_to_single_instance):
        """Sets the restrict_to_single_instance of this InlineResponse4041.


        :param restrict_to_single_instance: The restrict_to_single_instance of this InlineResponse4041.  # noqa: E501
        :type: bool
        """

        self._restrict_to_single_instance = restrict_to_single_instance

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse4041, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse4041):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
