# coding: utf-8

"""
    Form Service API

    A list of form and Xaas APIs.  This page describes the RESTful APIs for Form Service. The APIs facilitate CRUD operations on the various resources and entities used throughout Form service (Custom Resource , Form Definition etc.) and allow operations on them (creating custom resource type , fetch form status etc.).  Some of the APIs that list collections of resources also support OData like implementation. Below query params can be used across Form and XaaS entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/form-service/api/custom/resource-types?$orderby=name%20DESC```  2. `page`, `size` - page used in conjunction with `size` helps in pagination of resources.      ```/form-service/api/custom/resource-types?page=0&size=20```   # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Page1(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'page': 'int',
        'size': 'int',
        'sort': 'list[str]'
    }

    attribute_map = {
        'page': 'page',
        'size': 'size',
        'sort': 'sort'
    }

    def __init__(self, page=None, size=None, sort=None):  # noqa: E501
        """Page1 - a model defined in Swagger"""  # noqa: E501
        self._page = None
        self._size = None
        self._sort = None
        self.discriminator = None
        if page is not None:
            self.page = page
        if size is not None:
            self.size = size
        if sort is not None:
            self.sort = sort

    @property
    def page(self):
        """Gets the page of this Page1.  # noqa: E501


        :return: The page of this Page1.  # noqa: E501
        :rtype: int
        """
        return self._page

    @page.setter
    def page(self, page):
        """Sets the page of this Page1.


        :param page: The page of this Page1.  # noqa: E501
        :type: int
        """

        self._page = page

    @property
    def size(self):
        """Gets the size of this Page1.  # noqa: E501


        :return: The size of this Page1.  # noqa: E501
        :rtype: int
        """
        return self._size

    @size.setter
    def size(self, size):
        """Sets the size of this Page1.


        :param size: The size of this Page1.  # noqa: E501
        :type: int
        """

        self._size = size

    @property
    def sort(self):
        """Gets the sort of this Page1.  # noqa: E501


        :return: The sort of this Page1.  # noqa: E501
        :rtype: list[str]
        """
        return self._sort

    @sort.setter
    def sort(self, sort):
        """Sets the sort of this Page1.


        :param sort: The sort of this Page1.  # noqa: E501
        :type: list[str]
        """

        self._sort = sort

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Page1, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Page1):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
