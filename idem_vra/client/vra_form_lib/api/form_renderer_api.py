# coding: utf-8

"""
    Form Service API

    A list of form and Xaas APIs.  This page describes the RESTful APIs for Form Service. The APIs facilitate CRUD operations on the various resources and entities used throughout Form service (Custom Resource , Form Definition etc.) and allow operations on them (creating custom resource type , fetch form status etc.).  Some of the APIs that list collections of resources also support OData like implementation. Below query params can be used across Form and XaaS entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/form-service/api/custom/resource-types?$orderby=name%20DESC```  2. `page`, `size` - page used in conjunction with `size` helps in pagination of resources.      ```/form-service/api/custom/resource-types?page=0&size=20```   # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from idem_vra.client.vra_form_lib.api_client import ApiClient


class FormRendererApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def execute_external_action(self, body, **kwargs):  # noqa: E501
        """Execute an external action to get a value (for the UI)  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.execute_external_action(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererExternalvalueBody body: (required)
        :param str project_id: The projectId of the project chosen for the request.
        :param str source_id:
        :return: InlineResponse403
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.execute_external_action_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.execute_external_action_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def execute_external_action_with_http_info(self, body, **kwargs):  # noqa: E501
        """Execute an external action to get a value (for the UI)  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.execute_external_action_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererExternalvalueBody body: (required)
        :param str project_id: The projectId of the project chosen for the request.
        :param str source_id:
        :return: InlineResponse403
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'project_id', 'source_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method execute_external_action" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `execute_external_action`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'project_id' in params:
            query_params.append(('projectId', params['project_id']))  # noqa: E501
        if 'source_id' in params:
            query_params.append(('sourceId', params['source_id']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/renderer/external-value', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse403',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def execute_external_actions(self, body, **kwargs):  # noqa: E501
        """Execute multiple external actions to get a list of values (for the UI)  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.execute_external_actions(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param list[RendererExternalvaluesBody] body: (required)
        :param str project_id: The projectId of the project chosen for the request.
        :param str source_id:
        :return: list[InlineResponse403]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.execute_external_actions_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.execute_external_actions_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def execute_external_actions_with_http_info(self, body, **kwargs):  # noqa: E501
        """Execute multiple external actions to get a list of values (for the UI)  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.execute_external_actions_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param list[RendererExternalvaluesBody] body: (required)
        :param str project_id: The projectId of the project chosen for the request.
        :param str source_id:
        :return: list[InlineResponse403]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'project_id', 'source_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method execute_external_actions" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `execute_external_actions`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'project_id' in params:
            query_params.append(('projectId', params['project_id']))  # noqa: E501
        if 'source_id' in params:
            query_params.append(('sourceId', params['source_id']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/renderer/external-values', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[InlineResponse403]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def fetch_form(self, body, **kwargs):  # noqa: E501
        """Generate request form for a given library schema.  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.fetch_form(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererRequestBody body: (required)
        :param str source_type: The request source type
        :param str source_id: The request source id
        :param str form_type: The form type
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.fetch_form_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.fetch_form_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def fetch_form_with_http_info(self, body, **kwargs):  # noqa: E501
        """Generate request form for a given library schema.  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.fetch_form_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererRequestBody body: (required)
        :param str source_type: The request source type
        :param str source_id: The request source id
        :param str form_type: The form type
        :return: str
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'source_type', 'source_id', 'form_type']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method fetch_form" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `fetch_form`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'source_type' in params:
            query_params.append(('sourceType', params['source_type']))  # noqa: E501
        if 'source_id' in params:
            query_params.append(('sourceId', params['source_id']))  # noqa: E501
        if 'form_type' in params:
            query_params.append(('formType', params['form_type']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/renderer/request', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='str',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def fetch_model(self, body, **kwargs):  # noqa: E501
        """Generate request form model for a given library schema.  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.fetch_model(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererModelBody body: (required)
        :param str source_type: The request source type
        :param str source_id: The request source id
        :param str form_type: The form type
        :param str form_id: The request form id, used to find a form from a provider
        :param str resource_id: Resource id
        :param str deployment_id: Deployment id
        :return: InlineResponse400
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.fetch_model_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.fetch_model_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def fetch_model_with_http_info(self, body, **kwargs):  # noqa: E501
        """Generate request form model for a given library schema.  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.fetch_model_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererModelBody body: (required)
        :param str source_type: The request source type
        :param str source_id: The request source id
        :param str form_type: The form type
        :param str form_id: The request form id, used to find a form from a provider
        :param str resource_id: Resource id
        :param str deployment_id: Deployment id
        :return: InlineResponse400
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'source_type', 'source_id', 'form_type', 'form_id', 'resource_id', 'deployment_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method fetch_model" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `fetch_model`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'source_type' in params:
            query_params.append(('sourceType', params['source_type']))  # noqa: E501
        if 'source_id' in params:
            query_params.append(('sourceId', params['source_id']))  # noqa: E501
        if 'form_type' in params:
            query_params.append(('formType', params['form_type']))  # noqa: E501
        if 'form_id' in params:
            query_params.append(('formId', params['form_id']))  # noqa: E501
        if 'resource_id' in params:
            query_params.append(('resourceId', params['resource_id']))  # noqa: E501
        if 'deployment_id' in params:
            query_params.append(('deploymentId', params['deployment_id']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/renderer/model', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse400',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def lookup_external_resource(self, namespace, type, object_id, project_id, **kwargs):  # noqa: E501
        """Retrieve external resource object  # noqa: E501

        Retrieve external resource object depending on namespace type and id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.lookup_external_resource(namespace, type, object_id, project_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str namespace: namespace in which the vRO type is placed (required)
        :param str type: type of the SDK object (required)
        :param str object_id: unique id of the object (required)
        :param str project_id: id of current project (required)
        :return: InlineResponse4032
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.lookup_external_resource_with_http_info(namespace, type, object_id, project_id, **kwargs)  # noqa: E501
        else:
            (data) = self.lookup_external_resource_with_http_info(namespace, type, object_id, project_id, **kwargs)  # noqa: E501
            return data

    def lookup_external_resource_with_http_info(self, namespace, type, object_id, project_id, **kwargs):  # noqa: E501
        """Retrieve external resource object  # noqa: E501

        Retrieve external resource object depending on namespace type and id  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.lookup_external_resource_with_http_info(namespace, type, object_id, project_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str namespace: namespace in which the vRO type is placed (required)
        :param str type: type of the SDK object (required)
        :param str object_id: unique id of the object (required)
        :param str project_id: id of current project (required)
        :return: InlineResponse4032
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['namespace', 'type', 'object_id', 'project_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method lookup_external_resource" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'namespace' is set
        if ('namespace' not in params or
                params['namespace'] is None):
            raise ValueError("Missing the required parameter `namespace` when calling `lookup_external_resource`")  # noqa: E501
        # verify the required parameter 'type' is set
        if ('type' not in params or
                params['type'] is None):
            raise ValueError("Missing the required parameter `type` when calling `lookup_external_resource`")  # noqa: E501
        # verify the required parameter 'object_id' is set
        if ('object_id' not in params or
                params['object_id'] is None):
            raise ValueError("Missing the required parameter `object_id` when calling `lookup_external_resource`")  # noqa: E501
        # verify the required parameter 'project_id' is set
        if ('project_id' not in params or
                params['project_id'] is None):
            raise ValueError("Missing the required parameter `project_id` when calling `lookup_external_resource`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'namespace' in params:
            query_params.append(('namespace', params['namespace']))  # noqa: E501
        if 'type' in params:
            query_params.append(('type', params['type']))  # noqa: E501
        if 'object_id' in params:
            query_params.append(('objectId', params['object_id']))  # noqa: E501
        if 'project_id' in params:
            query_params.append(('projectId', params['project_id']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['*/*'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/renderer/external-resources', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse4032',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def lookup_external_resources(self, body, **kwargs):  # noqa: E501
        """Retrieve external resource value  # noqa: E501

        Retrieve external resource value depending on provided value source type  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.lookup_external_resources(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererExternalresourcesBody body: (required)
        :return: InlineResponse4001
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.lookup_external_resources_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.lookup_external_resources_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def lookup_external_resources_with_http_info(self, body, **kwargs):  # noqa: E501
        """Retrieve external resource value  # noqa: E501

        Retrieve external resource value depending on provided value source type  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.lookup_external_resources_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererExternalresourcesBody body: (required)
        :return: InlineResponse4001
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method lookup_external_resources" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `lookup_external_resources`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/renderer/external-resources', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse4001',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def perform_external_validation(self, body, **kwargs):  # noqa: E501
        """Perform external validations for a form (for the UI)  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.perform_external_validation(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererExternalvalidationBody body: (required)
        :param str project_id: The projectId of the project chosen for the request.
        :param str source_id:
        :return: list[InlineResponse4031]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.perform_external_validation_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.perform_external_validation_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def perform_external_validation_with_http_info(self, body, **kwargs):  # noqa: E501
        """Perform external validations for a form (for the UI)  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.perform_external_validation_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param RendererExternalvalidationBody body: (required)
        :param str project_id: The projectId of the project chosen for the request.
        :param str source_id:
        :return: list[InlineResponse4031]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'project_id', 'source_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method perform_external_validation" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `perform_external_validation`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'project_id' in params:
            query_params.append(('projectId', params['project_id']))  # noqa: E501
        if 'source_id' in params:
            query_params.append(('sourceId', params['source_id']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/renderer/external-validation', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[InlineResponse4031]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
