# coding: utf-8

"""
    Form Service API

    A list of form and Xaas APIs.  This page describes the RESTful APIs for Form Service. The APIs facilitate CRUD operations on the various resources and entities used throughout Form service (Custom Resource , Form Definition etc.) and allow operations on them (creating custom resource type , fetch form status etc.).  Some of the APIs that list collections of resources also support OData like implementation. Below query params can be used across Form and XaaS entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/form-service/api/custom/resource-types?$orderby=name%20DESC```  2. `page`, `size` - page used in conjunction with `size` helps in pagination of resources.      ```/form-service/api/custom/resource-types?page=0&size=20```   # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from idem_vra.client.vra_form_lib.api_client import ApiClient


class FormDefinitionApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def create_form(self, body, **kwargs):  # noqa: E501
        """Create form definition  # noqa: E501

        Create form definition.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_form(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param ApiFormsBody body: (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.create_form_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.create_form_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def create_form_with_http_info(self, body, **kwargs):  # noqa: E501
        """Create form definition  # noqa: E501

        Create form definition.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_form_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param ApiFormsBody body: (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_form" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `create_form`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def delete_form(self, id, **kwargs):  # noqa: E501
        """Delete form by ID  # noqa: E501

        Delete the form with the specified ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_form(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: Form ID (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.delete_form_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.delete_form_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def delete_form_with_http_info(self, id, **kwargs):  # noqa: E501
        """Delete form by ID  # noqa: E501

        Delete the form with the specified ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_form_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: Form ID (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_form" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params or
                params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_form`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/{id}', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def delete_form1(self, source_type, source_id, form_type, **kwargs):  # noqa: E501
        """Delete form by source  # noqa: E501

        Delete the form with the specified sourceType, sourceId and formType.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_form1(source_type, source_id, form_type, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_type: The form source type. It can be 'com.vmw.vro.workflow' or 'resource.action', or 'com.vmw.blueprint.version'. (required)
        :param str source_id: The form source ID (required)
        :param str form_type: The form type. It can be 'requestForm'. (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.delete_form1_with_http_info(source_type, source_id, form_type, **kwargs)  # noqa: E501
        else:
            (data) = self.delete_form1_with_http_info(source_type, source_id, form_type, **kwargs)  # noqa: E501
            return data

    def delete_form1_with_http_info(self, source_type, source_id, form_type, **kwargs):  # noqa: E501
        """Delete form by source  # noqa: E501

        Delete the form with the specified sourceType, sourceId and formType.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_form1_with_http_info(source_type, source_id, form_type, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_type: The form source type. It can be 'com.vmw.vro.workflow' or 'resource.action', or 'com.vmw.blueprint.version'. (required)
        :param str source_id: The form source ID (required)
        :param str form_type: The form type. It can be 'requestForm'. (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['source_type', 'source_id', 'form_type']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_form1" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'source_type' is set
        if ('source_type' not in params or
                params['source_type'] is None):
            raise ValueError("Missing the required parameter `source_type` when calling `delete_form1`")  # noqa: E501
        # verify the required parameter 'source_id' is set
        if ('source_id' not in params or
                params['source_id'] is None):
            raise ValueError("Missing the required parameter `source_id` when calling `delete_form1`")  # noqa: E501
        # verify the required parameter 'form_type' is set
        if ('form_type' not in params or
                params['form_type'] is None):
            raise ValueError("Missing the required parameter `form_type` when calling `delete_form1`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'source_type' in params:
            query_params.append(('sourceType', params['source_type']))  # noqa: E501
        if 'source_id' in params:
            query_params.append(('sourceId', params['source_id']))  # noqa: E501
        if 'form_type' in params:
            query_params.append(('formType', params['form_type']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/deleteBySourceAndType', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_form(self, id, **kwargs):  # noqa: E501
        """Get form by ID  # noqa: E501

        Retrieve the Form definition of a form with the specified ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_form(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: Form identifier (required)
        :param str form_format: Form definition format
        :return: ApiFormsBody
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_form_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_form_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def get_form_with_http_info(self, id, **kwargs):  # noqa: E501
        """Get form by ID  # noqa: E501

        Retrieve the Form definition of a form with the specified ID.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_form_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str id: Form identifier (required)
        :param str form_format: Form definition format
        :return: ApiFormsBody
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'form_format']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_form" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params or
                params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `get_form`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []
        if 'form_format' in params:
            query_params.append(('formFormat', params['form_format']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/{id}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiFormsBody',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_form1(self, ref, **kwargs):  # noqa: E501
        """Get form by providerRef  # noqa: E501

        Retrieve the form definition of a form with the specified provider reference.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_form1(ref, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str ref: Provider Reference (required)
        :return: ApiFormsBody
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_form1_with_http_info(ref, **kwargs)  # noqa: E501
        else:
            (data) = self.get_form1_with_http_info(ref, **kwargs)  # noqa: E501
            return data

    def get_form1_with_http_info(self, ref, **kwargs):  # noqa: E501
        """Get form by providerRef  # noqa: E501

        Retrieve the form definition of a form with the specified provider reference.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_form1_with_http_info(ref, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str ref: Provider Reference (required)
        :return: ApiFormsBody
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['ref']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_form1" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'ref' is set
        if ('ref' not in params or
                params['ref'] is None):
            raise ValueError("Missing the required parameter `ref` when calling `get_form1`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'ref' in params:
            path_params['ref'] = params['ref']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/provider/{ref}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiFormsBody',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def search_by_source_and_type(self, source_type, source_id, form_type, **kwargs):  # noqa: E501
        """Search form by source  # noqa: E501

        Search for form definition by source type, source ID and form type.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.search_by_source_and_type(source_type, source_id, form_type, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_type: The form source type. It can be 'com.vmw.vro.workflow' or 'resource.action'. (required)
        :param str source_id: The form source ID (required)
        :param str form_type: The form type. It can be 'requestForm'. (required)
        :param str form_format: Form definition format
        :return: ApiFormsBody
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.search_by_source_and_type_with_http_info(source_type, source_id, form_type, **kwargs)  # noqa: E501
        else:
            (data) = self.search_by_source_and_type_with_http_info(source_type, source_id, form_type, **kwargs)  # noqa: E501
            return data

    def search_by_source_and_type_with_http_info(self, source_type, source_id, form_type, **kwargs):  # noqa: E501
        """Search form by source  # noqa: E501

        Search for form definition by source type, source ID and form type.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.search_by_source_and_type_with_http_info(source_type, source_id, form_type, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str source_type: The form source type. It can be 'com.vmw.vro.workflow' or 'resource.action'. (required)
        :param str source_id: The form source ID (required)
        :param str form_type: The form type. It can be 'requestForm'. (required)
        :param str form_format: Form definition format
        :return: ApiFormsBody
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['source_type', 'source_id', 'form_type', 'form_format']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method search_by_source_and_type" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'source_type' is set
        if ('source_type' not in params or
                params['source_type'] is None):
            raise ValueError("Missing the required parameter `source_type` when calling `search_by_source_and_type`")  # noqa: E501
        # verify the required parameter 'source_id' is set
        if ('source_id' not in params or
                params['source_id'] is None):
            raise ValueError("Missing the required parameter `source_id` when calling `search_by_source_and_type`")  # noqa: E501
        # verify the required parameter 'form_type' is set
        if ('form_type' not in params or
                params['form_type'] is None):
            raise ValueError("Missing the required parameter `form_type` when calling `search_by_source_and_type`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'source_type' in params:
            query_params.append(('sourceType', params['source_type']))  # noqa: E501
        if 'source_id' in params:
            query_params.append(('sourceId', params['source_id']))  # noqa: E501
        if 'form_type' in params:
            query_params.append(('formType', params['form_type']))  # noqa: E501
        if 'form_format' in params:
            query_params.append(('formFormat', params['form_format']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/fetchBySourceAndType', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiFormsBody',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def search_form_definitions(self, **kwargs):  # noqa: E501
        """Search form and form versions by term  # noqa: E501

        Search for form definitions and form versions with the specified term.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.search_form_definitions(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int top: Number of records you want
        :param int skip: Number of records you want to skip
        :param Pageable pageable:
        :param str term: Search term
        :return: InlineResponse2004
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.search_form_definitions_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.search_form_definitions_with_http_info(**kwargs)  # noqa: E501
            return data

    def search_form_definitions_with_http_info(self, **kwargs):  # noqa: E501
        """Search form and form versions by term  # noqa: E501

        Search for form definitions and form versions with the specified term.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.search_form_definitions_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int top: Number of records you want
        :param int skip: Number of records you want to skip
        :param Pageable pageable:
        :param str term: Search term
        :return: InlineResponse2004
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['top', 'skip', 'pageable', 'term']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method search_form_definitions" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'top' in params:
            query_params.append(('$top', params['top']))  # noqa: E501
        if 'skip' in params:
            query_params.append(('$skip', params['skip']))  # noqa: E501
        if 'pageable' in params:
            query_params.append(('pageable', params['pageable']))  # noqa: E501
        if 'term' in params:
            query_params.append(('term', params['term']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/search', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse2004',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def search_form_status(self, body, **kwargs):  # noqa: E501
        """Fetch form status  # noqa: E501

        Retrieve statuses of forms specified by reference IDs.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.search_form_status(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param list[FormsFetchStatusBody] body: (required)
        :return: list[InlineResponse2001]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.search_form_status_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.search_form_status_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def search_form_status_with_http_info(self, body, **kwargs):  # noqa: E501
        """Fetch form status  # noqa: E501

        Retrieve statuses of forms specified by reference IDs.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.search_form_status_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param list[FormsFetchStatusBody] body: (required)
        :return: list[InlineResponse2001]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method search_form_status" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `search_form_status`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/form-service/api/forms/fetchStatus', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[InlineResponse2001]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
