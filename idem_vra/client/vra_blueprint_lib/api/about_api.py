# coding: utf-8

"""
    VMware Aria Automation Assembler Blueprint API

    A multi-cloud Blueprint API for VMware Aria Automation Services  This page describes the RESTful APIs for Blueprint. The APIs facilitate CRUD operations on the various resources and entities used throughout Blueprint (Blueprints, Blueprint Terraform Integrations , PropertyGroups , etc.) and allow operations on them (creating a Blueprint, creating a Property group, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Blueprint entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/blueprint/api/blueprints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/blueprint/api/blueprints?$top=10&$skip=2```   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from idem_vra.client.vra_blueprint_lib.api_client import ApiClient


class AboutApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_about_using_get(self, **kwargs):  # noqa: E501
        """Returns blueprint API information  # noqa: E501

        The page contains information about the supported API versions and the latest API version. The version parameter is optional but highly recommended. If you do not specify explicitly an exact version, you will be calling the latest supported API version. Here is an example of a call which specifies the exact version you are using: GET /blueprint/api/blueprints?apiVersion=2019-09-12  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_about_using_get(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: InlineResponse20018
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_about_using_get_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_about_using_get_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_about_using_get_with_http_info(self, **kwargs):  # noqa: E501
        """Returns blueprint API information  # noqa: E501

        The page contains information about the supported API versions and the latest API version. The version parameter is optional but highly recommended. If you do not specify explicitly an exact version, you will be calling the latest supported API version. Here is an example of a call which specifies the exact version you are using: GET /blueprint/api/blueprints?apiVersion=2019-09-12  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_about_using_get_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: InlineResponse20018
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_about_using_get" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Bearer']  # noqa: E501

        return self.api_client.call_api(
            '/blueprint/api/about', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse20018',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
