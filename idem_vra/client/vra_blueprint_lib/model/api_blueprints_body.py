# coding: utf-8

"""
    VMware Aria Automation Assembler Blueprint API

    A multi-cloud Blueprint API for VMware Aria Automation Services  This page describes the RESTful APIs for Blueprint. The APIs facilitate CRUD operations on the various resources and entities used throughout Blueprint (Blueprints, Blueprint Terraform Integrations , PropertyGroups , etc.) and allow operations on them (creating a Blueprint, creating a Property group, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Blueprint entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/blueprint/api/blueprints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/blueprint/api/blueprints?$top=10&$skip=2```   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ApiBlueprintsBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'created_at': 'datetime',
        'created_by': 'str',
        'updated_at': 'datetime',
        'updated_by': 'str',
        'org_id': 'str',
        'project_id': 'str',
        'project_name': 'str',
        'self_link': 'str',
        'name': 'str',
        'description': 'str',
        'status': 'str',
        'content': 'str',
        'valid': 'bool',
        'validation_messages': 'list[BlueprintapiblueprintsblueprintIdValidationMessages]',
        'total_versions': 'int',
        'total_released_versions': 'int',
        'request_scope_org': 'bool',
        'content_source_id': 'str',
        'content_source_path': 'str',
        'content_source_type': 'str',
        'content_source_sync_status': 'str',
        'content_source_sync_messages': 'list[str]',
        'content_source_sync_at': 'datetime'
    }

    attribute_map = {
        'id': 'id',
        'created_at': 'createdAt',
        'created_by': 'createdBy',
        'updated_at': 'updatedAt',
        'updated_by': 'updatedBy',
        'org_id': 'orgId',
        'project_id': 'projectId',
        'project_name': 'projectName',
        'self_link': 'selfLink',
        'name': 'name',
        'description': 'description',
        'status': 'status',
        'content': 'content',
        'valid': 'valid',
        'validation_messages': 'validationMessages',
        'total_versions': 'totalVersions',
        'total_released_versions': 'totalReleasedVersions',
        'request_scope_org': 'requestScopeOrg',
        'content_source_id': 'contentSourceId',
        'content_source_path': 'contentSourcePath',
        'content_source_type': 'contentSourceType',
        'content_source_sync_status': 'contentSourceSyncStatus',
        'content_source_sync_messages': 'contentSourceSyncMessages',
        'content_source_sync_at': 'contentSourceSyncAt'
    }

    def __init__(self, id=None, created_at=None, created_by=None, updated_at=None, updated_by=None, org_id=None, project_id=None, project_name=None, self_link=None, name=None, description=None, status=None, content=None, valid=None, validation_messages=None, total_versions=None, total_released_versions=None, request_scope_org=None, content_source_id=None, content_source_path=None, content_source_type=None, content_source_sync_status=None, content_source_sync_messages=None, content_source_sync_at=None):  # noqa: E501
        """ApiBlueprintsBody - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._created_at = None
        self._created_by = None
        self._updated_at = None
        self._updated_by = None
        self._org_id = None
        self._project_id = None
        self._project_name = None
        self._self_link = None
        self._name = None
        self._description = None
        self._status = None
        self._content = None
        self._valid = None
        self._validation_messages = None
        self._total_versions = None
        self._total_released_versions = None
        self._request_scope_org = None
        self._content_source_id = None
        self._content_source_path = None
        self._content_source_type = None
        self._content_source_sync_status = None
        self._content_source_sync_messages = None
        self._content_source_sync_at = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if created_at is not None:
            self.created_at = created_at
        if created_by is not None:
            self.created_by = created_by
        if updated_at is not None:
            self.updated_at = updated_at
        if updated_by is not None:
            self.updated_by = updated_by
        if org_id is not None:
            self.org_id = org_id
        if project_id is not None:
            self.project_id = project_id
        if project_name is not None:
            self.project_name = project_name
        if self_link is not None:
            self.self_link = self_link
        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if status is not None:
            self.status = status
        if content is not None:
            self.content = content
        if valid is not None:
            self.valid = valid
        if validation_messages is not None:
            self.validation_messages = validation_messages
        if total_versions is not None:
            self.total_versions = total_versions
        if total_released_versions is not None:
            self.total_released_versions = total_released_versions
        if request_scope_org is not None:
            self.request_scope_org = request_scope_org
        if content_source_id is not None:
            self.content_source_id = content_source_id
        if content_source_path is not None:
            self.content_source_path = content_source_path
        if content_source_type is not None:
            self.content_source_type = content_source_type
        if content_source_sync_status is not None:
            self.content_source_sync_status = content_source_sync_status
        if content_source_sync_messages is not None:
            self.content_source_sync_messages = content_source_sync_messages
        if content_source_sync_at is not None:
            self.content_source_sync_at = content_source_sync_at

    @property
    def id(self):
        """Gets the id of this ApiBlueprintsBody.  # noqa: E501

        Object ID  # noqa: E501

        :return: The id of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ApiBlueprintsBody.

        Object ID  # noqa: E501

        :param id: The id of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def created_at(self):
        """Gets the created_at of this ApiBlueprintsBody.  # noqa: E501

        Created time  # noqa: E501

        :return: The created_at of this ApiBlueprintsBody.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this ApiBlueprintsBody.

        Created time  # noqa: E501

        :param created_at: The created_at of this ApiBlueprintsBody.  # noqa: E501
        :type: datetime
        """

        self._created_at = created_at

    @property
    def created_by(self):
        """Gets the created_by of this ApiBlueprintsBody.  # noqa: E501

        Created by  # noqa: E501

        :return: The created_by of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this ApiBlueprintsBody.

        Created by  # noqa: E501

        :param created_by: The created_by of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def updated_at(self):
        """Gets the updated_at of this ApiBlueprintsBody.  # noqa: E501

        Updated time  # noqa: E501

        :return: The updated_at of this ApiBlueprintsBody.  # noqa: E501
        :rtype: datetime
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this ApiBlueprintsBody.

        Updated time  # noqa: E501

        :param updated_at: The updated_at of this ApiBlueprintsBody.  # noqa: E501
        :type: datetime
        """

        self._updated_at = updated_at

    @property
    def updated_by(self):
        """Gets the updated_by of this ApiBlueprintsBody.  # noqa: E501

        Updated by  # noqa: E501

        :return: The updated_by of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._updated_by

    @updated_by.setter
    def updated_by(self, updated_by):
        """Sets the updated_by of this ApiBlueprintsBody.

        Updated by  # noqa: E501

        :param updated_by: The updated_by of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._updated_by = updated_by

    @property
    def org_id(self):
        """Gets the org_id of this ApiBlueprintsBody.  # noqa: E501

        Org ID  # noqa: E501

        :return: The org_id of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this ApiBlueprintsBody.

        Org ID  # noqa: E501

        :param org_id: The org_id of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def project_id(self):
        """Gets the project_id of this ApiBlueprintsBody.  # noqa: E501

        Project ID  # noqa: E501

        :return: The project_id of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this ApiBlueprintsBody.

        Project ID  # noqa: E501

        :param project_id: The project_id of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._project_id = project_id

    @property
    def project_name(self):
        """Gets the project_name of this ApiBlueprintsBody.  # noqa: E501

        Project Name  # noqa: E501

        :return: The project_name of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._project_name

    @project_name.setter
    def project_name(self, project_name):
        """Sets the project_name of this ApiBlueprintsBody.

        Project Name  # noqa: E501

        :param project_name: The project_name of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._project_name = project_name

    @property
    def self_link(self):
        """Gets the self_link of this ApiBlueprintsBody.  # noqa: E501

        Blueprint self link  # noqa: E501

        :return: The self_link of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._self_link

    @self_link.setter
    def self_link(self, self_link):
        """Sets the self_link of this ApiBlueprintsBody.

        Blueprint self link  # noqa: E501

        :param self_link: The self_link of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._self_link = self_link

    @property
    def name(self):
        """Gets the name of this ApiBlueprintsBody.  # noqa: E501

        Blueprint name  # noqa: E501

        :return: The name of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ApiBlueprintsBody.

        Blueprint name  # noqa: E501

        :param name: The name of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def description(self):
        """Gets the description of this ApiBlueprintsBody.  # noqa: E501

        Blueprint description  # noqa: E501

        :return: The description of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ApiBlueprintsBody.

        Blueprint description  # noqa: E501

        :param description: The description of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def status(self):
        """Gets the status of this ApiBlueprintsBody.  # noqa: E501

        Blueprint status  # noqa: E501

        :return: The status of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this ApiBlueprintsBody.

        Blueprint status  # noqa: E501

        :param status: The status of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """
        allowed_values = ["DRAFT", "VERSIONED", "RELEASED"]  # noqa: E501
        if status not in allowed_values:
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def content(self):
        """Gets the content of this ApiBlueprintsBody.  # noqa: E501

        Blueprint YAML content  # noqa: E501

        :return: The content of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._content

    @content.setter
    def content(self, content):
        """Sets the content of this ApiBlueprintsBody.

        Blueprint YAML content  # noqa: E501

        :param content: The content of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._content = content

    @property
    def valid(self):
        """Gets the valid of this ApiBlueprintsBody.  # noqa: E501

        Validation result on update  # noqa: E501

        :return: The valid of this ApiBlueprintsBody.  # noqa: E501
        :rtype: bool
        """
        return self._valid

    @valid.setter
    def valid(self, valid):
        """Sets the valid of this ApiBlueprintsBody.

        Validation result on update  # noqa: E501

        :param valid: The valid of this ApiBlueprintsBody.  # noqa: E501
        :type: bool
        """

        self._valid = valid

    @property
    def validation_messages(self):
        """Gets the validation_messages of this ApiBlueprintsBody.  # noqa: E501

        Validation messages  # noqa: E501

        :return: The validation_messages of this ApiBlueprintsBody.  # noqa: E501
        :rtype: list[BlueprintapiblueprintsblueprintIdValidationMessages]
        """
        return self._validation_messages

    @validation_messages.setter
    def validation_messages(self, validation_messages):
        """Sets the validation_messages of this ApiBlueprintsBody.

        Validation messages  # noqa: E501

        :param validation_messages: The validation_messages of this ApiBlueprintsBody.  # noqa: E501
        :type: list[BlueprintapiblueprintsblueprintIdValidationMessages]
        """

        self._validation_messages = validation_messages

    @property
    def total_versions(self):
        """Gets the total_versions of this ApiBlueprintsBody.  # noqa: E501

        Total versions  # noqa: E501

        :return: The total_versions of this ApiBlueprintsBody.  # noqa: E501
        :rtype: int
        """
        return self._total_versions

    @total_versions.setter
    def total_versions(self, total_versions):
        """Sets the total_versions of this ApiBlueprintsBody.

        Total versions  # noqa: E501

        :param total_versions: The total_versions of this ApiBlueprintsBody.  # noqa: E501
        :type: int
        """

        self._total_versions = total_versions

    @property
    def total_released_versions(self):
        """Gets the total_released_versions of this ApiBlueprintsBody.  # noqa: E501

        Total released versions  # noqa: E501

        :return: The total_released_versions of this ApiBlueprintsBody.  # noqa: E501
        :rtype: int
        """
        return self._total_released_versions

    @total_released_versions.setter
    def total_released_versions(self, total_released_versions):
        """Sets the total_released_versions of this ApiBlueprintsBody.

        Total released versions  # noqa: E501

        :param total_released_versions: The total_released_versions of this ApiBlueprintsBody.  # noqa: E501
        :type: int
        """

        self._total_released_versions = total_released_versions

    @property
    def request_scope_org(self):
        """Gets the request_scope_org of this ApiBlueprintsBody.  # noqa: E501

        Flag to indicate blueprint can be requested from any project in org  # noqa: E501

        :return: The request_scope_org of this ApiBlueprintsBody.  # noqa: E501
        :rtype: bool
        """
        return self._request_scope_org

    @request_scope_org.setter
    def request_scope_org(self, request_scope_org):
        """Sets the request_scope_org of this ApiBlueprintsBody.

        Flag to indicate blueprint can be requested from any project in org  # noqa: E501

        :param request_scope_org: The request_scope_org of this ApiBlueprintsBody.  # noqa: E501
        :type: bool
        """

        self._request_scope_org = request_scope_org

    @property
    def content_source_id(self):
        """Gets the content_source_id of this ApiBlueprintsBody.  # noqa: E501

        Content source id  # noqa: E501

        :return: The content_source_id of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._content_source_id

    @content_source_id.setter
    def content_source_id(self, content_source_id):
        """Sets the content_source_id of this ApiBlueprintsBody.

        Content source id  # noqa: E501

        :param content_source_id: The content_source_id of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._content_source_id = content_source_id

    @property
    def content_source_path(self):
        """Gets the content_source_path of this ApiBlueprintsBody.  # noqa: E501

        Content source path  # noqa: E501

        :return: The content_source_path of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._content_source_path

    @content_source_path.setter
    def content_source_path(self, content_source_path):
        """Sets the content_source_path of this ApiBlueprintsBody.

        Content source path  # noqa: E501

        :param content_source_path: The content_source_path of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._content_source_path = content_source_path

    @property
    def content_source_type(self):
        """Gets the content_source_type of this ApiBlueprintsBody.  # noqa: E501

        Content source type  # noqa: E501

        :return: The content_source_type of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._content_source_type

    @content_source_type.setter
    def content_source_type(self, content_source_type):
        """Sets the content_source_type of this ApiBlueprintsBody.

        Content source type  # noqa: E501

        :param content_source_type: The content_source_type of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """

        self._content_source_type = content_source_type

    @property
    def content_source_sync_status(self):
        """Gets the content_source_sync_status of this ApiBlueprintsBody.  # noqa: E501

        Content source last sync status  # noqa: E501

        :return: The content_source_sync_status of this ApiBlueprintsBody.  # noqa: E501
        :rtype: str
        """
        return self._content_source_sync_status

    @content_source_sync_status.setter
    def content_source_sync_status(self, content_source_sync_status):
        """Sets the content_source_sync_status of this ApiBlueprintsBody.

        Content source last sync status  # noqa: E501

        :param content_source_sync_status: The content_source_sync_status of this ApiBlueprintsBody.  # noqa: E501
        :type: str
        """
        allowed_values = ["SUCCESSFUL", "FAILED"]  # noqa: E501
        if content_source_sync_status not in allowed_values:
            raise ValueError(
                "Invalid value for `content_source_sync_status` ({0}), must be one of {1}"  # noqa: E501
                .format(content_source_sync_status, allowed_values)
            )

        self._content_source_sync_status = content_source_sync_status

    @property
    def content_source_sync_messages(self):
        """Gets the content_source_sync_messages of this ApiBlueprintsBody.  # noqa: E501

        Content source last sync messages  # noqa: E501

        :return: The content_source_sync_messages of this ApiBlueprintsBody.  # noqa: E501
        :rtype: list[str]
        """
        return self._content_source_sync_messages

    @content_source_sync_messages.setter
    def content_source_sync_messages(self, content_source_sync_messages):
        """Sets the content_source_sync_messages of this ApiBlueprintsBody.

        Content source last sync messages  # noqa: E501

        :param content_source_sync_messages: The content_source_sync_messages of this ApiBlueprintsBody.  # noqa: E501
        :type: list[str]
        """

        self._content_source_sync_messages = content_source_sync_messages

    @property
    def content_source_sync_at(self):
        """Gets the content_source_sync_at of this ApiBlueprintsBody.  # noqa: E501

        Content source last sync time  # noqa: E501

        :return: The content_source_sync_at of this ApiBlueprintsBody.  # noqa: E501
        :rtype: datetime
        """
        return self._content_source_sync_at

    @content_source_sync_at.setter
    def content_source_sync_at(self, content_source_sync_at):
        """Sets the content_source_sync_at of this ApiBlueprintsBody.

        Content source last sync time  # noqa: E501

        :param content_source_sync_at: The content_source_sync_at of this ApiBlueprintsBody.  # noqa: E501
        :type: datetime
        """

        self._content_source_sync_at = content_source_sync_at

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ApiBlueprintsBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ApiBlueprintsBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
