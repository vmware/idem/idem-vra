# coding: utf-8

"""
    VMware Aria Automation Assembler Blueprint API

    A multi-cloud Blueprint API for VMware Aria Automation Services  This page describes the RESTful APIs for Blueprint. The APIs facilitate CRUD operations on the various resources and entities used throughout Blueprint (Blueprints, Blueprint Terraform Integrations , PropertyGroups , etc.) and allow operations on them (creating a Blueprint, creating a Property group, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Blueprint entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/blueprint/api/blueprints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/blueprint/api/blueprints?$top=10&$skip=2```   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse20012(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'resources': 'list[BlueprintResourcesPlanResources]'
    }

    attribute_map = {
        'resources': 'resources'
    }

    def __init__(self, resources=None):  # noqa: E501
        """InlineResponse20012 - a model defined in Swagger"""  # noqa: E501
        self._resources = None
        self.discriminator = None
        if resources is not None:
            self.resources = resources

    @property
    def resources(self):
        """Gets the resources of this InlineResponse20012.  # noqa: E501

        Blueprint plan tasks  # noqa: E501

        :return: The resources of this InlineResponse20012.  # noqa: E501
        :rtype: list[BlueprintResourcesPlanResources]
        """
        return self._resources

    @resources.setter
    def resources(self, resources):
        """Sets the resources of this InlineResponse20012.

        Blueprint plan tasks  # noqa: E501

        :param resources: The resources of this InlineResponse20012.  # noqa: E501
        :type: list[BlueprintResourcesPlanResources]
        """

        self._resources = resources

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse20012, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20012):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
