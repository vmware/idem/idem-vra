# coding: utf-8

"""
    VMware Aria Automation Assembler Blueprint API

    A multi-cloud Blueprint API for VMware Aria Automation Services  This page describes the RESTful APIs for Blueprint. The APIs facilitate CRUD operations on the various resources and entities used throughout Blueprint (Blueprints, Blueprint Terraform Integrations , PropertyGroups , etc.) and allow operations on them (creating a Blueprint, creating a Property group, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Blueprint entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/blueprint/api/blueprints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/blueprint/api/blueprints?$top=10&$skip=2```   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class BlueprintPlanResource(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'resource_name': 'str',
        'resource_type': 'str',
        'resource_reason': 'str',
        'new_properties': 'dict(str, object)',
        'old_properties': 'dict(str, object)',
        'depends_on_resources': 'list[str]',
        'task_names': 'list[str]'
    }

    attribute_map = {
        'resource_name': 'resourceName',
        'resource_type': 'resourceType',
        'resource_reason': 'resourceReason',
        'new_properties': 'newProperties',
        'old_properties': 'oldProperties',
        'depends_on_resources': 'dependsOnResources',
        'task_names': 'taskNames'
    }

    def __init__(self, resource_name=None, resource_type=None, resource_reason=None, new_properties=None, old_properties=None, depends_on_resources=None, task_names=None):  # noqa: E501
        """BlueprintPlanResource - a model defined in Swagger"""  # noqa: E501
        self._resource_name = None
        self._resource_type = None
        self._resource_reason = None
        self._new_properties = None
        self._old_properties = None
        self._depends_on_resources = None
        self._task_names = None
        self.discriminator = None
        if resource_name is not None:
            self.resource_name = resource_name
        if resource_type is not None:
            self.resource_type = resource_type
        if resource_reason is not None:
            self.resource_reason = resource_reason
        if new_properties is not None:
            self.new_properties = new_properties
        if old_properties is not None:
            self.old_properties = old_properties
        if depends_on_resources is not None:
            self.depends_on_resources = depends_on_resources
        if task_names is not None:
            self.task_names = task_names

    @property
    def resource_name(self):
        """Gets the resource_name of this BlueprintPlanResource.  # noqa: E501

        Resource name  # noqa: E501

        :return: The resource_name of this BlueprintPlanResource.  # noqa: E501
        :rtype: str
        """
        return self._resource_name

    @resource_name.setter
    def resource_name(self, resource_name):
        """Sets the resource_name of this BlueprintPlanResource.

        Resource name  # noqa: E501

        :param resource_name: The resource_name of this BlueprintPlanResource.  # noqa: E501
        :type: str
        """

        self._resource_name = resource_name

    @property
    def resource_type(self):
        """Gets the resource_type of this BlueprintPlanResource.  # noqa: E501

        Resource type  # noqa: E501

        :return: The resource_type of this BlueprintPlanResource.  # noqa: E501
        :rtype: str
        """
        return self._resource_type

    @resource_type.setter
    def resource_type(self, resource_type):
        """Sets the resource_type of this BlueprintPlanResource.

        Resource type  # noqa: E501

        :param resource_type: The resource_type of this BlueprintPlanResource.  # noqa: E501
        :type: str
        """

        self._resource_type = resource_type

    @property
    def resource_reason(self):
        """Gets the resource_reason of this BlueprintPlanResource.  # noqa: E501

        Resource reason  # noqa: E501

        :return: The resource_reason of this BlueprintPlanResource.  # noqa: E501
        :rtype: str
        """
        return self._resource_reason

    @resource_reason.setter
    def resource_reason(self, resource_reason):
        """Sets the resource_reason of this BlueprintPlanResource.

        Resource reason  # noqa: E501

        :param resource_reason: The resource_reason of this BlueprintPlanResource.  # noqa: E501
        :type: str
        """
        allowed_values = ["CREATE", "RECREATE", "UPDATE", "DELETE", "ACTION", "READ"]  # noqa: E501
        if resource_reason not in allowed_values:
            raise ValueError(
                "Invalid value for `resource_reason` ({0}), must be one of {1}"  # noqa: E501
                .format(resource_reason, allowed_values)
            )

        self._resource_reason = resource_reason

    @property
    def new_properties(self):
        """Gets the new_properties of this BlueprintPlanResource.  # noqa: E501

        Resource new properties  # noqa: E501

        :return: The new_properties of this BlueprintPlanResource.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._new_properties

    @new_properties.setter
    def new_properties(self, new_properties):
        """Sets the new_properties of this BlueprintPlanResource.

        Resource new properties  # noqa: E501

        :param new_properties: The new_properties of this BlueprintPlanResource.  # noqa: E501
        :type: dict(str, object)
        """

        self._new_properties = new_properties

    @property
    def old_properties(self):
        """Gets the old_properties of this BlueprintPlanResource.  # noqa: E501

        Resource old properties  # noqa: E501

        :return: The old_properties of this BlueprintPlanResource.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._old_properties

    @old_properties.setter
    def old_properties(self, old_properties):
        """Sets the old_properties of this BlueprintPlanResource.

        Resource old properties  # noqa: E501

        :param old_properties: The old_properties of this BlueprintPlanResource.  # noqa: E501
        :type: dict(str, object)
        """

        self._old_properties = old_properties

    @property
    def depends_on_resources(self):
        """Gets the depends_on_resources of this BlueprintPlanResource.  # noqa: E501

        Resource depends on other resources in the plan  # noqa: E501

        :return: The depends_on_resources of this BlueprintPlanResource.  # noqa: E501
        :rtype: list[str]
        """
        return self._depends_on_resources

    @depends_on_resources.setter
    def depends_on_resources(self, depends_on_resources):
        """Sets the depends_on_resources of this BlueprintPlanResource.

        Resource depends on other resources in the plan  # noqa: E501

        :param depends_on_resources: The depends_on_resources of this BlueprintPlanResource.  # noqa: E501
        :type: list[str]
        """

        self._depends_on_resources = depends_on_resources

    @property
    def task_names(self):
        """Gets the task_names of this BlueprintPlanResource.  # noqa: E501

        List of task names  # noqa: E501

        :return: The task_names of this BlueprintPlanResource.  # noqa: E501
        :rtype: list[str]
        """
        return self._task_names

    @task_names.setter
    def task_names(self, task_names):
        """Sets the task_names of this BlueprintPlanResource.

        List of task names  # noqa: E501

        :param task_names: The task_names of this BlueprintPlanResource.  # noqa: E501
        :type: list[str]
        """

        self._task_names = task_names

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(BlueprintPlanResource, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, BlueprintPlanResource):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
