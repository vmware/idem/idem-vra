# coding: utf-8

"""
    VMware Aria Automation Assembler / CMX Service API

    When using Kubernetes integration, deploy and manage Kubernetes clusters and namespaces. The APIs that list collections of resources also support OData like implementation. Below query params can be used across CMX entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/cmx/api/resources/tags?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/cmx/api/resources/tags?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/cmx/api/resources/tags?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /cmx/api/resources/tags?$filter=startswith(name, 'K8S')   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class K8SInstallerSteps(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'created_millis': 'int',
        'id': 'str',
        'index': 'int',
        'name': 'str',
        'org_id': 'str',
        'resource': 'K8SInstallerResource',
        'state': 'str',
        'type': 'str',
        'updated_millis': 'int'
    }

    attribute_map = {
        'created_millis': 'createdMillis',
        'id': 'id',
        'index': 'index',
        'name': 'name',
        'org_id': 'orgId',
        'resource': 'resource',
        'state': 'state',
        'type': 'type',
        'updated_millis': 'updatedMillis'
    }

    def __init__(self, created_millis=None, id=None, index=None, name=None, org_id=None, resource=None, state=None, type=None, updated_millis=None):  # noqa: E501
        """K8SInstallerSteps - a model defined in Swagger"""  # noqa: E501
        self._created_millis = None
        self._id = None
        self._index = None
        self._name = None
        self._org_id = None
        self._resource = None
        self._state = None
        self._type = None
        self._updated_millis = None
        self.discriminator = None
        if created_millis is not None:
            self.created_millis = created_millis
        if id is not None:
            self.id = id
        if index is not None:
            self.index = index
        if name is not None:
            self.name = name
        if org_id is not None:
            self.org_id = org_id
        if resource is not None:
            self.resource = resource
        if state is not None:
            self.state = state
        if type is not None:
            self.type = type
        if updated_millis is not None:
            self.updated_millis = updated_millis

    @property
    def created_millis(self):
        """Gets the created_millis of this K8SInstallerSteps.  # noqa: E501


        :return: The created_millis of this K8SInstallerSteps.  # noqa: E501
        :rtype: int
        """
        return self._created_millis

    @created_millis.setter
    def created_millis(self, created_millis):
        """Sets the created_millis of this K8SInstallerSteps.


        :param created_millis: The created_millis of this K8SInstallerSteps.  # noqa: E501
        :type: int
        """

        self._created_millis = created_millis

    @property
    def id(self):
        """Gets the id of this K8SInstallerSteps.  # noqa: E501


        :return: The id of this K8SInstallerSteps.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this K8SInstallerSteps.


        :param id: The id of this K8SInstallerSteps.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def index(self):
        """Gets the index of this K8SInstallerSteps.  # noqa: E501


        :return: The index of this K8SInstallerSteps.  # noqa: E501
        :rtype: int
        """
        return self._index

    @index.setter
    def index(self, index):
        """Sets the index of this K8SInstallerSteps.


        :param index: The index of this K8SInstallerSteps.  # noqa: E501
        :type: int
        """

        self._index = index

    @property
    def name(self):
        """Gets the name of this K8SInstallerSteps.  # noqa: E501


        :return: The name of this K8SInstallerSteps.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this K8SInstallerSteps.


        :param name: The name of this K8SInstallerSteps.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def org_id(self):
        """Gets the org_id of this K8SInstallerSteps.  # noqa: E501


        :return: The org_id of this K8SInstallerSteps.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this K8SInstallerSteps.


        :param org_id: The org_id of this K8SInstallerSteps.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def resource(self):
        """Gets the resource of this K8SInstallerSteps.  # noqa: E501


        :return: The resource of this K8SInstallerSteps.  # noqa: E501
        :rtype: K8SInstallerResource
        """
        return self._resource

    @resource.setter
    def resource(self, resource):
        """Sets the resource of this K8SInstallerSteps.


        :param resource: The resource of this K8SInstallerSteps.  # noqa: E501
        :type: K8SInstallerResource
        """

        self._resource = resource

    @property
    def state(self):
        """Gets the state of this K8SInstallerSteps.  # noqa: E501


        :return: The state of this K8SInstallerSteps.  # noqa: E501
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """Sets the state of this K8SInstallerSteps.


        :param state: The state of this K8SInstallerSteps.  # noqa: E501
        :type: str
        """
        allowed_values = ["INITIALIZED", "NOT_INSTALLED", "INSTALLING", "INSTALLED", "READY", "UNINSTALLING", "UNREACHABLE", "FAILED"]  # noqa: E501
        if state not in allowed_values:
            raise ValueError(
                "Invalid value for `state` ({0}), must be one of {1}"  # noqa: E501
                .format(state, allowed_values)
            )

        self._state = state

    @property
    def type(self):
        """Gets the type of this K8SInstallerSteps.  # noqa: E501


        :return: The type of this K8SInstallerSteps.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this K8SInstallerSteps.


        :param type: The type of this K8SInstallerSteps.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def updated_millis(self):
        """Gets the updated_millis of this K8SInstallerSteps.  # noqa: E501


        :return: The updated_millis of this K8SInstallerSteps.  # noqa: E501
        :rtype: int
        """
        return self._updated_millis

    @updated_millis.setter
    def updated_millis(self, updated_millis):
        """Sets the updated_millis of this K8SInstallerSteps.


        :param updated_millis: The updated_millis of this K8SInstallerSteps.  # noqa: E501
        :type: int
        """

        self._updated_millis = updated_millis

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(K8SInstallerSteps, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, K8SInstallerSteps):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
