# coding: utf-8

"""
    VMware Aria Automation Assembler / CMX Service API

    When using Kubernetes integration, deploy and manage Kubernetes clusters and namespaces. The APIs that list collections of resources also support OData like implementation. Below query params can be used across CMX entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/cmx/api/resources/tags?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/cmx/api/resources/tags?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/cmx/api/resources/tags?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /cmx/api/resources/tags?$filter=startswith(name, 'K8S')   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class IdRegisterBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'auth_credentials_link': 'str',
        'cluster_id': 'str',
        'created_millis': 'int',
        'custom_properties': 'dict(str, object)',
        'description': 'str',
        'external_link': 'str',
        'id': 'str',
        'installer_id': 'str',
        'name': 'str',
        'org_id': 'str',
        'owner': 'str',
        'persisted': 'bool',
        'project_id': 'str',
        'registered': 'bool',
        'shared': 'bool',
        'status': 'str',
        'updated_millis': 'int',
        'zone_project_assignment_id': 'str'
    }

    attribute_map = {
        'auth_credentials_link': 'authCredentialsLink',
        'cluster_id': 'clusterId',
        'created_millis': 'createdMillis',
        'custom_properties': 'customProperties',
        'description': 'description',
        'external_link': 'externalLink',
        'id': 'id',
        'installer_id': 'installerId',
        'name': 'name',
        'org_id': 'orgId',
        'owner': 'owner',
        'persisted': 'persisted',
        'project_id': 'projectId',
        'registered': 'registered',
        'shared': 'shared',
        'status': 'status',
        'updated_millis': 'updatedMillis',
        'zone_project_assignment_id': 'zoneProjectAssignmentId'
    }

    def __init__(self, auth_credentials_link=None, cluster_id=None, created_millis=None, custom_properties=None, description=None, external_link=None, id=None, installer_id=None, name=None, org_id=None, owner=None, persisted=None, project_id=None, registered=None, shared=None, status=None, updated_millis=None, zone_project_assignment_id=None):  # noqa: E501
        """IdRegisterBody - a model defined in Swagger"""  # noqa: E501
        self._auth_credentials_link = None
        self._cluster_id = None
        self._created_millis = None
        self._custom_properties = None
        self._description = None
        self._external_link = None
        self._id = None
        self._installer_id = None
        self._name = None
        self._org_id = None
        self._owner = None
        self._persisted = None
        self._project_id = None
        self._registered = None
        self._shared = None
        self._status = None
        self._updated_millis = None
        self._zone_project_assignment_id = None
        self.discriminator = None
        if auth_credentials_link is not None:
            self.auth_credentials_link = auth_credentials_link
        if cluster_id is not None:
            self.cluster_id = cluster_id
        if created_millis is not None:
            self.created_millis = created_millis
        if custom_properties is not None:
            self.custom_properties = custom_properties
        if description is not None:
            self.description = description
        if external_link is not None:
            self.external_link = external_link
        if id is not None:
            self.id = id
        if installer_id is not None:
            self.installer_id = installer_id
        if name is not None:
            self.name = name
        if org_id is not None:
            self.org_id = org_id
        if owner is not None:
            self.owner = owner
        if persisted is not None:
            self.persisted = persisted
        if project_id is not None:
            self.project_id = project_id
        if registered is not None:
            self.registered = registered
        if shared is not None:
            self.shared = shared
        if status is not None:
            self.status = status
        if updated_millis is not None:
            self.updated_millis = updated_millis
        if zone_project_assignment_id is not None:
            self.zone_project_assignment_id = zone_project_assignment_id

    @property
    def auth_credentials_link(self):
        """Gets the auth_credentials_link of this IdRegisterBody.  # noqa: E501


        :return: The auth_credentials_link of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._auth_credentials_link

    @auth_credentials_link.setter
    def auth_credentials_link(self, auth_credentials_link):
        """Sets the auth_credentials_link of this IdRegisterBody.


        :param auth_credentials_link: The auth_credentials_link of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._auth_credentials_link = auth_credentials_link

    @property
    def cluster_id(self):
        """Gets the cluster_id of this IdRegisterBody.  # noqa: E501


        :return: The cluster_id of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._cluster_id

    @cluster_id.setter
    def cluster_id(self, cluster_id):
        """Sets the cluster_id of this IdRegisterBody.


        :param cluster_id: The cluster_id of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._cluster_id = cluster_id

    @property
    def created_millis(self):
        """Gets the created_millis of this IdRegisterBody.  # noqa: E501


        :return: The created_millis of this IdRegisterBody.  # noqa: E501
        :rtype: int
        """
        return self._created_millis

    @created_millis.setter
    def created_millis(self, created_millis):
        """Sets the created_millis of this IdRegisterBody.


        :param created_millis: The created_millis of this IdRegisterBody.  # noqa: E501
        :type: int
        """

        self._created_millis = created_millis

    @property
    def custom_properties(self):
        """Gets the custom_properties of this IdRegisterBody.  # noqa: E501


        :return: The custom_properties of this IdRegisterBody.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._custom_properties

    @custom_properties.setter
    def custom_properties(self, custom_properties):
        """Sets the custom_properties of this IdRegisterBody.


        :param custom_properties: The custom_properties of this IdRegisterBody.  # noqa: E501
        :type: dict(str, object)
        """

        self._custom_properties = custom_properties

    @property
    def description(self):
        """Gets the description of this IdRegisterBody.  # noqa: E501


        :return: The description of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this IdRegisterBody.


        :param description: The description of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def external_link(self):
        """Gets the external_link of this IdRegisterBody.  # noqa: E501


        :return: The external_link of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._external_link

    @external_link.setter
    def external_link(self, external_link):
        """Sets the external_link of this IdRegisterBody.


        :param external_link: The external_link of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._external_link = external_link

    @property
    def id(self):
        """Gets the id of this IdRegisterBody.  # noqa: E501


        :return: The id of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this IdRegisterBody.


        :param id: The id of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def installer_id(self):
        """Gets the installer_id of this IdRegisterBody.  # noqa: E501


        :return: The installer_id of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._installer_id

    @installer_id.setter
    def installer_id(self, installer_id):
        """Sets the installer_id of this IdRegisterBody.


        :param installer_id: The installer_id of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._installer_id = installer_id

    @property
    def name(self):
        """Gets the name of this IdRegisterBody.  # noqa: E501


        :return: The name of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this IdRegisterBody.


        :param name: The name of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def org_id(self):
        """Gets the org_id of this IdRegisterBody.  # noqa: E501


        :return: The org_id of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this IdRegisterBody.


        :param org_id: The org_id of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def owner(self):
        """Gets the owner of this IdRegisterBody.  # noqa: E501


        :return: The owner of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._owner

    @owner.setter
    def owner(self, owner):
        """Sets the owner of this IdRegisterBody.


        :param owner: The owner of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._owner = owner

    @property
    def persisted(self):
        """Gets the persisted of this IdRegisterBody.  # noqa: E501


        :return: The persisted of this IdRegisterBody.  # noqa: E501
        :rtype: bool
        """
        return self._persisted

    @persisted.setter
    def persisted(self, persisted):
        """Sets the persisted of this IdRegisterBody.


        :param persisted: The persisted of this IdRegisterBody.  # noqa: E501
        :type: bool
        """

        self._persisted = persisted

    @property
    def project_id(self):
        """Gets the project_id of this IdRegisterBody.  # noqa: E501


        :return: The project_id of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this IdRegisterBody.


        :param project_id: The project_id of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._project_id = project_id

    @property
    def registered(self):
        """Gets the registered of this IdRegisterBody.  # noqa: E501


        :return: The registered of this IdRegisterBody.  # noqa: E501
        :rtype: bool
        """
        return self._registered

    @registered.setter
    def registered(self, registered):
        """Sets the registered of this IdRegisterBody.


        :param registered: The registered of this IdRegisterBody.  # noqa: E501
        :type: bool
        """

        self._registered = registered

    @property
    def shared(self):
        """Gets the shared of this IdRegisterBody.  # noqa: E501


        :return: The shared of this IdRegisterBody.  # noqa: E501
        :rtype: bool
        """
        return self._shared

    @shared.setter
    def shared(self, shared):
        """Sets the shared of this IdRegisterBody.


        :param shared: The shared of this IdRegisterBody.  # noqa: E501
        :type: bool
        """

        self._shared = shared

    @property
    def status(self):
        """Gets the status of this IdRegisterBody.  # noqa: E501


        :return: The status of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this IdRegisterBody.


        :param status: The status of this IdRegisterBody.  # noqa: E501
        :type: str
        """
        allowed_values = ["ALLOCATED", "READY", "FAILED", "UNREACHABLE", "TERMINATING", "REMOVED"]  # noqa: E501
        if status not in allowed_values:
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def updated_millis(self):
        """Gets the updated_millis of this IdRegisterBody.  # noqa: E501


        :return: The updated_millis of this IdRegisterBody.  # noqa: E501
        :rtype: int
        """
        return self._updated_millis

    @updated_millis.setter
    def updated_millis(self, updated_millis):
        """Sets the updated_millis of this IdRegisterBody.


        :param updated_millis: The updated_millis of this IdRegisterBody.  # noqa: E501
        :type: int
        """

        self._updated_millis = updated_millis

    @property
    def zone_project_assignment_id(self):
        """Gets the zone_project_assignment_id of this IdRegisterBody.  # noqa: E501


        :return: The zone_project_assignment_id of this IdRegisterBody.  # noqa: E501
        :rtype: str
        """
        return self._zone_project_assignment_id

    @zone_project_assignment_id.setter
    def zone_project_assignment_id(self, zone_project_assignment_id):
        """Sets the zone_project_assignment_id of this IdRegisterBody.


        :param zone_project_assignment_id: The zone_project_assignment_id of this IdRegisterBody.  # noqa: E501
        :type: str
        """

        self._zone_project_assignment_id = zone_project_assignment_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(IdRegisterBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IdRegisterBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
