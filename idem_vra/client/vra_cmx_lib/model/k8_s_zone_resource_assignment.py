# coding: utf-8

"""
    VMware Aria Automation Assembler / CMX Service API

    When using Kubernetes integration, deploy and manage Kubernetes clusters and namespaces. The APIs that list collections of resources also support OData like implementation. Below query params can be used across CMX entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/cmx/api/resources/tags?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/cmx/api/resources/tags?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/cmx/api/resources/tags?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /cmx/api/resources/tags?$filter=startswith(name, 'K8S')   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class K8SZoneResourceAssignment(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'created_millis': 'int',
        'enabled': 'bool',
        'id': 'str',
        'org_id': 'str',
        'priority': 'int',
        'resource_id': 'str',
        'resource_type': 'str',
        'tag_ids': 'list[str]',
        'tag_links': 'list[str]',
        'tags': 'list[Cmxapiresourcesk8szonesTags]',
        'updated_millis': 'int'
    }

    attribute_map = {
        'created_millis': 'createdMillis',
        'enabled': 'enabled',
        'id': 'id',
        'org_id': 'orgId',
        'priority': 'priority',
        'resource_id': 'resourceId',
        'resource_type': 'resourceType',
        'tag_ids': 'tagIds',
        'tag_links': 'tagLinks',
        'tags': 'tags',
        'updated_millis': 'updatedMillis'
    }

    def __init__(self, created_millis=None, enabled=None, id=None, org_id=None, priority=None, resource_id=None, resource_type=None, tag_ids=None, tag_links=None, tags=None, updated_millis=None):  # noqa: E501
        """K8SZoneResourceAssignment - a model defined in Swagger"""  # noqa: E501
        self._created_millis = None
        self._enabled = None
        self._id = None
        self._org_id = None
        self._priority = None
        self._resource_id = None
        self._resource_type = None
        self._tag_ids = None
        self._tag_links = None
        self._tags = None
        self._updated_millis = None
        self.discriminator = None
        if created_millis is not None:
            self.created_millis = created_millis
        if enabled is not None:
            self.enabled = enabled
        if id is not None:
            self.id = id
        if org_id is not None:
            self.org_id = org_id
        if priority is not None:
            self.priority = priority
        if resource_id is not None:
            self.resource_id = resource_id
        if resource_type is not None:
            self.resource_type = resource_type
        if tag_ids is not None:
            self.tag_ids = tag_ids
        if tag_links is not None:
            self.tag_links = tag_links
        if tags is not None:
            self.tags = tags
        if updated_millis is not None:
            self.updated_millis = updated_millis

    @property
    def created_millis(self):
        """Gets the created_millis of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The created_millis of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: int
        """
        return self._created_millis

    @created_millis.setter
    def created_millis(self, created_millis):
        """Sets the created_millis of this K8SZoneResourceAssignment.


        :param created_millis: The created_millis of this K8SZoneResourceAssignment.  # noqa: E501
        :type: int
        """

        self._created_millis = created_millis

    @property
    def enabled(self):
        """Gets the enabled of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The enabled of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: bool
        """
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        """Sets the enabled of this K8SZoneResourceAssignment.


        :param enabled: The enabled of this K8SZoneResourceAssignment.  # noqa: E501
        :type: bool
        """

        self._enabled = enabled

    @property
    def id(self):
        """Gets the id of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The id of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this K8SZoneResourceAssignment.


        :param id: The id of this K8SZoneResourceAssignment.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def org_id(self):
        """Gets the org_id of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The org_id of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this K8SZoneResourceAssignment.


        :param org_id: The org_id of this K8SZoneResourceAssignment.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def priority(self):
        """Gets the priority of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The priority of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: int
        """
        return self._priority

    @priority.setter
    def priority(self, priority):
        """Sets the priority of this K8SZoneResourceAssignment.


        :param priority: The priority of this K8SZoneResourceAssignment.  # noqa: E501
        :type: int
        """

        self._priority = priority

    @property
    def resource_id(self):
        """Gets the resource_id of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The resource_id of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: str
        """
        return self._resource_id

    @resource_id.setter
    def resource_id(self, resource_id):
        """Sets the resource_id of this K8SZoneResourceAssignment.


        :param resource_id: The resource_id of this K8SZoneResourceAssignment.  # noqa: E501
        :type: str
        """

        self._resource_id = resource_id

    @property
    def resource_type(self):
        """Gets the resource_type of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The resource_type of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: str
        """
        return self._resource_type

    @resource_type.setter
    def resource_type(self, resource_type):
        """Sets the resource_type of this K8SZoneResourceAssignment.


        :param resource_type: The resource_type of this K8SZoneResourceAssignment.  # noqa: E501
        :type: str
        """
        allowed_values = ["PKS_PLAN"]  # noqa: E501
        if resource_type not in allowed_values:
            raise ValueError(
                "Invalid value for `resource_type` ({0}), must be one of {1}"  # noqa: E501
                .format(resource_type, allowed_values)
            )

        self._resource_type = resource_type

    @property
    def tag_ids(self):
        """Gets the tag_ids of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The tag_ids of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: list[str]
        """
        return self._tag_ids

    @tag_ids.setter
    def tag_ids(self, tag_ids):
        """Sets the tag_ids of this K8SZoneResourceAssignment.


        :param tag_ids: The tag_ids of this K8SZoneResourceAssignment.  # noqa: E501
        :type: list[str]
        """

        self._tag_ids = tag_ids

    @property
    def tag_links(self):
        """Gets the tag_links of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The tag_links of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: list[str]
        """
        return self._tag_links

    @tag_links.setter
    def tag_links(self, tag_links):
        """Sets the tag_links of this K8SZoneResourceAssignment.


        :param tag_links: The tag_links of this K8SZoneResourceAssignment.  # noqa: E501
        :type: list[str]
        """

        self._tag_links = tag_links

    @property
    def tags(self):
        """Gets the tags of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The tags of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: list[Cmxapiresourcesk8szonesTags]
        """
        return self._tags

    @tags.setter
    def tags(self, tags):
        """Sets the tags of this K8SZoneResourceAssignment.


        :param tags: The tags of this K8SZoneResourceAssignment.  # noqa: E501
        :type: list[Cmxapiresourcesk8szonesTags]
        """

        self._tags = tags

    @property
    def updated_millis(self):
        """Gets the updated_millis of this K8SZoneResourceAssignment.  # noqa: E501


        :return: The updated_millis of this K8SZoneResourceAssignment.  # noqa: E501
        :rtype: int
        """
        return self._updated_millis

    @updated_millis.setter
    def updated_millis(self, updated_millis):
        """Sets the updated_millis of this K8SZoneResourceAssignment.


        :param updated_millis: The updated_millis of this K8SZoneResourceAssignment.  # noqa: E501
        :type: int
        """

        self._updated_millis = updated_millis

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(K8SZoneResourceAssignment, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, K8SZoneResourceAssignment):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
