# coding: utf-8

"""
    Project Service API

    Project Service exposes a REST API, providing access to operations with projects Example: 1. `$orderby` - Sorting criteria in the format: property (asc | desc). Default sort order is ascending.     ```/project-service/api/projects?$orderby=name%20desc``` 2. `$top` -  limits the requested number of resources.     ```/project-service/api/projects?$top=10``` 3. `$skip` -  client can skip a given number of resources.      ```/project-service/api/projects?$skip=2``` 4. `$top` & `$skip` - used in conjunction helps in pagination of resources        ```/project-service/api/projects?$top=10&$skip=2``` 5. `$filter` -  returns a subset of resources that satisfy the given predicate expression.     ```/project-service/api/projects?$filter=startswith(name, 'Test')```  # noqa: E501

    OpenAPI spec version: 2020-08-10
    Contact: sales@vmware.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from idem_vra.client.vra_rbac_lib.api_client import ApiClient


class UserAuthContextApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_auth_context(self, **kwargs):  # noqa: E501
        """Retrieve the logged in user's auth context  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_auth_context(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param bool exclude_supervisor_role_projects: Filters projects based on the supervisor role. When this filter is true it will not include the projects in which the current user is having only supervisor role
        :param bool include_groups: Include Group information. When the flag is true it will include all the groups that has been added to the project and the user is a part of.
        :param str api_version:
        :return: InlineResponse2006
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_auth_context_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_auth_context_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_auth_context_with_http_info(self, **kwargs):  # noqa: E501
        """Retrieve the logged in user's auth context  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_auth_context_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param bool exclude_supervisor_role_projects: Filters projects based on the supervisor role. When this filter is true it will not include the projects in which the current user is having only supervisor role
        :param bool include_groups: Include Group information. When the flag is true it will include all the groups that has been added to the project and the user is a part of.
        :param str api_version:
        :return: InlineResponse2006
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['exclude_supervisor_role_projects', 'include_groups', 'api_version']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_auth_context" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'exclude_supervisor_role_projects' in params:
            query_params.append(('excludeSupervisorRoleProjects', params['exclude_supervisor_role_projects']))  # noqa: E501
        if 'include_groups' in params:
            query_params.append(('includeGroups', params['include_groups']))  # noqa: E501
        if 'api_version' in params:
            query_params.append(('apiVersion', params['api_version']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['*/*'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Authorization']  # noqa: E501

        return self.api_client.call_api(
            '/rbac-service/api/auth-context', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='InlineResponse2006',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
