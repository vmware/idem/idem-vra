# coding: utf-8

"""
    VMware Aria Automation Pipelines APIs

    Aria Automation Pipelines is a continuous integration and continuous delivery (CICD) tool that you use to build Pipelines that model the software release process in your DevOps lifecycle. By creating Pipelines, you build the code infrastructure that delivers your software rapidly and continuously.  This page describes the RESTful APIs for Aria Automation Pipelines. The APIs facilitate CRUD operations on the various resources and entities used throughout Aria Automation Pipelines (Pipelines, Endpoints, Variables, etc.) and allow operations on them (executing a Pipeline, validating an Endpoint connection, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Aria Automation Pipelines entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/codestream/api/endpoints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/codestream/api/endpoints?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/codestream/api/endpoints?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /codestream/api/endpoints?$filter=startswith(name, 'ABC')     /codestream/api/endpoints?$filter=toupper(name) eq 'ABCD-JENKINS'     /codestream/api/endpoints?$filter=substringof(%27bc%27,tolower(name))     /codestream/api/endpoints?$filter=name eq 'ABCD' and project eq 'demo'   # noqa: E501

    OpenAPI spec version: 2019-10-17
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class DockerRegistryWebHookSpec1(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'description': 'str',
        'enabled': 'bool',
        'endpoint': 'str',
        'external_listener_link': 'str',
        'image_name_reg_ex_pattern': 'str',
        'input': 'object',
        'name': 'str',
        'pipeline': 'str',
        'project': 'str',
        'refresh_token': 'str',
        'repo_name': 'str',
        'secret_token': 'str',
        'server_type': 'str',
        'slug': 'str',
        'tag_name_pattern': 'str'
    }

    attribute_map = {
        'description': 'description',
        'enabled': 'enabled',
        'endpoint': 'endpoint',
        'external_listener_link': 'externalListenerLink',
        'image_name_reg_ex_pattern': 'imageNameRegExPattern',
        'input': 'input',
        'name': 'name',
        'pipeline': 'pipeline',
        'project': 'project',
        'refresh_token': 'refreshToken',
        'repo_name': 'repoName',
        'secret_token': 'secretToken',
        'server_type': 'serverType',
        'slug': 'slug',
        'tag_name_pattern': 'tagNamePattern'
    }

    def __init__(self, description=None, enabled=None, endpoint=None, external_listener_link=None, image_name_reg_ex_pattern=None, input=None, name=None, pipeline=None, project=None, refresh_token=None, repo_name=None, secret_token=None, server_type=None, slug=None, tag_name_pattern=None):  # noqa: E501
        """DockerRegistryWebHookSpec1 - a model defined in Swagger"""  # noqa: E501
        self._description = None
        self._enabled = None
        self._endpoint = None
        self._external_listener_link = None
        self._image_name_reg_ex_pattern = None
        self._input = None
        self._name = None
        self._pipeline = None
        self._project = None
        self._refresh_token = None
        self._repo_name = None
        self._secret_token = None
        self._server_type = None
        self._slug = None
        self._tag_name_pattern = None
        self.discriminator = None
        if description is not None:
            self.description = description
        if enabled is not None:
            self.enabled = enabled
        if endpoint is not None:
            self.endpoint = endpoint
        if external_listener_link is not None:
            self.external_listener_link = external_listener_link
        if image_name_reg_ex_pattern is not None:
            self.image_name_reg_ex_pattern = image_name_reg_ex_pattern
        if input is not None:
            self.input = input
        self.name = name
        if pipeline is not None:
            self.pipeline = pipeline
        if project is not None:
            self.project = project
        if refresh_token is not None:
            self.refresh_token = refresh_token
        if repo_name is not None:
            self.repo_name = repo_name
        if secret_token is not None:
            self.secret_token = secret_token
        if server_type is not None:
            self.server_type = server_type
        if slug is not None:
            self.slug = slug
        if tag_name_pattern is not None:
            self.tag_name_pattern = tag_name_pattern

    @property
    def description(self):
        """Gets the description of this DockerRegistryWebHookSpec1.  # noqa: E501

        Docker webhook description.  # noqa: E501

        :return: The description of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this DockerRegistryWebHookSpec1.

        Docker webhook description.  # noqa: E501

        :param description: The description of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def enabled(self):
        """Gets the enabled of this DockerRegistryWebHookSpec1.  # noqa: E501

        Indicates whether Docker webhook is enabled or not.  # noqa: E501

        :return: The enabled of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: bool
        """
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        """Sets the enabled of this DockerRegistryWebHookSpec1.

        Indicates whether Docker webhook is enabled or not.  # noqa: E501

        :param enabled: The enabled of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: bool
        """

        self._enabled = enabled

    @property
    def endpoint(self):
        """Gets the endpoint of this DockerRegistryWebHookSpec1.  # noqa: E501

        Docker endpoint.  # noqa: E501

        :return: The endpoint of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._endpoint

    @endpoint.setter
    def endpoint(self, endpoint):
        """Sets the endpoint of this DockerRegistryWebHookSpec1.

        Docker endpoint.  # noqa: E501

        :param endpoint: The endpoint of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._endpoint = endpoint

    @property
    def external_listener_link(self):
        """Gets the external_listener_link of this DockerRegistryWebHookSpec1.  # noqa: E501

        Docker webhook listener link.  # noqa: E501

        :return: The external_listener_link of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._external_listener_link

    @external_listener_link.setter
    def external_listener_link(self, external_listener_link):
        """Sets the external_listener_link of this DockerRegistryWebHookSpec1.

        Docker webhook listener link.  # noqa: E501

        :param external_listener_link: The external_listener_link of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._external_listener_link = external_listener_link

    @property
    def image_name_reg_ex_pattern(self):
        """Gets the image_name_reg_ex_pattern of this DockerRegistryWebHookSpec1.  # noqa: E501

        If provided then the pipeline execution is triggered only when the given image name regex matches the image name in the received payload.  # noqa: E501

        :return: The image_name_reg_ex_pattern of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._image_name_reg_ex_pattern

    @image_name_reg_ex_pattern.setter
    def image_name_reg_ex_pattern(self, image_name_reg_ex_pattern):
        """Sets the image_name_reg_ex_pattern of this DockerRegistryWebHookSpec1.

        If provided then the pipeline execution is triggered only when the given image name regex matches the image name in the received payload.  # noqa: E501

        :param image_name_reg_ex_pattern: The image_name_reg_ex_pattern of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._image_name_reg_ex_pattern = image_name_reg_ex_pattern

    @property
    def input(self):
        """Gets the input of this DockerRegistryWebHookSpec1.  # noqa: E501

        Pipeline Execution input properties.  # noqa: E501

        :return: The input of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: object
        """
        return self._input

    @input.setter
    def input(self, input):
        """Sets the input of this DockerRegistryWebHookSpec1.

        Pipeline Execution input properties.  # noqa: E501

        :param input: The input of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: object
        """

        self._input = input

    @property
    def name(self):
        """Gets the name of this DockerRegistryWebHookSpec1.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option  # noqa: E501

        :return: The name of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this DockerRegistryWebHookSpec1.

        A human-friendly name used as an identifier in APIs that support this option  # noqa: E501

        :param name: The name of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def pipeline(self):
        """Gets the pipeline of this DockerRegistryWebHookSpec1.  # noqa: E501

        Pipeline name which is meant to be triggered when a docker event occur.  # noqa: E501

        :return: The pipeline of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._pipeline

    @pipeline.setter
    def pipeline(self, pipeline):
        """Sets the pipeline of this DockerRegistryWebHookSpec1.

        Pipeline name which is meant to be triggered when a docker event occur.  # noqa: E501

        :param pipeline: The pipeline of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._pipeline = pipeline

    @property
    def project(self):
        """Gets the project of this DockerRegistryWebHookSpec1.  # noqa: E501

        The project this entity belongs to.  # noqa: E501

        :return: The project of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._project

    @project.setter
    def project(self, project):
        """Sets the project of this DockerRegistryWebHookSpec1.

        The project this entity belongs to.  # noqa: E501

        :param project: The project of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._project = project

    @property
    def refresh_token(self):
        """Gets the refresh_token of this DockerRegistryWebHookSpec1.  # noqa: E501

        Codestream API token.  # noqa: E501

        :return: The refresh_token of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._refresh_token

    @refresh_token.setter
    def refresh_token(self, refresh_token):
        """Sets the refresh_token of this DockerRegistryWebHookSpec1.

        Codestream API token.  # noqa: E501

        :param refresh_token: The refresh_token of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._refresh_token = refresh_token

    @property
    def repo_name(self):
        """Gets the repo_name of this DockerRegistryWebHookSpec1.  # noqa: E501

        Docker Repo Name.  # noqa: E501

        :return: The repo_name of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._repo_name

    @repo_name.setter
    def repo_name(self, repo_name):
        """Sets the repo_name of this DockerRegistryWebHookSpec1.

        Docker Repo Name.  # noqa: E501

        :param repo_name: The repo_name of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._repo_name = repo_name

    @property
    def secret_token(self):
        """Gets the secret_token of this DockerRegistryWebHookSpec1.  # noqa: E501

        Secret token to validate received payloads.  # noqa: E501

        :return: The secret_token of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._secret_token

    @secret_token.setter
    def secret_token(self, secret_token):
        """Sets the secret_token of this DockerRegistryWebHookSpec1.

        Secret token to validate received payloads.  # noqa: E501

        :param secret_token: The secret_token of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._secret_token = secret_token

    @property
    def server_type(self):
        """Gets the server_type of this DockerRegistryWebHookSpec1.  # noqa: E501

        Docker server type.  # noqa: E501

        :return: The server_type of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._server_type

    @server_type.setter
    def server_type(self, server_type):
        """Sets the server_type of this DockerRegistryWebHookSpec1.

        Docker server type.  # noqa: E501

        :param server_type: The server_type of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._server_type = server_type

    @property
    def slug(self):
        """Gets the slug of this DockerRegistryWebHookSpec1.  # noqa: E501

        Docker webhook name.  # noqa: E501

        :return: The slug of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._slug

    @slug.setter
    def slug(self, slug):
        """Sets the slug of this DockerRegistryWebHookSpec1.

        Docker webhook name.  # noqa: E501

        :param slug: The slug of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._slug = slug

    @property
    def tag_name_pattern(self):
        """Gets the tag_name_pattern of this DockerRegistryWebHookSpec1.  # noqa: E501

        If provided then the pipeline execution is triggered only when the given tag name regex matches the tag name(s) in the received payload.  # noqa: E501

        :return: The tag_name_pattern of this DockerRegistryWebHookSpec1.  # noqa: E501
        :rtype: str
        """
        return self._tag_name_pattern

    @tag_name_pattern.setter
    def tag_name_pattern(self, tag_name_pattern):
        """Sets the tag_name_pattern of this DockerRegistryWebHookSpec1.

        If provided then the pipeline execution is triggered only when the given tag name regex matches the tag name(s) in the received payload.  # noqa: E501

        :param tag_name_pattern: The tag_name_pattern of this DockerRegistryWebHookSpec1.  # noqa: E501
        :type: str
        """

        self._tag_name_pattern = tag_name_pattern

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(DockerRegistryWebHookSpec1, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, DockerRegistryWebHookSpec1):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
