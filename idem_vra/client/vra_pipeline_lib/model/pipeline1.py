# coding: utf-8

"""
    VMware Aria Automation Pipelines APIs

    Aria Automation Pipelines is a continuous integration and continuous delivery (CICD) tool that you use to build Pipelines that model the software release process in your DevOps lifecycle. By creating Pipelines, you build the code infrastructure that delivers your software rapidly and continuously.  This page describes the RESTful APIs for Aria Automation Pipelines. The APIs facilitate CRUD operations on the various resources and entities used throughout Aria Automation Pipelines (Pipelines, Endpoints, Variables, etc.) and allow operations on them (executing a Pipeline, validating an Endpoint connection, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Aria Automation Pipelines entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/codestream/api/endpoints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/codestream/api/endpoints?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/codestream/api/endpoints?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /codestream/api/endpoints?$filter=startswith(name, 'ABC')     /codestream/api/endpoints?$filter=toupper(name) eq 'ABCD-JENKINS'     /codestream/api/endpoints?$filter=substringof(%27bc%27,tolower(name))     /codestream/api/endpoints?$filter=name eq 'ABCD' and project eq 'demo'   # noqa: E501

    OpenAPI spec version: 2019-10-17
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Pipeline1(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'create_time_in_micros': 'int',
        'input_meta': 'dict(str, PropertyMetaData1)',
        'link': 'str',
        'project_id': 'str',
        'update_time_in_micros': 'int',
        'warnings': 'list[ValidatorResponse1]',
        'concurrency': 'int',
        'created_at': 'str',
        'created_by': 'str',
        'description': 'str',
        'enabled': 'bool',
        '_global': 'bool',
        'icon': 'str',
        'id': 'str',
        'input': 'object',
        'name': 'str',
        'notifications': 'NotificationConfiguration1',
        'options': 'list[str]',
        'output': 'object',
        'project': 'str',
        'rollbacks': 'list[RollbackConfiguration1]',
        'stage_order': 'list[str]',
        'stages': 'dict(str, Stage1)',
        'starred': 'PipelineStarredProperty',
        'state': 'str',
        'tags': 'list[str]',
        'updated_at': 'str',
        'updated_by': 'str',
        'version': 'str',
        'workspace': 'Workspace1'
    }

    attribute_map = {
        'create_time_in_micros': '_createTimeInMicros',
        'input_meta': '_inputMeta',
        'link': '_link',
        'project_id': '_projectId',
        'update_time_in_micros': '_updateTimeInMicros',
        'warnings': '_warnings',
        'concurrency': 'concurrency',
        'created_at': 'createdAt',
        'created_by': 'createdBy',
        'description': 'description',
        'enabled': 'enabled',
        '_global': 'global',
        'icon': 'icon',
        'id': 'id',
        'input': 'input',
        'name': 'name',
        'notifications': 'notifications',
        'options': 'options',
        'output': 'output',
        'project': 'project',
        'rollbacks': 'rollbacks',
        'stage_order': 'stageOrder',
        'stages': 'stages',
        'starred': 'starred',
        'state': 'state',
        'tags': 'tags',
        'updated_at': 'updatedAt',
        'updated_by': 'updatedBy',
        'version': 'version',
        'workspace': 'workspace'
    }

    def __init__(self, create_time_in_micros=None, input_meta=None, link=None, project_id=None, update_time_in_micros=None, warnings=None, concurrency=None, created_at=None, created_by=None, description=None, enabled=None, _global=None, icon=None, id=None, input=None, name=None, notifications=None, options=None, output=None, project=None, rollbacks=None, stage_order=None, stages=None, starred=None, state=None, tags=None, updated_at=None, updated_by=None, version=None, workspace=None):  # noqa: E501
        """Pipeline1 - a model defined in Swagger"""  # noqa: E501
        self._create_time_in_micros = None
        self._input_meta = None
        self._link = None
        self._project_id = None
        self._update_time_in_micros = None
        self._warnings = None
        self._concurrency = None
        self._created_at = None
        self._created_by = None
        self._description = None
        self._enabled = None
        self.__global = None
        self._icon = None
        self._id = None
        self._input = None
        self._name = None
        self._notifications = None
        self._options = None
        self._output = None
        self._project = None
        self._rollbacks = None
        self._stage_order = None
        self._stages = None
        self._starred = None
        self._state = None
        self._tags = None
        self._updated_at = None
        self._updated_by = None
        self._version = None
        self._workspace = None
        self.discriminator = None
        if create_time_in_micros is not None:
            self.create_time_in_micros = create_time_in_micros
        if input_meta is not None:
            self.input_meta = input_meta
        if link is not None:
            self.link = link
        if project_id is not None:
            self.project_id = project_id
        if update_time_in_micros is not None:
            self.update_time_in_micros = update_time_in_micros
        if warnings is not None:
            self.warnings = warnings
        if concurrency is not None:
            self.concurrency = concurrency
        if created_at is not None:
            self.created_at = created_at
        if created_by is not None:
            self.created_by = created_by
        if description is not None:
            self.description = description
        if enabled is not None:
            self.enabled = enabled
        if _global is not None:
            self._global = _global
        if icon is not None:
            self.icon = icon
        if id is not None:
            self.id = id
        if input is not None:
            self.input = input
        self.name = name
        if notifications is not None:
            self.notifications = notifications
        if options is not None:
            self.options = options
        if output is not None:
            self.output = output
        if project is not None:
            self.project = project
        if rollbacks is not None:
            self.rollbacks = rollbacks
        if stage_order is not None:
            self.stage_order = stage_order
        if stages is not None:
            self.stages = stages
        if starred is not None:
            self.starred = starred
        if state is not None:
            self.state = state
        if tags is not None:
            self.tags = tags
        if updated_at is not None:
            self.updated_at = updated_at
        if updated_by is not None:
            self.updated_by = updated_by
        if version is not None:
            self.version = version
        if workspace is not None:
            self.workspace = workspace

    @property
    def create_time_in_micros(self):
        """Gets the create_time_in_micros of this Pipeline1.  # noqa: E501

        This field is provided for backward compatibility. Contains the same value as the 'createdAt' field as a UNIX timestamp in microseconds  # noqa: E501

        :return: The create_time_in_micros of this Pipeline1.  # noqa: E501
        :rtype: int
        """
        return self._create_time_in_micros

    @create_time_in_micros.setter
    def create_time_in_micros(self, create_time_in_micros):
        """Sets the create_time_in_micros of this Pipeline1.

        This field is provided for backward compatibility. Contains the same value as the 'createdAt' field as a UNIX timestamp in microseconds  # noqa: E501

        :param create_time_in_micros: The create_time_in_micros of this Pipeline1.  # noqa: E501
        :type: int
        """

        self._create_time_in_micros = create_time_in_micros

    @property
    def input_meta(self):
        """Gets the input_meta of this Pipeline1.  # noqa: E501

        Additional information about Input Properties  # noqa: E501

        :return: The input_meta of this Pipeline1.  # noqa: E501
        :rtype: dict(str, PropertyMetaData1)
        """
        return self._input_meta

    @input_meta.setter
    def input_meta(self, input_meta):
        """Sets the input_meta of this Pipeline1.

        Additional information about Input Properties  # noqa: E501

        :param input_meta: The input_meta of this Pipeline1.  # noqa: E501
        :type: dict(str, PropertyMetaData1)
        """

        self._input_meta = input_meta

    @property
    def link(self):
        """Gets the link of this Pipeline1.  # noqa: E501

        Partial URL that provides details of the resource.  # noqa: E501

        :return: The link of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._link

    @link.setter
    def link(self, link):
        """Sets the link of this Pipeline1.

        Partial URL that provides details of the resource.  # noqa: E501

        :param link: The link of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._link = link

    @property
    def project_id(self):
        """Gets the project_id of this Pipeline1.  # noqa: E501

        Contains project id of the entity  # noqa: E501

        :return: The project_id of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this Pipeline1.

        Contains project id of the entity  # noqa: E501

        :param project_id: The project_id of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._project_id = project_id

    @property
    def update_time_in_micros(self):
        """Gets the update_time_in_micros of this Pipeline1.  # noqa: E501

        This field is provided for backward compatibility. Contains the same value as the 'updatedAt' field as a UNIX timestamp in microseconds  # noqa: E501

        :return: The update_time_in_micros of this Pipeline1.  # noqa: E501
        :rtype: int
        """
        return self._update_time_in_micros

    @update_time_in_micros.setter
    def update_time_in_micros(self, update_time_in_micros):
        """Sets the update_time_in_micros of this Pipeline1.

        This field is provided for backward compatibility. Contains the same value as the 'updatedAt' field as a UNIX timestamp in microseconds  # noqa: E501

        :param update_time_in_micros: The update_time_in_micros of this Pipeline1.  # noqa: E501
        :type: int
        """

        self._update_time_in_micros = update_time_in_micros

    @property
    def warnings(self):
        """Gets the warnings of this Pipeline1.  # noqa: E501

        Contains any warnings that result from failed validations of any Pipeline fields  # noqa: E501

        :return: The warnings of this Pipeline1.  # noqa: E501
        :rtype: list[ValidatorResponse1]
        """
        return self._warnings

    @warnings.setter
    def warnings(self, warnings):
        """Sets the warnings of this Pipeline1.

        Contains any warnings that result from failed validations of any Pipeline fields  # noqa: E501

        :param warnings: The warnings of this Pipeline1.  # noqa: E501
        :type: list[ValidatorResponse1]
        """

        self._warnings = warnings

    @property
    def concurrency(self):
        """Gets the concurrency of this Pipeline1.  # noqa: E501

        Number of Executions of the Pipeline that can run concurrently.  # noqa: E501

        :return: The concurrency of this Pipeline1.  # noqa: E501
        :rtype: int
        """
        return self._concurrency

    @concurrency.setter
    def concurrency(self, concurrency):
        """Sets the concurrency of this Pipeline1.

        Number of Executions of the Pipeline that can run concurrently.  # noqa: E501

        :param concurrency: The concurrency of this Pipeline1.  # noqa: E501
        :type: int
        """

        self._concurrency = concurrency

    @property
    def created_at(self):
        """Gets the created_at of this Pipeline1.  # noqa: E501

        Date when the entity was created. The date is in ISO 8601 with time zone  # noqa: E501

        :return: The created_at of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this Pipeline1.

        Date when the entity was created. The date is in ISO 8601 with time zone  # noqa: E501

        :param created_at: The created_at of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def created_by(self):
        """Gets the created_by of this Pipeline1.  # noqa: E501

        The user that created this entity  # noqa: E501

        :return: The created_by of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this Pipeline1.

        The user that created this entity  # noqa: E501

        :param created_by: The created_by of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def description(self):
        """Gets the description of this Pipeline1.  # noqa: E501

        A human-friendly description.  # noqa: E501

        :return: The description of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this Pipeline1.

        A human-friendly description.  # noqa: E501

        :param description: The description of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def enabled(self):
        """Gets the enabled of this Pipeline1.  # noqa: E501

        Indicates if the Pipeline is in enabled state.  # noqa: E501

        :return: The enabled of this Pipeline1.  # noqa: E501
        :rtype: bool
        """
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        """Sets the enabled of this Pipeline1.

        Indicates if the Pipeline is in enabled state.  # noqa: E501

        :param enabled: The enabled of this Pipeline1.  # noqa: E501
        :type: bool
        """

        self._enabled = enabled

    @property
    def _global(self):
        """Gets the _global of this Pipeline1.  # noqa: E501

        Indicates if the pipeline is shared with all projects in an Org.  # noqa: E501

        :return: The _global of this Pipeline1.  # noqa: E501
        :rtype: bool
        """
        return self.__global

    @_global.setter
    def _global(self, _global):
        """Sets the _global of this Pipeline1.

        Indicates if the pipeline is shared with all projects in an Org.  # noqa: E501

        :param _global: The _global of this Pipeline1.  # noqa: E501
        :type: bool
        """

        self.__global = _global

    @property
    def icon(self):
        """Gets the icon of this Pipeline1.  # noqa: E501

        String description of the icon used for this Pipeline.  # noqa: E501

        :return: The icon of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._icon

    @icon.setter
    def icon(self, icon):
        """Sets the icon of this Pipeline1.

        String description of the icon used for this Pipeline.  # noqa: E501

        :param icon: The icon of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._icon = icon

    @property
    def id(self):
        """Gets the id of this Pipeline1.  # noqa: E501

        The id of this resource.  # noqa: E501

        :return: The id of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Pipeline1.

        The id of this resource.  # noqa: E501

        :param id: The id of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def input(self):
        """Gets the input of this Pipeline1.  # noqa: E501

        Map representing the Input properties for the Pipeline.  # noqa: E501

        :return: The input of this Pipeline1.  # noqa: E501
        :rtype: object
        """
        return self._input

    @input.setter
    def input(self, input):
        """Sets the input of this Pipeline1.

        Map representing the Input properties for the Pipeline.  # noqa: E501

        :param input: The input of this Pipeline1.  # noqa: E501
        :type: object
        """

        self._input = input

    @property
    def name(self):
        """Gets the name of this Pipeline1.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option  # noqa: E501

        :return: The name of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Pipeline1.

        A human-friendly name used as an identifier in APIs that support this option  # noqa: E501

        :param name: The name of this Pipeline1.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def notifications(self):
        """Gets the notifications of this Pipeline1.  # noqa: E501


        :return: The notifications of this Pipeline1.  # noqa: E501
        :rtype: NotificationConfiguration1
        """
        return self._notifications

    @notifications.setter
    def notifications(self, notifications):
        """Sets the notifications of this Pipeline1.


        :param notifications: The notifications of this Pipeline1.  # noqa: E501
        :type: NotificationConfiguration1
        """

        self._notifications = notifications

    @property
    def options(self):
        """Gets the options of this Pipeline1.  # noqa: E501

        Represents the different options to trigger a Pipeline. Selecting an option auto injects the Input properties needed to execute a Pipeline with that trigger.  # noqa: E501

        :return: The options of this Pipeline1.  # noqa: E501
        :rtype: list[str]
        """
        return self._options

    @options.setter
    def options(self, options):
        """Sets the options of this Pipeline1.

        Represents the different options to trigger a Pipeline. Selecting an option auto injects the Input properties needed to execute a Pipeline with that trigger.  # noqa: E501

        :param options: The options of this Pipeline1.  # noqa: E501
        :type: list[str]
        """

        self._options = options

    @property
    def output(self):
        """Gets the output of this Pipeline1.  # noqa: E501

        Map representing the Output properties for the Pipeline.  # noqa: E501

        :return: The output of this Pipeline1.  # noqa: E501
        :rtype: object
        """
        return self._output

    @output.setter
    def output(self, output):
        """Sets the output of this Pipeline1.

        Map representing the Output properties for the Pipeline.  # noqa: E501

        :param output: The output of this Pipeline1.  # noqa: E501
        :type: object
        """

        self._output = output

    @property
    def project(self):
        """Gets the project of this Pipeline1.  # noqa: E501

        The project this entity belongs to.  # noqa: E501

        :return: The project of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._project

    @project.setter
    def project(self, project):
        """Sets the project of this Pipeline1.

        The project this entity belongs to.  # noqa: E501

        :param project: The project of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._project = project

    @property
    def rollbacks(self):
        """Gets the rollbacks of this Pipeline1.  # noqa: E501

        Represents the various Rollback Configurations for the Pipeline  # noqa: E501

        :return: The rollbacks of this Pipeline1.  # noqa: E501
        :rtype: list[RollbackConfiguration1]
        """
        return self._rollbacks

    @rollbacks.setter
    def rollbacks(self, rollbacks):
        """Sets the rollbacks of this Pipeline1.

        Represents the various Rollback Configurations for the Pipeline  # noqa: E501

        :param rollbacks: The rollbacks of this Pipeline1.  # noqa: E501
        :type: list[RollbackConfiguration1]
        """

        self._rollbacks = rollbacks

    @property
    def stage_order(self):
        """Gets the stage_order of this Pipeline1.  # noqa: E501

        Represents the order in which Stages will be executed.  # noqa: E501

        :return: The stage_order of this Pipeline1.  # noqa: E501
        :rtype: list[str]
        """
        return self._stage_order

    @stage_order.setter
    def stage_order(self, stage_order):
        """Sets the stage_order of this Pipeline1.

        Represents the order in which Stages will be executed.  # noqa: E501

        :param stage_order: The stage_order of this Pipeline1.  # noqa: E501
        :type: list[str]
        """

        self._stage_order = stage_order

    @property
    def stages(self):
        """Gets the stages of this Pipeline1.  # noqa: E501

        Map representing the details of the various Stages of the Pipeline.  # noqa: E501

        :return: The stages of this Pipeline1.  # noqa: E501
        :rtype: dict(str, Stage1)
        """
        return self._stages

    @stages.setter
    def stages(self, stages):
        """Sets the stages of this Pipeline1.

        Map representing the details of the various Stages of the Pipeline.  # noqa: E501

        :param stages: The stages of this Pipeline1.  # noqa: E501
        :type: dict(str, Stage1)
        """

        self._stages = stages

    @property
    def starred(self):
        """Gets the starred of this Pipeline1.  # noqa: E501


        :return: The starred of this Pipeline1.  # noqa: E501
        :rtype: PipelineStarredProperty
        """
        return self._starred

    @starred.setter
    def starred(self, starred):
        """Sets the starred of this Pipeline1.


        :param starred: The starred of this Pipeline1.  # noqa: E501
        :type: PipelineStarredProperty
        """

        self._starred = starred

    @property
    def state(self):
        """Gets the state of this Pipeline1.  # noqa: E501

        Indicates if the Pipeline is enabled/disabled/released to catalog.  # noqa: E501

        :return: The state of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """Sets the state of this Pipeline1.

        Indicates if the Pipeline is enabled/disabled/released to catalog.  # noqa: E501

        :param state: The state of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._state = state

    @property
    def tags(self):
        """Gets the tags of this Pipeline1.  # noqa: E501

        A set of tag keys and optional values that were set on on the resource.  # noqa: E501

        :return: The tags of this Pipeline1.  # noqa: E501
        :rtype: list[str]
        """
        return self._tags

    @tags.setter
    def tags(self, tags):
        """Sets the tags of this Pipeline1.

        A set of tag keys and optional values that were set on on the resource.  # noqa: E501

        :param tags: The tags of this Pipeline1.  # noqa: E501
        :type: list[str]
        """

        self._tags = tags

    @property
    def updated_at(self):
        """Gets the updated_at of this Pipeline1.  # noqa: E501

        Date when the entity was last updated. The date is in ISO 8601 with time zone.  # noqa: E501

        :return: The updated_at of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this Pipeline1.

        Date when the entity was last updated. The date is in ISO 8601 with time zone.  # noqa: E501

        :param updated_at: The updated_at of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._updated_at = updated_at

    @property
    def updated_by(self):
        """Gets the updated_by of this Pipeline1.  # noqa: E501

        The user that last updated this entity  # noqa: E501

        :return: The updated_by of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._updated_by

    @updated_by.setter
    def updated_by(self, updated_by):
        """Sets the updated_by of this Pipeline1.

        The user that last updated this entity  # noqa: E501

        :param updated_by: The updated_by of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._updated_by = updated_by

    @property
    def version(self):
        """Gets the version of this Pipeline1.  # noqa: E501

        Version of the resource.  # noqa: E501

        :return: The version of this Pipeline1.  # noqa: E501
        :rtype: str
        """
        return self._version

    @version.setter
    def version(self, version):
        """Sets the version of this Pipeline1.

        Version of the resource.  # noqa: E501

        :param version: The version of this Pipeline1.  # noqa: E501
        :type: str
        """

        self._version = version

    @property
    def workspace(self):
        """Gets the workspace of this Pipeline1.  # noqa: E501


        :return: The workspace of this Pipeline1.  # noqa: E501
        :rtype: Workspace1
        """
        return self._workspace

    @workspace.setter
    def workspace(self, workspace):
        """Sets the workspace of this Pipeline1.


        :param workspace: The workspace of this Pipeline1.  # noqa: E501
        :type: Workspace1
        """

        self._workspace = workspace

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Pipeline1, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Pipeline1):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
