# coding: utf-8

"""
    VMware Aria Automation Pipelines APIs

    Aria Automation Pipelines is a continuous integration and continuous delivery (CICD) tool that you use to build Pipelines that model the software release process in your DevOps lifecycle. By creating Pipelines, you build the code infrastructure that delivers your software rapidly and continuously.  This page describes the RESTful APIs for Aria Automation Pipelines. The APIs facilitate CRUD operations on the various resources and entities used throughout Aria Automation Pipelines (Pipelines, Endpoints, Variables, etc.) and allow operations on them (executing a Pipeline, validating an Endpoint connection, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Aria Automation Pipelines entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/codestream/api/endpoints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/codestream/api/endpoints?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/codestream/api/endpoints?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /codestream/api/endpoints?$filter=startswith(name, 'ABC')     /codestream/api/endpoints?$filter=toupper(name) eq 'ABCD-JENKINS'     /codestream/api/endpoints?$filter=substringof(%27bc%27,tolower(name))     /codestream/api/endpoints?$filter=name eq 'ABCD' and project eq 'demo'   # noqa: E501

    OpenAPI spec version: 2019-10-17
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class GerritListenerSpec3(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'api_token': 'str',
        'connected': 'bool',
        'description': 'str',
        'endpoint': 'str',
        'name': 'str',
        'project': 'str'
    }

    attribute_map = {
        'api_token': 'apiToken',
        'connected': 'connected',
        'description': 'description',
        'endpoint': 'endpoint',
        'name': 'name',
        'project': 'project'
    }

    def __init__(self, api_token=None, connected=None, description=None, endpoint=None, name=None, project=None):  # noqa: E501
        """GerritListenerSpec3 - a model defined in Swagger"""  # noqa: E501
        self._api_token = None
        self._connected = None
        self._description = None
        self._endpoint = None
        self._name = None
        self._project = None
        self.discriminator = None
        self.api_token = api_token
        if connected is not None:
            self.connected = connected
        self.description = description
        self.endpoint = endpoint
        self.name = name
        if project is not None:
            self.project = project

    @property
    def api_token(self):
        """Gets the api_token of this GerritListenerSpec3.  # noqa: E501

        This token is used to authenticate when calling VMware Cloud Services APIs. These tokens are scoped within the organization.  # noqa: E501

        :return: The api_token of this GerritListenerSpec3.  # noqa: E501
        :rtype: str
        """
        return self._api_token

    @api_token.setter
    def api_token(self, api_token):
        """Sets the api_token of this GerritListenerSpec3.

        This token is used to authenticate when calling VMware Cloud Services APIs. These tokens are scoped within the organization.  # noqa: E501

        :param api_token: The api_token of this GerritListenerSpec3.  # noqa: E501
        :type: str
        """
        if api_token is None:
            raise ValueError("Invalid value for `api_token`, must not be `None`")  # noqa: E501

        self._api_token = api_token

    @property
    def connected(self):
        """Gets the connected of this GerritListenerSpec3.  # noqa: E501

        Indicates whether the connection with the Gerrit Server to start receiving events is created or not.  # noqa: E501

        :return: The connected of this GerritListenerSpec3.  # noqa: E501
        :rtype: bool
        """
        return self._connected

    @connected.setter
    def connected(self, connected):
        """Sets the connected of this GerritListenerSpec3.

        Indicates whether the connection with the Gerrit Server to start receiving events is created or not.  # noqa: E501

        :param connected: The connected of this GerritListenerSpec3.  # noqa: E501
        :type: bool
        """

        self._connected = connected

    @property
    def description(self):
        """Gets the description of this GerritListenerSpec3.  # noqa: E501

        A human-friendly description.  # noqa: E501

        :return: The description of this GerritListenerSpec3.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this GerritListenerSpec3.

        A human-friendly description.  # noqa: E501

        :param description: The description of this GerritListenerSpec3.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def endpoint(self):
        """Gets the endpoint of this GerritListenerSpec3.  # noqa: E501

        The name of the Gerrit Endpoint.  # noqa: E501

        :return: The endpoint of this GerritListenerSpec3.  # noqa: E501
        :rtype: str
        """
        return self._endpoint

    @endpoint.setter
    def endpoint(self, endpoint):
        """Sets the endpoint of this GerritListenerSpec3.

        The name of the Gerrit Endpoint.  # noqa: E501

        :param endpoint: The endpoint of this GerritListenerSpec3.  # noqa: E501
        :type: str
        """
        if endpoint is None:
            raise ValueError("Invalid value for `endpoint`, must not be `None`")  # noqa: E501

        self._endpoint = endpoint

    @property
    def name(self):
        """Gets the name of this GerritListenerSpec3.  # noqa: E501

        A human-friendly name used as an identifier in APIs that support this option  # noqa: E501

        :return: The name of this GerritListenerSpec3.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this GerritListenerSpec3.

        A human-friendly name used as an identifier in APIs that support this option  # noqa: E501

        :param name: The name of this GerritListenerSpec3.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def project(self):
        """Gets the project of this GerritListenerSpec3.  # noqa: E501

        The project this entity belongs to.  # noqa: E501

        :return: The project of this GerritListenerSpec3.  # noqa: E501
        :rtype: str
        """
        return self._project

    @project.setter
    def project(self, project):
        """Sets the project of this GerritListenerSpec3.

        The project this entity belongs to.  # noqa: E501

        :param project: The project of this GerritListenerSpec3.  # noqa: E501
        :type: str
        """

        self._project = project

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(GerritListenerSpec3, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, GerritListenerSpec3):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
