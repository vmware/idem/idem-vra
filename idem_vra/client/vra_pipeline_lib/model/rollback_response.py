# coding: utf-8

"""
    VMware Aria Automation Pipelines APIs

    Aria Automation Pipelines is a continuous integration and continuous delivery (CICD) tool that you use to build Pipelines that model the software release process in your DevOps lifecycle. By creating Pipelines, you build the code infrastructure that delivers your software rapidly and continuously.  This page describes the RESTful APIs for Aria Automation Pipelines. The APIs facilitate CRUD operations on the various resources and entities used throughout Aria Automation Pipelines (Pipelines, Endpoints, Variables, etc.) and allow operations on them (executing a Pipeline, validating an Endpoint connection, etc.).  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across Aria Automation Pipelines entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/codestream/api/endpoints?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/codestream/api/endpoints?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/codestream/api/endpoints?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /codestream/api/endpoints?$filter=startswith(name, 'ABC')     /codestream/api/endpoints?$filter=toupper(name) eq 'ABCD-JENKINS'     /codestream/api/endpoints?$filter=substringof(%27bc%27,tolower(name))     /codestream/api/endpoints?$filter=name eq 'ABCD' and project eq 'demo'   # noqa: E501

    OpenAPI spec version: 2019-10-17
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class RollbackResponse(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'link': 'str',
        'index': 'int',
        'name': 'str',
        'output': 'object',
        'status': 'str',
        'status_message': 'str'
    }

    attribute_map = {
        'link': '_link',
        'index': 'index',
        'name': 'name',
        'output': 'output',
        'status': 'status',
        'status_message': 'statusMessage'
    }

    def __init__(self, link=None, index=None, name=None, output=None, status=None, status_message=None):  # noqa: E501
        """RollbackResponse - a model defined in Swagger"""  # noqa: E501
        self._link = None
        self._index = None
        self._name = None
        self._output = None
        self._status = None
        self._status_message = None
        self.discriminator = None
        if link is not None:
            self.link = link
        if index is not None:
            self.index = index
        if name is not None:
            self.name = name
        if output is not None:
            self.output = output
        if status is not None:
            self.status = status
        if status_message is not None:
            self.status_message = status_message

    @property
    def link(self):
        """Gets the link of this RollbackResponse.  # noqa: E501

        Execution link of the rollback Pipeline.  # noqa: E501

        :return: The link of this RollbackResponse.  # noqa: E501
        :rtype: str
        """
        return self._link

    @link.setter
    def link(self, link):
        """Sets the link of this RollbackResponse.

        Execution link of the rollback Pipeline.  # noqa: E501

        :param link: The link of this RollbackResponse.  # noqa: E501
        :type: str
        """

        self._link = link

    @property
    def index(self):
        """Gets the index of this RollbackResponse.  # noqa: E501

        Execution index of the rollback Pipeline.  # noqa: E501

        :return: The index of this RollbackResponse.  # noqa: E501
        :rtype: int
        """
        return self._index

    @index.setter
    def index(self, index):
        """Sets the index of this RollbackResponse.

        Execution index of the rollback Pipeline.  # noqa: E501

        :param index: The index of this RollbackResponse.  # noqa: E501
        :type: int
        """

        self._index = index

    @property
    def name(self):
        """Gets the name of this RollbackResponse.  # noqa: E501

        Name of the rollback Pipeline.  # noqa: E501

        :return: The name of this RollbackResponse.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this RollbackResponse.

        Name of the rollback Pipeline.  # noqa: E501

        :param name: The name of this RollbackResponse.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def output(self):
        """Gets the output of this RollbackResponse.  # noqa: E501

        Output properties of a rollback Pipeline.  # noqa: E501

        :return: The output of this RollbackResponse.  # noqa: E501
        :rtype: object
        """
        return self._output

    @output.setter
    def output(self, output):
        """Sets the output of this RollbackResponse.

        Output properties of a rollback Pipeline.  # noqa: E501

        :param output: The output of this RollbackResponse.  # noqa: E501
        :type: object
        """

        self._output = output

    @property
    def status(self):
        """Gets the status of this RollbackResponse.  # noqa: E501

        Execution status of a rollback Pipeline.  # noqa: E501

        :return: The status of this RollbackResponse.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this RollbackResponse.

        Execution status of a rollback Pipeline.  # noqa: E501

        :param status: The status of this RollbackResponse.  # noqa: E501
        :type: str
        """
        allowed_values = ["NOT_STARTED", "STARTED", "RUNNING", "CANCELING", "WAITING", "RESUMING", "PAUSING", "PAUSED", "CANCELED", "COMPLETED", "FAILED", "SKIPPED", "QUEUED", "FAILED_CONTINUE", "ROLLING_BACK", "ROLLBACK_FAILED", "PREPARING_WORKSPACE", "ROLLBACK_COMPLETED"]  # noqa: E501
        if status not in allowed_values:
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def status_message(self):
        """Gets the status_message of this RollbackResponse.  # noqa: E501

        Execution status message of a rollback Pipeline.  # noqa: E501

        :return: The status_message of this RollbackResponse.  # noqa: E501
        :rtype: str
        """
        return self._status_message

    @status_message.setter
    def status_message(self, status_message):
        """Sets the status_message of this RollbackResponse.

        Execution status message of a rollback Pipeline.  # noqa: E501

        :param status_message: The status_message of this RollbackResponse.  # noqa: E501
        :type: str
        """

        self._status_message = status_message

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(RollbackResponse, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RollbackResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
