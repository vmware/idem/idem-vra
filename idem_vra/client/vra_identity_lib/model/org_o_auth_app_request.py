# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class OrgOAuthAppRequest(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'secret': 'str',
        'display_name': 'str',
        'description': 'str',
        'redirect_uris': 'list[str]',
        'grant_types': 'list[str]',
        'access_token_ttl': 'int',
        'refresh_token_ttl': 'int',
        'max_groups_in_id_token': 'int',
        'allowed_scopes': 'CspgatewayamapiorgsorgIdoauthappsAllowedScopes'
    }

    attribute_map = {
        'id': 'id',
        'secret': 'secret',
        'display_name': 'displayName',
        'description': 'description',
        'redirect_uris': 'redirectUris',
        'grant_types': 'grantTypes',
        'access_token_ttl': 'accessTokenTTL',
        'refresh_token_ttl': 'refreshTokenTTL',
        'max_groups_in_id_token': 'maxGroupsInIdToken',
        'allowed_scopes': 'allowedScopes'
    }

    def __init__(self, id=None, secret=None, display_name=None, description=None, redirect_uris=None, grant_types=None, access_token_ttl=None, refresh_token_ttl=None, max_groups_in_id_token=None, allowed_scopes=None):  # noqa: E501
        """OrgOAuthAppRequest - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._secret = None
        self._display_name = None
        self._description = None
        self._redirect_uris = None
        self._grant_types = None
        self._access_token_ttl = None
        self._refresh_token_ttl = None
        self._max_groups_in_id_token = None
        self._allowed_scopes = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if secret is not None:
            self.secret = secret
        if display_name is not None:
            self.display_name = display_name
        if description is not None:
            self.description = description
        if redirect_uris is not None:
            self.redirect_uris = redirect_uris
        if grant_types is not None:
            self.grant_types = grant_types
        if access_token_ttl is not None:
            self.access_token_ttl = access_token_ttl
        if refresh_token_ttl is not None:
            self.refresh_token_ttl = refresh_token_ttl
        if max_groups_in_id_token is not None:
            self.max_groups_in_id_token = max_groups_in_id_token
        if allowed_scopes is not None:
            self.allowed_scopes = allowed_scopes

    @property
    def id(self):
        """Gets the id of this OrgOAuthAppRequest.  # noqa: E501

        The client ID. If not set one will be generated.  # noqa: E501

        :return: The id of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this OrgOAuthAppRequest.

        The client ID. If not set one will be generated.  # noqa: E501

        :param id: The id of this OrgOAuthAppRequest.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def secret(self):
        """Gets the secret of this OrgOAuthAppRequest.  # noqa: E501

        The client secret. If not set one will be generated.  # noqa: E501

        :return: The secret of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: str
        """
        return self._secret

    @secret.setter
    def secret(self, secret):
        """Sets the secret of this OrgOAuthAppRequest.

        The client secret. If not set one will be generated.  # noqa: E501

        :param secret: The secret of this OrgOAuthAppRequest.  # noqa: E501
        :type: str
        """

        self._secret = secret

    @property
    def display_name(self):
        """Gets the display_name of this OrgOAuthAppRequest.  # noqa: E501

        Display name for the client.  # noqa: E501

        :return: The display_name of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: str
        """
        return self._display_name

    @display_name.setter
    def display_name(self, display_name):
        """Sets the display_name of this OrgOAuthAppRequest.

        Display name for the client.  # noqa: E501

        :param display_name: The display_name of this OrgOAuthAppRequest.  # noqa: E501
        :type: str
        """

        self._display_name = display_name

    @property
    def description(self):
        """Gets the description of this OrgOAuthAppRequest.  # noqa: E501

        Description for the client.  # noqa: E501

        :return: The description of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this OrgOAuthAppRequest.

        Description for the client.  # noqa: E501

        :param description: The description of this OrgOAuthAppRequest.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def redirect_uris(self):
        """Gets the redirect_uris of this OrgOAuthAppRequest.  # noqa: E501

        Redirect URIs for the client. Only the first one will be taken into account by the Identity Service.  # noqa: E501

        :return: The redirect_uris of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: list[str]
        """
        return self._redirect_uris

    @redirect_uris.setter
    def redirect_uris(self, redirect_uris):
        """Sets the redirect_uris of this OrgOAuthAppRequest.

        Redirect URIs for the client. Only the first one will be taken into account by the Identity Service.  # noqa: E501

        :param redirect_uris: The redirect_uris of this OrgOAuthAppRequest.  # noqa: E501
        :type: list[str]
        """

        self._redirect_uris = redirect_uris

    @property
    def grant_types(self):
        """Gets the grant_types of this OrgOAuthAppRequest.  # noqa: E501

        Client grant types.  # noqa: E501

        :return: The grant_types of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: list[str]
        """
        return self._grant_types

    @grant_types.setter
    def grant_types(self, grant_types):
        """Sets the grant_types of this OrgOAuthAppRequest.

        Client grant types.  # noqa: E501

        :param grant_types: The grant_types of this OrgOAuthAppRequest.  # noqa: E501
        :type: list[str]
        """

        self._grant_types = grant_types

    @property
    def access_token_ttl(self):
        """Gets the access_token_ttl of this OrgOAuthAppRequest.  # noqa: E501

        Time to live for the access token, generated for this client, in seconds. Defaults to 0 if not set, i.e. the token will be issued already expired.  # noqa: E501

        :return: The access_token_ttl of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: int
        """
        return self._access_token_ttl

    @access_token_ttl.setter
    def access_token_ttl(self, access_token_ttl):
        """Sets the access_token_ttl of this OrgOAuthAppRequest.

        Time to live for the access token, generated for this client, in seconds. Defaults to 0 if not set, i.e. the token will be issued already expired.  # noqa: E501

        :param access_token_ttl: The access_token_ttl of this OrgOAuthAppRequest.  # noqa: E501
        :type: int
        """

        self._access_token_ttl = access_token_ttl

    @property
    def refresh_token_ttl(self):
        """Gets the refresh_token_ttl of this OrgOAuthAppRequest.  # noqa: E501

        Time to live for the refresh token, generated for this client, in seconds. Defaults to 0 if not set, i.e. the token will be issued already expired.  # noqa: E501

        :return: The refresh_token_ttl of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: int
        """
        return self._refresh_token_ttl

    @refresh_token_ttl.setter
    def refresh_token_ttl(self, refresh_token_ttl):
        """Sets the refresh_token_ttl of this OrgOAuthAppRequest.

        Time to live for the refresh token, generated for this client, in seconds. Defaults to 0 if not set, i.e. the token will be issued already expired.  # noqa: E501

        :param refresh_token_ttl: The refresh_token_ttl of this OrgOAuthAppRequest.  # noqa: E501
        :type: int
        """

        self._refresh_token_ttl = refresh_token_ttl

    @property
    def max_groups_in_id_token(self):
        """Gets the max_groups_in_id_token of this OrgOAuthAppRequest.  # noqa: E501

        For CSP compatibility, ignored by the Identity Service. Note that the value will be persisted and may become effective in the future releases.  # noqa: E501

        :return: The max_groups_in_id_token of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: int
        """
        return self._max_groups_in_id_token

    @max_groups_in_id_token.setter
    def max_groups_in_id_token(self, max_groups_in_id_token):
        """Sets the max_groups_in_id_token of this OrgOAuthAppRequest.

        For CSP compatibility, ignored by the Identity Service. Note that the value will be persisted and may become effective in the future releases.  # noqa: E501

        :param max_groups_in_id_token: The max_groups_in_id_token of this OrgOAuthAppRequest.  # noqa: E501
        :type: int
        """

        self._max_groups_in_id_token = max_groups_in_id_token

    @property
    def allowed_scopes(self):
        """Gets the allowed_scopes of this OrgOAuthAppRequest.  # noqa: E501


        :return: The allowed_scopes of this OrgOAuthAppRequest.  # noqa: E501
        :rtype: CspgatewayamapiorgsorgIdoauthappsAllowedScopes
        """
        return self._allowed_scopes

    @allowed_scopes.setter
    def allowed_scopes(self, allowed_scopes):
        """Sets the allowed_scopes of this OrgOAuthAppRequest.


        :param allowed_scopes: The allowed_scopes of this OrgOAuthAppRequest.  # noqa: E501
        :type: CspgatewayamapiorgsorgIdoauthappsAllowedScopes
        """

        self._allowed_scopes = allowed_scopes

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(OrgOAuthAppRequest, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, OrgOAuthAppRequest):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
