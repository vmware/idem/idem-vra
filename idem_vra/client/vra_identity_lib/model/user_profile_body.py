# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class UserProfileBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'default_org_id': 'str',
        'locale': 'str',
        'language': 'str',
        'preferred_theme': 'str',
        'metadata': 'CspgatewayamapiloggedinuserprofileMetadata'
    }

    attribute_map = {
        'default_org_id': 'defaultOrgId',
        'locale': 'locale',
        'language': 'language',
        'preferred_theme': 'preferredTheme',
        'metadata': 'metadata'
    }

    def __init__(self, default_org_id=None, locale=None, language=None, preferred_theme=None, metadata=None):  # noqa: E501
        """UserProfileBody - a model defined in Swagger"""  # noqa: E501
        self._default_org_id = None
        self._locale = None
        self._language = None
        self._preferred_theme = None
        self._metadata = None
        self.discriminator = None
        if default_org_id is not None:
            self.default_org_id = default_org_id
        if locale is not None:
            self.locale = locale
        if language is not None:
            self.language = language
        if preferred_theme is not None:
            self.preferred_theme = preferred_theme
        if metadata is not None:
            self.metadata = metadata

    @property
    def default_org_id(self):
        """Gets the default_org_id of this UserProfileBody.  # noqa: E501

        Currently one user can belong to exactly one organization.  # noqa: E501

        :return: The default_org_id of this UserProfileBody.  # noqa: E501
        :rtype: str
        """
        return self._default_org_id

    @default_org_id.setter
    def default_org_id(self, default_org_id):
        """Sets the default_org_id of this UserProfileBody.

        Currently one user can belong to exactly one organization.  # noqa: E501

        :param default_org_id: The default_org_id of this UserProfileBody.  # noqa: E501
        :type: str
        """

        self._default_org_id = default_org_id

    @property
    def locale(self):
        """Gets the locale of this UserProfileBody.  # noqa: E501

        Preferred user locale.  # noqa: E501

        :return: The locale of this UserProfileBody.  # noqa: E501
        :rtype: str
        """
        return self._locale

    @locale.setter
    def locale(self, locale):
        """Sets the locale of this UserProfileBody.

        Preferred user locale.  # noqa: E501

        :param locale: The locale of this UserProfileBody.  # noqa: E501
        :type: str
        """

        self._locale = locale

    @property
    def language(self):
        """Gets the language of this UserProfileBody.  # noqa: E501

        Preferred user language.  # noqa: E501

        :return: The language of this UserProfileBody.  # noqa: E501
        :rtype: str
        """
        return self._language

    @language.setter
    def language(self, language):
        """Sets the language of this UserProfileBody.

        Preferred user language.  # noqa: E501

        :param language: The language of this UserProfileBody.  # noqa: E501
        :type: str
        """

        self._language = language

    @property
    def preferred_theme(self):
        """Gets the preferred_theme of this UserProfileBody.  # noqa: E501

        Preferred user theme.  # noqa: E501

        :return: The preferred_theme of this UserProfileBody.  # noqa: E501
        :rtype: str
        """
        return self._preferred_theme

    @preferred_theme.setter
    def preferred_theme(self, preferred_theme):
        """Sets the preferred_theme of this UserProfileBody.

        Preferred user theme.  # noqa: E501

        :param preferred_theme: The preferred_theme of this UserProfileBody.  # noqa: E501
        :type: str
        """

        self._preferred_theme = preferred_theme

    @property
    def metadata(self):
        """Gets the metadata of this UserProfileBody.  # noqa: E501


        :return: The metadata of this UserProfileBody.  # noqa: E501
        :rtype: CspgatewayamapiloggedinuserprofileMetadata
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this UserProfileBody.


        :param metadata: The metadata of this UserProfileBody.  # noqa: E501
        :type: CspgatewayamapiloggedinuserprofileMetadata
        """

        self._metadata = metadata

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(UserProfileBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, UserProfileBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
