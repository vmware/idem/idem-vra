# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class OrgIdRolesBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'organization_roles': 'Cspgatewayamapiv3usersuserIdorgsorgIdrolesOrganizationRoles',
        'service_roles': 'list[Cspgatewayamapiv3usersuserIdorgsorgIdrolesServiceRoles]'
    }

    attribute_map = {
        'organization_roles': 'organizationRoles',
        'service_roles': 'serviceRoles'
    }

    def __init__(self, organization_roles=None, service_roles=None):  # noqa: E501
        """OrgIdRolesBody - a model defined in Swagger"""  # noqa: E501
        self._organization_roles = None
        self._service_roles = None
        self.discriminator = None
        if organization_roles is not None:
            self.organization_roles = organization_roles
        if service_roles is not None:
            self.service_roles = service_roles

    @property
    def organization_roles(self):
        """Gets the organization_roles of this OrgIdRolesBody.  # noqa: E501


        :return: The organization_roles of this OrgIdRolesBody.  # noqa: E501
        :rtype: Cspgatewayamapiv3usersuserIdorgsorgIdrolesOrganizationRoles
        """
        return self._organization_roles

    @organization_roles.setter
    def organization_roles(self, organization_roles):
        """Sets the organization_roles of this OrgIdRolesBody.


        :param organization_roles: The organization_roles of this OrgIdRolesBody.  # noqa: E501
        :type: Cspgatewayamapiv3usersuserIdorgsorgIdrolesOrganizationRoles
        """

        self._organization_roles = organization_roles

    @property
    def service_roles(self):
        """Gets the service_roles of this OrgIdRolesBody.  # noqa: E501


        :return: The service_roles of this OrgIdRolesBody.  # noqa: E501
        :rtype: list[Cspgatewayamapiv3usersuserIdorgsorgIdrolesServiceRoles]
        """
        return self._service_roles

    @service_roles.setter
    def service_roles(self, service_roles):
        """Sets the service_roles of this OrgIdRolesBody.


        :param service_roles: The service_roles of this OrgIdRolesBody.  # noqa: E501
        :type: list[Cspgatewayamapiv3usersuserIdorgsorgIdrolesServiceRoles]
        """

        self._service_roles = service_roles

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(OrgIdRolesBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, OrgIdRolesBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
