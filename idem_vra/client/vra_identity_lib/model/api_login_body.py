# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ApiLoginBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'username': 'str',
        'password': 'str',
        'domain': 'str',
        'scope': 'str'
    }

    attribute_map = {
        'username': 'username',
        'password': 'password',
        'domain': 'domain',
        'scope': 'scope'
    }

    def __init__(self, username=None, password=None, domain=None, scope=None):  # noqa: E501
        """ApiLoginBody - a model defined in Swagger"""  # noqa: E501
        self._username = None
        self._password = None
        self._domain = None
        self._scope = None
        self.discriminator = None
        self.username = username
        self.password = password
        if domain is not None:
            self.domain = domain
        if scope is not None:
            self.scope = scope

    @property
    def username(self):
        """Gets the username of this ApiLoginBody.  # noqa: E501

        The username.  # noqa: E501

        :return: The username of this ApiLoginBody.  # noqa: E501
        :rtype: str
        """
        return self._username

    @username.setter
    def username(self, username):
        """Sets the username of this ApiLoginBody.

        The username.  # noqa: E501

        :param username: The username of this ApiLoginBody.  # noqa: E501
        :type: str
        """
        if username is None:
            raise ValueError("Invalid value for `username`, must not be `None`")  # noqa: E501

        self._username = username

    @property
    def password(self):
        """Gets the password of this ApiLoginBody.  # noqa: E501

        The password.  # noqa: E501

        :return: The password of this ApiLoginBody.  # noqa: E501
        :rtype: str
        """
        return self._password

    @password.setter
    def password(self, password):
        """Sets the password of this ApiLoginBody.

        The password.  # noqa: E501

        :param password: The password of this ApiLoginBody.  # noqa: E501
        :type: str
        """
        if password is None:
            raise ValueError("Invalid value for `password`, must not be `None`")  # noqa: E501

        self._password = password

    @property
    def domain(self):
        """Gets the domain of this ApiLoginBody.  # noqa: E501

        The user's domain.  # noqa: E501

        :return: The domain of this ApiLoginBody.  # noqa: E501
        :rtype: str
        """
        return self._domain

    @domain.setter
    def domain(self, domain):
        """Sets the domain of this ApiLoginBody.

        The user's domain.  # noqa: E501

        :param domain: The domain of this ApiLoginBody.  # noqa: E501
        :type: str
        """

        self._domain = domain

    @property
    def scope(self):
        """Gets the scope of this ApiLoginBody.  # noqa: E501

        Scope of the issued token.  # noqa: E501

        :return: The scope of this ApiLoginBody.  # noqa: E501
        :rtype: str
        """
        return self._scope

    @scope.setter
    def scope(self, scope):
        """Sets the scope of this ApiLoginBody.

        Scope of the issued token.  # noqa: E501

        :param scope: The scope of this ApiLoginBody.  # noqa: E501
        :type: str
        """

        self._scope = scope

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ApiLoginBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ApiLoginBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
