# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse20036(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'issuer': 'str',
        'authorization_endpoint': 'str',
        'token_endpoint': 'str',
        'jwks_uri': 'str',
        'subject_types_supported': 'list[str]',
        'response_types_supported': 'list[str]',
        'id_token_signing_alg_values_supported': 'list[str]',
        'token_endpoint_auth_methods_supported': 'list[str]',
        'claims_supported': 'list[str]',
        'scopes_supported': 'list[str]',
        'code_challenge_methods_supported': 'list[str]',
        'userinfo_endpoint': 'str',
        'end_session_endpoint': 'str'
    }

    attribute_map = {
        'issuer': 'issuer',
        'authorization_endpoint': 'authorization_endpoint',
        'token_endpoint': 'token_endpoint',
        'jwks_uri': 'jwks_uri',
        'subject_types_supported': 'subject_types_supported',
        'response_types_supported': 'response_types_supported',
        'id_token_signing_alg_values_supported': 'id_token_signing_alg_values_supported',
        'token_endpoint_auth_methods_supported': 'token_endpoint_auth_methods_supported',
        'claims_supported': 'claims_supported',
        'scopes_supported': 'scopes_supported',
        'code_challenge_methods_supported': 'code_challenge_methods_supported',
        'userinfo_endpoint': 'userinfo_endpoint',
        'end_session_endpoint': 'end_session_endpoint'
    }

    def __init__(self, issuer=None, authorization_endpoint=None, token_endpoint=None, jwks_uri=None, subject_types_supported=None, response_types_supported=None, id_token_signing_alg_values_supported=None, token_endpoint_auth_methods_supported=None, claims_supported=None, scopes_supported=None, code_challenge_methods_supported=None, userinfo_endpoint=None, end_session_endpoint=None):  # noqa: E501
        """InlineResponse20036 - a model defined in Swagger"""  # noqa: E501
        self._issuer = None
        self._authorization_endpoint = None
        self._token_endpoint = None
        self._jwks_uri = None
        self._subject_types_supported = None
        self._response_types_supported = None
        self._id_token_signing_alg_values_supported = None
        self._token_endpoint_auth_methods_supported = None
        self._claims_supported = None
        self._scopes_supported = None
        self._code_challenge_methods_supported = None
        self._userinfo_endpoint = None
        self._end_session_endpoint = None
        self.discriminator = None
        if issuer is not None:
            self.issuer = issuer
        if authorization_endpoint is not None:
            self.authorization_endpoint = authorization_endpoint
        if token_endpoint is not None:
            self.token_endpoint = token_endpoint
        if jwks_uri is not None:
            self.jwks_uri = jwks_uri
        if subject_types_supported is not None:
            self.subject_types_supported = subject_types_supported
        if response_types_supported is not None:
            self.response_types_supported = response_types_supported
        if id_token_signing_alg_values_supported is not None:
            self.id_token_signing_alg_values_supported = id_token_signing_alg_values_supported
        if token_endpoint_auth_methods_supported is not None:
            self.token_endpoint_auth_methods_supported = token_endpoint_auth_methods_supported
        if claims_supported is not None:
            self.claims_supported = claims_supported
        if scopes_supported is not None:
            self.scopes_supported = scopes_supported
        if code_challenge_methods_supported is not None:
            self.code_challenge_methods_supported = code_challenge_methods_supported
        if userinfo_endpoint is not None:
            self.userinfo_endpoint = userinfo_endpoint
        if end_session_endpoint is not None:
            self.end_session_endpoint = end_session_endpoint

    @property
    def issuer(self):
        """Gets the issuer of this InlineResponse20036.  # noqa: E501


        :return: The issuer of this InlineResponse20036.  # noqa: E501
        :rtype: str
        """
        return self._issuer

    @issuer.setter
    def issuer(self, issuer):
        """Sets the issuer of this InlineResponse20036.


        :param issuer: The issuer of this InlineResponse20036.  # noqa: E501
        :type: str
        """

        self._issuer = issuer

    @property
    def authorization_endpoint(self):
        """Gets the authorization_endpoint of this InlineResponse20036.  # noqa: E501


        :return: The authorization_endpoint of this InlineResponse20036.  # noqa: E501
        :rtype: str
        """
        return self._authorization_endpoint

    @authorization_endpoint.setter
    def authorization_endpoint(self, authorization_endpoint):
        """Sets the authorization_endpoint of this InlineResponse20036.


        :param authorization_endpoint: The authorization_endpoint of this InlineResponse20036.  # noqa: E501
        :type: str
        """

        self._authorization_endpoint = authorization_endpoint

    @property
    def token_endpoint(self):
        """Gets the token_endpoint of this InlineResponse20036.  # noqa: E501


        :return: The token_endpoint of this InlineResponse20036.  # noqa: E501
        :rtype: str
        """
        return self._token_endpoint

    @token_endpoint.setter
    def token_endpoint(self, token_endpoint):
        """Sets the token_endpoint of this InlineResponse20036.


        :param token_endpoint: The token_endpoint of this InlineResponse20036.  # noqa: E501
        :type: str
        """

        self._token_endpoint = token_endpoint

    @property
    def jwks_uri(self):
        """Gets the jwks_uri of this InlineResponse20036.  # noqa: E501


        :return: The jwks_uri of this InlineResponse20036.  # noqa: E501
        :rtype: str
        """
        return self._jwks_uri

    @jwks_uri.setter
    def jwks_uri(self, jwks_uri):
        """Sets the jwks_uri of this InlineResponse20036.


        :param jwks_uri: The jwks_uri of this InlineResponse20036.  # noqa: E501
        :type: str
        """

        self._jwks_uri = jwks_uri

    @property
    def subject_types_supported(self):
        """Gets the subject_types_supported of this InlineResponse20036.  # noqa: E501


        :return: The subject_types_supported of this InlineResponse20036.  # noqa: E501
        :rtype: list[str]
        """
        return self._subject_types_supported

    @subject_types_supported.setter
    def subject_types_supported(self, subject_types_supported):
        """Sets the subject_types_supported of this InlineResponse20036.


        :param subject_types_supported: The subject_types_supported of this InlineResponse20036.  # noqa: E501
        :type: list[str]
        """

        self._subject_types_supported = subject_types_supported

    @property
    def response_types_supported(self):
        """Gets the response_types_supported of this InlineResponse20036.  # noqa: E501


        :return: The response_types_supported of this InlineResponse20036.  # noqa: E501
        :rtype: list[str]
        """
        return self._response_types_supported

    @response_types_supported.setter
    def response_types_supported(self, response_types_supported):
        """Sets the response_types_supported of this InlineResponse20036.


        :param response_types_supported: The response_types_supported of this InlineResponse20036.  # noqa: E501
        :type: list[str]
        """

        self._response_types_supported = response_types_supported

    @property
    def id_token_signing_alg_values_supported(self):
        """Gets the id_token_signing_alg_values_supported of this InlineResponse20036.  # noqa: E501


        :return: The id_token_signing_alg_values_supported of this InlineResponse20036.  # noqa: E501
        :rtype: list[str]
        """
        return self._id_token_signing_alg_values_supported

    @id_token_signing_alg_values_supported.setter
    def id_token_signing_alg_values_supported(self, id_token_signing_alg_values_supported):
        """Sets the id_token_signing_alg_values_supported of this InlineResponse20036.


        :param id_token_signing_alg_values_supported: The id_token_signing_alg_values_supported of this InlineResponse20036.  # noqa: E501
        :type: list[str]
        """

        self._id_token_signing_alg_values_supported = id_token_signing_alg_values_supported

    @property
    def token_endpoint_auth_methods_supported(self):
        """Gets the token_endpoint_auth_methods_supported of this InlineResponse20036.  # noqa: E501


        :return: The token_endpoint_auth_methods_supported of this InlineResponse20036.  # noqa: E501
        :rtype: list[str]
        """
        return self._token_endpoint_auth_methods_supported

    @token_endpoint_auth_methods_supported.setter
    def token_endpoint_auth_methods_supported(self, token_endpoint_auth_methods_supported):
        """Sets the token_endpoint_auth_methods_supported of this InlineResponse20036.


        :param token_endpoint_auth_methods_supported: The token_endpoint_auth_methods_supported of this InlineResponse20036.  # noqa: E501
        :type: list[str]
        """

        self._token_endpoint_auth_methods_supported = token_endpoint_auth_methods_supported

    @property
    def claims_supported(self):
        """Gets the claims_supported of this InlineResponse20036.  # noqa: E501


        :return: The claims_supported of this InlineResponse20036.  # noqa: E501
        :rtype: list[str]
        """
        return self._claims_supported

    @claims_supported.setter
    def claims_supported(self, claims_supported):
        """Sets the claims_supported of this InlineResponse20036.


        :param claims_supported: The claims_supported of this InlineResponse20036.  # noqa: E501
        :type: list[str]
        """

        self._claims_supported = claims_supported

    @property
    def scopes_supported(self):
        """Gets the scopes_supported of this InlineResponse20036.  # noqa: E501


        :return: The scopes_supported of this InlineResponse20036.  # noqa: E501
        :rtype: list[str]
        """
        return self._scopes_supported

    @scopes_supported.setter
    def scopes_supported(self, scopes_supported):
        """Sets the scopes_supported of this InlineResponse20036.


        :param scopes_supported: The scopes_supported of this InlineResponse20036.  # noqa: E501
        :type: list[str]
        """

        self._scopes_supported = scopes_supported

    @property
    def code_challenge_methods_supported(self):
        """Gets the code_challenge_methods_supported of this InlineResponse20036.  # noqa: E501


        :return: The code_challenge_methods_supported of this InlineResponse20036.  # noqa: E501
        :rtype: list[str]
        """
        return self._code_challenge_methods_supported

    @code_challenge_methods_supported.setter
    def code_challenge_methods_supported(self, code_challenge_methods_supported):
        """Sets the code_challenge_methods_supported of this InlineResponse20036.


        :param code_challenge_methods_supported: The code_challenge_methods_supported of this InlineResponse20036.  # noqa: E501
        :type: list[str]
        """

        self._code_challenge_methods_supported = code_challenge_methods_supported

    @property
    def userinfo_endpoint(self):
        """Gets the userinfo_endpoint of this InlineResponse20036.  # noqa: E501


        :return: The userinfo_endpoint of this InlineResponse20036.  # noqa: E501
        :rtype: str
        """
        return self._userinfo_endpoint

    @userinfo_endpoint.setter
    def userinfo_endpoint(self, userinfo_endpoint):
        """Sets the userinfo_endpoint of this InlineResponse20036.


        :param userinfo_endpoint: The userinfo_endpoint of this InlineResponse20036.  # noqa: E501
        :type: str
        """

        self._userinfo_endpoint = userinfo_endpoint

    @property
    def end_session_endpoint(self):
        """Gets the end_session_endpoint of this InlineResponse20036.  # noqa: E501


        :return: The end_session_endpoint of this InlineResponse20036.  # noqa: E501
        :rtype: str
        """
        return self._end_session_endpoint

    @end_session_endpoint.setter
    def end_session_endpoint(self, end_session_endpoint):
        """Sets the end_session_endpoint of this InlineResponse20036.


        :param end_session_endpoint: The end_session_endpoint of this InlineResponse20036.  # noqa: E501
        :type: str
        """

        self._end_session_endpoint = end_session_endpoint

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse20036, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20036):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
