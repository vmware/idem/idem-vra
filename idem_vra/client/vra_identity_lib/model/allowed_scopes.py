# coding: utf-8

"""
    Identity Service API

    A list of identity, account and service management APIs.  # noqa: E501

    OpenAPI spec version: 1.6.5
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class AllowedScopes(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'all_roles': 'bool',
        'services_scopes': 'list[CspgatewayamapiorgsorgIdoauthappsAllowedScopesServicesScopes]',
        'organization_scopes': 'CspgatewayamapiorgsorgIdoauthappsAllowedScopesOrganizationScopes',
        'general_scopes': 'list[str]'
    }

    attribute_map = {
        'all_roles': 'allRoles',
        'services_scopes': 'servicesScopes',
        'organization_scopes': 'organizationScopes',
        'general_scopes': 'generalScopes'
    }

    def __init__(self, all_roles=None, services_scopes=None, organization_scopes=None, general_scopes=None):  # noqa: E501
        """AllowedScopes - a model defined in Swagger"""  # noqa: E501
        self._all_roles = None
        self._services_scopes = None
        self._organization_scopes = None
        self._general_scopes = None
        self.discriminator = None
        if all_roles is not None:
            self.all_roles = all_roles
        if services_scopes is not None:
            self.services_scopes = services_scopes
        if organization_scopes is not None:
            self.organization_scopes = organization_scopes
        if general_scopes is not None:
            self.general_scopes = general_scopes

    @property
    def all_roles(self):
        """Gets the all_roles of this AllowedScopes.  # noqa: E501

        For CSP compatibility, ignored by the Identity Service.  # noqa: E501

        :return: The all_roles of this AllowedScopes.  # noqa: E501
        :rtype: bool
        """
        return self._all_roles

    @all_roles.setter
    def all_roles(self, all_roles):
        """Sets the all_roles of this AllowedScopes.

        For CSP compatibility, ignored by the Identity Service.  # noqa: E501

        :param all_roles: The all_roles of this AllowedScopes.  # noqa: E501
        :type: bool
        """

        self._all_roles = all_roles

    @property
    def services_scopes(self):
        """Gets the services_scopes of this AllowedScopes.  # noqa: E501

        Service roles to be granted.  # noqa: E501

        :return: The services_scopes of this AllowedScopes.  # noqa: E501
        :rtype: list[CspgatewayamapiorgsorgIdoauthappsAllowedScopesServicesScopes]
        """
        return self._services_scopes

    @services_scopes.setter
    def services_scopes(self, services_scopes):
        """Sets the services_scopes of this AllowedScopes.

        Service roles to be granted.  # noqa: E501

        :param services_scopes: The services_scopes of this AllowedScopes.  # noqa: E501
        :type: list[CspgatewayamapiorgsorgIdoauthappsAllowedScopesServicesScopes]
        """

        self._services_scopes = services_scopes

    @property
    def organization_scopes(self):
        """Gets the organization_scopes of this AllowedScopes.  # noqa: E501


        :return: The organization_scopes of this AllowedScopes.  # noqa: E501
        :rtype: CspgatewayamapiorgsorgIdoauthappsAllowedScopesOrganizationScopes
        """
        return self._organization_scopes

    @organization_scopes.setter
    def organization_scopes(self, organization_scopes):
        """Sets the organization_scopes of this AllowedScopes.


        :param organization_scopes: The organization_scopes of this AllowedScopes.  # noqa: E501
        :type: CspgatewayamapiorgsorgIdoauthappsAllowedScopesOrganizationScopes
        """

        self._organization_scopes = organization_scopes

    @property
    def general_scopes(self):
        """Gets the general_scopes of this AllowedScopes.  # noqa: E501

        For CSP compatibility, ignored by the Identity Service.  # noqa: E501

        :return: The general_scopes of this AllowedScopes.  # noqa: E501
        :rtype: list[str]
        """
        return self._general_scopes

    @general_scopes.setter
    def general_scopes(self, general_scopes):
        """Sets the general_scopes of this AllowedScopes.

        For CSP compatibility, ignored by the Identity Service.  # noqa: E501

        :param general_scopes: The general_scopes of this AllowedScopes.  # noqa: E501
        :type: list[str]
        """

        self._general_scopes = general_scopes

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(AllowedScopes, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, AllowedScopes):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
