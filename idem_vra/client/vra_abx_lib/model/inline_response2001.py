# coding: utf-8

"""
    ABX Service - APIs

    Create or manage actions and their versions. Execute actions and flows.  This page describes the RESTful APIs for ABX Service. The APIs facilitate CRUD operations on creation and management of Actions and their Versions and also allow Execution of Actions and Flows.  The APIs that list collections of resources  also support OData like implementation. Below query params can be used across ABX Service APIs.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/abx/api/resources/actions?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/abx/api/resources/actions?$expand=true&$top=10&$skip=2```  3. `page` and `$size` - page used in conjunction with `$size` helps in pagination of resources.      ```/abx/api/resources/actions?$expand=true&page=0&$size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /abx/api/resources/actions?$filter=startswith(name, 'ABC')     /abx/api/resources/actions?$filter=toupper(name) eq 'ABCD-ACTION'     /abx/api/resources/actions?$filter=substringof(%27bc%27,tolower(name))     /abx/api/resources/actions?$filter=name eq 'ABCD' and actionType eq 'SCRIPT'   # noqa: E501

    OpenAPI spec version: 2019-09-12
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse2001(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'name': 'str',
        'created_millis': 'int',
        'org_id': 'str',
        'description': 'str',
        'action_id': 'str',
        'project_id': 'str',
        'created_by': 'str',
        'released': 'bool',
        'action': 'AbxapiresourcesactionsidversionsAction',
        'content_id': 'str',
        'git_commit_id': 'str'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'created_millis': 'createdMillis',
        'org_id': 'orgId',
        'description': 'description',
        'action_id': 'actionId',
        'project_id': 'projectId',
        'created_by': 'createdBy',
        'released': 'released',
        'action': 'action',
        'content_id': 'contentId',
        'git_commit_id': 'gitCommitId'
    }

    def __init__(self, id=None, name=None, created_millis=None, org_id=None, description=None, action_id=None, project_id=None, created_by=None, released=None, action=None, content_id=None, git_commit_id=None):  # noqa: E501
        """InlineResponse2001 - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._name = None
        self._created_millis = None
        self._org_id = None
        self._description = None
        self._action_id = None
        self._project_id = None
        self._created_by = None
        self._released = None
        self._action = None
        self._content_id = None
        self._git_commit_id = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if created_millis is not None:
            self.created_millis = created_millis
        if org_id is not None:
            self.org_id = org_id
        if description is not None:
            self.description = description
        if action_id is not None:
            self.action_id = action_id
        if project_id is not None:
            self.project_id = project_id
        if created_by is not None:
            self.created_by = created_by
        if released is not None:
            self.released = released
        if action is not None:
            self.action = action
        if content_id is not None:
            self.content_id = content_id
        if git_commit_id is not None:
            self.git_commit_id = git_commit_id

    @property
    def id(self):
        """Gets the id of this InlineResponse2001.  # noqa: E501


        :return: The id of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse2001.


        :param id: The id of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this InlineResponse2001.  # noqa: E501


        :return: The name of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse2001.


        :param name: The name of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def created_millis(self):
        """Gets the created_millis of this InlineResponse2001.  # noqa: E501


        :return: The created_millis of this InlineResponse2001.  # noqa: E501
        :rtype: int
        """
        return self._created_millis

    @created_millis.setter
    def created_millis(self, created_millis):
        """Sets the created_millis of this InlineResponse2001.


        :param created_millis: The created_millis of this InlineResponse2001.  # noqa: E501
        :type: int
        """

        self._created_millis = created_millis

    @property
    def org_id(self):
        """Gets the org_id of this InlineResponse2001.  # noqa: E501


        :return: The org_id of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id):
        """Sets the org_id of this InlineResponse2001.


        :param org_id: The org_id of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._org_id = org_id

    @property
    def description(self):
        """Gets the description of this InlineResponse2001.  # noqa: E501


        :return: The description of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineResponse2001.


        :param description: The description of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def action_id(self):
        """Gets the action_id of this InlineResponse2001.  # noqa: E501


        :return: The action_id of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._action_id

    @action_id.setter
    def action_id(self, action_id):
        """Sets the action_id of this InlineResponse2001.


        :param action_id: The action_id of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._action_id = action_id

    @property
    def project_id(self):
        """Gets the project_id of this InlineResponse2001.  # noqa: E501


        :return: The project_id of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this InlineResponse2001.


        :param project_id: The project_id of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._project_id = project_id

    @property
    def created_by(self):
        """Gets the created_by of this InlineResponse2001.  # noqa: E501


        :return: The created_by of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this InlineResponse2001.


        :param created_by: The created_by of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def released(self):
        """Gets the released of this InlineResponse2001.  # noqa: E501


        :return: The released of this InlineResponse2001.  # noqa: E501
        :rtype: bool
        """
        return self._released

    @released.setter
    def released(self, released):
        """Sets the released of this InlineResponse2001.


        :param released: The released of this InlineResponse2001.  # noqa: E501
        :type: bool
        """

        self._released = released

    @property
    def action(self):
        """Gets the action of this InlineResponse2001.  # noqa: E501


        :return: The action of this InlineResponse2001.  # noqa: E501
        :rtype: AbxapiresourcesactionsidversionsAction
        """
        return self._action

    @action.setter
    def action(self, action):
        """Sets the action of this InlineResponse2001.


        :param action: The action of this InlineResponse2001.  # noqa: E501
        :type: AbxapiresourcesactionsidversionsAction
        """

        self._action = action

    @property
    def content_id(self):
        """Gets the content_id of this InlineResponse2001.  # noqa: E501


        :return: The content_id of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._content_id

    @content_id.setter
    def content_id(self, content_id):
        """Sets the content_id of this InlineResponse2001.


        :param content_id: The content_id of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._content_id = content_id

    @property
    def git_commit_id(self):
        """Gets the git_commit_id of this InlineResponse2001.  # noqa: E501


        :return: The git_commit_id of this InlineResponse2001.  # noqa: E501
        :rtype: str
        """
        return self._git_commit_id

    @git_commit_id.setter
    def git_commit_id(self, git_commit_id):
        """Sets the git_commit_id of this InlineResponse2001.


        :param git_commit_id: The git_commit_id of this InlineResponse2001.  # noqa: E501
        :type: str
        """

        self._git_commit_id = git_commit_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse2001, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse2001):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
