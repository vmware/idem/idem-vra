# coding: utf-8

"""
    VMware Aria Automation Assembler / Service Broker API

    A multi-cloud API for Cloud Automation Services  The APIs that list collections of resources also support OData like implementation. Below query params can be used across Assembler / Service Broker entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/catalog/api/items?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/catalog/api/items?$top=10&$skip=2```  3. `page` and `size` - page used in conjunction with `$size` helps in pagination of resources.      ```/catalog/api/items?page=0&size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /catalog/api/items?$filter=startswith(name, 'Act')     /catalog/api/items?$filter=toupper(name) eq 'ACTIVATE'     /catalog/api/items?$filter=substringof('renameVM', name)     /catalog/api/items?$filter=name eq 'ABCD'  # noqa: E501

    OpenAPI spec version: 2020-08-25
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class TagBasedMeteringItem(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'item_name': 'str',
        'tag_based_meterings': 'list[PriceapiprivatepricingcardsidTagBasedMeterings]'
    }

    attribute_map = {
        'item_name': 'itemName',
        'tag_based_meterings': 'tagBasedMeterings'
    }

    def __init__(self, item_name=None, tag_based_meterings=None):  # noqa: E501
        """TagBasedMeteringItem - a model defined in Swagger"""  # noqa: E501
        self._item_name = None
        self._tag_based_meterings = None
        self.discriminator = None
        self.item_name = item_name
        if tag_based_meterings is not None:
            self.tag_based_meterings = tag_based_meterings

    @property
    def item_name(self):
        """Gets the item_name of this TagBasedMeteringItem.  # noqa: E501


        :return: The item_name of this TagBasedMeteringItem.  # noqa: E501
        :rtype: str
        """
        return self._item_name

    @item_name.setter
    def item_name(self, item_name):
        """Sets the item_name of this TagBasedMeteringItem.


        :param item_name: The item_name of this TagBasedMeteringItem.  # noqa: E501
        :type: str
        """
        if item_name is None:
            raise ValueError("Invalid value for `item_name`, must not be `None`")  # noqa: E501

        self._item_name = item_name

    @property
    def tag_based_meterings(self):
        """Gets the tag_based_meterings of this TagBasedMeteringItem.  # noqa: E501


        :return: The tag_based_meterings of this TagBasedMeteringItem.  # noqa: E501
        :rtype: list[PriceapiprivatepricingcardsidTagBasedMeterings]
        """
        return self._tag_based_meterings

    @tag_based_meterings.setter
    def tag_based_meterings(self, tag_based_meterings):
        """Sets the tag_based_meterings of this TagBasedMeteringItem.


        :param tag_based_meterings: The tag_based_meterings of this TagBasedMeteringItem.  # noqa: E501
        :type: list[PriceapiprivatepricingcardsidTagBasedMeterings]
        """

        self._tag_based_meterings = tag_based_meterings

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(TagBasedMeteringItem, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, TagBasedMeteringItem):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
