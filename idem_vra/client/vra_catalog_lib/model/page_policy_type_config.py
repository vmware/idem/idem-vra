# coding: utf-8

"""
    VMware Aria Automation Assembler / Service Broker API

    A multi-cloud API for Cloud Automation Services  The APIs that list collections of resources also support OData like implementation. Below query params can be used across Assembler / Service Broker entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/catalog/api/items?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/catalog/api/items?$top=10&$skip=2```  3. `page` and `size` - page used in conjunction with `$size` helps in pagination of resources.      ```/catalog/api/items?page=0&size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /catalog/api/items?$filter=startswith(name, 'Act')     /catalog/api/items?$filter=toupper(name) eq 'ACTIVATE'     /catalog/api/items?$filter=substringof('renameVM', name)     /catalog/api/items?$filter=name eq 'ABCD'  # noqa: E501

    OpenAPI spec version: 2020-08-25
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PagePolicyTypeConfig(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'enable_dry_run': 'bool',
        'enable_update_notification': 'bool',
        'enable_enforcement_type': 'bool',
        'enable_single_project_scope': 'bool'
    }

    attribute_map = {
        'enable_dry_run': 'enableDryRun',
        'enable_update_notification': 'enableUpdateNotification',
        'enable_enforcement_type': 'enableEnforcementType',
        'enable_single_project_scope': 'enableSingleProjectScope'
    }

    def __init__(self, enable_dry_run=None, enable_update_notification=None, enable_enforcement_type=None, enable_single_project_scope=None):  # noqa: E501
        """PagePolicyTypeConfig - a model defined in Swagger"""  # noqa: E501
        self._enable_dry_run = None
        self._enable_update_notification = None
        self._enable_enforcement_type = None
        self._enable_single_project_scope = None
        self.discriminator = None
        if enable_dry_run is not None:
            self.enable_dry_run = enable_dry_run
        if enable_update_notification is not None:
            self.enable_update_notification = enable_update_notification
        if enable_enforcement_type is not None:
            self.enable_enforcement_type = enable_enforcement_type
        if enable_single_project_scope is not None:
            self.enable_single_project_scope = enable_single_project_scope

    @property
    def enable_dry_run(self):
        """Gets the enable_dry_run of this PagePolicyTypeConfig.  # noqa: E501


        :return: The enable_dry_run of this PagePolicyTypeConfig.  # noqa: E501
        :rtype: bool
        """
        return self._enable_dry_run

    @enable_dry_run.setter
    def enable_dry_run(self, enable_dry_run):
        """Sets the enable_dry_run of this PagePolicyTypeConfig.


        :param enable_dry_run: The enable_dry_run of this PagePolicyTypeConfig.  # noqa: E501
        :type: bool
        """

        self._enable_dry_run = enable_dry_run

    @property
    def enable_update_notification(self):
        """Gets the enable_update_notification of this PagePolicyTypeConfig.  # noqa: E501


        :return: The enable_update_notification of this PagePolicyTypeConfig.  # noqa: E501
        :rtype: bool
        """
        return self._enable_update_notification

    @enable_update_notification.setter
    def enable_update_notification(self, enable_update_notification):
        """Sets the enable_update_notification of this PagePolicyTypeConfig.


        :param enable_update_notification: The enable_update_notification of this PagePolicyTypeConfig.  # noqa: E501
        :type: bool
        """

        self._enable_update_notification = enable_update_notification

    @property
    def enable_enforcement_type(self):
        """Gets the enable_enforcement_type of this PagePolicyTypeConfig.  # noqa: E501


        :return: The enable_enforcement_type of this PagePolicyTypeConfig.  # noqa: E501
        :rtype: bool
        """
        return self._enable_enforcement_type

    @enable_enforcement_type.setter
    def enable_enforcement_type(self, enable_enforcement_type):
        """Sets the enable_enforcement_type of this PagePolicyTypeConfig.


        :param enable_enforcement_type: The enable_enforcement_type of this PagePolicyTypeConfig.  # noqa: E501
        :type: bool
        """

        self._enable_enforcement_type = enable_enforcement_type

    @property
    def enable_single_project_scope(self):
        """Gets the enable_single_project_scope of this PagePolicyTypeConfig.  # noqa: E501


        :return: The enable_single_project_scope of this PagePolicyTypeConfig.  # noqa: E501
        :rtype: bool
        """
        return self._enable_single_project_scope

    @enable_single_project_scope.setter
    def enable_single_project_scope(self, enable_single_project_scope):
        """Sets the enable_single_project_scope of this PagePolicyTypeConfig.


        :param enable_single_project_scope: The enable_single_project_scope of this PagePolicyTypeConfig.  # noqa: E501
        :type: bool
        """

        self._enable_single_project_scope = enable_single_project_scope

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PagePolicyTypeConfig, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PagePolicyTypeConfig):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
