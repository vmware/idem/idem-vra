# coding: utf-8

"""
    VMware Aria Automation Assembler / Service Broker API

    A multi-cloud API for Cloud Automation Services  The APIs that list collections of resources also support OData like implementation. Below query params can be used across Assembler / Service Broker entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/catalog/api/items?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/catalog/api/items?$top=10&$skip=2```  3. `page` and `size` - page used in conjunction with `$size` helps in pagination of resources.      ```/catalog/api/items?page=0&size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /catalog/api/items?$filter=startswith(name, 'Act')     /catalog/api/items?$filter=toupper(name) eq 'ACTIVATE'     /catalog/api/items?$filter=substringof('renameVM', name)     /catalog/api/items?$filter=name eq 'ABCD'  # noqa: E501

    OpenAPI spec version: 2020-08-25
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class UserEvent(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'request_id': 'str',
        'requested_by': 'str',
        'resource_ids': 'list[str]',
        'name': 'str',
        'details': 'str',
        'inputs': 'dict(str, object)',
        'outputs': 'dict(str, object)',
        'status': 'str',
        'created_at': 'datetime',
        'updated_at': 'datetime'
    }

    attribute_map = {
        'id': 'id',
        'request_id': 'requestId',
        'requested_by': 'requestedBy',
        'resource_ids': 'resourceIds',
        'name': 'name',
        'details': 'details',
        'inputs': 'inputs',
        'outputs': 'outputs',
        'status': 'status',
        'created_at': 'createdAt',
        'updated_at': 'updatedAt'
    }

    def __init__(self, id=None, request_id=None, requested_by=None, resource_ids=None, name=None, details=None, inputs=None, outputs=None, status=None, created_at=None, updated_at=None):  # noqa: E501
        """UserEvent - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._request_id = None
        self._requested_by = None
        self._resource_ids = None
        self._name = None
        self._details = None
        self._inputs = None
        self._outputs = None
        self._status = None
        self._created_at = None
        self._updated_at = None
        self.discriminator = None
        self.id = id
        self.request_id = request_id
        self.requested_by = requested_by
        self.resource_ids = resource_ids
        self.name = name
        if details is not None:
            self.details = details
        if inputs is not None:
            self.inputs = inputs
        if outputs is not None:
            self.outputs = outputs
        self.status = status
        if created_at is not None:
            self.created_at = created_at
        if updated_at is not None:
            self.updated_at = updated_at

    @property
    def id(self):
        """Gets the id of this UserEvent.  # noqa: E501

        Id  # noqa: E501

        :return: The id of this UserEvent.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this UserEvent.

        Id  # noqa: E501

        :param id: The id of this UserEvent.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def request_id(self):
        """Gets the request_id of this UserEvent.  # noqa: E501

        Request id  # noqa: E501

        :return: The request_id of this UserEvent.  # noqa: E501
        :rtype: str
        """
        return self._request_id

    @request_id.setter
    def request_id(self, request_id):
        """Sets the request_id of this UserEvent.

        Request id  # noqa: E501

        :param request_id: The request_id of this UserEvent.  # noqa: E501
        :type: str
        """
        if request_id is None:
            raise ValueError("Invalid value for `request_id`, must not be `None`")  # noqa: E501

        self._request_id = request_id

    @property
    def requested_by(self):
        """Gets the requested_by of this UserEvent.  # noqa: E501

        User that initiated the event  # noqa: E501

        :return: The requested_by of this UserEvent.  # noqa: E501
        :rtype: str
        """
        return self._requested_by

    @requested_by.setter
    def requested_by(self, requested_by):
        """Sets the requested_by of this UserEvent.

        User that initiated the event  # noqa: E501

        :param requested_by: The requested_by of this UserEvent.  # noqa: E501
        :type: str
        """
        if requested_by is None:
            raise ValueError("Invalid value for `requested_by`, must not be `None`")  # noqa: E501

        self._requested_by = requested_by

    @property
    def resource_ids(self):
        """Gets the resource_ids of this UserEvent.  # noqa: E501

        Resource id  # noqa: E501

        :return: The resource_ids of this UserEvent.  # noqa: E501
        :rtype: list[str]
        """
        return self._resource_ids

    @resource_ids.setter
    def resource_ids(self, resource_ids):
        """Sets the resource_ids of this UserEvent.

        Resource id  # noqa: E501

        :param resource_ids: The resource_ids of this UserEvent.  # noqa: E501
        :type: list[str]
        """
        if resource_ids is None:
            raise ValueError("Invalid value for `resource_ids`, must not be `None`")  # noqa: E501

        self._resource_ids = resource_ids

    @property
    def name(self):
        """Gets the name of this UserEvent.  # noqa: E501

        Short user-friendly label of the event  # noqa: E501

        :return: The name of this UserEvent.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this UserEvent.

        Short user-friendly label of the event  # noqa: E501

        :param name: The name of this UserEvent.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def details(self):
        """Gets the details of this UserEvent.  # noqa: E501

        Details of the user event. If the event is a user interaction the details contain the vRO Manual User Interaction ID.  # noqa: E501

        :return: The details of this UserEvent.  # noqa: E501
        :rtype: str
        """
        return self._details

    @details.setter
    def details(self, details):
        """Sets the details of this UserEvent.

        Details of the user event. If the event is a user interaction the details contain the vRO Manual User Interaction ID.  # noqa: E501

        :param details: The details of this UserEvent.  # noqa: E501
        :type: str
        """

        self._details = details

    @property
    def inputs(self):
        """Gets the inputs of this UserEvent.  # noqa: E501

        User event inputs. If user event is a user interaction the inputs form can be retrieved from Orchestrator Gateway API /vro/forms/user-interaction/{userInteractionId}  # noqa: E501

        :return: The inputs of this UserEvent.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._inputs

    @inputs.setter
    def inputs(self, inputs):
        """Sets the inputs of this UserEvent.

        User event inputs. If user event is a user interaction the inputs form can be retrieved from Orchestrator Gateway API /vro/forms/user-interaction/{userInteractionId}  # noqa: E501

        :param inputs: The inputs of this UserEvent.  # noqa: E501
        :type: dict(str, object)
        """

        self._inputs = inputs

    @property
    def outputs(self):
        """Gets the outputs of this UserEvent.  # noqa: E501

        User event outputs  # noqa: E501

        :return: The outputs of this UserEvent.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._outputs

    @outputs.setter
    def outputs(self, outputs):
        """Sets the outputs of this UserEvent.

        User event outputs  # noqa: E501

        :param outputs: The outputs of this UserEvent.  # noqa: E501
        :type: dict(str, object)
        """

        self._outputs = outputs

    @property
    def status(self):
        """Gets the status of this UserEvent.  # noqa: E501

        User event status.  # noqa: E501

        :return: The status of this UserEvent.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this UserEvent.

        User event status.  # noqa: E501

        :param status: The status of this UserEvent.  # noqa: E501
        :type: str
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501
        allowed_values = ["SUCCESSFUL", "FAILED", "PENDING", "SUCCESSFUL, FAILED, PENDING"]  # noqa: E501
        if status not in allowed_values:
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def created_at(self):
        """Gets the created_at of this UserEvent.  # noqa: E501

        Time at which the event was created  # noqa: E501

        :return: The created_at of this UserEvent.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this UserEvent.

        Time at which the event was created  # noqa: E501

        :param created_at: The created_at of this UserEvent.  # noqa: E501
        :type: datetime
        """

        self._created_at = created_at

    @property
    def updated_at(self):
        """Gets the updated_at of this UserEvent.  # noqa: E501

        Time at which the event was updated  # noqa: E501

        :return: The updated_at of this UserEvent.  # noqa: E501
        :rtype: datetime
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this UserEvent.

        Time at which the event was updated  # noqa: E501

        :param updated_at: The updated_at of this UserEvent.  # noqa: E501
        :type: datetime
        """

        self._updated_at = updated_at

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(UserEvent, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, UserEvent):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
