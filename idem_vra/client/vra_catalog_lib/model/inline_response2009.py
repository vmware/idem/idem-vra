# coding: utf-8

"""
    VMware Aria Automation Assembler / Service Broker API

    A multi-cloud API for Cloud Automation Services  The APIs that list collections of resources also support OData like implementation. Below query params can be used across Assembler / Service Broker entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/catalog/api/items?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/catalog/api/items?$top=10&$skip=2```  3. `page` and `size` - page used in conjunction with `$size` helps in pagination of resources.      ```/catalog/api/items?page=0&size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /catalog/api/items?$filter=startswith(name, 'Act')     /catalog/api/items?$filter=toupper(name) eq 'ACTIVATE'     /catalog/api/items?$filter=substringof('renameVM', name)     /catalog/api/items?$filter=name eq 'ABCD'  # noqa: E501

    OpenAPI spec version: 2020-08-25
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse2009(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'deployment_id': 'str',
        'request_id': 'str',
        'project_id': 'str',
        'resource_id': 'str'
    }

    attribute_map = {
        'deployment_id': 'deploymentId',
        'request_id': 'requestId',
        'project_id': 'projectId',
        'resource_id': 'resourceId'
    }

    def __init__(self, deployment_id=None, request_id=None, project_id=None, resource_id=None):  # noqa: E501
        """InlineResponse2009 - a model defined in Swagger"""  # noqa: E501
        self._deployment_id = None
        self._request_id = None
        self._project_id = None
        self._resource_id = None
        self.discriminator = None
        if deployment_id is not None:
            self.deployment_id = deployment_id
        if request_id is not None:
            self.request_id = request_id
        if project_id is not None:
            self.project_id = project_id
        if resource_id is not None:
            self.resource_id = resource_id

    @property
    def deployment_id(self):
        """Gets the deployment_id of this InlineResponse2009.  # noqa: E501

        Identifier of the requested deployment id to which the request applies to  # noqa: E501

        :return: The deployment_id of this InlineResponse2009.  # noqa: E501
        :rtype: str
        """
        return self._deployment_id

    @deployment_id.setter
    def deployment_id(self, deployment_id):
        """Sets the deployment_id of this InlineResponse2009.

        Identifier of the requested deployment id to which the request applies to  # noqa: E501

        :param deployment_id: The deployment_id of this InlineResponse2009.  # noqa: E501
        :type: str
        """

        self._deployment_id = deployment_id

    @property
    def request_id(self):
        """Gets the request_id of this InlineResponse2009.  # noqa: E501

        Request identifier  # noqa: E501

        :return: The request_id of this InlineResponse2009.  # noqa: E501
        :rtype: str
        """
        return self._request_id

    @request_id.setter
    def request_id(self, request_id):
        """Sets the request_id of this InlineResponse2009.

        Request identifier  # noqa: E501

        :param request_id: The request_id of this InlineResponse2009.  # noqa: E501
        :type: str
        """

        self._request_id = request_id

    @property
    def project_id(self):
        """Gets the project_id of this InlineResponse2009.  # noqa: E501

        Project identifier  # noqa: E501

        :return: The project_id of this InlineResponse2009.  # noqa: E501
        :rtype: str
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this InlineResponse2009.

        Project identifier  # noqa: E501

        :param project_id: The project_id of this InlineResponse2009.  # noqa: E501
        :type: str
        """

        self._project_id = project_id

    @property
    def resource_id(self):
        """Gets the resource_id of this InlineResponse2009.  # noqa: E501

        Resource ID  # noqa: E501

        :return: The resource_id of this InlineResponse2009.  # noqa: E501
        :rtype: str
        """
        return self._resource_id

    @resource_id.setter
    def resource_id(self, resource_id):
        """Sets the resource_id of this InlineResponse2009.

        Resource ID  # noqa: E501

        :param resource_id: The resource_id of this InlineResponse2009.  # noqa: E501
        :type: str
        """

        self._resource_id = resource_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse2009, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse2009):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
