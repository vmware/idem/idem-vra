# coding: utf-8

"""
    VMware Aria Automation Assembler / Service Broker API

    A multi-cloud API for Cloud Automation Services  The APIs that list collections of resources also support OData like implementation. Below query params can be used across Assembler / Service Broker entities.  Example:  1. `$orderby` - returns a result with the rows being sorted by the values of provided attribute.      ```/catalog/api/items?$orderby=name%20desc```  2. `$top`, `$skip` - `$top` returns the requested number of resources. Used with `$skip`, the client can skip a given number of resources.      ```/catalog/api/items?$top=10&$skip=2```  3. `page` and `size` - page used in conjunction with `$size` helps in pagination of resources.      ```/catalog/api/items?page=0&size=5```  4. `$filter` - `$filter` returns a subset of resources that satisfy the given predicate expression.      ```     /catalog/api/items?$filter=startswith(name, 'Act')     /catalog/api/items?$filter=toupper(name) eq 'ACTIVATE'     /catalog/api/items?$filter=substringof('renameVM', name)     /catalog/api/items?$filter=name eq 'ABCD'  # noqa: E501

    OpenAPI spec version: 2020-08-25
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse20024(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'name': 'str',
        'category': 'str',
        'description': 'str',
        'subject': 'str',
        'body': 'str',
        'entity_schema': 'DeploymentapiresourcesresourceIdactionsSchema',
        'created_at': 'datetime',
        'created_by': 'str',
        'last_updated_at': 'datetime',
        'last_updated_by': 'str'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'category': 'category',
        'description': 'description',
        'subject': 'subject',
        'body': 'body',
        'entity_schema': 'entitySchema',
        'created_at': 'createdAt',
        'created_by': 'createdBy',
        'last_updated_at': 'lastUpdatedAt',
        'last_updated_by': 'lastUpdatedBy'
    }

    def __init__(self, id=None, name=None, category=None, description=None, subject=None, body=None, entity_schema=None, created_at=None, created_by=None, last_updated_at=None, last_updated_by=None):  # noqa: E501
        """InlineResponse20024 - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._name = None
        self._category = None
        self._description = None
        self._subject = None
        self._body = None
        self._entity_schema = None
        self._created_at = None
        self._created_by = None
        self._last_updated_at = None
        self._last_updated_by = None
        self.discriminator = None
        self.id = id
        self.name = name
        if category is not None:
            self.category = category
        if description is not None:
            self.description = description
        self.subject = subject
        if body is not None:
            self.body = body
        if entity_schema is not None:
            self.entity_schema = entity_schema
        if created_at is not None:
            self.created_at = created_at
        if created_by is not None:
            self.created_by = created_by
        if last_updated_at is not None:
            self.last_updated_at = last_updated_at
        if last_updated_by is not None:
            self.last_updated_by = last_updated_by

    @property
    def id(self):
        """Gets the id of this InlineResponse20024.  # noqa: E501

        Notification scenario id  # noqa: E501

        :return: The id of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this InlineResponse20024.

        Notification scenario id  # noqa: E501

        :param id: The id of this InlineResponse20024.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def name(self):
        """Gets the name of this InlineResponse20024.  # noqa: E501

        Notification scenario name  # noqa: E501

        :return: The name of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this InlineResponse20024.

        Notification scenario name  # noqa: E501

        :param name: The name of this InlineResponse20024.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def category(self):
        """Gets the category of this InlineResponse20024.  # noqa: E501

        Notification scenario category  # noqa: E501

        :return: The category of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._category

    @category.setter
    def category(self, category):
        """Sets the category of this InlineResponse20024.

        Notification scenario category  # noqa: E501

        :param category: The category of this InlineResponse20024.  # noqa: E501
        :type: str
        """

        self._category = category

    @property
    def description(self):
        """Gets the description of this InlineResponse20024.  # noqa: E501

        Notification scenario description  # noqa: E501

        :return: The description of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this InlineResponse20024.

        Notification scenario description  # noqa: E501

        :param description: The description of this InlineResponse20024.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def subject(self):
        """Gets the subject of this InlineResponse20024.  # noqa: E501

        Notification scenario subject  # noqa: E501

        :return: The subject of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._subject

    @subject.setter
    def subject(self, subject):
        """Sets the subject of this InlineResponse20024.

        Notification scenario subject  # noqa: E501

        :param subject: The subject of this InlineResponse20024.  # noqa: E501
        :type: str
        """
        if subject is None:
            raise ValueError("Invalid value for `subject`, must not be `None`")  # noqa: E501

        self._subject = subject

    @property
    def body(self):
        """Gets the body of this InlineResponse20024.  # noqa: E501

        Notification scenario body  # noqa: E501

        :return: The body of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._body

    @body.setter
    def body(self, body):
        """Sets the body of this InlineResponse20024.

        Notification scenario body  # noqa: E501

        :param body: The body of this InlineResponse20024.  # noqa: E501
        :type: str
        """

        self._body = body

    @property
    def entity_schema(self):
        """Gets the entity_schema of this InlineResponse20024.  # noqa: E501


        :return: The entity_schema of this InlineResponse20024.  # noqa: E501
        :rtype: DeploymentapiresourcesresourceIdactionsSchema
        """
        return self._entity_schema

    @entity_schema.setter
    def entity_schema(self, entity_schema):
        """Sets the entity_schema of this InlineResponse20024.


        :param entity_schema: The entity_schema of this InlineResponse20024.  # noqa: E501
        :type: DeploymentapiresourcesresourceIdactionsSchema
        """

        self._entity_schema = entity_schema

    @property
    def created_at(self):
        """Gets the created_at of this InlineResponse20024.  # noqa: E501

        Creation time  # noqa: E501

        :return: The created_at of this InlineResponse20024.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this InlineResponse20024.

        Creation time  # noqa: E501

        :param created_at: The created_at of this InlineResponse20024.  # noqa: E501
        :type: datetime
        """

        self._created_at = created_at

    @property
    def created_by(self):
        """Gets the created_by of this InlineResponse20024.  # noqa: E501

        Created By  # noqa: E501

        :return: The created_by of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this InlineResponse20024.

        Created By  # noqa: E501

        :param created_by: The created_by of this InlineResponse20024.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def last_updated_at(self):
        """Gets the last_updated_at of this InlineResponse20024.  # noqa: E501

        Update time  # noqa: E501

        :return: The last_updated_at of this InlineResponse20024.  # noqa: E501
        :rtype: datetime
        """
        return self._last_updated_at

    @last_updated_at.setter
    def last_updated_at(self, last_updated_at):
        """Sets the last_updated_at of this InlineResponse20024.

        Update time  # noqa: E501

        :param last_updated_at: The last_updated_at of this InlineResponse20024.  # noqa: E501
        :type: datetime
        """

        self._last_updated_at = last_updated_at

    @property
    def last_updated_by(self):
        """Gets the last_updated_by of this InlineResponse20024.  # noqa: E501

        Updated By  # noqa: E501

        :return: The last_updated_by of this InlineResponse20024.  # noqa: E501
        :rtype: str
        """
        return self._last_updated_by

    @last_updated_by.setter
    def last_updated_by(self, last_updated_by):
        """Sets the last_updated_by of this InlineResponse20024.

        Updated By  # noqa: E501

        :param last_updated_by: The last_updated_by of this InlineResponse20024.  # noqa: E501
        :type: str
        """

        self._last_updated_by = last_updated_by

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse20024, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse20024):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
