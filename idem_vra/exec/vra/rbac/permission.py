from idem_vra.client.vra_rbac_lib.api import PermissionApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_all_permissions(hub, ctx, **kwargs):
    """Get all permissions  Performs GET /rbac-service/api/permissions


    :param boolean excludeOrganizationScopedOnly: (optional in query) Exclude organization scoped only permission. When the flag is true, it
      will not include the permissions which are applicable only to
      organization
    :param boolean excludeProjectScoped: (optional in query) Exclude Project scoped Permissions. When the flag is true, it will not
      include the project scoped permissions.
    :param string apiVersion: (optional in query)
    """

    try:

        hub.log.debug("GET /rbac-service/api/permissions")

        api = PermissionApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2020-08-10"

        ret = api.get_all_permissions(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PermissionApi.get_all_permissions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_permission(hub, ctx, p_id, **kwargs):
    """Retrieve a permission by id  Performs GET /rbac-service/api/permissions/{id}


    :param string p_id: (required in path)
    :param string apiVersion: (optional in query)
    """

    try:

        hub.log.debug("GET /rbac-service/api/permissions/{id}")

        api = PermissionApi(hub.clients["idem_vra.client.vra_rbac_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2020-08-10"

        ret = api.get_permission(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PermissionApi.get_permission: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
