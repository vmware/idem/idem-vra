from idem_vra.client.vra_pipeline_lib.api import TriggersApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def clone_gerrit_listener_by_name_using_post(
    hub, ctx, p_name, p_project, **kwargs
):
    """Clone a Gerrit Listener by project and name Clone a Gerrit Listener with the given project and name Performs POST /codestream/api/gerrit-listeners/{project}/{name}


    :param string p_name: (required in path) The name of the Gerrit Listener
    :param string p_project: (required in path) The project the Gerrit Listener belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Description for the cloned entity.
    :param string name: (optional in body) Name field for the cloned entity.
    :param string requestType: (optional in body) Action to be performed on the service.
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-listeners/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "requestType" in kwargs:
            hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
            body["requestType"] = kwargs.get("requestType")
            del kwargs["requestType"]

        ret = api.clone_gerrit_listener_by_name_using_post(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.clone_gerrit_listener_by_name_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def clone_gerrit_listener_using_post(hub, ctx, p_id, **kwargs):
    """Clone a Gerrit Listener Clone a Gerrit Listener Performs POST /codestream/api/gerrit-listeners/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Description for the cloned entity.
    :param string name: (optional in body) Name field for the cloned entity.
    :param string requestType: (optional in body) Action to be performed on the service.
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-listeners/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "requestType" in kwargs:
            hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
            body["requestType"] = kwargs.get("requestType")
            del kwargs["requestType"]

        ret = api.clone_gerrit_listener_using_post(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.clone_gerrit_listener_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def clone_gerrit_trigger_by_name_using_post(
    hub, ctx, p_name, p_project, **kwargs
):
    """Clone a Gerrit Trigger by project and name Clone a Gerrit Trigger with the given project and name Performs POST /codestream/api/gerrit-triggers/{project}/{name}


    :param string p_name: (required in path) The name of the Gerrit Trigger
    :param string p_project: (required in path) The project the Gerrit Trigger belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Description for the cloned entity.
    :param string name: (optional in body) Name field for the cloned entity.
    :param string requestType: (optional in body) Action to be performed on the service.
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-triggers/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "requestType" in kwargs:
            hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
            body["requestType"] = kwargs.get("requestType")
            del kwargs["requestType"]

        ret = api.clone_gerrit_trigger_by_name_using_post(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.clone_gerrit_trigger_by_name_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def clone_gerrit_trigger_using_post(hub, ctx, p_id, **kwargs):
    """Clone a Gerrit Trigger Clone a Gerrit Trigger Performs POST /codestream/api/gerrit-triggers/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Description for the cloned entity.
    :param string name: (optional in body) Name field for the cloned entity.
    :param string requestType: (optional in body) Action to be performed on the service.
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-triggers/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "requestType" in kwargs:
            hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
            body["requestType"] = kwargs.get("requestType")
            del kwargs["requestType"]

        ret = api.clone_gerrit_trigger_using_post(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.clone_gerrit_trigger_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_docker_registry_webhook_using_post(hub, ctx, name, **kwargs):
    """Create a Docker Registry Webhook Create a Docker Registry Webhook Performs POST /codestream/api/registry-webhooks


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Docker webhook description.
    :param boolean enabled: (optional in body) Indicates whether Docker webhook is enabled or not.
    :param string endpoint: (optional in body) Docker endpoint.
    :param string externalListenerLink: (optional in body) Docker webhook listener link.
    :param string imageNameRegExPattern: (optional in body) If provided then the pipeline execution is triggered only when the
      given image name regex matches the image name in the received payload.
    :param object input: (optional in body) Pipeline Execution input properties.
    :param string pipeline: (optional in body) Pipeline name which is meant to be triggered when a docker event
      occur.
    :param string project: (optional in body) The project this entity belongs to.
    :param string refreshToken: (optional in body) Codestream API token.
    :param string repoName: (optional in body) Docker Repo Name.
    :param string secretToken: (optional in body) Secret token to validate received payloads.
    :param string serverType: (optional in body) Docker server type.
    :param string slug: (optional in body) Docker webhook name.
    :param string tagNamePattern: (optional in body) If provided then the pipeline execution is triggered only when the
      given tag name regex matches the tag name(s) in the received payload.
    """

    try:

        hub.log.debug("POST /codestream/api/registry-webhooks")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "endpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'endpoint' = {kwargs['endpoint']}")
            body["endpoint"] = kwargs.get("endpoint")
            del kwargs["endpoint"]
        if "externalListenerLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'externalListenerLink' = {kwargs['externalListenerLink']}"
            )
            body["externalListenerLink"] = kwargs.get("externalListenerLink")
            del kwargs["externalListenerLink"]
        if "imageNameRegExPattern" in kwargs:
            hub.log.debug(
                f"Got kwarg 'imageNameRegExPattern' = {kwargs['imageNameRegExPattern']}"
            )
            body["imageNameRegExPattern"] = kwargs.get("imageNameRegExPattern")
            del kwargs["imageNameRegExPattern"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "pipeline" in kwargs:
            hub.log.debug(f"Got kwarg 'pipeline' = {kwargs['pipeline']}")
            body["pipeline"] = kwargs.get("pipeline")
            del kwargs["pipeline"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "refreshToken" in kwargs:
            hub.log.debug(f"Got kwarg 'refreshToken' = {kwargs['refreshToken']}")
            body["refreshToken"] = kwargs.get("refreshToken")
            del kwargs["refreshToken"]
        if "repoName" in kwargs:
            hub.log.debug(f"Got kwarg 'repoName' = {kwargs['repoName']}")
            body["repoName"] = kwargs.get("repoName")
            del kwargs["repoName"]
        if "secretToken" in kwargs:
            hub.log.debug(f"Got kwarg 'secretToken' = {kwargs['secretToken']}")
            body["secretToken"] = kwargs.get("secretToken")
            del kwargs["secretToken"]
        if "serverType" in kwargs:
            hub.log.debug(f"Got kwarg 'serverType' = {kwargs['serverType']}")
            body["serverType"] = kwargs.get("serverType")
            del kwargs["serverType"]
        if "slug" in kwargs:
            hub.log.debug(f"Got kwarg 'slug' = {kwargs['slug']}")
            body["slug"] = kwargs.get("slug")
            del kwargs["slug"]
        if "tagNamePattern" in kwargs:
            hub.log.debug(f"Got kwarg 'tagNamePattern' = {kwargs['tagNamePattern']}")
            body["tagNamePattern"] = kwargs.get("tagNamePattern")
            del kwargs["tagNamePattern"]

        ret = api.create_docker_registry_webhook_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.create_docker_registry_webhook_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_gerrit_listener_using_post(
    hub, ctx, apiToken, description, endpoint, name, **kwargs
):
    """Create a Gerrit Listener Create a Gerrit Listener Performs POST /codestream/api/gerrit-listeners


    :param string apiToken: (required in body) This token is used to authenticate when calling VMware Cloud Services
      APIs. These tokens are scoped within the organization.
    :param string description: (required in body) A human-friendly description.
    :param string endpoint: (required in body) The name of the Gerrit Endpoint.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean connected: (optional in body) Indicates whether the connection with the Gerrit Server to start
      receiving events is created or not.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-listeners")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["apiToken"] = apiToken
        body["description"] = description
        body["endpoint"] = endpoint
        body["name"] = name

        if "connected" in kwargs:
            hub.log.debug(f"Got kwarg 'connected' = {kwargs['connected']}")
            body["connected"] = kwargs.get("connected")
            del kwargs["connected"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.create_gerrit_listener_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.create_gerrit_listener_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_gerrit_trigger_using_post(
    hub, ctx, branch, description, gerritProject, listener, name, **kwargs
):
    """Create a Gerrit Trigger Create a Gerrit Trigger Performs POST /codestream/api/gerrit-triggers


    :param string branch: (required in body) Gerrit Project Branch on which the change has to be monitored.
    :param string description: (required in body) A human-friendly description.
    :param string gerritProject: (required in body) Gerrit Project which is to be monitored for the change.
    :param string listener: (required in body) Gerrit Listener which will receive the events for this trigger.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param array configurations: (optional in body) The configuration of an Gerrit Event.
    :param boolean enabled: (optional in body) Indicates that this trigger will be addressed on receiving respective
      events.
    :param array exclusions: (optional in body) Provide file exclusions as conditions for the trigger.
    :param array inclusions: (optional in body) Provide file inclusions as conditions for the trigger.
    :param boolean prioritizeExclusion: (optional in body) Prioritize Exclusion ensures that pipelines are not triggered even if
      any of the files in a commit match the specified files in the
      exclusion paths or regex.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-triggers")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["branch"] = branch
        body["description"] = description
        body["gerritProject"] = gerritProject
        body["listener"] = listener
        body["name"] = name

        if "configurations" in kwargs:
            hub.log.debug(f"Got kwarg 'configurations' = {kwargs['configurations']}")
            body["configurations"] = kwargs.get("configurations")
            del kwargs["configurations"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "exclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'exclusions' = {kwargs['exclusions']}")
            body["exclusions"] = kwargs.get("exclusions")
            del kwargs["exclusions"]
        if "inclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'inclusions' = {kwargs['inclusions']}")
            body["inclusions"] = kwargs.get("inclusions")
            del kwargs["inclusions"]
        if "prioritizeExclusion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prioritizeExclusion' = {kwargs['prioritizeExclusion']}"
            )
            body["prioritizeExclusion"] = kwargs.get("prioritizeExclusion")
            del kwargs["prioritizeExclusion"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.create_gerrit_trigger_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.create_gerrit_trigger_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_git_webhook_using_post(hub, ctx, name, **kwargs):
    """Create a Git Webhook Create a Git Webhook Performs POST /codestream/api/git-webhooks


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string branchName: (optional in body) Branch name for which the webhook has been configured.
    :param integer delayTimeInMins: (optional in body) Delay time after which the pipeline execution is triggered.
    :param string description: (optional in body) A human-friendly description.
    :param string endpoint: (optional in body) Git endpoint.
    :param string eventCategory: (optional in body) Git event type.
    :param array exclusions: (optional in body) Provide file exclusions as conditions for the trigger.
    :param string externalListenerLink: (optional in body) Git webhook listener link.
    :param array inclusions: (optional in body) Provide file inclusions as conditions for the trigger.
    :param object input: (optional in body) Pipeline Execution input properties.
    :param boolean insecureSsl: (optional in body) verify SSL certificates when delivering payloads
    :param string pipeline: (optional in body) Pipeline name which is meant to be triggered when a git event occur.
    :param boolean prioritizeExclusion: (optional in body) Prioritize Exclusion ensures that pipelines are not triggered even if
      any ” +
      “of the files in a commit match the specified files in the
      exclusion paths or regex.
    :param string project: (optional in body) The project this entity belongs to.
    :param string refreshToken: (optional in body) Codestream API token.
    :param string repoName: (optional in body) Repo name for which the webhook has been configured.
    :param string secretToken: (optional in body) Secret token to validate received payloads.
    :param string serverType: (optional in body) Git Server Type.
    :param string serverWebhookId: (optional in body) Git webhook id.
    """

    try:

        hub.log.debug("POST /codestream/api/git-webhooks")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "branchName" in kwargs:
            hub.log.debug(f"Got kwarg 'branchName' = {kwargs['branchName']}")
            body["branchName"] = kwargs.get("branchName")
            del kwargs["branchName"]
        if "delayTimeInMins" in kwargs:
            hub.log.debug(f"Got kwarg 'delayTimeInMins' = {kwargs['delayTimeInMins']}")
            body["delayTimeInMins"] = kwargs.get("delayTimeInMins")
            del kwargs["delayTimeInMins"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "endpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'endpoint' = {kwargs['endpoint']}")
            body["endpoint"] = kwargs.get("endpoint")
            del kwargs["endpoint"]
        if "eventCategory" in kwargs:
            hub.log.debug(f"Got kwarg 'eventCategory' = {kwargs['eventCategory']}")
            body["eventCategory"] = kwargs.get("eventCategory")
            del kwargs["eventCategory"]
        if "exclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'exclusions' = {kwargs['exclusions']}")
            body["exclusions"] = kwargs.get("exclusions")
            del kwargs["exclusions"]
        if "externalListenerLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'externalListenerLink' = {kwargs['externalListenerLink']}"
            )
            body["externalListenerLink"] = kwargs.get("externalListenerLink")
            del kwargs["externalListenerLink"]
        if "inclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'inclusions' = {kwargs['inclusions']}")
            body["inclusions"] = kwargs.get("inclusions")
            del kwargs["inclusions"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "insecureSsl" in kwargs:
            hub.log.debug(f"Got kwarg 'insecureSsl' = {kwargs['insecureSsl']}")
            body["insecureSsl"] = kwargs.get("insecureSsl")
            del kwargs["insecureSsl"]
        if "pipeline" in kwargs:
            hub.log.debug(f"Got kwarg 'pipeline' = {kwargs['pipeline']}")
            body["pipeline"] = kwargs.get("pipeline")
            del kwargs["pipeline"]
        if "prioritizeExclusion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prioritizeExclusion' = {kwargs['prioritizeExclusion']}"
            )
            body["prioritizeExclusion"] = kwargs.get("prioritizeExclusion")
            del kwargs["prioritizeExclusion"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "refreshToken" in kwargs:
            hub.log.debug(f"Got kwarg 'refreshToken' = {kwargs['refreshToken']}")
            body["refreshToken"] = kwargs.get("refreshToken")
            del kwargs["refreshToken"]
        if "repoName" in kwargs:
            hub.log.debug(f"Got kwarg 'repoName' = {kwargs['repoName']}")
            body["repoName"] = kwargs.get("repoName")
            del kwargs["repoName"]
        if "secretToken" in kwargs:
            hub.log.debug(f"Got kwarg 'secretToken' = {kwargs['secretToken']}")
            body["secretToken"] = kwargs.get("secretToken")
            del kwargs["secretToken"]
        if "serverType" in kwargs:
            hub.log.debug(f"Got kwarg 'serverType' = {kwargs['serverType']}")
            body["serverType"] = kwargs.get("serverType")
            del kwargs["serverType"]
        if "serverWebhookId" in kwargs:
            hub.log.debug(f"Got kwarg 'serverWebhookId' = {kwargs['serverWebhookId']}")
            body["serverWebhookId"] = kwargs.get("serverWebhookId")
            del kwargs["serverWebhookId"]

        ret = api.create_git_webhook_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.create_git_webhook_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_docker_registry_event_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Docker Registry Event Delete a Docker Registry Event Performs DELETE /codestream/api/registry-events/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/registry-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_docker_registry_event_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_docker_registry_event_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_docker_registry_webhook_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Docker Registry Webhook Delete a Docker Registry Webhook Performs DELETE /codestream/api/registry-webhooks/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/registry-webhooks/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_docker_registry_webhook_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_docker_registry_webhook_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_docker_registry_webhook_by_name_using_delete(
    hub, ctx, p_name, p_project, **kwargs
):
    """Delete a Docker Registry Webhook using name Delete a Docker Registry Webhook using name Performs DELETE /codestream/api/registry-webhooks/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/registry-webhooks/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_docker_registry_webhook_by_name_using_delete(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_docker_registry_webhook_by_name_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_gerrit_event_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Gerrit Event Delete a Gerrit Event Performs DELETE /codestream/api/gerrit-events/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/gerrit-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_gerrit_event_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_gerrit_event_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_gerrit_listener_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Gerrit Listener with id Delete a Gerrit Listener with id Performs DELETE /codestream/api/gerrit-listeners/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/gerrit-listeners/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_gerrit_listener_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_gerrit_listener_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_gerrit_listener_by_name_using_delete(
    hub, ctx, p_name, p_project, **kwargs
):
    """Delete a Gerrit Listener with project and name Delete a Gerrit Listener with project and name Performs DELETE /codestream/api/gerrit-listeners/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/gerrit-listeners/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_gerrit_listener_by_name_using_delete(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_gerrit_listener_by_name_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_gerrit_trigger_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Gerrit Trigger with id Delete a Gerrit Trigger with id Performs DELETE /codestream/api/gerrit-triggers/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/gerrit-triggers/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_gerrit_trigger_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_gerrit_trigger_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_gerrit_trigger_by_name_using_delete(
    hub, ctx, p_name, p_project, **kwargs
):
    """Delete a Gerrit Trigger with project and name Delete a Gerrit Trigger with project and name Performs DELETE /codestream/api/gerrit-triggers/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/gerrit-triggers/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_gerrit_trigger_by_name_using_delete(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_gerrit_trigger_by_name_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_git_events_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Git Event Delete a Git Event Performs DELETE /codestream/api/git-events/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/git-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_git_events_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_git_events_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_git_webhook_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Git Webhook Delete a Git Webhook Performs DELETE /codestream/api/git-webhooks/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/git-webhooks/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_git_webhook_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_git_webhook_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_git_webhook_by_name_using_delete(
    hub, ctx, p_name, p_project, **kwargs
):
    """Delete a Git Webhook by name Delete a Git Webhook by name Performs DELETE /codestream/api/git-webhooks/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/git-webhooks/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_git_webhook_by_name_using_delete(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.delete_git_webhook_by_name_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_docker_registry_events_using_get(hub, ctx, **kwargs):
    """Get all Docker Registry Events Get all Docker Registry Events Performs GET /codestream/api/registry-events


    :param string filter: (optional in query) To list Docker Registry Events with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Docker Registry Events for listing
    :param string top: (optional in query) To list top n Docker Registry Events for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/registry-events")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_docker_registry_events_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_all_docker_registry_events_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_docker_registry_webhooks_using_get(hub, ctx, **kwargs):
    """Get all Docker Registry Webhooks Get all Docker Registry Webhooks Performs GET /codestream/api/registry-webhooks


    :param string filter: (optional in query) To list Docker Registry Webhooks with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Docker Registry Webhooks for listing
    :param string top: (optional in query) To list top n Docker Registry Webhooks for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/registry-webhooks")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_docker_registry_webhooks_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_all_docker_registry_webhooks_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_gerrit_events_using_get(hub, ctx, **kwargs):
    """Get all Gerrit Events Get all Gerrit Events Performs GET /codestream/api/gerrit-events


    :param string filter: (optional in query) To list gerrit events with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n gerrit events for listing
    :param string top: (optional in query) To list top n gerrit events for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-events")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_gerrit_events_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_all_gerrit_events_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_gerrit_listeners_using_get(hub, ctx, **kwargs):
    """Get all gerrit listeners Get all gerrit listeners Performs GET /codestream/api/gerrit-listeners


    :param string filter: (optional in query) To list gerrit listeners with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n gerrit listeners for listing
    :param string top: (optional in query) To list top n gerrit listeners for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-listeners")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_gerrit_listeners_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_all_gerrit_listeners_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_gerrit_triggers_using_get(hub, ctx, **kwargs):
    """Get all gerrit triggers Get all gerrit triggers Performs GET /codestream/api/gerrit-triggers


    :param string filter: (optional in query) To list gerrit triggers with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n gerrit triggers for listing
    :param string top: (optional in query) To list top n gerrit triggers for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-triggers")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_gerrit_triggers_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_all_gerrit_triggers_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_git_events_using_get(hub, ctx, **kwargs):
    """Get all Git Events Get all Git Events Performs GET /codestream/api/git-events


    :param string filter: (optional in query) To list Git Events with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Git Events for listing
    :param string top: (optional in query) To list top n Git Events for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/git-events")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_git_events_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_all_git_events_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_git_webhooks_using_get(hub, ctx, **kwargs):
    """Get all Git Webhooks Get all Git Webhooks Performs GET /codestream/api/git-webhooks


    :param string filter: (optional in query) To list Git Webhooks with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Git Webhooks for listing
    :param string top: (optional in query) To list top n Git Webhooks for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/git-webhooks")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_git_webhooks_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_all_git_webhooks_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_docker_registry_event_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a Docker Registry Event Get a Docker Registry Event based on id Performs GET /codestream/api/registry-events/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/registry-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_docker_registry_event_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_docker_registry_event_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_docker_registry_webhook_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a Docker Registry Webhook Get a Docker Registry Webhook based on id Performs GET /codestream/api/registry-webhooks/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/registry-webhooks/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_docker_registry_webhook_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_docker_registry_webhook_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_docker_registry_webhook_name_using_get(
    hub, ctx, p_name, p_project, **kwargs
):
    """Get a Docker Registry Webhook by name Get a Docker Registry Webhook by Name Performs GET /codestream/api/registry-webhooks/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/registry-webhooks/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_docker_registry_webhook_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_docker_registry_webhook_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gerrit_event_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a gerrit Event Get a gerrit Event based on id Performs GET /codestream/api/gerrit-events/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_gerrit_event_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_gerrit_event_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gerrit_listener_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a gerrit listener Get a gerrit listeners based on id Performs GET /codestream/api/gerrit-listeners/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-listeners/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_gerrit_listener_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_gerrit_listener_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gerrit_listener_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Get a gerrit listener by project and name Get a gerrit listener based on project and name Performs GET /codestream/api/gerrit-listeners/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-listeners/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_gerrit_listener_by_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_gerrit_listener_by_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gerrit_projects_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get Gerrit Projects from the Gerrit Listener using id Get Gerrit Projects from the Gerrit Listener using id Performs GET /codestream/api/gerrit-listeners/{id}/projects


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-listeners/{id}/projects")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_gerrit_projects_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_gerrit_projects_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gerrit_projects_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Get Gerrit Projects from the Gerrit Listener using project and name Get Gerrit Projects from the Gerrit Listener using project and name Performs GET /codestream/api/gerrit-listeners/{project}/{name}/projects


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-listeners/{project}/{name}/projects")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_gerrit_projects_by_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_gerrit_projects_by_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gerrit_trigger_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a gerrit trigger Get a gerrit trigger based on id Performs GET /codestream/api/gerrit-triggers/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-triggers/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_gerrit_trigger_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_gerrit_trigger_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gerrit_trigger_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Get a gerrit trigger by project and name Get a gerrit trigger based on project and name Performs GET /codestream/api/gerrit-triggers/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/gerrit-triggers/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_gerrit_trigger_by_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_gerrit_trigger_by_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_git_event_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a Git Event Get a Git Event based on id Performs GET /codestream/api/git-events/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/git-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_git_event_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_git_event_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_git_webhook_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a Git Webhook Get a Git Webhook based on id Performs GET /codestream/api/git-webhooks/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/git-webhooks/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_git_webhook_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_git_webhook_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_git_webhook_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Get a Git Webhook by name Get a Git Webhook by Name Performs GET /codestream/api/git-webhooks/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/git-webhooks/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_git_webhook_by_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.get_git_webhook_by_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def manual_gerrit_event_trigger_using_post(
    hub, ctx, changeId, project, trigger, **kwargs
):
    """Manual trigger Gerrit event Manual trigger Gerrit event Performs POST /codestream/api/gerrit-events


    :param string changeId: (required in body) The ChangeSet Id corresponding to which the manual trigger has to be
      performed.
    :param string project: (required in body) The tango project to which Gerrit Trigger belongs to.
    :param string trigger: (required in body) Name of the Gerrit Trigger for which event has to be analysed.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-events")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["changeId"] = changeId
        body["project"] = project
        body["trigger"] = trigger

        ret = api.manual_gerrit_event_trigger_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.manual_gerrit_event_trigger_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_docker_registry_webhook_by_id_using_patch(hub, ctx, p_id, **kwargs):
    """Patch a Docker Registry Webhook with id Patch a Docker Registry Webhook with id Performs PATCH /codestream/api/registry-webhooks/{id}


    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean enabled: (optional in body) Indicates whether Git/Docker webhook is enabled or not.
    """

    try:

        hub.log.debug("PATCH /codestream/api/registry-webhooks/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]

        ret = api.patch_docker_registry_webhook_by_id_using_patch(
            body, id=p_id, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.patch_docker_registry_webhook_by_id_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_docker_registry_webhook_by_name_using_patch(
    hub, ctx, p_name, p_project, **kwargs
):
    """Patch a Docker Registry Webhook with project and name Patch a Docker Registry Webhook with project and name Performs PATCH /codestream/api/registry-webhooks/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean enabled: (optional in body) Indicates whether Git/Docker webhook is enabled or not.
    """

    try:

        hub.log.debug("PATCH /codestream/api/registry-webhooks/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]

        ret = api.patch_docker_registry_webhook_by_name_using_patch(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.patch_docker_registry_webhook_by_name_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_gerrit_listener_by_id_using_patch(hub, ctx, p_id, connected, **kwargs):
    """Patch a Gerrit Listener with id Patch a Gerrit Listener with id Performs PATCH /codestream/api/gerrit-listeners/{id}


    :param string p_id: (required in path) id
    :param boolean connected: (required in body) Connect/Disconnect a Gerrit Listener.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("PATCH /codestream/api/gerrit-listeners/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["connected"] = connected

        ret = api.patch_gerrit_listener_by_id_using_patch(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.patch_gerrit_listener_by_id_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_gerrit_listener_by_name_using_patch(
    hub, ctx, p_name, p_project, connected, **kwargs
):
    """Patch a Gerrit Listener with project and name Patch a Gerrit Listener with project and name Performs PATCH /codestream/api/gerrit-listeners/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param boolean connected: (required in body) Connect/Disconnect a Gerrit Listener.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("PATCH /codestream/api/gerrit-listeners/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["connected"] = connected

        ret = api.patch_gerrit_listener_by_name_using_patch(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.patch_gerrit_listener_by_name_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_gerrit_trigger_by_id_using_patch(hub, ctx, p_id, enabled, **kwargs):
    """Patch a Gerrit Trigger with id Patch a Gerrit Trigger with id Performs PATCH /codestream/api/gerrit-triggers/{id}


    :param string p_id: (required in path) id
    :param boolean enabled: (required in body) Enable/Disable a Gerrit Trigger.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("PATCH /codestream/api/gerrit-triggers/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["enabled"] = enabled

        ret = api.patch_gerrit_trigger_by_id_using_patch(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.patch_gerrit_trigger_by_id_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_gerrit_trigger_by_name_using_patch(
    hub, ctx, p_name, p_project, enabled, **kwargs
):
    """Patch a Gerrit Trigger with project and name Patch a Gerrit Trigger with project and name Performs PATCH /codestream/api/gerrit-triggers/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param boolean enabled: (required in body) Enable/Disable a Gerrit Trigger.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("PATCH /codestream/api/gerrit-triggers/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["enabled"] = enabled

        ret = api.patch_gerrit_trigger_by_name_using_patch(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.patch_gerrit_trigger_by_name_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def resume_or_rerun_docker_registry_event_using_post(
    hub, ctx, q_action, p_id, **kwargs
):
    """Re-run/Resume Docker Registry Event Re-run/Resume Docker Registry Event Performs POST /codestream/api/registry-events/{id}


    :param string q_action: (required in query) Resume/Rerun
    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("POST /codestream/api/registry-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.resume_or_rerun_docker_registry_event_using_post(
            action=q_action, id=p_id, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.resume_or_rerun_docker_registry_event_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def resume_or_rerun_gerrit_event_using_post(hub, ctx, q_action, p_id, **kwargs):
    """Re-run/Resume Gerrit event Re-run/Resume Gerrit event Performs POST /codestream/api/gerrit-events/{id}


    :param string q_action: (required in query) Resume/Rerun
    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.resume_or_rerun_gerrit_event_using_post(
            action=q_action, id=p_id, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.resume_or_rerun_gerrit_event_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def resume_or_rerun_git_event_using_post(hub, ctx, q_action, p_id, **kwargs):
    """Re-run/Resume Git Event Re-run/Resume Git Event Performs POST /codestream/api/git-events/{id}


    :param string q_action: (required in query) Resume/Rerun
    :param string p_id: (required in path) id
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("POST /codestream/api/git-events/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.resume_or_rerun_git_event_using_post(
            action=q_action, id=p_id, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.resume_or_rerun_git_event_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_docker_registry_webhook_by_id_using_put(
    hub, ctx, p_id, name, **kwargs
):
    """Update a Docker Registry Webhook Update a Docker Registry Webhook Performs PUT /codestream/api/registry-webhooks/{id}


    :param string p_id: (required in path) id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Docker webhook description.
    :param boolean enabled: (optional in body) Indicates whether Docker webhook is enabled or not.
    :param string endpoint: (optional in body) Docker endpoint.
    :param string externalListenerLink: (optional in body) Docker webhook listener link.
    :param string imageNameRegExPattern: (optional in body) If provided then the pipeline execution is triggered only when the
      given image name regex matches the image name in the received payload.
    :param object input: (optional in body) Pipeline Execution input properties.
    :param string pipeline: (optional in body) Pipeline name which is meant to be triggered when a docker event
      occur.
    :param string project: (optional in body) The project this entity belongs to.
    :param string refreshToken: (optional in body) Codestream API token.
    :param string repoName: (optional in body) Docker Repo Name.
    :param string secretToken: (optional in body) Secret token to validate received payloads.
    :param string serverType: (optional in body) Docker server type.
    :param string slug: (optional in body) Docker webhook name.
    :param string tagNamePattern: (optional in body) If provided then the pipeline execution is triggered only when the
      given tag name regex matches the tag name(s) in the received payload.
    """

    try:

        hub.log.debug("PUT /codestream/api/registry-webhooks/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "endpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'endpoint' = {kwargs['endpoint']}")
            body["endpoint"] = kwargs.get("endpoint")
            del kwargs["endpoint"]
        if "externalListenerLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'externalListenerLink' = {kwargs['externalListenerLink']}"
            )
            body["externalListenerLink"] = kwargs.get("externalListenerLink")
            del kwargs["externalListenerLink"]
        if "imageNameRegExPattern" in kwargs:
            hub.log.debug(
                f"Got kwarg 'imageNameRegExPattern' = {kwargs['imageNameRegExPattern']}"
            )
            body["imageNameRegExPattern"] = kwargs.get("imageNameRegExPattern")
            del kwargs["imageNameRegExPattern"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "pipeline" in kwargs:
            hub.log.debug(f"Got kwarg 'pipeline' = {kwargs['pipeline']}")
            body["pipeline"] = kwargs.get("pipeline")
            del kwargs["pipeline"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "refreshToken" in kwargs:
            hub.log.debug(f"Got kwarg 'refreshToken' = {kwargs['refreshToken']}")
            body["refreshToken"] = kwargs.get("refreshToken")
            del kwargs["refreshToken"]
        if "repoName" in kwargs:
            hub.log.debug(f"Got kwarg 'repoName' = {kwargs['repoName']}")
            body["repoName"] = kwargs.get("repoName")
            del kwargs["repoName"]
        if "secretToken" in kwargs:
            hub.log.debug(f"Got kwarg 'secretToken' = {kwargs['secretToken']}")
            body["secretToken"] = kwargs.get("secretToken")
            del kwargs["secretToken"]
        if "serverType" in kwargs:
            hub.log.debug(f"Got kwarg 'serverType' = {kwargs['serverType']}")
            body["serverType"] = kwargs.get("serverType")
            del kwargs["serverType"]
        if "slug" in kwargs:
            hub.log.debug(f"Got kwarg 'slug' = {kwargs['slug']}")
            body["slug"] = kwargs.get("slug")
            del kwargs["slug"]
        if "tagNamePattern" in kwargs:
            hub.log.debug(f"Got kwarg 'tagNamePattern' = {kwargs['tagNamePattern']}")
            body["tagNamePattern"] = kwargs.get("tagNamePattern")
            del kwargs["tagNamePattern"]

        ret = api.update_docker_registry_webhook_by_id_using_put(
            body, id=p_id, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_docker_registry_webhook_by_id_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_docker_registry_webhook_by_name_using_put(
    hub, ctx, p_name, p_project, name, **kwargs
):
    """Update a Docker Registry Webhook using name Update a Docker Registry Webhook using name Performs PUT /codestream/api/registry-webhooks/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Docker webhook description.
    :param boolean enabled: (optional in body) Indicates whether Docker webhook is enabled or not.
    :param string endpoint: (optional in body) Docker endpoint.
    :param string externalListenerLink: (optional in body) Docker webhook listener link.
    :param string imageNameRegExPattern: (optional in body) If provided then the pipeline execution is triggered only when the
      given image name regex matches the image name in the received payload.
    :param object input: (optional in body) Pipeline Execution input properties.
    :param string pipeline: (optional in body) Pipeline name which is meant to be triggered when a docker event
      occur.
    :param string project: (optional in body) The project this entity belongs to.
    :param string refreshToken: (optional in body) Codestream API token.
    :param string repoName: (optional in body) Docker Repo Name.
    :param string secretToken: (optional in body) Secret token to validate received payloads.
    :param string serverType: (optional in body) Docker server type.
    :param string slug: (optional in body) Docker webhook name.
    :param string tagNamePattern: (optional in body) If provided then the pipeline execution is triggered only when the
      given tag name regex matches the tag name(s) in the received payload.
    """

    try:

        hub.log.debug("PUT /codestream/api/registry-webhooks/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "endpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'endpoint' = {kwargs['endpoint']}")
            body["endpoint"] = kwargs.get("endpoint")
            del kwargs["endpoint"]
        if "externalListenerLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'externalListenerLink' = {kwargs['externalListenerLink']}"
            )
            body["externalListenerLink"] = kwargs.get("externalListenerLink")
            del kwargs["externalListenerLink"]
        if "imageNameRegExPattern" in kwargs:
            hub.log.debug(
                f"Got kwarg 'imageNameRegExPattern' = {kwargs['imageNameRegExPattern']}"
            )
            body["imageNameRegExPattern"] = kwargs.get("imageNameRegExPattern")
            del kwargs["imageNameRegExPattern"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "pipeline" in kwargs:
            hub.log.debug(f"Got kwarg 'pipeline' = {kwargs['pipeline']}")
            body["pipeline"] = kwargs.get("pipeline")
            del kwargs["pipeline"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "refreshToken" in kwargs:
            hub.log.debug(f"Got kwarg 'refreshToken' = {kwargs['refreshToken']}")
            body["refreshToken"] = kwargs.get("refreshToken")
            del kwargs["refreshToken"]
        if "repoName" in kwargs:
            hub.log.debug(f"Got kwarg 'repoName' = {kwargs['repoName']}")
            body["repoName"] = kwargs.get("repoName")
            del kwargs["repoName"]
        if "secretToken" in kwargs:
            hub.log.debug(f"Got kwarg 'secretToken' = {kwargs['secretToken']}")
            body["secretToken"] = kwargs.get("secretToken")
            del kwargs["secretToken"]
        if "serverType" in kwargs:
            hub.log.debug(f"Got kwarg 'serverType' = {kwargs['serverType']}")
            body["serverType"] = kwargs.get("serverType")
            del kwargs["serverType"]
        if "slug" in kwargs:
            hub.log.debug(f"Got kwarg 'slug' = {kwargs['slug']}")
            body["slug"] = kwargs.get("slug")
            del kwargs["slug"]
        if "tagNamePattern" in kwargs:
            hub.log.debug(f"Got kwarg 'tagNamePattern' = {kwargs['tagNamePattern']}")
            body["tagNamePattern"] = kwargs.get("tagNamePattern")
            del kwargs["tagNamePattern"]

        ret = api.update_docker_registry_webhook_by_name_using_put(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_docker_registry_webhook_by_name_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_gerrit_listener_by_id_using_put(
    hub, ctx, p_id, apiToken, description, endpoint, name, **kwargs
):
    """Update a Gerrit Listener with id Update a Gerrit Listener with id Performs PUT /codestream/api/gerrit-listeners/{id}


    :param string p_id: (required in path) id
    :param string apiToken: (required in body) This token is used to authenticate when calling VMware Cloud Services
      APIs. These tokens are scoped within the organization.
    :param string description: (required in body) A human-friendly description.
    :param string endpoint: (required in body) The name of the Gerrit Endpoint.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean connected: (optional in body) Indicates whether the connection with the Gerrit Server to start
      receiving events is created or not.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("PUT /codestream/api/gerrit-listeners/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["apiToken"] = apiToken
        body["description"] = description
        body["endpoint"] = endpoint
        body["name"] = name

        if "connected" in kwargs:
            hub.log.debug(f"Got kwarg 'connected' = {kwargs['connected']}")
            body["connected"] = kwargs.get("connected")
            del kwargs["connected"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.update_gerrit_listener_by_id_using_put(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_gerrit_listener_by_id_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_gerrit_listener_by_name_using_put(
    hub, ctx, p_name, p_project, apiToken, description, endpoint, name, **kwargs
):
    """Update a Gerrit Listener with project and name Update a Gerrit Listener with project and name Performs PUT /codestream/api/gerrit-listeners/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string apiToken: (required in body) This token is used to authenticate when calling VMware Cloud Services
      APIs. These tokens are scoped within the organization.
    :param string description: (required in body) A human-friendly description.
    :param string endpoint: (required in body) The name of the Gerrit Endpoint.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean connected: (optional in body) Indicates whether the connection with the Gerrit Server to start
      receiving events is created or not.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("PUT /codestream/api/gerrit-listeners/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["apiToken"] = apiToken
        body["description"] = description
        body["endpoint"] = endpoint
        body["name"] = name

        if "connected" in kwargs:
            hub.log.debug(f"Got kwarg 'connected' = {kwargs['connected']}")
            body["connected"] = kwargs.get("connected")
            del kwargs["connected"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.update_gerrit_listener_by_name_using_put(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_gerrit_listener_by_name_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_gerrit_trigger_by_id_using_put(
    hub, ctx, p_id, branch, description, gerritProject, listener, name, **kwargs
):
    """Update a Gerrit Trigger with id Update a Gerrit Trigger with id Performs PUT /codestream/api/gerrit-triggers/{id}


    :param string p_id: (required in path) id
    :param string branch: (required in body) Gerrit Project Branch on which the change has to be monitored.
    :param string description: (required in body) A human-friendly description.
    :param string gerritProject: (required in body) Gerrit Project which is to be monitored for the change.
    :param string listener: (required in body) Gerrit Listener which will receive the events for this trigger.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param array configurations: (optional in body) The configuration of an Gerrit Event.
    :param boolean enabled: (optional in body) Indicates that this trigger will be addressed on receiving respective
      events.
    :param array exclusions: (optional in body) Provide file exclusions as conditions for the trigger.
    :param array inclusions: (optional in body) Provide file inclusions as conditions for the trigger.
    :param boolean prioritizeExclusion: (optional in body) Prioritize Exclusion ensures that pipelines are not triggered even if
      any of the files in a commit match the specified files in the
      exclusion paths or regex.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("PUT /codestream/api/gerrit-triggers/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["branch"] = branch
        body["description"] = description
        body["gerritProject"] = gerritProject
        body["listener"] = listener
        body["name"] = name

        if "configurations" in kwargs:
            hub.log.debug(f"Got kwarg 'configurations' = {kwargs['configurations']}")
            body["configurations"] = kwargs.get("configurations")
            del kwargs["configurations"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "exclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'exclusions' = {kwargs['exclusions']}")
            body["exclusions"] = kwargs.get("exclusions")
            del kwargs["exclusions"]
        if "inclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'inclusions' = {kwargs['inclusions']}")
            body["inclusions"] = kwargs.get("inclusions")
            del kwargs["inclusions"]
        if "prioritizeExclusion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prioritizeExclusion' = {kwargs['prioritizeExclusion']}"
            )
            body["prioritizeExclusion"] = kwargs.get("prioritizeExclusion")
            del kwargs["prioritizeExclusion"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.update_gerrit_trigger_by_id_using_put(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_gerrit_trigger_by_id_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_gerrit_trigger_by_name_using_put(
    hub,
    ctx,
    p_name,
    p_project,
    branch,
    description,
    gerritProject,
    listener,
    name,
    **kwargs,
):
    """Update a Gerrit Trigger with project and name Update a Gerrit Trigger with project and name Performs PUT /codestream/api/gerrit-triggers/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string branch: (required in body) Gerrit Project Branch on which the change has to be monitored.
    :param string description: (required in body) A human-friendly description.
    :param string gerritProject: (required in body) Gerrit Project which is to be monitored for the change.
    :param string listener: (required in body) Gerrit Listener which will receive the events for this trigger.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param array configurations: (optional in body) The configuration of an Gerrit Event.
    :param boolean enabled: (optional in body) Indicates that this trigger will be addressed on receiving respective
      events.
    :param array exclusions: (optional in body) Provide file exclusions as conditions for the trigger.
    :param array inclusions: (optional in body) Provide file inclusions as conditions for the trigger.
    :param boolean prioritizeExclusion: (optional in body) Prioritize Exclusion ensures that pipelines are not triggered even if
      any of the files in a commit match the specified files in the
      exclusion paths or regex.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("PUT /codestream/api/gerrit-triggers/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["branch"] = branch
        body["description"] = description
        body["gerritProject"] = gerritProject
        body["listener"] = listener
        body["name"] = name

        if "configurations" in kwargs:
            hub.log.debug(f"Got kwarg 'configurations' = {kwargs['configurations']}")
            body["configurations"] = kwargs.get("configurations")
            del kwargs["configurations"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "exclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'exclusions' = {kwargs['exclusions']}")
            body["exclusions"] = kwargs.get("exclusions")
            del kwargs["exclusions"]
        if "inclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'inclusions' = {kwargs['inclusions']}")
            body["inclusions"] = kwargs.get("inclusions")
            del kwargs["inclusions"]
        if "prioritizeExclusion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prioritizeExclusion' = {kwargs['prioritizeExclusion']}"
            )
            body["prioritizeExclusion"] = kwargs.get("prioritizeExclusion")
            del kwargs["prioritizeExclusion"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.update_gerrit_trigger_by_name_using_put(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_gerrit_trigger_by_name_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_git_webhook_by_id_using_put(hub, ctx, p_id, name, **kwargs):
    """Update a Git Webhook Update a Git Webhook Performs PUT /codestream/api/git-webhooks/{id}


    :param string p_id: (required in path) id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string branchName: (optional in body) Branch name for which the webhook has been configured.
    :param integer delayTimeInMins: (optional in body) Delay time after which the pipeline execution is triggered.
    :param string description: (optional in body) A human-friendly description.
    :param string endpoint: (optional in body) Git endpoint.
    :param string eventCategory: (optional in body) Git event type.
    :param array exclusions: (optional in body) Provide file exclusions as conditions for the trigger.
    :param string externalListenerLink: (optional in body) Git webhook listener link.
    :param array inclusions: (optional in body) Provide file inclusions as conditions for the trigger.
    :param object input: (optional in body) Pipeline Execution input properties.
    :param boolean insecureSsl: (optional in body) verify SSL certificates when delivering payloads
    :param string pipeline: (optional in body) Pipeline name which is meant to be triggered when a git event occur.
    :param boolean prioritizeExclusion: (optional in body) Prioritize Exclusion ensures that pipelines are not triggered even if
      any ” +
      “of the files in a commit match the specified files in the
      exclusion paths or regex.
    :param string project: (optional in body) The project this entity belongs to.
    :param string refreshToken: (optional in body) Codestream API token.
    :param string repoName: (optional in body) Repo name for which the webhook has been configured.
    :param string secretToken: (optional in body) Secret token to validate received payloads.
    :param string serverType: (optional in body) Git Server Type.
    :param string serverWebhookId: (optional in body) Git webhook id.
    """

    try:

        hub.log.debug("PUT /codestream/api/git-webhooks/{id}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "branchName" in kwargs:
            hub.log.debug(f"Got kwarg 'branchName' = {kwargs['branchName']}")
            body["branchName"] = kwargs.get("branchName")
            del kwargs["branchName"]
        if "delayTimeInMins" in kwargs:
            hub.log.debug(f"Got kwarg 'delayTimeInMins' = {kwargs['delayTimeInMins']}")
            body["delayTimeInMins"] = kwargs.get("delayTimeInMins")
            del kwargs["delayTimeInMins"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "endpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'endpoint' = {kwargs['endpoint']}")
            body["endpoint"] = kwargs.get("endpoint")
            del kwargs["endpoint"]
        if "eventCategory" in kwargs:
            hub.log.debug(f"Got kwarg 'eventCategory' = {kwargs['eventCategory']}")
            body["eventCategory"] = kwargs.get("eventCategory")
            del kwargs["eventCategory"]
        if "exclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'exclusions' = {kwargs['exclusions']}")
            body["exclusions"] = kwargs.get("exclusions")
            del kwargs["exclusions"]
        if "externalListenerLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'externalListenerLink' = {kwargs['externalListenerLink']}"
            )
            body["externalListenerLink"] = kwargs.get("externalListenerLink")
            del kwargs["externalListenerLink"]
        if "inclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'inclusions' = {kwargs['inclusions']}")
            body["inclusions"] = kwargs.get("inclusions")
            del kwargs["inclusions"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "insecureSsl" in kwargs:
            hub.log.debug(f"Got kwarg 'insecureSsl' = {kwargs['insecureSsl']}")
            body["insecureSsl"] = kwargs.get("insecureSsl")
            del kwargs["insecureSsl"]
        if "pipeline" in kwargs:
            hub.log.debug(f"Got kwarg 'pipeline' = {kwargs['pipeline']}")
            body["pipeline"] = kwargs.get("pipeline")
            del kwargs["pipeline"]
        if "prioritizeExclusion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prioritizeExclusion' = {kwargs['prioritizeExclusion']}"
            )
            body["prioritizeExclusion"] = kwargs.get("prioritizeExclusion")
            del kwargs["prioritizeExclusion"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "refreshToken" in kwargs:
            hub.log.debug(f"Got kwarg 'refreshToken' = {kwargs['refreshToken']}")
            body["refreshToken"] = kwargs.get("refreshToken")
            del kwargs["refreshToken"]
        if "repoName" in kwargs:
            hub.log.debug(f"Got kwarg 'repoName' = {kwargs['repoName']}")
            body["repoName"] = kwargs.get("repoName")
            del kwargs["repoName"]
        if "secretToken" in kwargs:
            hub.log.debug(f"Got kwarg 'secretToken' = {kwargs['secretToken']}")
            body["secretToken"] = kwargs.get("secretToken")
            del kwargs["secretToken"]
        if "serverType" in kwargs:
            hub.log.debug(f"Got kwarg 'serverType' = {kwargs['serverType']}")
            body["serverType"] = kwargs.get("serverType")
            del kwargs["serverType"]
        if "serverWebhookId" in kwargs:
            hub.log.debug(f"Got kwarg 'serverWebhookId' = {kwargs['serverWebhookId']}")
            body["serverWebhookId"] = kwargs.get("serverWebhookId")
            del kwargs["serverWebhookId"]

        ret = api.update_git_webhook_by_id_using_put(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_git_webhook_by_id_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_git_webhook_by_name_using_put(
    hub, ctx, p_name, p_project, name, **kwargs
):
    """Update a Git Webhook using name Update a Git Webhook using name Performs PUT /codestream/api/git-webhooks/{project}/{name}


    :param string p_name: (required in path) name
    :param string p_project: (required in path) project
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string branchName: (optional in body) Branch name for which the webhook has been configured.
    :param integer delayTimeInMins: (optional in body) Delay time after which the pipeline execution is triggered.
    :param string description: (optional in body) A human-friendly description.
    :param string endpoint: (optional in body) Git endpoint.
    :param string eventCategory: (optional in body) Git event type.
    :param array exclusions: (optional in body) Provide file exclusions as conditions for the trigger.
    :param string externalListenerLink: (optional in body) Git webhook listener link.
    :param array inclusions: (optional in body) Provide file inclusions as conditions for the trigger.
    :param object input: (optional in body) Pipeline Execution input properties.
    :param boolean insecureSsl: (optional in body) verify SSL certificates when delivering payloads
    :param string pipeline: (optional in body) Pipeline name which is meant to be triggered when a git event occur.
    :param boolean prioritizeExclusion: (optional in body) Prioritize Exclusion ensures that pipelines are not triggered even if
      any ” +
      “of the files in a commit match the specified files in the
      exclusion paths or regex.
    :param string project: (optional in body) The project this entity belongs to.
    :param string refreshToken: (optional in body) Codestream API token.
    :param string repoName: (optional in body) Repo name for which the webhook has been configured.
    :param string secretToken: (optional in body) Secret token to validate received payloads.
    :param string serverType: (optional in body) Git Server Type.
    :param string serverWebhookId: (optional in body) Git webhook id.
    """

    try:

        hub.log.debug("PUT /codestream/api/git-webhooks/{project}/{name}")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "branchName" in kwargs:
            hub.log.debug(f"Got kwarg 'branchName' = {kwargs['branchName']}")
            body["branchName"] = kwargs.get("branchName")
            del kwargs["branchName"]
        if "delayTimeInMins" in kwargs:
            hub.log.debug(f"Got kwarg 'delayTimeInMins' = {kwargs['delayTimeInMins']}")
            body["delayTimeInMins"] = kwargs.get("delayTimeInMins")
            del kwargs["delayTimeInMins"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "endpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'endpoint' = {kwargs['endpoint']}")
            body["endpoint"] = kwargs.get("endpoint")
            del kwargs["endpoint"]
        if "eventCategory" in kwargs:
            hub.log.debug(f"Got kwarg 'eventCategory' = {kwargs['eventCategory']}")
            body["eventCategory"] = kwargs.get("eventCategory")
            del kwargs["eventCategory"]
        if "exclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'exclusions' = {kwargs['exclusions']}")
            body["exclusions"] = kwargs.get("exclusions")
            del kwargs["exclusions"]
        if "externalListenerLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'externalListenerLink' = {kwargs['externalListenerLink']}"
            )
            body["externalListenerLink"] = kwargs.get("externalListenerLink")
            del kwargs["externalListenerLink"]
        if "inclusions" in kwargs:
            hub.log.debug(f"Got kwarg 'inclusions' = {kwargs['inclusions']}")
            body["inclusions"] = kwargs.get("inclusions")
            del kwargs["inclusions"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "insecureSsl" in kwargs:
            hub.log.debug(f"Got kwarg 'insecureSsl' = {kwargs['insecureSsl']}")
            body["insecureSsl"] = kwargs.get("insecureSsl")
            del kwargs["insecureSsl"]
        if "pipeline" in kwargs:
            hub.log.debug(f"Got kwarg 'pipeline' = {kwargs['pipeline']}")
            body["pipeline"] = kwargs.get("pipeline")
            del kwargs["pipeline"]
        if "prioritizeExclusion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prioritizeExclusion' = {kwargs['prioritizeExclusion']}"
            )
            body["prioritizeExclusion"] = kwargs.get("prioritizeExclusion")
            del kwargs["prioritizeExclusion"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "refreshToken" in kwargs:
            hub.log.debug(f"Got kwarg 'refreshToken' = {kwargs['refreshToken']}")
            body["refreshToken"] = kwargs.get("refreshToken")
            del kwargs["refreshToken"]
        if "repoName" in kwargs:
            hub.log.debug(f"Got kwarg 'repoName' = {kwargs['repoName']}")
            body["repoName"] = kwargs.get("repoName")
            del kwargs["repoName"]
        if "secretToken" in kwargs:
            hub.log.debug(f"Got kwarg 'secretToken' = {kwargs['secretToken']}")
            body["secretToken"] = kwargs.get("secretToken")
            del kwargs["secretToken"]
        if "serverType" in kwargs:
            hub.log.debug(f"Got kwarg 'serverType' = {kwargs['serverType']}")
            body["serverType"] = kwargs.get("serverType")
            del kwargs["serverType"]
        if "serverWebhookId" in kwargs:
            hub.log.debug(f"Got kwarg 'serverWebhookId' = {kwargs['serverWebhookId']}")
            body["serverWebhookId"] = kwargs.get("serverWebhookId")
            del kwargs["serverWebhookId"]

        ret = api.update_git_webhook_by_name_using_put(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.update_git_webhook_by_name_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def validate_gerrit_listener_using_post(
    hub, ctx, apiToken, description, endpoint, name, **kwargs
):
    """Validate a Gerrit Listener Validate a Gerrit Listener Performs POST /codestream/api/gerrit-listeners/validate


    :param string apiToken: (required in body) This token is used to authenticate when calling VMware Cloud Services
      APIs. These tokens are scoped within the organization.
    :param string description: (required in body) A human-friendly description.
    :param string endpoint: (required in body) The name of the Gerrit Endpoint.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param boolean connected: (optional in body) Indicates whether the connection with the Gerrit Server to start
      receiving events is created or not.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("POST /codestream/api/gerrit-listeners/validate")

        api = TriggersApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["apiToken"] = apiToken
        body["description"] = description
        body["endpoint"] = endpoint
        body["name"] = name

        if "connected" in kwargs:
            hub.log.debug(f"Got kwarg 'connected' = {kwargs['connected']}")
            body["connected"] = kwargs.get("connected")
            del kwargs["connected"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.validate_gerrit_listener_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TriggersApi.validate_gerrit_listener_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
