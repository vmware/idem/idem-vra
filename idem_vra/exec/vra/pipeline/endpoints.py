from idem_vra.client.vra_pipeline_lib.api import EndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_endpoint_using_post(hub, ctx, name, properties, type, **kwargs):
    """Create an Endpoint Create an Endpoint based on the given project Performs POST /codestream/api/endpoints


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param object properties: (required in body) Endpoint specific properties
    :param string type: (required in body) The type of this Endpoint instance.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param boolean isRestricted: (optional in body) This type of Endpoint can be created, updated or deleted by admin
      only. If a restricted Endpoint is consumed in a pipeline, and that
      pipeline is executed by a non-admin user, then the execution will fail
      at the task which is consuming this restricted Endpoint. Only admin
      can then resume this pipeline to make it progress.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("POST /codestream/api/endpoints")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name
        body["properties"] = properties
        body["type"] = type

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "isRestricted" in kwargs:
            hub.log.debug(f"Got kwarg 'isRestricted' = {kwargs['isRestricted']}")
            body["isRestricted"] = kwargs.get("isRestricted")
            del kwargs["isRestricted"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.create_endpoint_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.create_endpoint_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_endpoint_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete an Endpoint by id Delete an Endpoint with the given id Performs DELETE /codestream/api/endpoints/{id}


    :param string p_id: (required in path) The ID of the Endpoint
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/endpoints/{id}")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_endpoint_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.delete_endpoint_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_endpoint_by_name_using_delete(hub, ctx, p_name, p_project, **kwargs):
    """Delete an Endpoint by project and name Delete an Endpoint with the given name Performs DELETE /codestream/api/endpoints/{project}/{name}


    :param string p_name: (required in path) The name of the Endpoint
    :param string p_project: (required in path) The project the Endpoint belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/endpoints/{project}/{name}")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_endpoint_by_name_using_delete(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.delete_endpoint_by_name_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_endpoints_using_get(hub, ctx, **kwargs):
    """Get all Endpoints Get all Endpoints with specified paging and filter parameters Performs GET /codestream/api/endpoints


    :param string filter: (optional in query) To list Endpoints with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Endpoints for listing
    :param string top: (optional in query) To list top n Endpoints for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/endpoints")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_endpoints_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.get_all_endpoints_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_endpoint_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get an Endpoint Get an Endpoint with the given id Performs GET /codestream/api/endpoints/{id}


    :param string p_id: (required in path) The ID of the Endpoint
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/endpoints/{id}")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_endpoint_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.get_endpoint_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_endpoint_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Get an Endpoint by project and name Get an Endpoint with the given project and name Performs GET /codestream/api/endpoints/{project}/{name}


    :param string p_name: (required in path) The name of the Endpoint
    :param string p_project: (required in path) The project the Endpoint belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/endpoints/{project}/{name}")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_endpoint_by_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.get_endpoint_by_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_endpoint_properties_using_get(hub, ctx, p_type, **kwargs):
    """Get endpoint properties Get endpoint properties with the given endpoint type Performs GET /codestream/api/endpoint-tiles/{type}


    :param string p_type: (required in path) type
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/endpoint-tiles/{type}")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_endpoint_properties_using_get(type=p_type, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.get_endpoint_properties_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_endpoint_tiles_using_get(hub, ctx, **kwargs):
    """Get all endpoint tiles Get all supported endpoint tiles Performs GET /codestream/api/endpoint-tiles


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/endpoint-tiles")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_endpoint_tiles_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.get_endpoint_tiles_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_using_get(hub, ctx, q_url, **kwargs):
    """Get endpoint certificate Get endpoint certificate chain for validation Performs GET /codestream/api/endpoint-certificate


    :param string q_url: (required in query) URL of the server
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/endpoint-certificate")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_using_get(url=q_url, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.get_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_endpoint_by_id_using_put(
    hub, ctx, p_id, name, properties, type, **kwargs
):
    """Update an Endpoint by id Update an Endpoint with the given id Performs PUT /codestream/api/endpoints/{id}


    :param string p_id: (required in path) The ID of the Endpoint
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param object properties: (required in body) Endpoint specific properties
    :param string type: (required in body) The type of this Endpoint instance.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param boolean isRestricted: (optional in body) This type of Endpoint can be created, updated or deleted by admin
      only. If a restricted Endpoint is consumed in a pipeline, and that
      pipeline is executed by a non-admin user, then the execution will fail
      at the task which is consuming this restricted Endpoint. Only admin
      can then resume this pipeline to make it progress.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("PUT /codestream/api/endpoints/{id}")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name
        body["properties"] = properties
        body["type"] = type

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "isRestricted" in kwargs:
            hub.log.debug(f"Got kwarg 'isRestricted' = {kwargs['isRestricted']}")
            body["isRestricted"] = kwargs.get("isRestricted")
            del kwargs["isRestricted"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.update_endpoint_by_id_using_put(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.update_endpoint_by_id_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_endpoint_by_name_using_put(
    hub, ctx, p_name, p_project, name, properties, type, **kwargs
):
    """Update an Endpoint by project and name Update an Endpoint with the given project and name Performs PUT /codestream/api/endpoints/{project}/{name}


    :param string p_name: (required in path) The name of the Endpoint
    :param string p_project: (required in path) The project the Endpoint belongs to
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param object properties: (required in body) Endpoint specific properties
    :param string type: (required in body) The type of this Endpoint instance.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param boolean isRestricted: (optional in body) This type of Endpoint can be created, updated or deleted by admin
      only. If a restricted Endpoint is consumed in a pipeline, and that
      pipeline is executed by a non-admin user, then the execution will fail
      at the task which is consuming this restricted Endpoint. Only admin
      can then resume this pipeline to make it progress.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("PUT /codestream/api/endpoints/{project}/{name}")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name
        body["properties"] = properties
        body["type"] = type

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "isRestricted" in kwargs:
            hub.log.debug(f"Got kwarg 'isRestricted' = {kwargs['isRestricted']}")
            body["isRestricted"] = kwargs.get("isRestricted")
            del kwargs["isRestricted"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.update_endpoint_by_name_using_put(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.update_endpoint_by_name_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def validate_endpoint_using_post(hub, ctx, name, properties, type, **kwargs):
    """Validate endpoint Validates the given endpoint Performs POST /codestream/api/endpoint-validation


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param object properties: (required in body) Endpoint specific properties
    :param string type: (required in body) The type of this Endpoint instance.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description.
    :param string id: (optional in body) id of the endpoint, if already present
    :param boolean isRestricted: (optional in body) This type of Endpoint can be created, updated or deleted by admin
      only. If a restricted Endpoint is consumed in a pipeline, and that
      pipeline is executed by a non-admin user, then the execution will fail
      at the task which is consuming this restricted Endpoint. Only admin
      can then resume this pipeline to make it progress.
    :param string project: (optional in body) The project this entity belongs to.
    """

    try:

        hub.log.debug("POST /codestream/api/endpoint-validation")

        api = EndpointsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name
        body["properties"] = properties
        body["type"] = type

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "isRestricted" in kwargs:
            hub.log.debug(f"Got kwarg 'isRestricted' = {kwargs['isRestricted']}")
            body["isRestricted"] = kwargs.get("isRestricted")
            del kwargs["isRestricted"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]

        ret = api.validate_endpoint_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsApi.validate_endpoint_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
