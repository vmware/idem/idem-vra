from idem_vra.client.vra_pipeline_lib.api import AboutApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def handle_get_about_using_get(hub, ctx, **kwargs):
    """Get about page The page contains information about the supported API versions and the latest
      API version. The version parameter is optional but highly recommended.
      If you do not specify explicitly an exact version, you will be calling the
      latest supported API version.
      Here is an example of a call which specifies the exact version you are using:
      GET /codestream/api/pipelines?apiVersion=2019-10-17 Performs GET /codestream/api/about


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/about")

        api = AboutApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.handle_get_about_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AboutApi.handle_get_about_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
