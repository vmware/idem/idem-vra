from idem_vra.client.vra_pipeline_lib.api import PipelinesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def act_on_pipeline_using_post(hub, ctx, p_id, **kwargs):
    """Clone a Pipeline Clone a Pipeline with the given id Performs POST /codestream/api/pipelines/{id}


    :param string p_id: (required in path) The ID of the Pipeline to be cloned
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Description for the cloned entity.
    :param string name: (optional in body) Name field for the cloned entity.
    :param string requestType: (optional in body) Action to be performed on the service.
    """

    try:

        hub.log.debug("POST /codestream/api/pipelines/{id}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "requestType" in kwargs:
            hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
            body["requestType"] = kwargs.get("requestType")
            del kwargs["requestType"]

        ret = api.act_on_pipeline_using_post(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.act_on_pipeline_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def clone_pipeline_by_name_using_post(hub, ctx, p_name, p_project, **kwargs):
    """Clone a Pipeline by project and name Clone a Pipeline with the given project and name Performs POST /codestream/api/pipelines/{project}/{name}


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) Description for the cloned entity.
    :param string name: (optional in body) Name field for the cloned entity.
    :param string requestType: (optional in body) Action to be performed on the service.
    """

    try:

        hub.log.debug("POST /codestream/api/pipelines/{project}/{name}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "requestType" in kwargs:
            hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
            body["requestType"] = kwargs.get("requestType")
            del kwargs["requestType"]

        ret = api.clone_pipeline_by_name_using_post(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.clone_pipeline_by_name_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_pipeline_using_post(hub, ctx, name, **kwargs):
    """Create a Pipeline Create a Pipeline based on the given project Performs POST /codestream/api/pipelines


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param object _inputMeta: (optional in body) Additional information about Input Properties
    :param integer concurrency: (optional in body) Number of Executions of the Pipeline that can run concurrently.
    :param string description: (optional in body) A human-friendly description.
    :param boolean enabled: (optional in body) Indicates if the Pipeline is in enabled state.
    :param boolean global: (optional in body) Indicates if the pipeline is shared with all projects in an Org.
    :param string icon: (optional in body) String description of the icon used for this Pipeline.
    :param object input: (optional in body) Map representing the Input properties for the Pipeline.
    :param Any notifications: (optional in body)
    :param array options: (optional in body) Represents the different options to trigger a Pipeline. Selecting an
      option auto injects the Input properties needed to execute a Pipeline
      with that trigger.
    :param object output: (optional in body) Map representing the Output properties for the Pipeline.
    :param string project: (optional in body) The project this entity belongs to.
    :param array rollbacks: (optional in body) Represents the various Rollback Configurations for the Pipeline
    :param array stageOrder: (optional in body) Represents the order in which Stages will be executed.
    :param object stages: (optional in body) Map representing the details of the various Stages of the Pipeline.
    :param Any starred: (optional in body)
    :param string state: (optional in body) Indicates if the Pipeline is enabled/disabled/released to catalog.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on on the
      resource.
    :param Any workspace: (optional in body)
    """

    try:

        hub.log.debug("POST /codestream/api/pipelines")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "_inputMeta" in kwargs:
            hub.log.debug(f"Got kwarg '_inputMeta' = {kwargs['_inputMeta']}")
            body["_inputMeta"] = kwargs.get("_inputMeta")
            del kwargs["_inputMeta"]
        if "concurrency" in kwargs:
            hub.log.debug(f"Got kwarg 'concurrency' = {kwargs['concurrency']}")
            body["concurrency"] = kwargs.get("concurrency")
            del kwargs["concurrency"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "icon" in kwargs:
            hub.log.debug(f"Got kwarg 'icon' = {kwargs['icon']}")
            body["icon"] = kwargs.get("icon")
            del kwargs["icon"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "notifications" in kwargs:
            hub.log.debug(f"Got kwarg 'notifications' = {kwargs['notifications']}")
            body["notifications"] = kwargs.get("notifications")
            del kwargs["notifications"]
        if "options" in kwargs:
            hub.log.debug(f"Got kwarg 'options' = {kwargs['options']}")
            body["options"] = kwargs.get("options")
            del kwargs["options"]
        if "output" in kwargs:
            hub.log.debug(f"Got kwarg 'output' = {kwargs['output']}")
            body["output"] = kwargs.get("output")
            del kwargs["output"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "rollbacks" in kwargs:
            hub.log.debug(f"Got kwarg 'rollbacks' = {kwargs['rollbacks']}")
            body["rollbacks"] = kwargs.get("rollbacks")
            del kwargs["rollbacks"]
        if "stageOrder" in kwargs:
            hub.log.debug(f"Got kwarg 'stageOrder' = {kwargs['stageOrder']}")
            body["stageOrder"] = kwargs.get("stageOrder")
            del kwargs["stageOrder"]
        if "stages" in kwargs:
            hub.log.debug(f"Got kwarg 'stages' = {kwargs['stages']}")
            body["stages"] = kwargs.get("stages")
            del kwargs["stages"]
        if "starred" in kwargs:
            hub.log.debug(f"Got kwarg 'starred' = {kwargs['starred']}")
            body["starred"] = kwargs.get("starred")
            del kwargs["starred"]
        if "state" in kwargs:
            hub.log.debug(f"Got kwarg 'state' = {kwargs['state']}")
            body["state"] = kwargs.get("state")
            del kwargs["state"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "workspace" in kwargs:
            hub.log.debug(f"Got kwarg 'workspace' = {kwargs['workspace']}")
            body["workspace"] = kwargs.get("workspace")
            del kwargs["workspace"]

        ret = api.create_pipeline_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.create_pipeline_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_pipeline_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete a Pipeline by id Delete a Pipeline with the given id Performs DELETE /codestream/api/pipelines/{id}


    :param string p_id: (required in path) The ID of the Pipeline
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/pipelines/{id}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_pipeline_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.delete_pipeline_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_pipeline_by_name_using_delete(hub, ctx, p_name, p_project, **kwargs):
    """Delete a Pipeline by project and name Delete a Pipeline with the given project and name Performs DELETE /codestream/api/pipelines/{project}/{name}


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/pipelines/{project}/{name}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_pipeline_by_name_using_delete(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.delete_pipeline_by_name_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def execute_pipeline_by_id_using_post(hub, ctx, p_id, **kwargs):
    """Execute a Pipeline Execute a Pipeline based on the given id Performs POST /codestream/api/pipelines/{id}/executions


    :param string p_id: (required in path) The ID of the Pipeline
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string comments: (optional in body)
    :param string executionId: (optional in body)
    :param string executionLink: (optional in body)
    :param object input: (optional in body)
    :param string projectId: (optional in body)
    :param string projectName: (optional in body)
    :param string source: (optional in body)
    :param array tags: (optional in body)
    """

    try:

        hub.log.debug("POST /codestream/api/pipelines/{id}/executions")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "comments" in kwargs:
            hub.log.debug(f"Got kwarg 'comments' = {kwargs['comments']}")
            body["comments"] = kwargs.get("comments")
            del kwargs["comments"]
        if "executionId" in kwargs:
            hub.log.debug(f"Got kwarg 'executionId' = {kwargs['executionId']}")
            body["executionId"] = kwargs.get("executionId")
            del kwargs["executionId"]
        if "executionLink" in kwargs:
            hub.log.debug(f"Got kwarg 'executionLink' = {kwargs['executionLink']}")
            body["executionLink"] = kwargs.get("executionLink")
            del kwargs["executionLink"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.execute_pipeline_by_id_using_post(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.execute_pipeline_by_id_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def execute_pipeline_by_name_using_post(hub, ctx, p_name, p_project, **kwargs):
    """Execute a Pipeline Execute a Pipeline based on the given project and name Performs POST /codestream/api/pipelines/{project}/{name}/executions


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string comments: (optional in body)
    :param string executionId: (optional in body)
    :param string executionLink: (optional in body)
    :param object input: (optional in body)
    :param string projectId: (optional in body)
    :param string projectName: (optional in body)
    :param string source: (optional in body)
    :param array tags: (optional in body)
    """

    try:

        hub.log.debug("POST /codestream/api/pipelines/{project}/{name}/executions")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "comments" in kwargs:
            hub.log.debug(f"Got kwarg 'comments' = {kwargs['comments']}")
            body["comments"] = kwargs.get("comments")
            del kwargs["comments"]
        if "executionId" in kwargs:
            hub.log.debug(f"Got kwarg 'executionId' = {kwargs['executionId']}")
            body["executionId"] = kwargs.get("executionId")
            del kwargs["executionId"]
        if "executionLink" in kwargs:
            hub.log.debug(f"Got kwarg 'executionLink' = {kwargs['executionLink']}")
            body["executionLink"] = kwargs.get("executionLink")
            del kwargs["executionLink"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.execute_pipeline_by_name_using_post(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.execute_pipeline_by_name_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def export_using_get(hub, ctx, **kwargs):
    """Export Exports a single pipeline (and endpoints referred in that pipeline) or a list
      of pipelines/endpoints in a given project or a custom integration with given
      list of versions as YAML in a given project Performs GET /codestream/api/export


    :param string customIntegration: (optional in query) Name of the Custom Integration to be exported
    :param string customIntegrationVersions: (optional in query) Comma separated list of versions of the given custom integration to be
      exported
    :param string endpoints: (optional in query) Comma separated list of endpoints to be exported in a given project
    :param string exportAllCustomIntegrationVersions: (optional in query) Flag to state if all versions of given custom integration need to be
      exported
    :param string exportOnlyReleasedCustomIntegrationVersions: (optional in query) Flag to state if only released versions of given custom integration
      need to be exported
    :param string pipeline: (optional in query) Name of the Pipeline to be exported. Here, all endpoints referred in
      the pipeline also get exported
    :param string pipelines: (optional in query) Comma separated list of pipelines to be exported in a given project
    :param string project: (optional in query) Name of the Project to which Endpoint(s)/Pipeline(s) belong to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/export")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.export_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.export_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_pipelines_using_get(hub, ctx, **kwargs):
    """Get all Pipelines Get all Pipelines with specified paging and filter parameters. Performs GET /codestream/api/pipelines


    :param string filter: (optional in query) To list with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Pipelines for listing
    :param string top: (optional in query) To list top n Pipelines for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/pipelines")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_pipelines_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_all_pipelines_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_execution_by_index_and_pipeline_id_using_get(
    hub, ctx, p_id, p_index, **kwargs
):
    """Get an Execution Get an Execution based on the given pipeline id and execution index Performs GET /codestream/api/pipelines/{id}/executions/{index}


    :param string p_id: (required in path) The ID of the Pipeline
    :param string p_index: (required in path) The index of the Execution
    :param string queryParams: (optional in query) Value of expand type for the execution
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/pipelines/{id}/executions/{index}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_execution_by_index_and_pipeline_id_using_get(
            id=p_id, index=p_index, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_execution_by_index_and_pipeline_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_execution_by_name_and_index_using_get(
    hub, ctx, p_index, p_name, p_project, **kwargs
):
    """Get an Execution Get an Execution based on the given pipeline id and execution index Performs GET /codestream/api/pipelines/{project}/{name}/executions/{index}


    :param string p_index: (required in path) The index of the Execution
    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string queryParams: (optional in query) Value of expand type for the execution
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug(
            "GET /codestream/api/pipelines/{project}/{name}/executions/{index}"
        )

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_execution_by_name_and_index_using_get(
            index=p_index, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_execution_by_name_and_index_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_executions_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get all Executions of a Pipeline Get all Executions of a Pipeline with specified paging and filter parameters. Performs GET /codestream/api/pipelines/{id}/executions


    :param string p_id: (required in path) The ID of the Pipeline
    :param string filter: (optional in query) To list with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Executions for listing
    :param string top: (optional in query) To list top n Executions for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/pipelines/{id}/executions")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_executions_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_executions_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_executions_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Get all Executions of a Pipeline Get all Executions of a Pipeline with specified paging and filter parameters. Performs GET /codestream/api/pipelines/{project}/{name}/executions


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string filter: (optional in query) To list with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Executions for listing
    :param string top: (optional in query) To list top n Executions for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/pipelines/{project}/{name}/executions")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_executions_by_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_executions_by_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_pipeline_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get a Pipeline Get a Pipeline with the given id Performs GET /codestream/api/pipelines/{id}


    :param string p_id: (required in path) The ID of the Pipeline
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/pipelines/{id}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_pipeline_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_pipeline_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_pipeline_by_name_using_get(hub, ctx, p_name, p_project, **kwargs):
    """Get a Pipeline by project and name Get a Pipeline with the given project and name Performs GET /codestream/api/pipelines/{project}/{name}


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/pipelines/{project}/{name}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_pipeline_by_name_using_get(
            name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_pipeline_by_name_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_pipeline_tiles_using_get(hub, ctx, **kwargs):
    """Get Pipeline Tiles Get Pipeline Tiles Performs GET /codestream/api/pipeline-tiles


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/pipeline-tiles")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_pipeline_tiles_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.get_pipeline_tiles_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def import_yaml_using_post(hub, ctx, **kwargs):
    """Import Imports pipeline(s)/endpoint(s) into Aria Automation Pipelines. Performs POST /codestream/api/import


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("POST /codestream/api/import")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.import_yaml_using_post(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.import_yaml_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_pipeline_by_name_using_patch(hub, ctx, p_name, p_project, **kwargs):
    """Update a Pipeline by project and name Update a Pipeline with the given project and name Performs PATCH /codestream/api/pipelines/{project}/{name}


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description for the Pipeline.
    :param boolean enabled: (optional in body) Indicates if the Pipeline is in enabled state.
    :param boolean global: (optional in body) Indicates if the pipeline is shared with all projects in an Org.
    :param string name: (optional in body) A human-friendly name used as an identifier for the Pipeline.
    :param string state: (optional in body) Indicates if the Pipeline is enabled/disabled/released to catalog.
    :param array tags: (optional in body) A set of tag keys and optional values that need to be set on the
      Pipeline.
    """

    try:

        hub.log.debug("PATCH /codestream/api/pipelines/{project}/{name}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "state" in kwargs:
            hub.log.debug(f"Got kwarg 'state' = {kwargs['state']}")
            body["state"] = kwargs.get("state")
            del kwargs["state"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.patch_pipeline_by_name_using_patch(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.patch_pipeline_by_name_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_pipeline_global_by_name_using_patch(
    hub, ctx, p_name, p_project, **kwargs
):
    """Update a pipeline by pipeline name to be shared across projects Update a pipeline to be shared across projects Performs PATCH /codestream/api/pipelines/{project}/{name}/global


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description for the Pipeline.
    :param boolean enabled: (optional in body) Indicates if the Pipeline is in enabled state.
    :param boolean global: (optional in body) Indicates if the pipeline is shared with all projects in an Org.
    :param string name: (optional in body) A human-friendly name used as an identifier for the Pipeline.
    :param string state: (optional in body) Indicates if the Pipeline is enabled/disabled/released to catalog.
    :param array tags: (optional in body) A set of tag keys and optional values that need to be set on the
      Pipeline.
    """

    try:

        hub.log.debug("PATCH /codestream/api/pipelines/{project}/{name}/global")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "state" in kwargs:
            hub.log.debug(f"Got kwarg 'state' = {kwargs['state']}")
            body["state"] = kwargs.get("state")
            del kwargs["state"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.patch_pipeline_global_by_name_using_patch(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.patch_pipeline_global_by_name_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_pipeline_global_using_patch(hub, ctx, p_id, **kwargs):
    """Update a pipeline by id to be shared across all projects in an Org. Update a pipeline to be shared across projects Performs PATCH /codestream/api/pipelines/{id}/global


    :param string p_id: (required in path) The ID of the Pipeline
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description for the Pipeline.
    :param boolean enabled: (optional in body) Indicates if the Pipeline is in enabled state.
    :param boolean global: (optional in body) Indicates if the pipeline is shared with all projects in an Org.
    :param string name: (optional in body) A human-friendly name used as an identifier for the Pipeline.
    :param string state: (optional in body) Indicates if the Pipeline is enabled/disabled/released to catalog.
    :param array tags: (optional in body) A set of tag keys and optional values that need to be set on the
      Pipeline.
    """

    try:

        hub.log.debug("PATCH /codestream/api/pipelines/{id}/global")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "state" in kwargs:
            hub.log.debug(f"Got kwarg 'state' = {kwargs['state']}")
            body["state"] = kwargs.get("state")
            del kwargs["state"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.patch_pipeline_global_using_patch(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.patch_pipeline_global_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_pipeline_using_patch(hub, ctx, p_id, **kwargs):
    """Update a Pipeline by id Update a Pipeline with the given id Performs PATCH /codestream/api/pipelines/{id}


    :param string p_id: (required in path) The ID of the Pipeline
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string description: (optional in body) A human-friendly description for the Pipeline.
    :param boolean enabled: (optional in body) Indicates if the Pipeline is in enabled state.
    :param boolean global: (optional in body) Indicates if the pipeline is shared with all projects in an Org.
    :param string name: (optional in body) A human-friendly name used as an identifier for the Pipeline.
    :param string state: (optional in body) Indicates if the Pipeline is enabled/disabled/released to catalog.
    :param array tags: (optional in body) A set of tag keys and optional values that need to be set on the
      Pipeline.
    """

    try:

        hub.log.debug("PATCH /codestream/api/pipelines/{id}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "state" in kwargs:
            hub.log.debug(f"Got kwarg 'state' = {kwargs['state']}")
            body["state"] = kwargs.get("state")
            del kwargs["state"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.patch_pipeline_using_patch(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.patch_pipeline_using_patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_pipeline_by_name_using_put(
    hub, ctx, p_name, p_project, name, **kwargs
):
    """Update a Pipeline by project and name Update a Pipeline with the given project and name Performs PUT /codestream/api/pipelines/{project}/{name}


    :param string p_name: (required in path) The name of the Pipeline
    :param string p_project: (required in path) The project the Pipeline belongs to
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param object _inputMeta: (optional in body) Additional information about Input Properties
    :param integer concurrency: (optional in body) Number of Executions of the Pipeline that can run concurrently.
    :param string description: (optional in body) A human-friendly description.
    :param boolean enabled: (optional in body) Indicates if the Pipeline is in enabled state.
    :param boolean global: (optional in body) Indicates if the pipeline is shared with all projects in an Org.
    :param string icon: (optional in body) String description of the icon used for this Pipeline.
    :param object input: (optional in body) Map representing the Input properties for the Pipeline.
    :param Any notifications: (optional in body)
    :param array options: (optional in body) Represents the different options to trigger a Pipeline. Selecting an
      option auto injects the Input properties needed to execute a Pipeline
      with that trigger.
    :param object output: (optional in body) Map representing the Output properties for the Pipeline.
    :param string project: (optional in body) The project this entity belongs to.
    :param array rollbacks: (optional in body) Represents the various Rollback Configurations for the Pipeline
    :param array stageOrder: (optional in body) Represents the order in which Stages will be executed.
    :param object stages: (optional in body) Map representing the details of the various Stages of the Pipeline.
    :param Any starred: (optional in body)
    :param string state: (optional in body) Indicates if the Pipeline is enabled/disabled/released to catalog.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on on the
      resource.
    :param Any workspace: (optional in body)
    """

    try:

        hub.log.debug("PUT /codestream/api/pipelines/{project}/{name}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "_inputMeta" in kwargs:
            hub.log.debug(f"Got kwarg '_inputMeta' = {kwargs['_inputMeta']}")
            body["_inputMeta"] = kwargs.get("_inputMeta")
            del kwargs["_inputMeta"]
        if "concurrency" in kwargs:
            hub.log.debug(f"Got kwarg 'concurrency' = {kwargs['concurrency']}")
            body["concurrency"] = kwargs.get("concurrency")
            del kwargs["concurrency"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "icon" in kwargs:
            hub.log.debug(f"Got kwarg 'icon' = {kwargs['icon']}")
            body["icon"] = kwargs.get("icon")
            del kwargs["icon"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "notifications" in kwargs:
            hub.log.debug(f"Got kwarg 'notifications' = {kwargs['notifications']}")
            body["notifications"] = kwargs.get("notifications")
            del kwargs["notifications"]
        if "options" in kwargs:
            hub.log.debug(f"Got kwarg 'options' = {kwargs['options']}")
            body["options"] = kwargs.get("options")
            del kwargs["options"]
        if "output" in kwargs:
            hub.log.debug(f"Got kwarg 'output' = {kwargs['output']}")
            body["output"] = kwargs.get("output")
            del kwargs["output"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "rollbacks" in kwargs:
            hub.log.debug(f"Got kwarg 'rollbacks' = {kwargs['rollbacks']}")
            body["rollbacks"] = kwargs.get("rollbacks")
            del kwargs["rollbacks"]
        if "stageOrder" in kwargs:
            hub.log.debug(f"Got kwarg 'stageOrder' = {kwargs['stageOrder']}")
            body["stageOrder"] = kwargs.get("stageOrder")
            del kwargs["stageOrder"]
        if "stages" in kwargs:
            hub.log.debug(f"Got kwarg 'stages' = {kwargs['stages']}")
            body["stages"] = kwargs.get("stages")
            del kwargs["stages"]
        if "starred" in kwargs:
            hub.log.debug(f"Got kwarg 'starred' = {kwargs['starred']}")
            body["starred"] = kwargs.get("starred")
            del kwargs["starred"]
        if "state" in kwargs:
            hub.log.debug(f"Got kwarg 'state' = {kwargs['state']}")
            body["state"] = kwargs.get("state")
            del kwargs["state"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "workspace" in kwargs:
            hub.log.debug(f"Got kwarg 'workspace' = {kwargs['workspace']}")
            body["workspace"] = kwargs.get("workspace")
            del kwargs["workspace"]

        ret = api.update_pipeline_by_name_using_put(
            body, name=p_name, project=p_project, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.update_pipeline_by_name_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_pipeline_using_put(hub, ctx, p_id, name, **kwargs):
    """Update a Pipeline by id Update a Pipeline with the given id Performs PUT /codestream/api/pipelines/{id}


    :param string p_id: (required in path) The ID of the Pipeline
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param object _inputMeta: (optional in body) Additional information about Input Properties
    :param integer concurrency: (optional in body) Number of Executions of the Pipeline that can run concurrently.
    :param string description: (optional in body) A human-friendly description.
    :param boolean enabled: (optional in body) Indicates if the Pipeline is in enabled state.
    :param boolean global: (optional in body) Indicates if the pipeline is shared with all projects in an Org.
    :param string icon: (optional in body) String description of the icon used for this Pipeline.
    :param object input: (optional in body) Map representing the Input properties for the Pipeline.
    :param Any notifications: (optional in body)
    :param array options: (optional in body) Represents the different options to trigger a Pipeline. Selecting an
      option auto injects the Input properties needed to execute a Pipeline
      with that trigger.
    :param object output: (optional in body) Map representing the Output properties for the Pipeline.
    :param string project: (optional in body) The project this entity belongs to.
    :param array rollbacks: (optional in body) Represents the various Rollback Configurations for the Pipeline
    :param array stageOrder: (optional in body) Represents the order in which Stages will be executed.
    :param object stages: (optional in body) Map representing the details of the various Stages of the Pipeline.
    :param Any starred: (optional in body)
    :param string state: (optional in body) Indicates if the Pipeline is enabled/disabled/released to catalog.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on on the
      resource.
    :param Any workspace: (optional in body)
    """

    try:

        hub.log.debug("PUT /codestream/api/pipelines/{id}")

        api = PipelinesApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["name"] = name

        if "_inputMeta" in kwargs:
            hub.log.debug(f"Got kwarg '_inputMeta' = {kwargs['_inputMeta']}")
            body["_inputMeta"] = kwargs.get("_inputMeta")
            del kwargs["_inputMeta"]
        if "concurrency" in kwargs:
            hub.log.debug(f"Got kwarg 'concurrency' = {kwargs['concurrency']}")
            body["concurrency"] = kwargs.get("concurrency")
            del kwargs["concurrency"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enabled" in kwargs:
            hub.log.debug(f"Got kwarg 'enabled' = {kwargs['enabled']}")
            body["enabled"] = kwargs.get("enabled")
            del kwargs["enabled"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "icon" in kwargs:
            hub.log.debug(f"Got kwarg 'icon' = {kwargs['icon']}")
            body["icon"] = kwargs.get("icon")
            del kwargs["icon"]
        if "input" in kwargs:
            hub.log.debug(f"Got kwarg 'input' = {kwargs['input']}")
            body["input"] = kwargs.get("input")
            del kwargs["input"]
        if "notifications" in kwargs:
            hub.log.debug(f"Got kwarg 'notifications' = {kwargs['notifications']}")
            body["notifications"] = kwargs.get("notifications")
            del kwargs["notifications"]
        if "options" in kwargs:
            hub.log.debug(f"Got kwarg 'options' = {kwargs['options']}")
            body["options"] = kwargs.get("options")
            del kwargs["options"]
        if "output" in kwargs:
            hub.log.debug(f"Got kwarg 'output' = {kwargs['output']}")
            body["output"] = kwargs.get("output")
            del kwargs["output"]
        if "project" in kwargs:
            hub.log.debug(f"Got kwarg 'project' = {kwargs['project']}")
            body["project"] = kwargs.get("project")
            del kwargs["project"]
        if "rollbacks" in kwargs:
            hub.log.debug(f"Got kwarg 'rollbacks' = {kwargs['rollbacks']}")
            body["rollbacks"] = kwargs.get("rollbacks")
            del kwargs["rollbacks"]
        if "stageOrder" in kwargs:
            hub.log.debug(f"Got kwarg 'stageOrder' = {kwargs['stageOrder']}")
            body["stageOrder"] = kwargs.get("stageOrder")
            del kwargs["stageOrder"]
        if "stages" in kwargs:
            hub.log.debug(f"Got kwarg 'stages' = {kwargs['stages']}")
            body["stages"] = kwargs.get("stages")
            del kwargs["stages"]
        if "starred" in kwargs:
            hub.log.debug(f"Got kwarg 'starred' = {kwargs['starred']}")
            body["starred"] = kwargs.get("starred")
            del kwargs["starred"]
        if "state" in kwargs:
            hub.log.debug(f"Got kwarg 'state' = {kwargs['state']}")
            body["state"] = kwargs.get("state")
            del kwargs["state"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "workspace" in kwargs:
            hub.log.debug(f"Got kwarg 'workspace' = {kwargs['workspace']}")
            body["workspace"] = kwargs.get("workspace")
            del kwargs["workspace"]

        ret = api.update_pipeline_using_put(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PipelinesApi.update_pipeline_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
