from idem_vra.client.vra_pipeline_lib.api import ExecutionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete_all_executions_using_delete(hub, ctx, **kwargs):
    """Delete all Executions Delete executions that match the specified filter. Performs DELETE /codestream/api/executions


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/executions")

        api = ExecutionsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_all_executions_using_delete(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ExecutionsApi.delete_all_executions_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_execution_by_id_using_delete(hub, ctx, p_id, **kwargs):
    """Delete an Execution by id Delete an Execution Performs DELETE /codestream/api/executions/{id}


    :param string p_id: (required in path) The ID of the Execution
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("DELETE /codestream/api/executions/{id}")

        api = ExecutionsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.delete_execution_by_id_using_delete(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ExecutionsApi.delete_execution_by_id_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_executions_using_get(hub, ctx, **kwargs):
    """Get all Executions Get all Executions with specified paging and filter parameters. Performs GET /codestream/api/executions


    :param string filter: (optional in query) To list with OData like filter
    :param string orderby: (optional in query) Order by attribute
    :param string skip: (optional in query) To skip n Executions for listing
    :param string top: (optional in query) To list top n Executions for listing
    :param string page: (optional in query) To select nth page for listing
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/executions")

        api = ExecutionsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_all_executions_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ExecutionsApi.get_all_executions_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_execution_by_id_using_get(hub, ctx, p_id, **kwargs):
    """Get an Execution Get an Execution by id Performs GET /codestream/api/executions/{id}


    :param string p_id: (required in path) The ID of the Execution
    :param string queryParams: (optional in query) Value of expand type for the execution
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    """

    try:

        hub.log.debug("GET /codestream/api/executions/{id}")

        api = ExecutionsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        ret = api.get_execution_by_id_using_get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ExecutionsApi.get_execution_by_id_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def modify_execution_using_post(hub, ctx, q_action, p_id, **kwargs):
    """Act on an Execution Actions supported are pause, resume, cancel and tag Performs POST /codestream/api/executions/{id}


    :param string q_action: (required in query) Action to perform on the Execution. Can be any of pause, resume,
      cancel and tag
    :param string p_id: (required in path) The ID of the Execution
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string comments: (optional in body) Comments to update an execution. Existing comments will be replaced by
      these comments.
    :param string reason: (optional in body) Reason for performing the action on the given Execution.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on on the
      resource.
    """

    try:

        hub.log.debug("POST /codestream/api/executions/{id}")

        api = ExecutionsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}

        if "comments" in kwargs:
            hub.log.debug(f"Got kwarg 'comments' = {kwargs['comments']}")
            body["comments"] = kwargs.get("comments")
            del kwargs["comments"]
        if "reason" in kwargs:
            hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
            body["reason"] = kwargs.get("reason")
            del kwargs["reason"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.modify_execution_using_post(body, action=q_action, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ExecutionsApi.modify_execution_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_using_post(hub, ctx, ids, status, **kwargs):
    """Updates a batch of user operations Approves/ Rejects a set of user operations Performs POST /codestream/api/batch-user-operations


    :param array ids: (required in body) The list of user-op ids to be batch approved/rejected.
    :param string status: (required in body) The status of approval requests.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /codestream/api/about
    :param string Authorization: (optional in header) Bearer token
    :param string responseMessage: (optional in body) The response message which the responder would like to give.
    """

    try:

        hub.log.debug("POST /codestream/api/batch-user-operations")

        api = ExecutionsApi(hub.clients["idem_vra.client.vra_pipeline_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-10-17"

        body = {}
        body["ids"] = ids
        body["status"] = status

        if "responseMessage" in kwargs:
            hub.log.debug(f"Got kwarg 'responseMessage' = {kwargs['responseMessage']}")
            body["responseMessage"] = kwargs.get("responseMessage")
            del kwargs["responseMessage"]

        ret = api.update_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ExecutionsApi.update_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
