from idem_vra.client.vra_identity_lib.api import ServiceDefinitionV2ControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_all_by_org_service_definitions(hub, ctx, p_orgId, **kwargs):
    """Get all service definitions the organization has access to.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/slc/api/v2/orgs/{orgId}/services


    :param string p_orgId: (required in path)
    :param boolean excludeUngated: (optional in query)
    :param string locale: (optional in query)
    :param integer pageStart: (optional in query)
    :param integer pageLimit: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/slc/api/v2/orgs/{orgId}/services")

        api = ServiceDefinitionV2ControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_all_by_org_service_definitions(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ServiceDefinitionV2ControllerApi.get_all_by_org_service_definitions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_paged_service_definition_orgs(hub, ctx, p_serviceDefinitionId, **kwargs):
    """Get organizations with access to specified service. Currently all organizations have access to all of the service definitions.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner  c
      Organization Member  c
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/slc/api/v2/definitions/{serviceDefinitionId}/orgs


    :param string p_serviceDefinitionId: (required in path)
    :param integer pageStart: (optional in query)
    :param integer pageLimit: (optional in query)
    """

    try:

        hub.log.debug(
            "GET /csp/gateway/slc/api/v2/definitions/{serviceDefinitionId}/orgs"
        )

        api = ServiceDefinitionV2ControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_paged_service_definition_orgs(
            service_definition_id=p_serviceDefinitionId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ServiceDefinitionV2ControllerApi.get_paged_service_definition_orgs: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
