from idem_vra.client.vra_identity_lib.api import UserControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_user_in_any_organization1(hub, ctx, p_acct, **kwargs):
    """Get the user belonging to the current organization. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner  c
      Organization Member  c
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/users/{acct}


    :param string p_acct: (required in path)
    :param string expandProfile: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/users/{acct}")

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_in_any_organization1(acct=p_acct, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.get_user_in_any_organization1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_info_in_organization1(hub, ctx, p_acct, p_orgId, **kwargs):
    """Get the user details. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/users/{acct}/orgs/{orgId}/info


    :param string p_acct: (required in path)
    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/users/{acct}/orgs/{orgId}/info")

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_info_in_organization1(acct=p_acct, org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.get_user_info_in_organization1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_roles_in_organization1(hub, ctx, p_userId, p_orgId, **kwargs):
    """Get the users roles. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/roles


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/roles")

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_roles_in_organization1(
            user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.get_user_roles_in_organization1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_roles_on_org_with_group_info(hub, ctx, p_userId, p_orgId, **kwargs):
    """Get user roles with groups inheritance information within the specified
      organization The user roles with groups inheritance information is not a CSP API call.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/access


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/access")

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_roles_on_org_with_group_info(
            user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.get_user_roles_on_org_with_group_info: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_service_roles_in_organization1(
    hub, ctx, p_userId, p_orgId, **kwargs
):
    """Get the users service roles. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/service-roles


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    :param string serviceDefinitionLink: (optional in query)
    """

    try:

        hub.log.debug(
            "GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/service-roles"
        )

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_service_roles_in_organization1(
            user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.get_user_service_roles_in_organization1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_short_info_in_organization(hub, ctx, p_userId, p_orgId, **kwargs):
    """Get the user details. Currently one user can belong to exactly one organization. The short-info is
      not a CSP API call and will be removed in future versions.
      Access Policy
      Role  Access
      Platform operator  c
      Organization Owner  c
      Organization Member  c
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/short-info


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/users/{userId}/orgs/{orgId}/short-info")

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_short_info_in_organization(
            user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.get_user_short_info_in_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_user_roles_in_organization(hub, ctx, p_userId, p_orgId, **kwargs):
    """Edit the users roles. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs PATCH /csp/gateway/am/api/users/{userId}/orgs/{orgId}/roles


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    :param array roleNamesToAdd: (optional in body) Deprecated: Role names to add (use rolesToAdd instead)
    :param array rolesToAdd: (optional in body) Roles to add
    :param array roleNamesToRemove: (optional in body) Deprecated: Role names to remove (use rolesToRemove instead)
    :param array rolesToRemove: (optional in body) Roles to remove
    """

    try:

        hub.log.debug("PATCH /csp/gateway/am/api/users/{userId}/orgs/{orgId}/roles")

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        body = {}

        if "roleNamesToAdd" in kwargs:
            hub.log.debug(f"Got kwarg 'roleNamesToAdd' = {kwargs['roleNamesToAdd']}")
            body["roleNamesToAdd"] = kwargs.get("roleNamesToAdd")
            del kwargs["roleNamesToAdd"]
        if "rolesToAdd" in kwargs:
            hub.log.debug(f"Got kwarg 'rolesToAdd' = {kwargs['rolesToAdd']}")
            body["rolesToAdd"] = kwargs.get("rolesToAdd")
            del kwargs["rolesToAdd"]
        if "roleNamesToRemove" in kwargs:
            hub.log.debug(
                f"Got kwarg 'roleNamesToRemove' = {kwargs['roleNamesToRemove']}"
            )
            body["roleNamesToRemove"] = kwargs.get("roleNamesToRemove")
            del kwargs["roleNamesToRemove"]
        if "rolesToRemove" in kwargs:
            hub.log.debug(f"Got kwarg 'rolesToRemove' = {kwargs['rolesToRemove']}")
            body["rolesToRemove"] = kwargs.get("rolesToRemove")
            del kwargs["rolesToRemove"]

        ret = api.patch_user_roles_in_organization(
            body, user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.patch_user_roles_in_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_user_service_roles_in_organization(
    hub, ctx, p_userId, p_orgId, serviceDefinitionLink, **kwargs
):
    """Edit the users service roles. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs PATCH /csp/gateway/am/api/users/{userId}/orgs/{orgId}/service-roles


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    :param string serviceDefinitionLink: (required in body)
    :param array roleNamesToAdd: (optional in body) Deprecated: Role names to add (use rolesToAdd instead)
    :param array rolesToAdd: (optional in body) Roles to add
    :param array roleNamesToRemove: (optional in body) Deprecated: Role names to remove (use rolesToRemove instead)
    :param array rolesToRemove: (optional in body) Roles to remove
    """

    try:

        hub.log.debug(
            "PATCH /csp/gateway/am/api/users/{userId}/orgs/{orgId}/service-roles"
        )

        api = UserControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        body = {}
        body["serviceDefinitionLink"] = serviceDefinitionLink

        if "roleNamesToAdd" in kwargs:
            hub.log.debug(f"Got kwarg 'roleNamesToAdd' = {kwargs['roleNamesToAdd']}")
            body["roleNamesToAdd"] = kwargs.get("roleNamesToAdd")
            del kwargs["roleNamesToAdd"]
        if "rolesToAdd" in kwargs:
            hub.log.debug(f"Got kwarg 'rolesToAdd' = {kwargs['rolesToAdd']}")
            body["rolesToAdd"] = kwargs.get("rolesToAdd")
            del kwargs["rolesToAdd"]
        if "roleNamesToRemove" in kwargs:
            hub.log.debug(
                f"Got kwarg 'roleNamesToRemove' = {kwargs['roleNamesToRemove']}"
            )
            body["roleNamesToRemove"] = kwargs.get("roleNamesToRemove")
            del kwargs["roleNamesToRemove"]
        if "rolesToRemove" in kwargs:
            hub.log.debug(f"Got kwarg 'rolesToRemove' = {kwargs['rolesToRemove']}")
            body["rolesToRemove"] = kwargs.get("rolesToRemove")
            del kwargs["rolesToRemove"]

        ret = api.patch_user_service_roles_in_organization(
            body, user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserControllerApi.patch_user_service_roles_in_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
