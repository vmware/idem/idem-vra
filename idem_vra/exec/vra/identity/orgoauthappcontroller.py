from idem_vra.client.vra_identity_lib.api import OrgOAuthAppControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_org_scoped_o_auth_client(hub, ctx, p_orgId, **kwargs):
    """Create an OAuth2 client. Creates an organization scoped OAuth2 client.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs POST /csp/gateway/am/api/orgs/{orgId}/oauth-apps


    :param string p_orgId: (required in path) The organization ID.
    :param string id: (optional in body) The client ID. If not set one will be generated.
    :param string secret: (optional in body) The client secret. If not set one will be generated.
    :param string displayName: (optional in body) Display name for the client.
    :param string description: (optional in body) Description for the client.
    :param array redirectUris: (optional in body) Redirect URIs for the client. Only the first one will be taken into
      account by the Identity Service.
    :param array grantTypes: (optional in body) Client grant types.
    :param integer accessTokenTTL: (optional in body) Time to live for the access token, generated for this client, in
      seconds. Defaults to 0 if not set, i.e. the token will be issued
      already expired.
    :param integer refreshTokenTTL: (optional in body) Time to live for the refresh token, generated for this client, in
      seconds. Defaults to 0 if not set, i.e. the token will be issued
      already expired.
    :param integer maxGroupsInIdToken: (optional in body) For CSP compatibility, ignored by the Identity Service. Note that the
      value will be persisted and may become effective in the future
      releases.
    :param Any allowedScopes: (optional in body)
    """

    try:

        hub.log.debug("POST /csp/gateway/am/api/orgs/{orgId}/oauth-apps")

        api = OrgOAuthAppControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "secret" in kwargs:
            hub.log.debug(f"Got kwarg 'secret' = {kwargs['secret']}")
            body["secret"] = kwargs.get("secret")
            del kwargs["secret"]
        if "displayName" in kwargs:
            hub.log.debug(f"Got kwarg 'displayName' = {kwargs['displayName']}")
            body["displayName"] = kwargs.get("displayName")
            del kwargs["displayName"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "redirectUris" in kwargs:
            hub.log.debug(f"Got kwarg 'redirectUris' = {kwargs['redirectUris']}")
            body["redirectUris"] = kwargs.get("redirectUris")
            del kwargs["redirectUris"]
        if "grantTypes" in kwargs:
            hub.log.debug(f"Got kwarg 'grantTypes' = {kwargs['grantTypes']}")
            body["grantTypes"] = kwargs.get("grantTypes")
            del kwargs["grantTypes"]
        if "accessTokenTTL" in kwargs:
            hub.log.debug(f"Got kwarg 'accessTokenTTL' = {kwargs['accessTokenTTL']}")
            body["accessTokenTTL"] = kwargs.get("accessTokenTTL")
            del kwargs["accessTokenTTL"]
        if "refreshTokenTTL" in kwargs:
            hub.log.debug(f"Got kwarg 'refreshTokenTTL' = {kwargs['refreshTokenTTL']}")
            body["refreshTokenTTL"] = kwargs.get("refreshTokenTTL")
            del kwargs["refreshTokenTTL"]
        if "maxGroupsInIdToken" in kwargs:
            hub.log.debug(
                f"Got kwarg 'maxGroupsInIdToken' = {kwargs['maxGroupsInIdToken']}"
            )
            body["maxGroupsInIdToken"] = kwargs.get("maxGroupsInIdToken")
            del kwargs["maxGroupsInIdToken"]
        if "allowedScopes" in kwargs:
            hub.log.debug(f"Got kwarg 'allowedScopes' = {kwargs['allowedScopes']}")
            body["allowedScopes"] = kwargs.get("allowedScopes")
            del kwargs["allowedScopes"]

        ret = api.create_org_scoped_o_auth_client(body, org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrgOAuthAppControllerApi.create_org_scoped_o_auth_client: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_org_scoped_o_auth_client(hub, ctx, p_orgId, **kwargs):
    """Deletes an OAuth2 client. Deletes an organization scoped OAuth2 client.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs DELETE /csp/gateway/am/api/orgs/{orgId}/oauth-apps


    :param string p_orgId: (required in path)
    :param array clientIdsToDelete: (optional in body) A set of client IDs to delete.
    """

    try:

        hub.log.debug("DELETE /csp/gateway/am/api/orgs/{orgId}/oauth-apps")

        api = OrgOAuthAppControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}

        if "clientIdsToDelete" in kwargs:
            hub.log.debug(
                f"Got kwarg 'clientIdsToDelete' = {kwargs['clientIdsToDelete']}"
            )
            body["clientIdsToDelete"] = kwargs.get("clientIdsToDelete")
            del kwargs["clientIdsToDelete"]

        ret = api.delete_org_scoped_o_auth_client(body, org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrgOAuthAppControllerApi.delete_org_scoped_o_auth_client: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_org_scoped_o_auth_client(hub, ctx, p_orgId, p_oauthAppId, **kwargs):
    """Get Organization Managed OAuth App. Get Organization Managed OAuth App that was created and is owned by the
      organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/orgs/{orgId}/oauth-apps/{oauthAppId}


    :param string p_orgId: (required in path)
    :param string p_oauthAppId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/oauth-apps/{oauthAppId}")

        api = OrgOAuthAppControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_org_scoped_o_auth_client(
            org_id=p_orgId, oauth_app_id=p_oauthAppId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrgOAuthAppControllerApi.get_org_scoped_o_auth_client: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
