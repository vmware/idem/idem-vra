from idem_vra.client.vra_identity_lib.api import ServiceDefinitionControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_all_by_org_service_definitions1(hub, ctx, p_orgId, **kwargs):
    """Deprecated: Get all service definitions the organization has access to.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/slc/api/orgs/{orgId}/services


    :param string p_orgId: (required in path)
    :param boolean excludeUngated: (optional in query)
    :param string locale: (optional in query)
    :param integer pageStart: (optional in query)
    :param integer pageLimit: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/slc/api/orgs/{orgId}/services")

        api = ServiceDefinitionControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_all_by_org_service_definitions1(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ServiceDefinitionControllerApi.get_all_by_org_service_definitions1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_service_definitions(hub, ctx, **kwargs):
    """Get all service definitions.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/slc/api/definitions


    :param string orgLink: (optional in query)
    :param string expand: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/slc/api/definitions")

        api = ServiceDefinitionControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_all_service_definitions(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ServiceDefinitionControllerApi.get_all_service_definitions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
