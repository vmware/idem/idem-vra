from idem_vra.client.vra_identity_lib.api import PrincipalUserControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_logged_in_user_details(hub, ctx, **kwargs):
    """Get the currently logged in users detailed information.       Access Policy
    Role  Access
    Platform operator
    Organization Owner
    Organization Member
    Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/details


    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user/details")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_logged_in_user_details(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_logged_in_user_details: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_logged_in_user_groups_on_org(hub, ctx, p_orgId, **kwargs):
    """Get Principal User Groups Information Within the Specified Organization Get Principal User Groups Information Within the Specified Organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/groups


    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/groups")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_logged_in_user_groups_on_org(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_logged_in_user_groups_on_org: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_logged_in_user(hub, ctx, **kwargs):
    """Get the currently logged in user.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user


    :param boolean expand: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_logged_in_user(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_logged_in_user: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_principal_user_profile(hub, ctx, **kwargs):
    """Get the currently logged in users profile.       Access Policy
    Role  Access
    Platform operator
    Organization Owner
    Organization Member
    Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/profile


    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user/profile")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_principal_user_profile(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_principal_user_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_default_org(hub, ctx, **kwargs):
    """Get the currently logged in users organizations. Currently one user can belong to exactly one organization.
    Access Policy
    Role  Access
    Platform operator
    Organization Owner
    Organization Member
    Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/default-org


    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user/default-org")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_user_default_org(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_user_default_org: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_org_info(hub, ctx, p_orgId, **kwargs):
    """Get the currently logged in users info in an organization. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/info


    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/info")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_user_org_info(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_user_org_info: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_org_roles(hub, ctx, p_orgId, **kwargs):
    """Get the currently logged in users roles.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/roles


    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/roles")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_user_org_roles(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_user_org_roles: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_org_service_roles(hub, ctx, p_orgId, **kwargs):
    """Get the currently logged in users service roles.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/service-roles


    :param string p_orgId: (required in path)
    :param string serviceDefinitionLink: (optional in query)
    """

    try:

        hub.log.debug(
            "GET /csp/gateway/am/api/loggedin/user/orgs/{orgId}/service-roles"
        )

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_user_org_service_roles(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_user_org_service_roles: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_orgs(hub, ctx, **kwargs):
    """Get the currently logged in users organizations. Currently one user can belong to exactly one organization. If expand parameter
      is passed, detailed information for the organizations will be returned.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/loggedin/user/orgs


    :param string expand: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/loggedin/user/orgs")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_user_orgs(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.get_user_orgs: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_user_preferences(hub, ctx, language, **kwargs):
    """Update the currently logged in users locale preferences.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs PUT /csp/gateway/am/api/loggedin/user/profile/locale-preferences


    :param string language: (required in body)
    :param string locale: (optional in body)
    """

    try:

        hub.log.debug(
            "PUT /csp/gateway/am/api/loggedin/user/profile/locale-preferences"
        )

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}
        body["language"] = language

        if "locale" in kwargs:
            hub.log.debug(f"Got kwarg 'locale' = {kwargs['locale']}")
            body["locale"] = kwargs.get("locale")
            del kwargs["locale"]

        ret = api.update_user_preferences(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.update_user_preferences: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_user_profile(hub, ctx, **kwargs):
    """Update the currently logged in users profile attributes.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs PATCH /csp/gateway/am/api/loggedin/user/profile


    :param string defaultOrgId: (optional in body) Currently one user can belong to exactly one organization.
    :param string locale: (optional in body) Preferred user locale.
    :param string language: (optional in body) Preferred user language.
    :param string preferredTheme: (optional in body) Preferred user theme.
    :param Any metadata: (optional in body)
    """

    try:

        hub.log.debug("PATCH /csp/gateway/am/api/loggedin/user/profile")

        api = PrincipalUserControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}

        if "defaultOrgId" in kwargs:
            hub.log.debug(f"Got kwarg 'defaultOrgId' = {kwargs['defaultOrgId']}")
            body["defaultOrgId"] = kwargs.get("defaultOrgId")
            del kwargs["defaultOrgId"]
        if "locale" in kwargs:
            hub.log.debug(f"Got kwarg 'locale' = {kwargs['locale']}")
            body["locale"] = kwargs.get("locale")
            del kwargs["locale"]
        if "language" in kwargs:
            hub.log.debug(f"Got kwarg 'language' = {kwargs['language']}")
            body["language"] = kwargs.get("language")
            del kwargs["language"]
        if "preferredTheme" in kwargs:
            hub.log.debug(f"Got kwarg 'preferredTheme' = {kwargs['preferredTheme']}")
            body["preferredTheme"] = kwargs.get("preferredTheme")
            del kwargs["preferredTheme"]
        if "metadata" in kwargs:
            hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
            body["metadata"] = kwargs.get("metadata")
            del kwargs["metadata"]

        ret = api.update_user_profile(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PrincipalUserControllerApi.update_user_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
