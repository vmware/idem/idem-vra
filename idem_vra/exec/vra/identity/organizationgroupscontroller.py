from idem_vra.client.vra_identity_lib.api import OrganizationGroupsControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_group_roles_on_organization(hub, ctx, p_orgId, p_groupId, **kwargs):
    """Get Group Roles On Organization Get roles of a group within organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/roles


    :param string p_orgId: (required in path) Unique identifier (GUID) of the organization.
    :param string p_groupId: (required in path) Unique identifier of the group.
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/roles")

        api = OrganizationGroupsControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_group_roles_on_organization(
            org_id=p_orgId, group_id=p_groupId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationGroupsControllerApi.get_group_roles_on_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_nested_groups_from_ad_group(hub, ctx, p_orgId, p_groupId, **kwargs):
    """Get nested groups from AD group. Get nested groups from AD group.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/groups


    :param string p_orgId: (required in path)
    :param string p_groupId: (required in path)
    :param integer pageStart: (optional in query)
    :param integer pageLimit: (optional in query)
    :param string searchTerm: (optional in query) Search criteria: the string to be searched within the group display
      name.
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/groups")

        api = OrganizationGroupsControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_nested_groups_from_ad_group(
            org_id=p_orgId, group_id=p_groupId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationGroupsControllerApi.get_nested_groups_from_ad_group: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_organization_groups(hub, ctx, p_orgId, **kwargs):
    """Get Organization Groups Get groups of a specific organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/orgs/{orgId}/groups


    :param string p_orgId: (required in path)
    :param integer pageStart: (optional in query)
    :param integer pageLimit: (optional in query)
    :param string groupId: (optional in query) Unique identifier of the group.
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/groups")

        api = OrganizationGroupsControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_organization_groups(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationGroupsControllerApi.get_organization_groups: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_paginated_group_users(hub, ctx, p_orgId, p_groupId, **kwargs):
    """Get users in group within organization. Get users in group within organization. Optionally filtered by given firstName,
      lastName or email with contains match. Optionally filter the users by using
      onlyDirectUsers with true to return only direct users and not return the users
      from nested groups.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/users


    :param string p_orgId: (required in path)
    :param string p_groupId: (required in path)
    :param integer pageStart: (optional in query)
    :param integer pageLimit: (optional in query)
    :param string firstName: (optional in query)
    :param string lastName: (optional in query)
    :param string email: (optional in query)
    :param boolean onlyDirectUsers: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/users")

        api = OrganizationGroupsControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_paginated_group_users(
            org_id=p_orgId, group_id=p_groupId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationGroupsControllerApi.get_paginated_group_users: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def remove_groups_from_organization(hub, ctx, p_orgId, **kwargs):
    """Remove Groups From Organization Remove groups from organization.Note:
      DEPRECATED: Response field failed will be deprecated. You can use the field
      failures instead.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs DELETE /csp/gateway/am/api/orgs/{orgId}/groups


    :param string p_orgId: (required in path)
    :param array ids: (optional in body)
    :param boolean notifyUsersInGroups: (optional in body)
    """

    try:

        hub.log.debug("DELETE /csp/gateway/am/api/orgs/{orgId}/groups")

        api = OrganizationGroupsControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}

        if "ids" in kwargs:
            hub.log.debug(f"Got kwarg 'ids' = {kwargs['ids']}")
            body["ids"] = kwargs.get("ids")
            del kwargs["ids"]
        if "notifyUsersInGroups" in kwargs:
            hub.log.debug(
                f"Got kwarg 'notifyUsersInGroups' = {kwargs['notifyUsersInGroups']}"
            )
            body["notifyUsersInGroups"] = kwargs.get("notifyUsersInGroups")
            del kwargs["notifyUsersInGroups"]

        ret = api.remove_groups_from_organization(body, org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationGroupsControllerApi.remove_groups_from_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_group_roles_on_organization(hub, ctx, p_orgId, p_groupId, **kwargs):
    """Update Group Roles On Organization Update roles of a group within organization.Note: Email notification for
      updating group roles of group is disabled by the Identity Service.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs PATCH /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/roles


    :param string p_orgId: (required in path) Unique identifier (GUID) of the organization.
    :param string p_groupId: (required in path) Unique identifier of the group.
    :param Any organizationRoles: (optional in body)
    :param array serviceRoles: (optional in body) Service roles to be updated.
    :param boolean notifyUsersInGroups: (optional in body) Publish email notification to group members. Ignored by the Identity
      Service
    """

    try:

        hub.log.debug("PATCH /csp/gateway/am/api/orgs/{orgId}/groups/{groupId}/roles")

        api = OrganizationGroupsControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}

        if "organizationRoles" in kwargs:
            hub.log.debug(
                f"Got kwarg 'organizationRoles' = {kwargs['organizationRoles']}"
            )
            body["organizationRoles"] = kwargs.get("organizationRoles")
            del kwargs["organizationRoles"]
        if "serviceRoles" in kwargs:
            hub.log.debug(f"Got kwarg 'serviceRoles' = {kwargs['serviceRoles']}")
            body["serviceRoles"] = kwargs.get("serviceRoles")
            del kwargs["serviceRoles"]
        if "notifyUsersInGroups" in kwargs:
            hub.log.debug(
                f"Got kwarg 'notifyUsersInGroups' = {kwargs['notifyUsersInGroups']}"
            )
            body["notifyUsersInGroups"] = kwargs.get("notifyUsersInGroups")
            del kwargs["notifyUsersInGroups"]

        ret = api.update_group_roles_on_organization(
            body, org_id=p_orgId, group_id=p_groupId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationGroupsControllerApi.update_group_roles_on_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
