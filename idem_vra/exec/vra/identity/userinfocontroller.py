from idem_vra.client.vra_identity_lib.api import UserInfoControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_access_token_info(hub, ctx, q_Authorization, **kwargs):
    """Display detailed user information compiled from the user access token and the
      associated ID token. The user information includes all user related fields (which are also part of
      the associated user ID token). Group ids and names are included.The API
      diverges from the CSP API only on email_verified field which is currently is
      not provided by this API.
      Access Policy
      Role  Access
      Anonymous    Performs GET /csp/gateway/am/api/userinfo


    :param string q_Authorization: (required in header)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/userinfo")

        api = UserInfoControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_access_token_info(authorization=q_Authorization, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserInfoControllerApi.get_access_token_info: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
