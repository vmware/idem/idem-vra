from idem_vra.client.vra_identity_lib.api import OpenidConfigurationControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_openid_configuration(hub, ctx, **kwargs):
    """OpenID Connect discovery endpoint Get discovery endpoint meta data as described in
    https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata.
    OpenID Connect 1.0 is a simple identity layer on top of the OAuth 2.0 protocol.
    It enables Clients to verify the identity of the End-User based on the
    authentication performed by an Authorization Server, as well as to obtain basic
    profile information about the End-User in an interoperable and REST-like
    manner.
    Access Policy
    Role  Access
    Anonymous    Performs GET /.well-known/openid-configuration


    """

    try:

        hub.log.debug("GET /.well-known/openid-configuration")

        api = OpenidConfigurationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_openid_configuration(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OpenidConfigurationControllerApi.get_openid_configuration: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
