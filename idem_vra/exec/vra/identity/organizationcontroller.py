from idem_vra.client.vra_identity_lib.api import OrganizationControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_by_id(hub, ctx, p_orgId, **kwargs):
    """Read an organization.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/orgs/{orgId}


    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}")

        api = OrganizationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_by_id(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationControllerApi.get_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_org_roles(hub, ctx, p_orgId, **kwargs):
    """Get Organization Roles Get list of organization roles.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/orgs/{orgId}/roles


    :param string p_orgId: (required in path) Unique identifier (GUID) of the organization.
    :param string name: (optional in query) Role name (or comma separated list of role names
    :param boolean expand: (optional in query) Indicates if response should be expanded, value is ignored - only
      existence of parameter is checked
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/roles")

        api = OrganizationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_org_roles(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationControllerApi.get_org_roles: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_org_sub_orgs(hub, ctx, p_orgId, **kwargs):
    """Get all organizations sub-orgs. Returns all sub organizations for the passed Org ID. The user needs to be
      either organization owner or a platform operator in order to get a result.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/orgs/{orgId}/sub-orgs


    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/sub-orgs")

        api = OrganizationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_org_sub_orgs(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationControllerApi.get_org_sub_orgs: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_role_by_org_id_and_role_id(hub, ctx, p_orgId, p_roleId, **kwargs):
    """Read a role.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/orgs/{orgId}/roles/{roleId}


    :param string p_orgId: (required in path)
    :param string p_roleId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/roles/{roleId}")

        api = OrganizationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_role_by_org_id_and_role_id(
            org_id=p_orgId, role_id=p_roleId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationControllerApi.get_role_by_org_id_and_role_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_org_roles(hub, ctx, p_orgId, userList, **kwargs):
    """Edit roles.       Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs PATCH /csp/gateway/am/api/orgs/{orgId}/roles


    :param string p_orgId: (required in path)
    :param array userList: (required in body)
    :param Any servicesRolesPatchRequest: (optional in body)
    """

    try:

        hub.log.debug("PATCH /csp/gateway/am/api/orgs/{orgId}/roles")

        api = OrganizationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}
        body["userList"] = userList

        if "servicesRolesPatchRequest" in kwargs:
            hub.log.debug(
                f"Got kwarg 'servicesRolesPatchRequest' = {kwargs['servicesRolesPatchRequest']}"
            )
            body["servicesRolesPatchRequest"] = kwargs.get("servicesRolesPatchRequest")
            del kwargs["servicesRolesPatchRequest"]

        ret = api.patch_org_roles(body, org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationControllerApi.patch_org_roles: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_org(hub, ctx, p_orgId, **kwargs):
    """Update organization details. NOTE: At the moment we only allow for displayName change.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs PATCH /csp/gateway/am/api/orgs/{orgId}


    :param string p_orgId: (required in path)
    :param string name: (optional in body)
    :param string displayName: (optional in body)
    :param object metadata: (optional in body)
    :param string id: (optional in body)
    :param string refLink: (optional in body)
    :param string parentRefLink: (optional in body)
    """

    try:

        hub.log.debug("PATCH /csp/gateway/am/api/orgs/{orgId}")

        api = OrganizationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}

        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "displayName" in kwargs:
            hub.log.debug(f"Got kwarg 'displayName' = {kwargs['displayName']}")
            body["displayName"] = kwargs.get("displayName")
            del kwargs["displayName"]
        if "metadata" in kwargs:
            hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
            body["metadata"] = kwargs.get("metadata")
            del kwargs["metadata"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "refLink" in kwargs:
            hub.log.debug(f"Got kwarg 'refLink' = {kwargs['refLink']}")
            body["refLink"] = kwargs.get("refLink")
            del kwargs["refLink"]
        if "parentRefLink" in kwargs:
            hub.log.debug(f"Got kwarg 'parentRefLink' = {kwargs['parentRefLink']}")
            body["parentRefLink"] = kwargs.get("parentRefLink")
            del kwargs["parentRefLink"]

        ret = api.patch_org(body, org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationControllerApi.patch_org: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def search_org_groups(hub, ctx, p_orgId, q_groupSearchTerm, **kwargs):
    """Find groups. Performs a search for groups in the organization.
      Access Policy
      Role  Access
      Platform operator  c
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/orgs/{orgId}/groups-search


    :param string p_orgId: (required in path)
    :param string q_groupSearchTerm: (required in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/groups-search")

        api = OrganizationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.search_org_groups(
            org_id=p_orgId, group_search_term=q_groupSearchTerm, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationControllerApi.search_org_groups: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
