from idem_vra.client.vra_identity_lib.api import UserV2ControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_user_in_any_organization(hub, ctx, p_userId, **kwargs):
    """Get the user (by user ID) belonging to the current organization. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner  c
      Organization Member  c
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/v2/users/{userId}


    :param string p_userId: (required in path)
    :param string expandProfile: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/v2/users/{userId}")

        api = UserV2ControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_in_any_organization(user_id=p_userId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserV2ControllerApi.get_user_in_any_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_info_in_organization(hub, ctx, p_userId, p_orgId, **kwargs):
    """Get the user details by user ID and organization ID. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/v2/users/{userId}/orgs/{orgId}/info


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/v2/users/{userId}/orgs/{orgId}/info")

        api = UserV2ControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_info_in_organization(
            user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserV2ControllerApi.get_user_info_in_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_roles_in_organization(hub, ctx, p_userId, p_orgId, **kwargs):
    """Get the users roles for a particular organization. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/v2/users/{userId}/orgs/{orgId}/roles


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/v2/users/{userId}/orgs/{orgId}/roles")

        api = UserV2ControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_roles_in_organization(
            user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserV2ControllerApi.get_user_roles_in_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_user_service_roles_in_organization(hub, ctx, p_userId, p_orgId, **kwargs):
    """Get the users roles for a particular organization. Currently one user can belong to exactly one organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/v2/users/{userId}/orgs/{orgId}/service-roles


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    :param string serviceDefinitionLink: (optional in query)
    """

    try:

        hub.log.debug(
            "GET /csp/gateway/am/api/v2/users/{userId}/orgs/{orgId}/service-roles"
        )

        api = UserV2ControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        ret = api.get_user_service_roles_in_organization(
            user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserV2ControllerApi.get_user_service_roles_in_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
