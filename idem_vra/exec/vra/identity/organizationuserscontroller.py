from idem_vra.client.vra_identity_lib.api import OrganizationUsersControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_paginated_org_users_info1(hub, ctx, p_orgId, **kwargs):
    """Paginates search for user. Get response encapsulating organization users.
      Fetched page is according to the page start and page limit passed as optional
      parameters. Defaults to page size of 20 and start from the first page. Note
      that pageStart</code> is 1-based index.
      We are currently not supporting the optional serviceDefinitionId</code>
      which is to filter users having access to a service in CSP.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/orgs/{orgId}/users


    :param string p_orgId: (required in path) Unique identifier (GUID) of the organization.
    :param string serviceDefinitionId: (optional in query) Service definition id used to filter users having access to the
      service.
    :param integer pageStart: (optional in query) Specifies the index that the set of results will begin with.
    :param integer pageLimit: (optional in query) Maximum number of users to return in response
    :param string expandProfile: (optional in query) Indicates if the response should be expanded with the user profile,
      the value is ignored, only the existence of parameter is checked.
    :param string includeGroupIdsInRoles: (optional in query) Indicates if the inherited roles in the response should indicate group
      information, the value is ignored, only the existence of parameter is
      checked.
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/users")

        api = OrganizationUsersControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_paginated_org_users_info1(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationUsersControllerApi.get_paginated_org_users_info1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_paginated_org_users_info(hub, ctx, p_orgId, **kwargs):
    """Paginates search for user. Get response encapsulating organization users.
      Fetched page is according to the page start and page limit passed as optional
      parameters. Defaults to page size of 20 and start from the first page. Note
      that pageStart</code> is 1-based index.
      We are currently not supporting the optional serviceDefinitionId</code>
      which is to filter users having access to a service in CSP.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)  c  Performs GET /csp/gateway/am/api/v2/orgs/{orgId}/users


    :param string p_orgId: (required in path) Unique identifier (GUID) of the organization.
    :param string serviceDefinitionId: (optional in query) Service definition id used to filter users having access to the
      service.
    :param integer pageStart: (optional in query) Specifies the index that the set of results will begin with.
    :param integer pageLimit: (optional in query) Maximum number of users to return in response
    :param string expandProfile: (optional in query) Indicates if the response should be expanded with the user profile,
      the value is ignored, only the existence of parameter is checked.
    :param string includeGroupIdsInRoles: (optional in query) Indicates if the inherited roles in the response should indicate group
      information, the value is ignored, only the existence of parameter is
      checked.
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/v2/orgs/{orgId}/users")

        api = OrganizationUsersControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_paginated_org_users_info(org_id=p_orgId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationUsersControllerApi.get_paginated_org_users_info: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def search_users(hub, ctx, p_orgId, q_userSearchTerm, **kwargs):
    """Search for users. Search users in organization having username, firstName, lastName or email
      which "contains" search term e.g. search for "test" will return test@vmware.com
      if test@vmware.com is part of the organization.
      Search results limited to first 20 results. Please refine the search term for
      accurate results. Organization members will receive basic user information.
      Organization owners will additionally receive role details of the users.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member
      Service Account (Whitelisted Client)    Performs GET /csp/gateway/am/api/orgs/{orgId}/users/search


    :param string p_orgId: (required in path) Unique identifier (GUID) of the organization.
    :param string q_userSearchTerm: (required in query) The string to be searched within firstName, lastName, username or
      email.
    :param string expandProfile: (optional in query) Indicates if the response should be expanded with the user profile,
      the value is ignored, only the existence of parameter is checked.
    :param string includeGroupIdsInRoles: (optional in query) Indicates if the inherited roles in the response should indicate group
      information, the value is ignored, only the existence of parameter is
      checked.
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/orgs/{orgId}/users/search")

        api = OrganizationUsersControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.search_users(
            org_id=p_orgId, user_search_term=q_userSearchTerm, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking OrganizationUsersControllerApi.search_users: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
