from idem_vra.client.vra_identity_lib.api import AuthenticationControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_access_token_pkce_flow(hub, ctx, q_authorization, grant_type, **kwargs):
    """Exchanges one of the following grants: authorization_code, refresh_token,
      client_credentials or client_delegate for access token. This end-point exchanges one of the following grants: authorization_code,
      refresh_token, client_credentials or client_delegate for an access token.
      Include the parameters using application/x-www-form-urlencoded format in the
      HTTP request body.
      Include Basic Base64_Encode(client_id:client_secret) value in the HTTP
      authorization header.
      Organization ID parameter notes: when organization id is missing from the
      request the default organization will be used.
      Upon password grant type, user default organization will be set if
      available.
      Upon client_credentials grant type, the organization who own the client will
      be set if available.
      Access Policy
      Role  Access
      Anonymous    Performs POST /csp/gateway/am/api/auth/token


    :param string q_authorization: (required in header) Basic auth client credentials [Basic
      Base64_Encode(client_id:client_secret)].Example:</strong>
      authorization: Basic
      Y2xpZW50X2lkOmNsaWVudF9zZWNyZXQ=</i>Decoding this string will
      literally return client_id:client_secret.Note: when using
      Public OAuth clients</strong>, Authorization is required using
      the format Basic Base64_Encode(client_id:) with empty client_secret.
    :param string grant_type: (required in body) OAuth grant types for different use cases.
    :param string identitysession: (optional in cookie)
    :param string code: (optional in body) Authorization code parameter. Mandatory for grant_type
      authorization_code.
    :param string redirect_uri: (optional in body) Service redirect uri. Mandatory for grant_type authorization_code.
    :param string refresh_token: (optional in body) Refresh token parameter. Available for grant_type refresh_token.
    :param string scope: (optional in body) Currently not supported. Present for CSP compatibility.
    :param string org_id: (optional in body) Unique identifier (GUID) of the organization. Available for grant_type
      client_credentials.
    :param string subject_token: (optional in body) Required if the grant_type is client_delegate. A security token that
      represents the identity of the party on behalf of whom the request is
      being made. In client_delegate flow, the token provided MUST BE the
      access token of the user.
    :param string subject_token_type: (optional in body) Required if the grant_type is client_delegate. The identifier for
      the subject token provided as per RFC 8693
      https://tools.ietf.org/html/rfc8693#section-3. In client_delegate
      flow, this value MUST BE urn:ietf:params:oauth:token-
      type:access_token.
    :param string code_verifier: (optional in body) A high-entropy cryptographic random key using the characters [A-Z] /
      [a-z] / [0-9] / - / . / _ / ~ with a minimum length of 43
      characters and a maximum length of 128 characters which was used to
      generate the code_challenge and obtain the authorization code.
      Required if PKCE was used in the authorization code grant request. For
      more information, refer the PKCE RFC at
      https://tools.ietf.org/html/rfc7636.
    """

    try:

        hub.log.debug("POST /csp/gateway/am/api/auth/token")

        api = AuthenticationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}
        body["grant_type"] = grant_type

        if "code" in kwargs:
            hub.log.debug(f"Got kwarg 'code' = {kwargs['code']}")
            body["code"] = kwargs.get("code")
            del kwargs["code"]
        if "redirect_uri" in kwargs:
            hub.log.debug(f"Got kwarg 'redirect_uri' = {kwargs['redirect_uri']}")
            body["redirect_uri"] = kwargs.get("redirect_uri")
            del kwargs["redirect_uri"]
        if "refresh_token" in kwargs:
            hub.log.debug(f"Got kwarg 'refresh_token' = {kwargs['refresh_token']}")
            body["refresh_token"] = kwargs.get("refresh_token")
            del kwargs["refresh_token"]
        if "scope" in kwargs:
            hub.log.debug(f"Got kwarg 'scope' = {kwargs['scope']}")
            body["scope"] = kwargs.get("scope")
            del kwargs["scope"]
        if "org_id" in kwargs:
            hub.log.debug(f"Got kwarg 'org_id' = {kwargs['org_id']}")
            body["org_id"] = kwargs.get("org_id")
            del kwargs["org_id"]
        if "subject_token" in kwargs:
            hub.log.debug(f"Got kwarg 'subject_token' = {kwargs['subject_token']}")
            body["subject_token"] = kwargs.get("subject_token")
            del kwargs["subject_token"]
        if "subject_token_type" in kwargs:
            hub.log.debug(
                f"Got kwarg 'subject_token_type' = {kwargs['subject_token_type']}"
            )
            body["subject_token_type"] = kwargs.get("subject_token_type")
            del kwargs["subject_token_type"]
        if "code_verifier" in kwargs:
            hub.log.debug(f"Got kwarg 'code_verifier' = {kwargs['code_verifier']}")
            body["code_verifier"] = kwargs.get("code_verifier")
            del kwargs["code_verifier"]

        ret = api.get_access_token_pkce_flow(
            body, authorization=q_authorization, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AuthenticationControllerApi.get_access_token_pkce_flow: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_access_token_with_authorization_request(
    hub, ctx, grant_type, state, **kwargs
):
    """Get an access token. Exchange authorization code, refresh token or client_credentials to user access
      token.
      Access Policy
      Role  Access
      Anonymous    Performs POST /csp/gateway/am/api/auth/authorize


    :param string grant_type: (required in body) The type of authorization to be performed.
    :param string state: (required in body) A transparent state of the request.
    :param string authorization: (optional in header)
    :param string refresh_token: (optional in body) The refresh token when grant_type</code> is set to
      refresh_token</code>
    :param string code: (optional in body) The authorization code when grant_type</code> is set to
      authorization_code</code>
    :param string redirect_uri: (optional in body) The URI to which a redirect will be performed upon successful
      authorization.
    :param string client_id: (optional in body) The client ID when grant_type</code> is set to
      client_credentials</code>. Will be ignored if the
      Authorization</code> header is set.
    :param string client_secret: (optional in body) The client secret when grant_type</code> is set to
      client_credentials</code>. Will be ignored if the
      Authorization</code> header is set.
    :param string scope: (optional in body) Currently not supported. Present for CSP compatibility.
    :param string orgId: (optional in body) When grant_type</code> is set to client_credentials</code>
      if this parameter is set the issued token will be limited to the
      specified organization.
    """

    try:

        hub.log.debug("POST /csp/gateway/am/api/auth/authorize")

        api = AuthenticationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}
        body["grant_type"] = grant_type
        body["state"] = state

        if "refresh_token" in kwargs:
            hub.log.debug(f"Got kwarg 'refresh_token' = {kwargs['refresh_token']}")
            body["refresh_token"] = kwargs.get("refresh_token")
            del kwargs["refresh_token"]
        if "code" in kwargs:
            hub.log.debug(f"Got kwarg 'code' = {kwargs['code']}")
            body["code"] = kwargs.get("code")
            del kwargs["code"]
        if "redirect_uri" in kwargs:
            hub.log.debug(f"Got kwarg 'redirect_uri' = {kwargs['redirect_uri']}")
            body["redirect_uri"] = kwargs.get("redirect_uri")
            del kwargs["redirect_uri"]
        if "client_id" in kwargs:
            hub.log.debug(f"Got kwarg 'client_id' = {kwargs['client_id']}")
            body["client_id"] = kwargs.get("client_id")
            del kwargs["client_id"]
        if "client_secret" in kwargs:
            hub.log.debug(f"Got kwarg 'client_secret' = {kwargs['client_secret']}")
            body["client_secret"] = kwargs.get("client_secret")
            del kwargs["client_secret"]
        if "scope" in kwargs:
            hub.log.debug(f"Got kwarg 'scope' = {kwargs['scope']}")
            body["scope"] = kwargs.get("scope")
            del kwargs["scope"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]

        ret = api.get_access_token_with_authorization_request(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AuthenticationControllerApi.get_access_token_with_authorization_request: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_access_token_with_refresh_token(hub, ctx, **kwargs):
    """Exchange organization scoped API-token for user access token. To obtain the access token please follow the steps described in the official
      product documentation. Using the token generated by the Identity Service API
      alone will not work due to a missing internal state.DEPRECATED: Passing
      the refresh_token param name, need to use api_token.
      Access Policy
      Role  Access
      Anonymous    Performs POST /csp/gateway/am/api/auth/api-tokens/authorize


    :param string refresh_token: (optional in body) The refresh token. Deprecated, need to use api_token
    :param string api_token: (optional in body) The API-token
    """

    try:

        hub.log.debug("POST /csp/gateway/am/api/auth/api-tokens/authorize")

        api = AuthenticationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}

        if "refresh_token" in kwargs:
            hub.log.debug(f"Got kwarg 'refresh_token' = {kwargs['refresh_token']}")
            body["refresh_token"] = kwargs.get("refresh_token")
            del kwargs["refresh_token"]
        if "api_token" in kwargs:
            hub.log.debug(f"Got kwarg 'api_token' = {kwargs['api_token']}")
            body["api_token"] = kwargs.get("api_token")
            del kwargs["api_token"]

        ret = api.get_access_token_with_refresh_token(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AuthenticationControllerApi.get_access_token_with_refresh_token: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_keys(hub, ctx, **kwargs):
    """Defines the public keys used to verify the authenticity of the JWT token. Defines the public keys used to verify the authenticity of the JWT token.
    Access Policy
    Role  Access
    Anonymous    Performs GET /csp/gateway/am/api/auth/keys


    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/auth/keys")

        api = AuthenticationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_keys(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AuthenticationControllerApi.get_keys: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_public_key(hub, ctx, **kwargs):
    """Returns the public key. Returns the public key to be used for verifying the idp generated tokens
      signature.
      Access Policy
      Role  Access
      Anonymous    Performs GET /csp/gateway/am/api/auth/token-public-key


    :param string jwks: (optional in query)
    :param string pem: (optional in query)
    """

    try:

        hub.log.debug("GET /csp/gateway/am/api/auth/token-public-key")

        api = AuthenticationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        ret = api.get_public_key(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AuthenticationControllerApi.get_public_key: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def logout(hub, ctx, idToken, **kwargs):
    """Performs logout. Performs a logout by invalidating the supplied token (if supplied) and
      returning an URL to navigate to. The token to invalidate is first looked up in
      the request body.
      Access Policy
      Role  Access
      Anonymous    Performs POST /csp/gateway/am/api/auth/logout


    :param string idToken: (required in body) The ID token.
    :param string idToken: (optional in header)
    """

    try:

        hub.log.debug("POST /csp/gateway/am/api/auth/logout")

        api = AuthenticationControllerApi(
            hub.clients["idem_vra.client.vra_identity_lib.api"]
        )

        body = {}
        body["idToken"] = idToken

        ret = api.logout(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AuthenticationControllerApi.logout: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
