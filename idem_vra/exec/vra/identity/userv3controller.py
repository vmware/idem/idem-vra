from idem_vra.client.vra_identity_lib.api import UserV3ControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def patch_user_roles_on_organization(hub, ctx, p_userId, p_orgId, **kwargs):
    """(BETA) Update user roles in organization. Update service and organization roles of a user in the organization.
      Access Policy
      Role  Access
      Platform operator
      Organization Owner
      Organization Member  c
      Service Account (Whitelisted Client)  c  Performs PATCH /csp/gateway/am/api/v3/users/{userId}/orgs/{orgId}/roles


    :param string p_userId: (required in path)
    :param string p_orgId: (required in path)
    :param Any organizationRoles: (optional in body)
    :param array serviceRoles: (optional in body)
    """

    try:

        hub.log.debug("PATCH /csp/gateway/am/api/v3/users/{userId}/orgs/{orgId}/roles")

        api = UserV3ControllerApi(hub.clients["idem_vra.client.vra_identity_lib.api"])

        body = {}

        if "organizationRoles" in kwargs:
            hub.log.debug(
                f"Got kwarg 'organizationRoles' = {kwargs['organizationRoles']}"
            )
            body["organizationRoles"] = kwargs.get("organizationRoles")
            del kwargs["organizationRoles"]
        if "serviceRoles" in kwargs:
            hub.log.debug(f"Got kwarg 'serviceRoles' = {kwargs['serviceRoles']}")
            body["serviceRoles"] = kwargs.get("serviceRoles")
            del kwargs["serviceRoles"]

        ret = api.patch_user_roles_on_organization(
            body, user_id=p_userId, org_id=p_orgId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserV3ControllerApi.patch_user_roles_on_organization: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
