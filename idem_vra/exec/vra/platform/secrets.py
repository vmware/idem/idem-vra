from idem_vra.client.vra_platform_lib.api import SecretsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_secret_v2(hub, ctx, name, value, **kwargs):
    """Creates a secret  Performs POST /platform/api/secrets


    :param string name: (required in body) A human-friendly name used as an identifier for a secret. Secret name
      should not contain special characters.
    :param string value: (required in body) The value of secret.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format. For versioning
      information refer to /platform/api/about.
    :param string description: (optional in body) A human-friendly description.
    :param array projectIdsToAdd: (optional in body) A list of project IDs to add this secret to.
    :param boolean orgScoped: (optional in body) This boolean checks if the secret is org scope or project scope
    """

    try:

        hub.log.debug("POST /platform/api/secrets")

        api = SecretsApi(hub.clients["idem_vra.client.vra_platform_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2023-01-01"

        body = {}
        body["name"] = name
        body["value"] = value

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "projectIdsToAdd" in kwargs:
            hub.log.debug(f"Got kwarg 'projectIdsToAdd' = {kwargs['projectIdsToAdd']}")
            body["projectIdsToAdd"] = kwargs.get("projectIdsToAdd")
            del kwargs["projectIdsToAdd"]
        if "orgScoped" in kwargs:
            hub.log.debug(f"Got kwarg 'orgScoped' = {kwargs['orgScoped']}")
            body["orgScoped"] = kwargs.get("orgScoped")
            del kwargs["orgScoped"]

        ret = api.create_secret_v2(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecretsApi.create_secret_v2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_secrets_v2(hub, ctx, **kwargs):
    """Get all secrets Get all secrets with specified paging parameters. Performs GET /platform/api/secrets


    :param string projectId: (optional in query) The id of the project
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format. For versioning
      information refer to /platform/api/about.
    """

    try:

        hub.log.debug("GET /platform/api/secrets")

        api = SecretsApi(hub.clients["idem_vra.client.vra_platform_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2023-01-01"

        ret = api.get_all_secrets_v2(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecretsApi.get_all_secrets_v2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_projects_linked_to_secret(hub, ctx, p_id, **kwargs):
    """Retrieves a paginated list of projects linked to a secret. Retrieves a paginated list of projects linked to a secret. Performs GET /platform/api/secrets/{id}/projects


    :param string p_id: (required in path) the id of the secret
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format. For versioning
      information refer to /platform/api/about.
    :param integer page: (optional in query) Number of page.
    :param integer size: (optional in query) Number of elements per page.
    """

    try:

        hub.log.debug("GET /platform/api/secrets/{id}/projects")

        api = SecretsApi(hub.clients["idem_vra.client.vra_platform_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2023-01-01"

        ret = api.get_projects_linked_to_secret(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecretsApi.get_projects_linked_to_secret: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_projects(hub, ctx, q_pageRequest, **kwargs):
    """Retrieves a paginated list of projects mapped and unmapped to a secret. Retrieves a paginated list of projects mapped and unmapped to a secret. Performs GET /platform/api/secrets/projects


    :param None q_pageRequest: (required in query)
    :param string id: (optional in query) The id of the secret
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format. For versioning
      information refer to /platform/api/about.
    """

    try:

        hub.log.debug("GET /platform/api/secrets/projects")

        api = SecretsApi(hub.clients["idem_vra.client.vra_platform_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2023-01-01"

        ret = api.get_projects(page_request=q_pageRequest, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecretsApi.get_projects: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_secret_v2(hub, ctx, p_id, name, value, **kwargs):
    """Updates a secret  Performs PATCH /platform/api/secrets/{id}


    :param string p_id: (required in path) the id of the secret
    :param string name: (required in body) A human-friendly name used as an identifier for a secret. Secret name
      should not contain special characters.
    :param string value: (required in body) The value of secret.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format. For versioning
      information refer to /platform/api/about.
    :param string description: (optional in body) A human-friendly description.
    :param boolean orgScoped: (optional in body) This boolean checks if the secret is org scope or project scope
    :param array projectIdsToAdd: (optional in body) A list of project IDs to add this secret to.
    :param array projectIdsToRemove: (optional in body) A list of project IDs to remove this secret from.
    """

    try:

        hub.log.debug("PATCH /platform/api/secrets/{id}")

        api = SecretsApi(hub.clients["idem_vra.client.vra_platform_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2023-01-01"

        body = {}
        body["name"] = name
        body["value"] = value

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "orgScoped" in kwargs:
            hub.log.debug(f"Got kwarg 'orgScoped' = {kwargs['orgScoped']}")
            body["orgScoped"] = kwargs.get("orgScoped")
            del kwargs["orgScoped"]
        if "projectIdsToAdd" in kwargs:
            hub.log.debug(f"Got kwarg 'projectIdsToAdd' = {kwargs['projectIdsToAdd']}")
            body["projectIdsToAdd"] = kwargs.get("projectIdsToAdd")
            del kwargs["projectIdsToAdd"]
        if "projectIdsToRemove" in kwargs:
            hub.log.debug(
                f"Got kwarg 'projectIdsToRemove' = {kwargs['projectIdsToRemove']}"
            )
            body["projectIdsToRemove"] = kwargs.get("projectIdsToRemove")
            del kwargs["projectIdsToRemove"]

        ret = api.update_secret_v2(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecretsApi.update_secret_v2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
