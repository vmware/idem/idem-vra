from idem_vra.client.vra_iaas_lib.api import DataCollectorApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_data_collector(hub, ctx, **kwargs):
    """Create Data collector Create a new Data Collector.
      Note: Data collector endpoints are not available in VMware Aria Automation (on-
      prem) release. Performs POST /iaas/api/data-collectors


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/data-collectors")

        api = DataCollectorApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.create_data_collector(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DataCollectorApi.create_data_collector: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_data_collector(hub, ctx, p_id, **kwargs):
    """Delete Data Collector Delete Data Collector with a given id.
      Note: Data collector endpoints are not available in VMware Aria Automation (on-
      prem) release. Performs DELETE /iaas/api/data-collectors/{id}


    :param string p_id: (required in path) The ID of the Data Collector.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/data-collectors/{id}")

        api = DataCollectorApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_data_collector(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DataCollectorApi.delete_data_collector: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_data_collector(hub, ctx, p_id, **kwargs):
    """Get Data Collector Get Data Collector with a given id.
      Note: Data collector endpoints are not available in VMware Aria Automation (on-
      prem) release. Performs GET /iaas/api/data-collectors/{id}


    :param string p_id: (required in path) The ID of the Data Collector.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/data-collectors/{id}")

        api = DataCollectorApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_data_collector(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DataCollectorApi.get_data_collector: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_data_collectors(hub, ctx, **kwargs):
    """Get Data Collectors Get all Data Collectors.
      Note: Data collector endpoints are not available in VMware Aria Automation (on-
      prem) release. Performs GET /iaas/api/data-collectors


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean disabled: (optional in query) If query param is provided with value equals to true, only disabled
      data collectors will be retrieved.
    """

    try:

        hub.log.debug("GET /iaas/api/data-collectors")

        api = DataCollectorApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_data_collectors(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DataCollectorApi.get_data_collectors: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
