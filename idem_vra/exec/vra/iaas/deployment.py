from idem_vra.client.vra_iaas_lib.api import DeploymentApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_deployment(hub, ctx, name, projectId, **kwargs):
    """Create deployment Create a new Deployment. Performs POST /iaas/api/deployments


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    """

    try:

        hub.log.debug("POST /iaas/api/deployments")

        api = DeploymentApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]

        ret = api.create_deployment(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentApi.create_deployment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_deployment(hub, ctx, p_id, **kwargs):
    """Delete a deployment Delete a deployment. Performs DELETE /iaas/api/deployments/{id}


    :param string p_id: (required in path) The id of the deployment.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean forceDelete: (optional in query) If true, best effort is made for deleting this deployment and all
      related resources. In some situations, this may leave provisioned
      infrastructure resources behind. Please ensure you remove them
      manually. If false, a standard delete action will be executed.
    """

    try:

        hub.log.debug("DELETE /iaas/api/deployments/{id}")

        api = DeploymentApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_deployment(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentApi.delete_deployment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployments(hub, ctx, **kwargs):
    """Get Deployments Get all deployments. Performs GET /iaas/api/deployments


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/deployments")

        api = DeploymentApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_deployments(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentApi.get_deployments: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_single_deployment(hub, ctx, p_id, **kwargs):
    """Get a single deployment Get a single deployment. Performs GET /iaas/api/deployments/{id}


    :param string p_id: (required in path) The id of the deployment.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/deployments/{id}")

        api = DeploymentApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_single_deployment(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentApi.get_single_deployment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
