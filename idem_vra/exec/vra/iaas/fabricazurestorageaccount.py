from idem_vra.client.vra_iaas_lib.api import FabricAzureStorageAccountApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_azure_storage_account(hub, ctx, p_id, **kwargs):
    """Get fabric Azure storage account Get fabric Azure storage account with a given id Performs GET /iaas/api/fabric-azure-storage-accounts/{id}


    :param string p_id: (required in path) The ID of the Fabric Azure Storage Account.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-azure-storage-accounts/{id}")

        api = FabricAzureStorageAccountApi(
            hub.clients["idem_vra.client.vra_iaas_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_fabric_azure_storage_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricAzureStorageAccountApi.get_fabric_azure_storage_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_fabric_azure_storage_accounts(hub, ctx, **kwargs):
    """Get fabric Azure storage accounts Get all fabric Azure storage accounts. Performs GET /iaas/api/fabric-azure-storage-accounts


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-azure-storage-accounts")

        api = FabricAzureStorageAccountApi(
            hub.clients["idem_vra.client.vra_iaas_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_fabric_azure_storage_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricAzureStorageAccountApi.get_fabric_azure_storage_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
