from idem_vra.client.vra_iaas_lib.api import FlavorProfileApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_flavor_profile(hub, ctx, name, flavorMapping, regionId, **kwargs):
    """Create flavor profile Create flavor profile Performs POST /iaas/api/flavor-profiles


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param object flavorMapping: (required in body) Map between global fabric flavor keys  and fabric flavor
      descriptions
    :param string regionId: (required in body) The id of the region for which this profile is created
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    """

    try:

        hub.log.debug("POST /iaas/api/flavor-profiles")

        api = FlavorProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["flavorMapping"] = flavorMapping
        body["regionId"] = regionId

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]

        ret = api.create_flavor_profile(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FlavorProfileApi.create_flavor_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_flavor_profile(hub, ctx, p_id, **kwargs):
    """Delete flavor profile Delete flavor profile with a given id Performs DELETE /iaas/api/flavor-profiles/{id}


    :param string p_id: (required in path) The ID of the flavor.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/flavor-profiles/{id}")

        api = FlavorProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_flavor_profile(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FlavorProfileApi.delete_flavor_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_flavor_profile(hub, ctx, p_id, **kwargs):
    """Get flavor profile Get flavor profile with a given id Performs GET /iaas/api/flavor-profiles/{id}


    :param string p_id: (required in path) The ID of the flavor.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/flavor-profiles/{id}")

        api = FlavorProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_flavor_profile(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FlavorProfileApi.get_flavor_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_flavor_profiles(hub, ctx, **kwargs):
    """Get flavor profile Get all flavor profile Performs GET /iaas/api/flavor-profiles


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/flavor-profiles")

        api = FlavorProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_flavor_profiles(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FlavorProfileApi.get_flavor_profiles: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_flavor_profile(hub, ctx, p_id, name, flavorMapping, **kwargs):
    """Update flavor profile Update flavor profile Performs PATCH /iaas/api/flavor-profiles/{id}


    :param string p_id: (required in path) The ID of the flavor.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param object flavorMapping: (required in body) Map between global fabric flavor keys  and fabric flavor
      descriptions
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    """

    try:

        hub.log.debug("PATCH /iaas/api/flavor-profiles/{id}")

        api = FlavorProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["flavorMapping"] = flavorMapping

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]

        ret = api.update_flavor_profile(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FlavorProfileApi.update_flavor_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
