from idem_vra.client.vra_iaas_lib.api import ImagesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_images(hub, ctx, **kwargs):
    """Get images Get all images defined in ImageProfile. Performs GET /iaas/api/images


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/images")

        api = ImagesApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_images(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ImagesApi.get_images: {err}")
        return ExecReturn(result=False, comment=str(err))
