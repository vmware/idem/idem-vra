from idem_vra.client.vra_iaas_lib.api import CloudAccountApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_aws_cloud_account_async(hub, ctx, name, **kwargs):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-aws


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param string accessKeyId: (optional in body) Aws Access key ID
    :param string secretAccessKey: (optional in body) Aws Secret Access Key
    :param string iamRoleArn: (optional in body) Aws ARN role to be assumed by Aria Auto account
    :param boolean trustedAccount: (optional in body) Create the account as trusted.
    :param array regions: (optional in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-aws")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "accessKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'accessKeyId' = {kwargs['accessKeyId']}")
            body["accessKeyId"] = kwargs.get("accessKeyId")
            del kwargs["accessKeyId"]
        if "secretAccessKey" in kwargs:
            hub.log.debug(f"Got kwarg 'secretAccessKey' = {kwargs['secretAccessKey']}")
            body["secretAccessKey"] = kwargs.get("secretAccessKey")
            del kwargs["secretAccessKey"]
        if "iamRoleArn" in kwargs:
            hub.log.debug(f"Got kwarg 'iamRoleArn' = {kwargs['iamRoleArn']}")
            body["iamRoleArn"] = kwargs.get("iamRoleArn")
            del kwargs["iamRoleArn"]
        if "trustedAccount" in kwargs:
            hub.log.debug(f"Got kwarg 'trustedAccount' = {kwargs['trustedAccount']}")
            body["trustedAccount"] = kwargs.get("trustedAccount")
            del kwargs["trustedAccount"]
        if "regions" in kwargs:
            hub.log.debug(f"Got kwarg 'regions' = {kwargs['regions']}")
            body["regions"] = kwargs.get("regions")
            del kwargs["regions"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.create_aws_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_aws_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_azure_cloud_account_async(
    hub,
    ctx,
    name,
    subscriptionId,
    tenantId,
    clientApplicationId,
    clientApplicationSecretKey,
    regions,
    **kwargs,
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-azure


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string subscriptionId: (required in body) Azure Subscribtion ID
    :param string tenantId: (required in body) Azure Tenant ID
    :param string clientApplicationId: (required in body) Azure Client Application ID
    :param string clientApplicationSecretKey: (required in body) Azure Client Application Secret Key
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-azure")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["subscriptionId"] = subscriptionId
        body["tenantId"] = tenantId
        body["clientApplicationId"] = clientApplicationId
        body["clientApplicationSecretKey"] = clientApplicationSecretKey
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.create_azure_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_azure_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_cloud_account_async(
    hub, ctx, name, cloudAccountType, cloudAccountProperties, regions, **kwargs
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string cloudAccountType: (required in body) Cloud account type
    :param object cloudAccountProperties: (required in body) Cloud Account specific properties supplied in as name value pairs
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
      regionInfos is a required parameter for AWS, AZURE, GCP, VSPHERE,
      VMC, VCF cloud account types.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the cloud
      account
    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      cloud account. In case of AAP pass a dummy value.
    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this cloud account
    :param object associatedMobilityCloudAccountIds: (optional in body) Cloud Account IDs and directionalities create associations to other
      vSphere cloud accounts that can be used for workload mobility. ID
      refers to an associated cloud account, and directionality can be
      unidirectional or bidirectional. Only supported on vSphere cloud
      accounts.
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the Cloud
      Account. In case of AAP, provide environment property here.Example:
      "customProperties": {
      "environment": "aap"
      }
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["cloudAccountType"] = cloudAccountType
        body["cloudAccountProperties"] = cloudAccountProperties
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "privateKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
            body["privateKeyId"] = kwargs.get("privateKeyId")
            del kwargs["privateKeyId"]
        if "privateKey" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
            body["privateKey"] = kwargs.get("privateKey")
            del kwargs["privateKey"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "associatedMobilityCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedMobilityCloudAccountIds' = {kwargs['associatedMobilityCloudAccountIds']}"
            )
            body["associatedMobilityCloudAccountIds"] = kwargs.get(
                "associatedMobilityCloudAccountIds"
            )
            del kwargs["associatedMobilityCloudAccountIds"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.create_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_gcp_cloud_account_async(
    hub, ctx, name, projectId, privateKeyId, privateKey, clientEmail, regions, **kwargs
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-gcp


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) GCP Project ID
    :param string privateKeyId: (required in body) GCP Private key ID
    :param string privateKey: (required in body) GCP Private key
    :param string clientEmail: (required in body) GCP Client email
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-gcp")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId
        body["privateKeyId"] = privateKeyId
        body["privateKey"] = privateKey
        body["clientEmail"] = clientEmail
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.create_gcp_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_gcp_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_nsx_alb_cloud_account_async(
    hub, ctx, name, hostName, username, password, regions, **kwargs
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-nsx-alb


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the NSX Advanced Load Balancer endpoint
    :param string username: (required in body) Username to authenticate with the cloud account
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    :param object cloudAccountProperties: (optional in body) Cloud Account specific properties supplied in as name value pairs
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-nsx-alb")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["username"] = username
        body["password"] = password
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "cloudAccountProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'cloudAccountProperties' = {kwargs['cloudAccountProperties']}"
            )
            body["cloudAccountProperties"] = kwargs.get("cloudAccountProperties")
            del kwargs["cloudAccountProperties"]

        ret = api.create_nsx_alb_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_nsx_alb_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_nsx_t_cloud_account_async(
    hub, ctx, name, hostName, dcid, username, password, **kwargs
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-nsx-t


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the NSX-T endpoint
    :param string dcid: (required in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release and hence the data collector Id is
      optional for VMware Aria Automation (on-prem).
    :param string username: (required in body) Username to authenticate with the cloud account
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param array associatedCloudAccountIds: (optional in body) vSphere cloud accounts associated with this NSX-T cloud account.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param boolean managerMode: (optional in body) Create NSX-T cloud account in Manager (legacy) mode. When set to true,
      NSX-T cloud account in created in Manager mode. Mode cannot be changed
      after cloud account is created. Default value is false.
    :param boolean isGlobalManager: (optional in body) Indicates whether this is an NSX-T Global Manager cloud account. NSX-T
      Global Manager can only be associated with NSX-T cloud accounts.
      Default value: false.
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-nsx-t")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["dcid"] = dcid
        body["username"] = username
        body["password"] = password

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "managerMode" in kwargs:
            hub.log.debug(f"Got kwarg 'managerMode' = {kwargs['managerMode']}")
            body["managerMode"] = kwargs.get("managerMode")
            del kwargs["managerMode"]
        if "isGlobalManager" in kwargs:
            hub.log.debug(f"Got kwarg 'isGlobalManager' = {kwargs['isGlobalManager']}")
            body["isGlobalManager"] = kwargs.get("isGlobalManager")
            del kwargs["isGlobalManager"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.create_nsx_t_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_nsx_t_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_nsx_v_cloud_account_async(
    hub, ctx, name, hostName, dcid, username, password, **kwargs
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-nsx-v


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the NSX-v endpoint
    :param string dcid: (required in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release and hence the data collector Id is
      optional for VMware Aria Automation (on-prem).
    :param string username: (required in body) Username to authenticate with the cloud account
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param array associatedCloudAccountIds: (optional in body) vSphere cloud account associated with this NSX-V cloud account. NSX-V
      cloud account can be associated with a single vSphere cloud account.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-nsx-v")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["dcid"] = dcid
        body["username"] = username
        body["password"] = password

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.create_nsx_v_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_nsx_v_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_v_sphere_cloud_account_async(
    hub, ctx, name, hostName, regions, **kwargs
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-vsphere


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the vSphere endpoint
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere
    :param array associatedCloudAccountIds: (optional in body) NSX-V or NSX-T account to associate with this vSphere cloud account.
      vSphere cloud account can be a single NSX-V cloud account or a single
      NSX-T cloud account.
    :param object associatedMobilityCloudAccountIds: (optional in body) Cloud account IDs and directionalities create associations to other
      vSphere cloud accounts that can be used for workload mobility. ID
      refers to an associated cloud account, and directionality can be
      unidirectional or bidirectional.
    :param string dcid: (optional in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release.
    :param string username: (optional in body) Username to authenticate with the cloud account. Not required when
      environment is set to aap.
    :param string password: (optional in body) Password for the user used to authenticate with the cloud Account. Not
      required when environment is set to aap.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    :param string environment: (optional in body) The environment where data collectors are deployed. When the data
      collectors are deployed on an aap-based cloud gateway appliance, use
      "aap".
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-vsphere")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "associatedMobilityCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedMobilityCloudAccountIds' = {kwargs['associatedMobilityCloudAccountIds']}"
            )
            body["associatedMobilityCloudAccountIds"] = kwargs.get(
                "associatedMobilityCloudAccountIds"
            )
            del kwargs["associatedMobilityCloudAccountIds"]
        if "dcid" in kwargs:
            hub.log.debug(f"Got kwarg 'dcid' = {kwargs['dcid']}")
            body["dcid"] = kwargs.get("dcid")
            del kwargs["dcid"]
        if "username" in kwargs:
            hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
            body["username"] = kwargs.get("username")
            del kwargs["username"]
        if "password" in kwargs:
            hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
            body["password"] = kwargs.get("password")
            del kwargs["password"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "environment" in kwargs:
            hub.log.debug(f"Got kwarg 'environment' = {kwargs['environment']}")
            body["environment"] = kwargs.get("environment")
            del kwargs["environment"]

        ret = api.create_v_sphere_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_v_sphere_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_vcf_cloud_account_async(
    hub,
    ctx,
    name,
    workloadDomainId,
    workloadDomainName,
    vcenterHostName,
    vcenterUsername,
    vcenterPassword,
    nsxHostName,
    nsxUsername,
    nsxPassword,
    regions,
    **kwargs,
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-vcf


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string workloadDomainId: (required in body) Id of the workload domain to add as VCF cloud account.
    :param string workloadDomainName: (required in body) Name of the workload domain to add as VCF cloud account.
    :param string vcenterHostName: (required in body) Host name for the vSphere from the specified workload domain.
    :param string vcenterUsername: (required in body) vCenter user name for the specified workload domain.The specified user
      requires CloudAdmin credentials. The user does not require
      CloudGlobalAdmin credentials.
    :param string vcenterPassword: (required in body) Password for the user used to authenticate with the vCenter in VCF
      cloud account
    :param string nsxHostName: (required in body) Host name for the NSX endpoint from the specified workload domain.
    :param string nsxUsername: (required in body) User name for the NSX manager in the specified workload domain.
    :param string nsxPassword: (required in body) Password for the user used to authenticate with the NSX-T manager in
      VCF cloud account
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param string sddcManagerId: (optional in body) SDDC manager integration id
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud
      Account.Cloud account capability tags may enable different features.
    :param string vcenterCertificate: (optional in body) vCenter Certificate
    :param string nsxCertificate: (optional in body) NSX Certificate
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere and NSX-T
    :param string dcId: (optional in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release.
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-vcf")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["workloadDomainId"] = workloadDomainId
        body["workloadDomainName"] = workloadDomainName
        body["vcenterHostName"] = vcenterHostName
        body["vcenterUsername"] = vcenterUsername
        body["vcenterPassword"] = vcenterPassword
        body["nsxHostName"] = nsxHostName
        body["nsxUsername"] = nsxUsername
        body["nsxPassword"] = nsxPassword
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "sddcManagerId" in kwargs:
            hub.log.debug(f"Got kwarg 'sddcManagerId' = {kwargs['sddcManagerId']}")
            body["sddcManagerId"] = kwargs.get("sddcManagerId")
            del kwargs["sddcManagerId"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "vcenterCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'vcenterCertificate' = {kwargs['vcenterCertificate']}"
            )
            body["vcenterCertificate"] = kwargs.get("vcenterCertificate")
            del kwargs["vcenterCertificate"]
        if "nsxCertificate" in kwargs:
            hub.log.debug(f"Got kwarg 'nsxCertificate' = {kwargs['nsxCertificate']}")
            body["nsxCertificate"] = kwargs.get("nsxCertificate")
            del kwargs["nsxCertificate"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "dcId" in kwargs:
            hub.log.debug(f"Got kwarg 'dcId' = {kwargs['dcId']}")
            body["dcId"] = kwargs.get("dcId")
            del kwargs["dcId"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.create_vcf_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_vcf_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_vmc_cloud_account_async(
    hub,
    ctx,
    name,
    apiKey,
    sddcId,
    username,
    password,
    hostName,
    nsxHostName,
    dcId,
    regions,
    **kwargs,
):
    """Create a cloud account Create a cloud account in the current organization Performs POST /iaas/api/cloud-accounts-vmc


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiKey: (required in body) VMC API access key. Not mandatory for AAP mode.
    :param string sddcId: (required in body) Identifier of the on-premise SDDC to be used by this cloud account.
      Note that NSX-V SDDCs are not supported.
    :param string username: (required in body) vCenter user name for the specified SDDC.The specified user requires
      CloudAdmin credentials. The user does not require CloudGlobalAdmin
      credentials. Not mandatory for AAP mode.
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account. Not
      mandatory for AAP mode.
    :param string hostName: (required in body) Enter the IP address or FQDN of the vCenter Server in the specified
      SDDC. The cloud proxy belongs on this vCenter.
    :param string nsxHostName: (required in body) The IP address of the NSX Manager server in the specified SDDC / FQDN.
    :param string dcId: (required in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors. Not mandatory for AAP mode.
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.
    :param string description: (optional in body) A human-friendly description.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud
      Account.Cloud account capability tags may enable different features.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere
    :param Any certificateInfo: (optional in body)
    :param string environment: (optional in body) The environment where the agent has been deployed. When the agent has
      been deployed using the "Add Ons" in VMC UI or Api use "aap".
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-vmc")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["apiKey"] = apiKey
        body["sddcId"] = sddcId
        body["username"] = username
        body["password"] = password
        body["hostName"] = hostName
        body["nsxHostName"] = nsxHostName
        body["dcId"] = dcId
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "environment" in kwargs:
            hub.log.debug(f"Got kwarg 'environment' = {kwargs['environment']}")
            body["environment"] = kwargs.get("environment")
            del kwargs["environment"]

        ret = api.create_vmc_cloud_account_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.create_vmc_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_aws_cloud_account(hub, ctx, p_id, **kwargs):
    """Delete an AWS cloud account Delete an AWS cloud account with a given id Performs DELETE /iaas/api/cloud-accounts-aws/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-aws/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_aws_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_aws_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_azure_cloud_account(hub, ctx, p_id, **kwargs):
    """Delete an Azure Cloud Account Delete an Azure Cloud Account with a given id Performs DELETE /iaas/api/cloud-accounts-azure/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-azure/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_azure_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_azure_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_cloud_account_nsx_t(hub, ctx, p_id, **kwargs):
    """Delete a NSX-T cloud account Delete a NSX-T cloud account with a given id Performs DELETE /iaas/api/cloud-accounts-nsx-t/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-nsx-t/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_cloud_account_nsx_t(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_cloud_account_nsx_t: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_cloud_account_nsx_v(hub, ctx, p_id, **kwargs):
    """Delete a NSV-V cloud account Delete a NSV-V cloud account with a given id Performs DELETE /iaas/api/cloud-accounts-nsx-v/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-nsx-v/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_cloud_account_nsx_v(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_cloud_account_nsx_v: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_cloud_account(hub, ctx, p_id, **kwargs):
    """Delete a cloud account Delete a cloud account with a given id Performs DELETE /iaas/api/cloud-accounts/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_gcp_cloud_account(hub, ctx, p_id, **kwargs):
    """Delete an GCP cloud account Delete an GCP cloud account with a given id Performs DELETE /iaas/api/cloud-accounts-gcp/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-gcp/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_gcp_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_gcp_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_nsx_alb_account_async(hub, ctx, p_id, **kwargs):
    """Delete a NSX Advanced Load Balancer cloud account Delete a NSX Advanced Load Balancer cloud account with a given id Performs DELETE /iaas/api/cloud-accounts-nsx-alb/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-nsx-alb/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_nsx_alb_account_async(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_nsx_alb_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_v_sphere_cloud_account(hub, ctx, p_id, **kwargs):
    """Delete a vSphere cloud account Delete a vSphere Cloud Account with a given id Performs DELETE /iaas/api/cloud-accounts-vsphere/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-vsphere/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_v_sphere_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_v_sphere_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_vcf_cloud_account(hub, ctx, p_id, **kwargs):
    """Delete an VCF cloud account Delete an VCF cloud account with a given id Performs DELETE /iaas/api/cloud-accounts-vcf/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-vcf/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_vcf_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_vcf_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_vmc_cloud_account(hub, ctx, p_id, **kwargs):
    """Delete an VMC cloud account Delete an VMC cloud account with a given id Performs DELETE /iaas/api/cloud-accounts-vmc/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/cloud-accounts-vmc/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_vmc_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.delete_vmc_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_aws_regions_async(hub, ctx, **kwargs):
    """Get the available regions for specified AWS cloud account Get the available regions for specified AWS cloud account Performs POST /iaas/api/cloud-accounts-aws/region-enumeration


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string accessKeyId: (optional in body) Aws Access key ID. Either provide accessKeyId or provide a
      cloudAccountId of an existing account.
    :param string secretAccessKey: (optional in body) Aws Secret Access Key. Either provide secretAccessKey or provide a
      cloudAccountId of an existing account.
    :param string cloudAccountId: (optional in body) Existing cloud account id. Either provide existing cloud account id,
      or accessKeyId/secretAccessKey credentials pair.
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-aws/region-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "accessKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'accessKeyId' = {kwargs['accessKeyId']}")
            body["accessKeyId"] = kwargs.get("accessKeyId")
            del kwargs["accessKeyId"]
        if "secretAccessKey" in kwargs:
            hub.log.debug(f"Got kwarg 'secretAccessKey' = {kwargs['secretAccessKey']}")
            body["secretAccessKey"] = kwargs.get("secretAccessKey")
            del kwargs["secretAccessKey"]
        if "cloudAccountId" in kwargs:
            hub.log.debug(f"Got kwarg 'cloudAccountId' = {kwargs['cloudAccountId']}")
            body["cloudAccountId"] = kwargs.get("cloudAccountId")
            del kwargs["cloudAccountId"]

        ret = api.enumerate_aws_regions_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_aws_regions_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_azure_regions_async(hub, ctx, **kwargs):
    """Get the available regions for specified Azure cloud account Get the available regions for specified Azure cloud account Performs POST /iaas/api/cloud-accounts-azure/region-enumeration


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string subscriptionId: (optional in body) Azure Subscribtion ID. Either provide subscriptionId or provide a
      cloudAccountId of an existing account.
    :param string tenantId: (optional in body) Azure Tenant ID. Either provide tenantId or provide a cloudAccountId
      of an existing account.
    :param string clientApplicationId: (optional in body) Azure Client Application ID. Either provide clientApplicationId or
      provide a cloudAccountId of an existing account.
    :param string clientApplicationSecretKey: (optional in body) Azure Client Application Secret Key. Either provide
      clientApplicationSecretKey or provide a cloudAccountId of an existing
      account.
    :param string cloudAccountId: (optional in body) Existing cloud account id. Either provide id of existing account, or
      cloud account credentials: clientApplicationId,
      clientApplicationSecretKey and tenantId.
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-azure/region-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "subscriptionId" in kwargs:
            hub.log.debug(f"Got kwarg 'subscriptionId' = {kwargs['subscriptionId']}")
            body["subscriptionId"] = kwargs.get("subscriptionId")
            del kwargs["subscriptionId"]
        if "tenantId" in kwargs:
            hub.log.debug(f"Got kwarg 'tenantId' = {kwargs['tenantId']}")
            body["tenantId"] = kwargs.get("tenantId")
            del kwargs["tenantId"]
        if "clientApplicationId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'clientApplicationId' = {kwargs['clientApplicationId']}"
            )
            body["clientApplicationId"] = kwargs.get("clientApplicationId")
            del kwargs["clientApplicationId"]
        if "clientApplicationSecretKey" in kwargs:
            hub.log.debug(
                f"Got kwarg 'clientApplicationSecretKey' = {kwargs['clientApplicationSecretKey']}"
            )
            body["clientApplicationSecretKey"] = kwargs.get(
                "clientApplicationSecretKey"
            )
            del kwargs["clientApplicationSecretKey"]
        if "cloudAccountId" in kwargs:
            hub.log.debug(f"Got kwarg 'cloudAccountId' = {kwargs['cloudAccountId']}")
            body["cloudAccountId"] = kwargs.get("cloudAccountId")
            del kwargs["cloudAccountId"]

        ret = api.enumerate_azure_regions_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_azure_regions_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_gcp_regions_async(hub, ctx, **kwargs):
    """Get the available regions for specified GCP cloud account Get the available regions for specified GCP cloud account Performs POST /iaas/api/cloud-accounts-gcp/region-enumeration


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string projectId: (optional in body) GCP Project ID. Either provide projectId or provide a cloudAccountId
      of an existing account.
    :param string privateKeyId: (optional in body) GCP Private key ID. Either provide privateKeyId or provide a
      cloudAccountId of an existing account.
    :param string privateKey: (optional in body) GCP Private key. Either provide privateKey or provide a cloudAccountId
      of an existing account.
    :param string clientEmail: (optional in body) GCP Client email. Either provide clientEmail or provide a
      cloudAccountId of an existing account.
    :param string cloudAccountId: (optional in body) Existing cloud account id. Either provide id of existing account, or
      cloud account credentials: projectId, privateKeyId, privateKey and
      clientEmail.
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-gcp/region-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "privateKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
            body["privateKeyId"] = kwargs.get("privateKeyId")
            del kwargs["privateKeyId"]
        if "privateKey" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
            body["privateKey"] = kwargs.get("privateKey")
            del kwargs["privateKey"]
        if "clientEmail" in kwargs:
            hub.log.debug(f"Got kwarg 'clientEmail' = {kwargs['clientEmail']}")
            body["clientEmail"] = kwargs.get("clientEmail")
            del kwargs["clientEmail"]
        if "cloudAccountId" in kwargs:
            hub.log.debug(f"Got kwarg 'cloudAccountId' = {kwargs['cloudAccountId']}")
            body["cloudAccountId"] = kwargs.get("cloudAccountId")
            del kwargs["cloudAccountId"]

        ret = api.enumerate_gcp_regions_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_gcp_regions_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_private_images_aws(hub, ctx, p_id, **kwargs):
    """Enumerate all private images for enabled regions of the specified AWS account  Performs POST /iaas/api/cloud-accounts-aws/{id}/private-image-enumeration


    :param string p_id: (required in path) Id of AWS cloud account to enumerate
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug(
            "POST /iaas/api/cloud-accounts-aws/{id}/private-image-enumeration"
        )

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.enumerate_private_images_aws(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_private_images_aws: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_private_images_azure(hub, ctx, p_id, **kwargs):
    """Enumerate all private images for enabled regions of the specified Azure account  Performs POST /iaas/api/cloud-accounts-azure/{id}/private-image-enumeration


    :param string p_id: (required in path) Id of Azure cloud account to enumerate
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug(
            "POST /iaas/api/cloud-accounts-azure/{id}/private-image-enumeration"
        )

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.enumerate_private_images_azure(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_private_images_azure: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_private_images_gcp(hub, ctx, p_id, **kwargs):
    """Enumerate all private images for enabled regions of the specified GCP account  Performs POST /iaas/api/cloud-accounts-gcp/{id}/private-image-enumeration


    :param string p_id: (required in path) Id of GCP cloud account to enumerate
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug(
            "POST /iaas/api/cloud-accounts-gcp/{id}/private-image-enumeration"
        )

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.enumerate_private_images_gcp(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_private_images_gcp: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_private_images_v_sphere(hub, ctx, p_id, **kwargs):
    """Enumerate all private images for enabled regions of the specified vSphere
      account  Performs POST /iaas/api/cloud-accounts-vsphere/{id}/private-image-enumeration


    :param string p_id: (required in path) Id of vSphere cloud account to enumerate
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug(
            "POST /iaas/api/cloud-accounts-vsphere/{id}/private-image-enumeration"
        )

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.enumerate_private_images_v_sphere(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_private_images_v_sphere: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_private_images_vcf(hub, ctx, p_id, **kwargs):
    """Enumerate all private images for enabled regions of the specified VCF account  Performs POST /iaas/api/cloud-accounts-vcf/{id}/private-image-enumeration


    :param string p_id: (required in path) Id of VCF cloud account to enumerate
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug(
            "POST /iaas/api/cloud-accounts-vcf/{id}/private-image-enumeration"
        )

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.enumerate_private_images_vcf(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_private_images_vcf: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_private_images_vmc(hub, ctx, p_id, **kwargs):
    """Enumerate all private images for enabled regions of the specified VMC account  Performs POST /iaas/api/cloud-accounts-vmc/{id}/private-image-enumeration


    :param string p_id: (required in path) Id of VMC cloud account to enumerate
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug(
            "POST /iaas/api/cloud-accounts-vmc/{id}/private-image-enumeration"
        )

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.enumerate_private_images_vmc(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_private_images_vmc: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_private_images(hub, ctx, p_id, **kwargs):
    """Enumerate all private images for enabled regions of the specified cloud account  Performs POST /iaas/api/cloud-accounts/{id}/private-image-enumeration


    :param string p_id: (required in path) Id of cloud account to enumerate
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts/{id}/private-image-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.enumerate_private_images(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_private_images: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_regions_async(hub, ctx, cloudAccountProperties, **kwargs):
    """Get the available regions for specified cloud account Get the available regions for specified cloud account Performs POST /iaas/api/cloud-accounts/region-enumeration


    :param object cloudAccountProperties: (required in body) Cloud Account specific properties supplied in as name value pairs. In
      case of AAP, provide environment property here. Example:
      "cloudAccountProperties": {
      "environment": "aap"
      }
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string cloudAccountType: (optional in body) Cloud account type
    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the cloud
      account. Either provide privateKeyId or provide a cloudAccountId of an
      existing account.
    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      cloud account. Either provide privateKey or provide a cloudAccountId
      of an existing account.
    :param string cloudAccountId: (optional in body) Existing cloud account id. Either provide existing cloud account Id,
      or privateKeyId/privateKey credentials pair.
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts/region-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["cloudAccountProperties"] = cloudAccountProperties

        if "cloudAccountType" in kwargs:
            hub.log.debug(
                f"Got kwarg 'cloudAccountType' = {kwargs['cloudAccountType']}"
            )
            body["cloudAccountType"] = kwargs.get("cloudAccountType")
            del kwargs["cloudAccountType"]
        if "privateKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
            body["privateKeyId"] = kwargs.get("privateKeyId")
            del kwargs["privateKeyId"]
        if "privateKey" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
            body["privateKey"] = kwargs.get("privateKey")
            del kwargs["privateKey"]
        if "cloudAccountId" in kwargs:
            hub.log.debug(f"Got kwarg 'cloudAccountId' = {kwargs['cloudAccountId']}")
            body["cloudAccountId"] = kwargs.get("cloudAccountId")
            del kwargs["cloudAccountId"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.enumerate_regions_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_regions_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_v_sphere_regions_async(hub, ctx, **kwargs):
    """Get the available regions for specified vSphere cloud account Get the available regions for specified vSphere cloud account Performs POST /iaas/api/cloud-accounts-vsphere/region-enumeration


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string hostName: (optional in body) Host name for the vSphere endpoint. Either provide hostName or provide
      a cloudAccountId of an existing account.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere
    :param string dcid: (optional in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release.
    :param string username: (optional in body) Username to authenticate with the cloud account. Either provide
      username or provide a cloudAccountId of an existing account.
    :param string password: (optional in body) Password for the user used to authenticate with the cloud Account.
      Either provide password or provide a cloudAccountId of an existing
      account.
    :param string cloudAccountId: (optional in body) Existing cloud account id. Either provide existing cloud account Id,
      or hostName, username, password.
    :param Any certificateInfo: (optional in body)
    :param string environment: (optional in body) The environment where data collectors are deployed. When the data
      collectors are deployed on a cloud gateway appliance, use "aap".
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-vsphere/region-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "hostName" in kwargs:
            hub.log.debug(f"Got kwarg 'hostName' = {kwargs['hostName']}")
            body["hostName"] = kwargs.get("hostName")
            del kwargs["hostName"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "dcid" in kwargs:
            hub.log.debug(f"Got kwarg 'dcid' = {kwargs['dcid']}")
            body["dcid"] = kwargs.get("dcid")
            del kwargs["dcid"]
        if "username" in kwargs:
            hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
            body["username"] = kwargs.get("username")
            del kwargs["username"]
        if "password" in kwargs:
            hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
            body["password"] = kwargs.get("password")
            del kwargs["password"]
        if "cloudAccountId" in kwargs:
            hub.log.debug(f"Got kwarg 'cloudAccountId' = {kwargs['cloudAccountId']}")
            body["cloudAccountId"] = kwargs.get("cloudAccountId")
            del kwargs["cloudAccountId"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "environment" in kwargs:
            hub.log.debug(f"Got kwarg 'environment' = {kwargs['environment']}")
            body["environment"] = kwargs.get("environment")
            del kwargs["environment"]

        ret = api.enumerate_v_sphere_regions_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_v_sphere_regions_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_vcf_regions_async(hub, ctx, **kwargs):
    """Get the available regions for specified VCF cloud account Get the available regions for specified VCF cloud account Performs POST /iaas/api/cloud-accounts-vcf/region-enumeration


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string sddcManagerId: (optional in body) SDDC manager integration id. Either provide sddcManagerId or provide a
      cloudAccountId of an existing account.
    :param string workloadDomainId: (optional in body) Id of the workload domain to add as VCF cloud account. Either provide
      workloadDomainId or provide a cloudAccountId of an existing account.
    :param string workloadDomainName: (optional in body) Name of the workload domain to add as VCF cloud account. Either
      provide workloadDomainName or provide a cloudAccountId of an existing
      account.
    :param string vcenterHostName: (optional in body) Host name for the vSphere from the specified workload domain. Either
      provide vcenterHostName or provide a cloudAccountId of an existing
      account.
    :param string vcenterUsername: (optional in body) vCenter user name for the specified workload domain.The specified user
      requires CloudAdmin credentials. The user does not require
      CloudGlobalAdmin credentials.
    :param string vcenterPassword: (optional in body) Password for the user used to authenticate with the vCenter in VCF
      cloud account.  Either provide vcenterPassword or provide a
      cloudAccountId of an existing account.
    :param string nsxHostName: (optional in body) Host name for the NSX endpoint from the specified workload domain.
      Either provide nsxHostName or provide a cloudAccountId of an existing
      account.
    :param string nsxUsername: (optional in body) User name for the NSX manager in the specified workload domain. Either
      provide nsxUsername or provide a cloudAccountId of an existing
      account.
    :param string nsxPassword: (optional in body) Password for the user used to authenticate with the NSX-T manager in
      VCF cloud account. Either provide nsxPassword or provide a
      cloudAccountId of an existing account.
    :param string cloudAccountId: (optional in body) Existing cloud account id. Either provide existing cloud account Id,
      or workloadDomainId, workloadDomainName, vcenterHostName,
      vcenterUsername, vcenterPassword, nsxHostName, nsxUsername and
      nsxPassword.
    :param string vcenterCertificate: (optional in body) vCenter Certificate
    :param string nsxCertificate: (optional in body) NSX Certificate
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere and NSX-T
    :param string dcId: (optional in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release.
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-vcf/region-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "sddcManagerId" in kwargs:
            hub.log.debug(f"Got kwarg 'sddcManagerId' = {kwargs['sddcManagerId']}")
            body["sddcManagerId"] = kwargs.get("sddcManagerId")
            del kwargs["sddcManagerId"]
        if "workloadDomainId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'workloadDomainId' = {kwargs['workloadDomainId']}"
            )
            body["workloadDomainId"] = kwargs.get("workloadDomainId")
            del kwargs["workloadDomainId"]
        if "workloadDomainName" in kwargs:
            hub.log.debug(
                f"Got kwarg 'workloadDomainName' = {kwargs['workloadDomainName']}"
            )
            body["workloadDomainName"] = kwargs.get("workloadDomainName")
            del kwargs["workloadDomainName"]
        if "vcenterHostName" in kwargs:
            hub.log.debug(f"Got kwarg 'vcenterHostName' = {kwargs['vcenterHostName']}")
            body["vcenterHostName"] = kwargs.get("vcenterHostName")
            del kwargs["vcenterHostName"]
        if "vcenterUsername" in kwargs:
            hub.log.debug(f"Got kwarg 'vcenterUsername' = {kwargs['vcenterUsername']}")
            body["vcenterUsername"] = kwargs.get("vcenterUsername")
            del kwargs["vcenterUsername"]
        if "vcenterPassword" in kwargs:
            hub.log.debug(f"Got kwarg 'vcenterPassword' = {kwargs['vcenterPassword']}")
            body["vcenterPassword"] = kwargs.get("vcenterPassword")
            del kwargs["vcenterPassword"]
        if "nsxHostName" in kwargs:
            hub.log.debug(f"Got kwarg 'nsxHostName' = {kwargs['nsxHostName']}")
            body["nsxHostName"] = kwargs.get("nsxHostName")
            del kwargs["nsxHostName"]
        if "nsxUsername" in kwargs:
            hub.log.debug(f"Got kwarg 'nsxUsername' = {kwargs['nsxUsername']}")
            body["nsxUsername"] = kwargs.get("nsxUsername")
            del kwargs["nsxUsername"]
        if "nsxPassword" in kwargs:
            hub.log.debug(f"Got kwarg 'nsxPassword' = {kwargs['nsxPassword']}")
            body["nsxPassword"] = kwargs.get("nsxPassword")
            del kwargs["nsxPassword"]
        if "cloudAccountId" in kwargs:
            hub.log.debug(f"Got kwarg 'cloudAccountId' = {kwargs['cloudAccountId']}")
            body["cloudAccountId"] = kwargs.get("cloudAccountId")
            del kwargs["cloudAccountId"]
        if "vcenterCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'vcenterCertificate' = {kwargs['vcenterCertificate']}"
            )
            body["vcenterCertificate"] = kwargs.get("vcenterCertificate")
            del kwargs["vcenterCertificate"]
        if "nsxCertificate" in kwargs:
            hub.log.debug(f"Got kwarg 'nsxCertificate' = {kwargs['nsxCertificate']}")
            body["nsxCertificate"] = kwargs.get("nsxCertificate")
            del kwargs["nsxCertificate"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "dcId" in kwargs:
            hub.log.debug(f"Got kwarg 'dcId' = {kwargs['dcId']}")
            body["dcId"] = kwargs.get("dcId")
            del kwargs["dcId"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.enumerate_vcf_regions_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_vcf_regions_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def enumerate_vmc_regions_async(hub, ctx, **kwargs):
    """Get the available regions for specified VMC cloud account Get the available regions for specified VMC cloud account Performs POST /iaas/api/cloud-accounts-vmc/region-enumeration


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string apiKey: (optional in body) VMC API access key. Either provide apiKey or provide a cloudAccountId
      of an existing account.
    :param string sddcId: (optional in body) Identifier of the on-premise SDDC to be used by this cloud account.
      Note that NSX-V SDDCs are not supported. Either provide sddcId or
      provide a cloudAccountId of an existing account.
    :param string username: (optional in body) vCenter user name for the specified SDDC.The specified user requires
      CloudAdmin credentials. The user does not require CloudGlobalAdmin
      credentials.Either provide username or provide a cloudAccountId of an
      existing account.
    :param string password: (optional in body) Password for the user used to authenticate with the cloud Account.
      Either provide password or provide a cloudAccountId of an existing
      account.
    :param string hostName: (optional in body) Enter the IP address or FQDN of the vCenter Server in the specified
      SDDC. The cloud proxy belongs on this vCenter.  Either provide
      hostName or provide a cloudAccountId of an existing account.
    :param string nsxHostName: (optional in body) The IP address of the NSX Manager server in the specified SDDC /
      FQDN.Either provide nsxHostName or provide a cloudAccountId of an
      existing account.
    :param string dcId: (optional in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors
    :param string cloudAccountId: (optional in body) Existing cloud account id. Either provide existing cloud account Id,
      or apiKey, sddcId, username, password, hostName, nsxHostName.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere
    :param string cspHostName: (optional in body) The host name of the CSP service.
    :param Any certificateInfo: (optional in body)
    :param string environment: (optional in body) The environment where the agent has been deployed. When the agent has
      been deployed using the "Add Ons" in VMC UI or Api use "aap".
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts-vmc/region-enumeration")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "apiKey" in kwargs:
            hub.log.debug(f"Got kwarg 'apiKey' = {kwargs['apiKey']}")
            body["apiKey"] = kwargs.get("apiKey")
            del kwargs["apiKey"]
        if "sddcId" in kwargs:
            hub.log.debug(f"Got kwarg 'sddcId' = {kwargs['sddcId']}")
            body["sddcId"] = kwargs.get("sddcId")
            del kwargs["sddcId"]
        if "username" in kwargs:
            hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
            body["username"] = kwargs.get("username")
            del kwargs["username"]
        if "password" in kwargs:
            hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
            body["password"] = kwargs.get("password")
            del kwargs["password"]
        if "hostName" in kwargs:
            hub.log.debug(f"Got kwarg 'hostName' = {kwargs['hostName']}")
            body["hostName"] = kwargs.get("hostName")
            del kwargs["hostName"]
        if "nsxHostName" in kwargs:
            hub.log.debug(f"Got kwarg 'nsxHostName' = {kwargs['nsxHostName']}")
            body["nsxHostName"] = kwargs.get("nsxHostName")
            del kwargs["nsxHostName"]
        if "dcId" in kwargs:
            hub.log.debug(f"Got kwarg 'dcId' = {kwargs['dcId']}")
            body["dcId"] = kwargs.get("dcId")
            del kwargs["dcId"]
        if "cloudAccountId" in kwargs:
            hub.log.debug(f"Got kwarg 'cloudAccountId' = {kwargs['cloudAccountId']}")
            body["cloudAccountId"] = kwargs.get("cloudAccountId")
            del kwargs["cloudAccountId"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "cspHostName" in kwargs:
            hub.log.debug(f"Got kwarg 'cspHostName' = {kwargs['cspHostName']}")
            body["cspHostName"] = kwargs.get("cspHostName")
            del kwargs["cspHostName"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "environment" in kwargs:
            hub.log.debug(f"Got kwarg 'environment' = {kwargs['environment']}")
            body["environment"] = kwargs.get("environment")
            del kwargs["environment"]

        ret = api.enumerate_vmc_regions_async(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.enumerate_vmc_regions_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_aws_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an AWS cloud account Get an AWS cloud account with a given id Performs GET /iaas/api/cloud-accounts-aws/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-aws/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_aws_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_aws_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_aws_cloud_accounts(hub, ctx, **kwargs):
    """Get AWS cloud accounts Get all AWS cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-aws


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-aws")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_aws_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_aws_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_azure_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an Azure Cloud Account Get an Azure Cloud Account with a given id Performs GET /iaas/api/cloud-accounts-azure/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-azure/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_azure_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_azure_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_azure_cloud_accounts(hub, ctx, **kwargs):
    """Get Azure cloud accounts Get all Azure cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-azure


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-azure")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_azure_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_azure_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_cloud_account(hub, ctx, p_id, **kwargs):
    """Get cloud account Get cloud account with a given id Performs GET /iaas/api/cloud-accounts/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_cloud_accounts(hub, ctx, **kwargs):
    """Get cloud accounts Get all cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gcp_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an GCP cloud account Get an GCP cloud account with a given id Performs GET /iaas/api/cloud-accounts-gcp/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-gcp/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_gcp_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_gcp_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_gcp_cloud_accounts(hub, ctx, **kwargs):
    """Get GCP cloud accounts Get all GCP cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-gcp


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-gcp")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_gcp_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_gcp_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_nsx_alb_cloud_account(hub, ctx, p_id, **kwargs):
    """Get a NSX Advanced Load Balancer cloud account Get a NSX Advanced Load Balancer with a given id Performs GET /iaas/api/cloud-accounts-nsx-alb/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-nsx-alb/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_nsx_alb_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_nsx_alb_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_nsx_alb_cloud_accounts(hub, ctx, **kwargs):
    """Get NSX Advanced Load Balancer cloud accounts Get all NSX Advanced Load Balancer cloud accounts within the current
      organization Performs GET /iaas/api/cloud-accounts-nsx-alb


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-nsx-alb")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_nsx_alb_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_nsx_alb_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_nsx_t_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an NSX-T cloud account Get an NSX-T cloud account with a given id Performs GET /iaas/api/cloud-accounts-nsx-t/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-nsx-t/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_nsx_t_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_nsx_t_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_nsx_t_cloud_accounts(hub, ctx, **kwargs):
    """Get NSX-T cloud accounts Get all NSX-T cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-nsx-t


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-nsx-t")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_nsx_t_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_nsx_t_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_nsx_v_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an NSX-V cloud account Get an NSX-V cloud account with a given id Performs GET /iaas/api/cloud-accounts-nsx-v/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-nsx-v/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_nsx_v_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_nsx_v_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_nsx_v_cloud_accounts(hub, ctx, **kwargs):
    """Get NSX-V cloud accounts Get all NSX-V cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-nsx-v


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-nsx-v")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_nsx_v_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_nsx_v_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_region_enumeration_result(hub, ctx, p_id, **kwargs):
    """Get region enumeration response Get region enumeration response for a given id Performs GET /iaas/api/cloud-accounts/region-enumeration/{id}


    :param string p_id: (required in path) The ID of enumeration response
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts/region-enumeration/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_region_enumeration_result(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_region_enumeration_result: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_v_sphere_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an vSphere cloud account Get an vSphere cloud account with a given id Performs GET /iaas/api/cloud-accounts-vsphere/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-vsphere/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_v_sphere_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_v_sphere_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_v_sphere_cloud_accounts(hub, ctx, **kwargs):
    """Get vSphere cloud accounts Get all vSphere cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-vsphere


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-vsphere")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_v_sphere_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_v_sphere_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_vcf_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an VCF cloud account Get an VCF cloud account with a given id Performs GET /iaas/api/cloud-accounts-vcf/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-vcf/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_vcf_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_vcf_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_vcf_cloud_accounts(hub, ctx, **kwargs):
    """Get VCF cloud accounts Get all VCF cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-vcf


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-vcf")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_vcf_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_vcf_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_vmc_cloud_account(hub, ctx, p_id, **kwargs):
    """Get an VMC cloud account Get an VMC cloud account with a given id Performs GET /iaas/api/cloud-accounts-vmc/{id}


    :param string p_id: (required in path) The ID of the Cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-vmc/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_vmc_cloud_account(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_vmc_cloud_account: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_vmc_cloud_accounts(hub, ctx, **kwargs):
    """Get VMC cloud accounts Get all VMC cloud accounts within the current organization Performs GET /iaas/api/cloud-accounts-vmc


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /iaas/api/cloud-accounts-vmc")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_vmc_cloud_accounts(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.get_vmc_cloud_accounts: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def run_endpoint_health_check(hub, ctx, p_id, **kwargs):
    """Starts cloud account health check Starts cloud account health check identified by its endpoint state Performs POST /iaas/api/cloud-accounts/{id}/health-check


    :param string p_id: (required in path) The ID of the Cloud account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string periodicHealthCheckId: (optional in query) If query param is provided then the endpoint health check is not
      started manually from the UI, but after a scheduled process.
    """

    try:

        hub.log.debug("POST /iaas/api/cloud-accounts/{id}/health-check")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.run_endpoint_health_check(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.run_endpoint_health_check: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_aws_cloud_account_async(hub, ctx, p_id, name, **kwargs):
    """Update AWS cloud account Update AWS cloud account Performs PATCH /iaas/api/cloud-accounts-aws/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param string accessKeyId: (optional in body) Aws Access key ID
    :param string secretAccessKey: (optional in body) Aws Secret Access Key
    :param string iamRoleArn: (optional in body) Aws ARN role to be assumed by Aria Auto account
    :param boolean trustedAccount: (optional in body) Create the account as trusted.
    :param array regions: (optional in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-aws/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "accessKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'accessKeyId' = {kwargs['accessKeyId']}")
            body["accessKeyId"] = kwargs.get("accessKeyId")
            del kwargs["accessKeyId"]
        if "secretAccessKey" in kwargs:
            hub.log.debug(f"Got kwarg 'secretAccessKey' = {kwargs['secretAccessKey']}")
            body["secretAccessKey"] = kwargs.get("secretAccessKey")
            del kwargs["secretAccessKey"]
        if "iamRoleArn" in kwargs:
            hub.log.debug(f"Got kwarg 'iamRoleArn' = {kwargs['iamRoleArn']}")
            body["iamRoleArn"] = kwargs.get("iamRoleArn")
            del kwargs["iamRoleArn"]
        if "trustedAccount" in kwargs:
            hub.log.debug(f"Got kwarg 'trustedAccount' = {kwargs['trustedAccount']}")
            body["trustedAccount"] = kwargs.get("trustedAccount")
            del kwargs["trustedAccount"]
        if "regions" in kwargs:
            hub.log.debug(f"Got kwarg 'regions' = {kwargs['regions']}")
            body["regions"] = kwargs.get("regions")
            del kwargs["regions"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.update_aws_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_aws_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_azure_cloud_account_async(
    hub,
    ctx,
    p_id,
    name,
    subscriptionId,
    tenantId,
    clientApplicationId,
    clientApplicationSecretKey,
    regions,
    **kwargs,
):
    """Update Azure cloud account Update Azure cloud account Performs PATCH /iaas/api/cloud-accounts-azure/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string subscriptionId: (required in body) Azure Subscribtion ID
    :param string tenantId: (required in body) Azure Tenant ID
    :param string clientApplicationId: (required in body) Azure Client Application ID
    :param string clientApplicationSecretKey: (required in body) Azure Client Application Secret Key
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-azure/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["subscriptionId"] = subscriptionId
        body["tenantId"] = tenantId
        body["clientApplicationId"] = clientApplicationId
        body["clientApplicationSecretKey"] = clientApplicationSecretKey
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.update_azure_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_azure_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_cloud_account_async(
    hub, ctx, p_id, name, cloudAccountProperties, regions, **kwargs
):
    """Update CloudAccount Update a single CloudAccount Performs PATCH /iaas/api/cloud-accounts/{id}


    :param string p_id: (required in path) The ID of the cloudAccount
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param object cloudAccountProperties: (required in body) Cloud Account specific properties supplied in as name value pairs
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
      regionInfos is a required parameter for AWS, AZURE, GCP, VSPHERE,
      VMC, VCF cloud account types.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the cloud
      account
    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      cloud account. In case of AAP pass a dummy value.
    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this cloud account
    :param object associatedMobilityCloudAccountIds: (optional in body) Cloud Account IDs and directionalities create associations to other
      vSphere cloud accounts that can be used for workload mobility. ID
      refers to an associated cloud account, and directionality can be
      unidirectional or bidirectional. Only supported on vSphere cloud
      accounts.
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the Cloud
      Account. In case of AAP, provide environment property here.Example:
      "customProperties": {
      "environment": "aap"
      }
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["cloudAccountProperties"] = cloudAccountProperties
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "privateKeyId" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKeyId' = {kwargs['privateKeyId']}")
            body["privateKeyId"] = kwargs.get("privateKeyId")
            del kwargs["privateKeyId"]
        if "privateKey" in kwargs:
            hub.log.debug(f"Got kwarg 'privateKey' = {kwargs['privateKey']}")
            body["privateKey"] = kwargs.get("privateKey")
            del kwargs["privateKey"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "associatedMobilityCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedMobilityCloudAccountIds' = {kwargs['associatedMobilityCloudAccountIds']}"
            )
            body["associatedMobilityCloudAccountIds"] = kwargs.get(
                "associatedMobilityCloudAccountIds"
            )
            del kwargs["associatedMobilityCloudAccountIds"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.update_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_gcp_cloud_account_async(
    hub,
    ctx,
    p_id,
    name,
    projectId,
    privateKeyId,
    privateKey,
    clientEmail,
    regions,
    **kwargs,
):
    """Update GCP cloud account Update GCP cloud account Performs PATCH /iaas/api/cloud-accounts-gcp/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) GCP Project ID
    :param string privateKeyId: (required in body) GCP Private key ID
    :param string privateKey: (required in body) GCP Private key
    :param string clientEmail: (required in body) GCP Client email
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-gcp/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId
        body["privateKeyId"] = privateKeyId
        body["privateKey"] = privateKey
        body["clientEmail"] = clientEmail
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.update_gcp_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_gcp_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_nsx_alb_cloud_account_async(
    hub, ctx, p_id, name, hostName, username, password, regions, **kwargs
):
    """Update NSX Advanced Load Balancer cloud account asynchronously Update NSX Advanced Load Balancer cloud account asynchronously Performs PATCH /iaas/api/cloud-accounts-nsx-alb/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the NSX Advanced Load Balancer endpoint
    :param string username: (required in body) Username to authenticate with the cloud account
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    :param object cloudAccountProperties: (optional in body) Cloud Account specific properties supplied in as name value pairs
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-nsx-alb/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["username"] = username
        body["password"] = password
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "cloudAccountProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'cloudAccountProperties' = {kwargs['cloudAccountProperties']}"
            )
            body["cloudAccountProperties"] = kwargs.get("cloudAccountProperties")
            del kwargs["cloudAccountProperties"]

        ret = api.update_nsx_alb_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_nsx_alb_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_nsx_t_cloud_account_async(
    hub, ctx, p_id, name, hostName, dcid, username, password, **kwargs
):
    """Update NSX-T cloud account Update NSX-T cloud account Performs PATCH /iaas/api/cloud-accounts-nsx-t/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the NSX-T endpoint
    :param string dcid: (required in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release and hence the data collector Id is
      optional for VMware Aria Automation (on-prem).
    :param string username: (required in body) Username to authenticate with the cloud account
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param array associatedCloudAccountIds: (optional in body) vSphere cloud accounts associated with this NSX-T cloud account.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-nsx-t/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["dcid"] = dcid
        body["username"] = username
        body["password"] = password

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.update_nsx_t_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_nsx_t_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_nsx_v_cloud_account_async(
    hub, ctx, p_id, name, hostName, dcid, username, password, **kwargs
):
    """Update NSX-V cloud account Update NSX-V cloud account Performs PATCH /iaas/api/cloud-accounts-nsx-v/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the NSX-v endpoint
    :param string dcid: (required in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release and hence the data collector Id is
      optional for VMware Aria Automation (on-prem).
    :param string username: (required in body) Username to authenticate with the cloud account
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param array associatedCloudAccountIds: (optional in body) vSphere cloud account associated with this NSX-V cloud account. NSX-V
      cloud account can be associated with a single vSphere cloud account.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-nsx-v/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["dcid"] = dcid
        body["username"] = username
        body["password"] = password

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.update_nsx_v_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_nsx_v_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_v_sphere_cloud_account_async(
    hub, ctx, p_id, name, hostName, regions, **kwargs
):
    """Update vSphere cloud account Update vSphere cloud account Performs PATCH /iaas/api/cloud-accounts-vsphere/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string hostName: (required in body) Host name for the vSphere endpoint
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere
    :param array associatedCloudAccountIds: (optional in body) NSX-V or NSX-T account to associate with this vSphere cloud account.
      vSphere cloud account can be a single NSX-V cloud account or a single
      NSX-T cloud account.
    :param object associatedMobilityCloudAccountIds: (optional in body) Cloud account IDs and directionalities create associations to other
      vSphere cloud accounts that can be used for workload mobility. ID
      refers to an associated cloud account, and directionality can be
      unidirectional or bidirectional.
    :param string dcid: (optional in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release.
    :param string username: (optional in body) Username to authenticate with the cloud account. Not required when
      environment is set to aap.
    :param string password: (optional in body) Password for the user used to authenticate with the cloud Account. Not
      required when environment is set to aap.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account
    :param Any certificateInfo: (optional in body)
    :param string environment: (optional in body) The environment where data collectors are deployed. When the data
      collectors are deployed on an aap-based cloud gateway appliance, use
      "aap".
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-vsphere/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["hostName"] = hostName
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "associatedCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedCloudAccountIds' = {kwargs['associatedCloudAccountIds']}"
            )
            body["associatedCloudAccountIds"] = kwargs.get("associatedCloudAccountIds")
            del kwargs["associatedCloudAccountIds"]
        if "associatedMobilityCloudAccountIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'associatedMobilityCloudAccountIds' = {kwargs['associatedMobilityCloudAccountIds']}"
            )
            body["associatedMobilityCloudAccountIds"] = kwargs.get(
                "associatedMobilityCloudAccountIds"
            )
            del kwargs["associatedMobilityCloudAccountIds"]
        if "dcid" in kwargs:
            hub.log.debug(f"Got kwarg 'dcid' = {kwargs['dcid']}")
            body["dcid"] = kwargs.get("dcid")
            del kwargs["dcid"]
        if "username" in kwargs:
            hub.log.debug(f"Got kwarg 'username' = {kwargs['username']}")
            body["username"] = kwargs.get("username")
            del kwargs["username"]
        if "password" in kwargs:
            hub.log.debug(f"Got kwarg 'password' = {kwargs['password']}")
            body["password"] = kwargs.get("password")
            del kwargs["password"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "environment" in kwargs:
            hub.log.debug(f"Got kwarg 'environment' = {kwargs['environment']}")
            body["environment"] = kwargs.get("environment")
            del kwargs["environment"]

        ret = api.update_v_sphere_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_v_sphere_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_vcf_cloud_account_async(
    hub,
    ctx,
    p_id,
    name,
    workloadDomainId,
    workloadDomainName,
    vcenterHostName,
    vcenterUsername,
    vcenterPassword,
    nsxHostName,
    nsxUsername,
    nsxPassword,
    regions,
    **kwargs,
):
    """Update VCF cloud account Update VCF cloud account Performs PATCH /iaas/api/cloud-accounts-vcf/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string workloadDomainId: (required in body) Id of the workload domain to add as VCF cloud account.
    :param string workloadDomainName: (required in body) Name of the workload domain to add as VCF cloud account.
    :param string vcenterHostName: (required in body) Host name for the vSphere from the specified workload domain.
    :param string vcenterUsername: (required in body) vCenter user name for the specified workload domain.The specified user
      requires CloudAdmin credentials. The user does not require
      CloudGlobalAdmin credentials.
    :param string vcenterPassword: (required in body) Password for the user used to authenticate with the vCenter in VCF
      cloud account
    :param string nsxHostName: (required in body) Host name for the NSX endpoint from the specified workload domain.
    :param string nsxUsername: (required in body) User name for the NSX manager in the specified workload domain.
    :param string nsxPassword: (required in body) Password for the user used to authenticate with the NSX-T manager in
      VCF cloud account
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param string sddcManagerId: (optional in body) SDDC manager integration id
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud
      Account.Cloud account capability tags may enable different features.
    :param string vcenterCertificate: (optional in body) vCenter Certificate
    :param string nsxCertificate: (optional in body) NSX Certificate
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere and NSX-T
    :param string dcId: (optional in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors.
      Note: Data collector endpoints are not available in VMware Aria
      Automation (on-prem) release.
    :param Any certificateInfo: (optional in body)
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-vcf/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["workloadDomainId"] = workloadDomainId
        body["workloadDomainName"] = workloadDomainName
        body["vcenterHostName"] = vcenterHostName
        body["vcenterUsername"] = vcenterUsername
        body["vcenterPassword"] = vcenterPassword
        body["nsxHostName"] = nsxHostName
        body["nsxUsername"] = nsxUsername
        body["nsxPassword"] = nsxPassword
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "sddcManagerId" in kwargs:
            hub.log.debug(f"Got kwarg 'sddcManagerId' = {kwargs['sddcManagerId']}")
            body["sddcManagerId"] = kwargs.get("sddcManagerId")
            del kwargs["sddcManagerId"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "vcenterCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'vcenterCertificate' = {kwargs['vcenterCertificate']}"
            )
            body["vcenterCertificate"] = kwargs.get("vcenterCertificate")
            del kwargs["vcenterCertificate"]
        if "nsxCertificate" in kwargs:
            hub.log.debug(f"Got kwarg 'nsxCertificate' = {kwargs['nsxCertificate']}")
            body["nsxCertificate"] = kwargs.get("nsxCertificate")
            del kwargs["nsxCertificate"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "dcId" in kwargs:
            hub.log.debug(f"Got kwarg 'dcId' = {kwargs['dcId']}")
            body["dcId"] = kwargs.get("dcId")
            del kwargs["dcId"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]

        ret = api.update_vcf_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_vcf_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_vmc_cloud_account_async(
    hub,
    ctx,
    p_id,
    name,
    apiKey,
    sddcId,
    username,
    password,
    hostName,
    nsxHostName,
    dcId,
    regions,
    **kwargs,
):
    """Update VMC cloud account Update VMC cloud account Performs PATCH /iaas/api/cloud-accounts-vmc/{id}


    :param string p_id: (required in path) Cloud account id
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiKey: (required in body) VMC API access key. Optional when updating.
    :param string sddcId: (required in body) Identifier of the on-premise SDDC to be used by this cloud account.
      Note that NSX-V SDDCs are not supported.
    :param string username: (required in body) vCenter user name for the specified SDDC.The specified user requires
      CloudAdmin credentials. The user does not require CloudGlobalAdmin
      credentials. Not mandatory for AAP mode.
    :param string password: (required in body) Password for the user used to authenticate with the cloud Account. Not
      mandatory for AAP mode.
    :param string hostName: (required in body) Enter the IP address or FQDN of the vCenter Server in the specified
      SDDC. The cloud proxy belongs on this vCenter.
    :param string nsxHostName: (required in body) The IP address of the NSX Manager server in the specified SDDC / FQDN.
    :param string dcId: (required in body) Identifier of a data collector vm deployed in the on premise
      infrastructure. Refer to the data-collector API to create or list data
      collectors. Not mandatory for AAP mode.
    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud
      Account.Cloud account capability tags may enable different features.
    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.
    :param boolean acceptSelfSignedCertificate: (optional in body) Accept self signed certificate when connecting to vSphere
    :param Any certificateInfo: (optional in body)
    :param string environment: (optional in body) The environment where the agent has been deployed. When the agent has
      been deployed using the "Add Ons" in VMC UI or Api use "aap".
    """

    try:

        hub.log.debug("PATCH /iaas/api/cloud-accounts-vmc/{id}")

        api = CloudAccountApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["apiKey"] = apiKey
        body["sddcId"] = sddcId
        body["username"] = username
        body["password"] = password
        body["hostName"] = hostName
        body["nsxHostName"] = nsxHostName
        body["dcId"] = dcId
        body["regions"] = regions

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "createDefaultZones" in kwargs:
            hub.log.debug(
                f"Got kwarg 'createDefaultZones' = {kwargs['createDefaultZones']}"
            )
            body["createDefaultZones"] = kwargs.get("createDefaultZones")
            del kwargs["createDefaultZones"]
        if "acceptSelfSignedCertificate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'acceptSelfSignedCertificate' = {kwargs['acceptSelfSignedCertificate']}"
            )
            body["acceptSelfSignedCertificate"] = kwargs.get(
                "acceptSelfSignedCertificate"
            )
            del kwargs["acceptSelfSignedCertificate"]
        if "certificateInfo" in kwargs:
            hub.log.debug(f"Got kwarg 'certificateInfo' = {kwargs['certificateInfo']}")
            body["certificateInfo"] = kwargs.get("certificateInfo")
            del kwargs["certificateInfo"]
        if "environment" in kwargs:
            hub.log.debug(f"Got kwarg 'environment' = {kwargs['environment']}")
            body["environment"] = kwargs.get("environment")
            del kwargs["environment"]

        ret = api.update_vmc_cloud_account_async(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CloudAccountApi.update_vmc_cloud_account_async: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
