from idem_vra.client.vra_iaas_lib.api import ComputeGatewayApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_compute_gateway(
    hub, ctx, name, projectId, networks, natRules, **kwargs
):
    """Create a compute gateway Create a new compute gateway. Performs POST /iaas/api/compute-gateways


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param array networks: (required in body) List of networks
    :param array natRules: (required in body) List of NAT Rules
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    """

    try:

        hub.log.debug("POST /iaas/api/compute-gateways")

        api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId
        body["networks"] = networks
        body["natRules"] = natRules

        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]

        ret = api.create_compute_gateway(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeGatewayApi.create_compute_gateway: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_compute_gateway(hub, ctx, p_id, **kwargs):
    """Delete a compute gateway Delete compute gateway with a given id Performs DELETE /iaas/api/compute-gateways/{id}


    :param string p_id: (required in path) The ID of the compute gateway.
    :param string apiVersion: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this compute gateway. Use with caution as
      force deleting may cause inconsistencies between the cloud provider
      and VMware Aria Automation.
    :param boolean forceDelete: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/compute-gateways/{id}")

        api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_compute_gateway(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeGatewayApi.delete_compute_gateway: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_compute_gateway(hub, ctx, p_id, **kwargs):
    """Get a compute gateway Get compute gateway with a given id Performs GET /iaas/api/compute-gateways/{id}


    :param string p_id: (required in path) The ID of the gateway.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/compute-gateways/{id}")

        api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_compute_gateway(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeGatewayApi.get_compute_gateway: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_compute_gateways(hub, ctx, **kwargs):
    """Get compute gateways Get all compute gateways Performs GET /iaas/api/compute-gateways


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/compute-gateways")

        api = ComputeGatewayApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_compute_gateways(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeGatewayApi.get_compute_gateways: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
