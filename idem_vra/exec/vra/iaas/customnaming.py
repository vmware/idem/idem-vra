from idem_vra.client.vra_iaas_lib.api import CustomNamingApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_custom_name(hub, ctx, **kwargs):
    """Create Custom Name Create Custom Name Performs POST /iaas/api/naming


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string description: (optional in body)
    :param array projects: (optional in body)
    :param array templates: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/naming")

        api = CustomNamingApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "projects" in kwargs:
            hub.log.debug(f"Got kwarg 'projects' = {kwargs['projects']}")
            body["projects"] = kwargs.get("projects")
            del kwargs["projects"]
        if "templates" in kwargs:
            hub.log.debug(f"Got kwarg 'templates' = {kwargs['templates']}")
            body["templates"] = kwargs.get("templates")
            del kwargs["templates"]

        ret = api.create_custom_name(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomNamingApi.create_custom_name: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_customname(hub, ctx, p_id, **kwargs):
    """Delete custom name Delete custom name with a given id Performs DELETE /iaas/api/naming/{id}


    :param string p_id: (required in path) The ID of the custom name.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/naming/{id}")

        api = CustomNamingApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_customname(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomNamingApi.delete_customname: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_custom_names(hub, ctx, **kwargs):
    """Get All Custom Names Get All Custom Names Performs GET /iaas/api/naming


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/naming")

        api = CustomNamingApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_all_custom_names(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomNamingApi.get_all_custom_names: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_custom_name_by_project_id(hub, ctx, p_id, **kwargs):
    """Get Custom Names For Project Id Get Custom Names For Project Id Performs GET /iaas/api/naming/projectId/{id}


    :param string p_id: (required in path) Project id.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/naming/projectId/{id}")

        api = CustomNamingApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_custom_name_by_project_id(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomNamingApi.get_custom_name_by_project_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_custom_name(hub, ctx, p_id, **kwargs):
    """Get Custom Name by Id Get Custom Name by Id Performs GET /iaas/api/naming/{id}


    :param string p_id: (required in path) The ID of the custom name.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/naming/{id}")

        api = CustomNamingApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_custom_name(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomNamingApi.get_custom_name: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_custom_name(hub, ctx, **kwargs):
    """Update custom name Update custom name Performs PUT /iaas/api/naming


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string description: (optional in body)
    :param array projects: (optional in body)
    :param array templates: (optional in body)
    """

    try:

        hub.log.debug("PUT /iaas/api/naming")

        api = CustomNamingApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "projects" in kwargs:
            hub.log.debug(f"Got kwarg 'projects' = {kwargs['projects']}")
            body["projects"] = kwargs.get("projects")
            del kwargs["projects"]
        if "templates" in kwargs:
            hub.log.debug(f"Got kwarg 'templates' = {kwargs['templates']}")
            body["templates"] = kwargs.get("templates")
            del kwargs["templates"]

        ret = api.update_custom_name(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomNamingApi.update_custom_name: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
