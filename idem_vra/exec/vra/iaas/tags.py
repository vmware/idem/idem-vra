from idem_vra.client.vra_iaas_lib.api import TagsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_tag(hub, ctx, key, **kwargs):
    """Create Tag Create a new tag Performs POST /iaas/api/tags


    :param string key: (required in body) Tags key.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string value: (optional in body) Tags value.
    """

    try:

        hub.log.debug("POST /iaas/api/tags")

        api = TagsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["key"] = key

        if "value" in kwargs:
            hub.log.debug(f"Got kwarg 'value' = {kwargs['value']}")
            body["value"] = kwargs.get("value")
            del kwargs["value"]

        ret = api.create_tag(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking TagsApi.create_tag: {err}")
        return ExecReturn(result=False, comment=str(err))


async def delete_tag(hub, ctx, p_id, **kwargs):
    """Delete Tag Delete a tag by ID Performs DELETE /iaas/api/tags/{id}


    :param string p_id: (required in path) The ID of the tag.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean ignoreUsage: (optional in query) Controls whether this is a delete operation while ignoring tag usage.
      If true, best effort is made for deleting this tag. All the tag
      assignments are removed. Only after successfully un-assigning the tag
      from resources, the tag is deleted from VMware Aria Automation. Note,
      that a discovered tag, if deleted, gets re-enumerated in the system
      after next data collection cycle and also gets self-assigned to the
      discovered resources.
    """

    try:

        hub.log.debug("DELETE /iaas/api/tags/{id}")

        api = TagsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_tag(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking TagsApi.delete_tag: {err}")
        return ExecReturn(result=False, comment=str(err))


async def get_tags_usage(hub, ctx, **kwargs):
    """Get usage of the tags Get documents of all resources that are assigned with the provided tags Performs POST /iaas/api/tags/tags-usage


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array tagIds: (optional in body) List of Tag IDs. All provided tags will be matched to all resources
      containing that tag.
    """

    try:

        hub.log.debug("POST /iaas/api/tags/tags-usage")

        api = TagsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "tagIds" in kwargs:
            hub.log.debug(f"Got kwarg 'tagIds' = {kwargs['tagIds']}")
            body["tagIds"] = kwargs.get("tagIds")
            del kwargs["tagIds"]

        ret = api.get_tags_usage(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TagsApi.get_tags_usage: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_tags(hub, ctx, **kwargs):
    """Get tags Get all tags Performs GET /iaas/api/tags


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/tags")

        api = TagsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_tags(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking TagsApi.get_tags: {err}")
        return ExecReturn(result=False, comment=str(err))
