from idem_vra.client.vra_iaas_lib.api import LogsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_event_logs(hub, ctx, **kwargs):
    """Get Event Logs Get all Event logs Performs GET /iaas/api/event-logs


    :param string startDate: (optional in query) Start Date e.g. 2020-12-01T08:00:00.000Z
    :param string endDate: (optional in query) End Date e.g. 2020-12-01T08:00:00.000Z
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param boolean select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/event-logs")

        api = LogsApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_event_logs(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking LogsApi.get_event_logs: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
