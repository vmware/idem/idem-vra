from idem_vra.client.vra_iaas_lib.api import NetworkIPRangeApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def allocate_network_ips_by_user(hub, ctx, p_id, **kwargs):
    """allocate network IPs by user. allocate network IPs by user Performs POST /iaas/api/network-ip-ranges/{id}/ip-addresses/allocate


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) Description
    :param array ipAddresses: (optional in body) A set of ip addresses IPv4 or IPv6.
    :param integer numberOfIps: (optional in body) Number of ip addresses to allocate from the network ip range.
    """

    try:

        hub.log.debug("POST /iaas/api/network-ip-ranges/{id}/ip-addresses/allocate")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "ipAddresses" in kwargs:
            hub.log.debug(f"Got kwarg 'ipAddresses' = {kwargs['ipAddresses']}")
            body["ipAddresses"] = kwargs.get("ipAddresses")
            del kwargs["ipAddresses"]
        if "numberOfIps" in kwargs:
            hub.log.debug(f"Got kwarg 'numberOfIps' = {kwargs['numberOfIps']}")
            body["numberOfIps"] = kwargs.get("numberOfIps")
            del kwargs["numberOfIps"]

        ret = api.allocate_network_ips_by_user(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.allocate_network_ips_by_user: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_internal_network_ip_range(
    hub, ctx, name, startIPAddress, endIPAddress, **kwargs
):
    """Create internal network IP range Creates an internal network IP range. Performs POST /iaas/api/network-ip-ranges


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string startIPAddress: (required in body) Start IP address of the range.
    :param string endIPAddress: (required in body) End IP address of the range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param array fabricNetworkIds: (optional in body) The Ids of the fabric networks.
    :param string ipVersion: (optional in body) IP address version: IPv4 or IPv6. Default: IPv4.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    """

    try:

        hub.log.debug("POST /iaas/api/network-ip-ranges")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["startIPAddress"] = startIPAddress
        body["endIPAddress"] = endIPAddress

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "fabricNetworkIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}"
            )
            body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
            del kwargs["fabricNetworkIds"]
        if "ipVersion" in kwargs:
            hub.log.debug(f"Got kwarg 'ipVersion' = {kwargs['ipVersion']}")
            body["ipVersion"] = kwargs.get("ipVersion")
            del kwargs["ipVersion"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.create_internal_network_ip_range(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.create_internal_network_ip_range: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_internal_network_ip_range(hub, ctx, p_id, **kwargs):
    """Delete internal network IP range Delete internal network IP range with a given id Performs DELETE /iaas/api/network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/network-ip-ranges/{id}")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_internal_network_ip_range(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.delete_internal_network_ip_range: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_allocated_and_released_ip_addresses(hub, ctx, p_id, **kwargs):
    """Get all allocated and released addresses of an IPAM network IP range Get all allocated and released addresses of an IPAM network IP range Performs GET /iaas/api/network-ip-ranges/{id}/ip-addresses


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/network-ip-ranges/{id}/ip-addresses")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_allocated_and_released_ip_addresses(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_allocated_and_released_ip_addresses: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_allocated_or_released_ip_address(
    hub, ctx, p_networkIPRangeId, p_ipAddressId, **kwargs
):
    """Get an allocated or released address of an IPAM network IP range Get an allocated or released address of an IPAM network IP range Performs GET /iaas/api/network-ip-ranges/{networkIPRangeId}/ip-addresses/{ipAddressId}


    :param string p_networkIPRangeId: (required in path) The ID of a network IP range.
    :param string p_ipAddressId: (required in path) The ID attribute of the IP Address
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug(
            "GET /iaas/api/network-ip-ranges/{networkIPRangeId}/ip-addresses/{ipAddressId}"
        )

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_allocated_or_released_ip_address(
            network_i_p_range_id=p_networkIPRangeId,
            ip_address_id=p_ipAddressId,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_allocated_or_released_ip_address: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_external_ip_block(hub, ctx, p_id, **kwargs):
    """Get specific external IP block by id An external IP block is network coming from external IPAM provider that can be
      used to create subnetworks inside it Performs GET /iaas/api/external-ip-blocks/{id}


    :param string p_id: (required in path) The ID of the external IP block
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/external-ip-blocks/{id}")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_external_ip_block(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_external_ip_block: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_external_ip_blocks(hub, ctx, **kwargs):
    """Get all external IP blocks An external IP block is network coming from external IPAM provider that can be
      used to create subnetworks inside it Performs GET /iaas/api/external-ip-blocks


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/external-ip-blocks")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_external_ip_blocks(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_external_ip_blocks: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_external_network_ip_range(hub, ctx, p_id, **kwargs):
    """Get external IPAM network IP range Get external IPAM network IP range with a given id Performs GET /iaas/api/external-network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the external IPAM network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/external-network-ip-ranges/{id}")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_external_network_ip_range(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_external_network_ip_range: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_external_network_ip_ranges(hub, ctx, **kwargs):
    """Get external IPAM network IP ranges Get all external IPAM network IP ranges Performs GET /iaas/api/external-network-ip-ranges


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/external-network-ip-ranges")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_external_network_ip_ranges(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_external_network_ip_ranges: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_internal_network_ip_range(hub, ctx, p_id, **kwargs):
    """Get internal IPAM network IP range Get internal IPAM network IP range with a given id Performs GET /iaas/api/network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/network-ip-ranges/{id}")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_internal_network_ip_range(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_internal_network_ip_range: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_internal_network_ip_ranges(hub, ctx, **kwargs):
    """Get internal IPAM network IP ranges Get all internal IPAM network IP ranges Performs GET /iaas/api/network-ip-ranges


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/network-ip-ranges")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_internal_network_ip_ranges(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.get_internal_network_ip_ranges: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def release_network_ips_by_user(hub, ctx, p_id, **kwargs):
    """release network IPs by user. release network IPs by user Performs POST /iaas/api/network-ip-ranges/{id}/ip-addresses/release


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array ipAddresses: (optional in body) A set of ip addresses IPv4 or IPv6.
    """

    try:

        hub.log.debug("POST /iaas/api/network-ip-ranges/{id}/ip-addresses/release")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "ipAddresses" in kwargs:
            hub.log.debug(f"Got kwarg 'ipAddresses' = {kwargs['ipAddresses']}")
            body["ipAddresses"] = kwargs.get("ipAddresses")
            del kwargs["ipAddresses"]

        ret = api.release_network_ips_by_user(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.release_network_ips_by_user: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def release_unregistered_network_ips(hub, ctx, p_id, **kwargs):
    """release unregistered network IPs Performs POST /iaas/api/network-ip-ranges/{id}/unregistered-ip-addresses/release


    :param string p_id: (required in path) The ID of the network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array ipAddresses: (optional in body) A set of ip addresses IPv4 or IPv6.
    """

    try:

        hub.log.debug(
            "POST /iaas/api/network-ip-ranges/{id}/unregistered-ip-addresses/release"
        )

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "ipAddresses" in kwargs:
            hub.log.debug(f"Got kwarg 'ipAddresses' = {kwargs['ipAddresses']}")
            body["ipAddresses"] = kwargs.get("ipAddresses")
            del kwargs["ipAddresses"]

        ret = api.release_unregistered_network_ips(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.release_unregistered_network_ips: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_external_network_ip_range(hub, ctx, p_id, **kwargs):
    """Update external IPAM network IP range. Assign the external IPAM network IP range to a different network and/or change
      the tags of the external IPAM network IP range. Performs PATCH /iaas/api/external-network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the external IPAM network IP range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array fabricNetworkIds: (optional in body) A list of fabric network Ids that this IP range should be associated
      with.
    """

    try:

        hub.log.debug("PATCH /iaas/api/external-network-ip-ranges/{id}")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "fabricNetworkIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}"
            )
            body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
            del kwargs["fabricNetworkIds"]

        ret = api.update_external_network_ip_range(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.update_external_network_ip_range: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_internal_network_ip_range(
    hub, ctx, p_id, name, startIPAddress, endIPAddress, **kwargs
):
    """Update internal network IP range. Update internal network IP range. Performs PATCH /iaas/api/network-ip-ranges/{id}


    :param string p_id: (required in path) The ID of the network IP range.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string startIPAddress: (required in body) Start IP address of the range.
    :param string endIPAddress: (required in body) End IP address of the range.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    :param array fabricNetworkIds: (optional in body) The Ids of the fabric networks.
    :param string ipVersion: (optional in body) IP address version: IPv4 or IPv6. Default: IPv4.
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    """

    try:

        hub.log.debug("PATCH /iaas/api/network-ip-ranges/{id}")

        api = NetworkIPRangeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["startIPAddress"] = startIPAddress
        body["endIPAddress"] = endIPAddress

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "fabricNetworkIds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'fabricNetworkIds' = {kwargs['fabricNetworkIds']}"
            )
            body["fabricNetworkIds"] = kwargs.get("fabricNetworkIds")
            del kwargs["fabricNetworkIds"]
        if "ipVersion" in kwargs:
            hub.log.debug(f"Got kwarg 'ipVersion' = {kwargs['ipVersion']}")
            body["ipVersion"] = kwargs.get("ipVersion")
            del kwargs["ipVersion"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.update_internal_network_ip_range(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NetworkIPRangeApi.update_internal_network_ip_range: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
