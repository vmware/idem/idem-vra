from idem_vra.client.vra_iaas_lib.api import PropertyApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete_configuration_property(hub, ctx, p_id, **kwargs):
    """Delete a configuration property Delete a configuration property Performs DELETE /iaas/api/configuration-properties/{id}


    :param string p_id: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/configuration-properties/{id}")

        api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_configuration_property(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyApi.delete_configuration_property: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_configuration_properties(hub, ctx, **kwargs):
    """Get configuration properties Get all configuration properties Performs GET /iaas/api/configuration-properties


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/configuration-properties")

        api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_configuration_properties(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyApi.get_configuration_properties: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_configuration_property(hub, ctx, p_id, **kwargs):
    """Get single configuration property Get single configuration property Performs GET /iaas/api/configuration-properties/{id}


    :param string p_id: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/configuration-properties/{id}")

        api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_configuration_property(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyApi.get_configuration_property: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_configuration_property(hub, ctx, key, value, **kwargs):
    """Update or create configuration property. Update or create configuration property. Performs PATCH /iaas/api/configuration-properties


    :param string key: (required in body) The key of the property.
    :param string value: (required in body) The value of the property.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("PATCH /iaas/api/configuration-properties")

        api = PropertyApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["key"] = key
        body["value"] = value

        ret = api.patch_configuration_property(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyApi.patch_configuration_property: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
