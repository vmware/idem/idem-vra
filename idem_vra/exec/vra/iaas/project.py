from idem_vra.client.vra_iaas_lib.api import ProjectApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_project(hub, ctx, name, **kwargs):
    """Create project Create project Performs POST /iaas/api/projects


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean validatePrincipals: (optional in query) If true, a limit of 20 principals is enforced. Additionally each
      principal is validated in the Identity provider and important rules
      for group email formats are enforced.
    :param string description: (optional in body) A human-friendly description.
    :param array administrators: (optional in body) List of administrator users associated with the project. Only
      administrators can manage projects configuration.
    :param array members: (optional in body) List of member users associated with the project.
    :param array viewers: (optional in body) List of viewer users associated with the project.
    :param array supervisors: (optional in body) List of supervisor users associated with the project.
    :param array zoneAssignmentConfigurations: (optional in body) List of configurations for zone assignment to a project.
    :param object constraints: (optional in body) List of storage, network and extensibility constraints to be applied
      when provisioning through this project.
    :param integer operationTimeout: (optional in body) The timeout that should be used for Blueprint operations and
      Provisioning tasks. The timeout is in seconds
    :param string machineNamingTemplate: (optional in body) The naming template to be used for machines provisioned in this
      project
    :param boolean sharedResources: (optional in body) Specifies whether the resources in this projects are shared or not. If
      not set default will be used.
    :param string placementPolicy: (optional in body) Placement policy for the project. Determines how a zone will be
      selected for provisioning. DEFAULT, SPREAD or SPREAD_MEMORY.
    :param object customProperties: (optional in body) The project custom properties which are added to all requests in this
      project
    """

    try:

        hub.log.debug("POST /iaas/api/projects")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "administrators" in kwargs:
            hub.log.debug(f"Got kwarg 'administrators' = {kwargs['administrators']}")
            body["administrators"] = kwargs.get("administrators")
            del kwargs["administrators"]
        if "members" in kwargs:
            hub.log.debug(f"Got kwarg 'members' = {kwargs['members']}")
            body["members"] = kwargs.get("members")
            del kwargs["members"]
        if "viewers" in kwargs:
            hub.log.debug(f"Got kwarg 'viewers' = {kwargs['viewers']}")
            body["viewers"] = kwargs.get("viewers")
            del kwargs["viewers"]
        if "supervisors" in kwargs:
            hub.log.debug(f"Got kwarg 'supervisors' = {kwargs['supervisors']}")
            body["supervisors"] = kwargs.get("supervisors")
            del kwargs["supervisors"]
        if "zoneAssignmentConfigurations" in kwargs:
            hub.log.debug(
                f"Got kwarg 'zoneAssignmentConfigurations' = {kwargs['zoneAssignmentConfigurations']}"
            )
            body["zoneAssignmentConfigurations"] = kwargs.get(
                "zoneAssignmentConfigurations"
            )
            del kwargs["zoneAssignmentConfigurations"]
        if "constraints" in kwargs:
            hub.log.debug(f"Got kwarg 'constraints' = {kwargs['constraints']}")
            body["constraints"] = kwargs.get("constraints")
            del kwargs["constraints"]
        if "operationTimeout" in kwargs:
            hub.log.debug(
                f"Got kwarg 'operationTimeout' = {kwargs['operationTimeout']}"
            )
            body["operationTimeout"] = kwargs.get("operationTimeout")
            del kwargs["operationTimeout"]
        if "machineNamingTemplate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'machineNamingTemplate' = {kwargs['machineNamingTemplate']}"
            )
            body["machineNamingTemplate"] = kwargs.get("machineNamingTemplate")
            del kwargs["machineNamingTemplate"]
        if "sharedResources" in kwargs:
            hub.log.debug(f"Got kwarg 'sharedResources' = {kwargs['sharedResources']}")
            body["sharedResources"] = kwargs.get("sharedResources")
            del kwargs["sharedResources"]
        if "placementPolicy" in kwargs:
            hub.log.debug(f"Got kwarg 'placementPolicy' = {kwargs['placementPolicy']}")
            body["placementPolicy"] = kwargs.get("placementPolicy")
            del kwargs["placementPolicy"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]

        ret = api.create_project(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.create_project: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_project(hub, ctx, p_id, **kwargs):
    """Delete project Delete project with a given id Performs DELETE /iaas/api/projects/{id}


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/projects/{id}")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_project(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.delete_project: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_project_resource_metadata(hub, ctx, p_id, **kwargs):
    """Get project resource metadata Get project resource metadata by a given project id Performs GET /iaas/api/projects/{id}/resource-metadata


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/projects/{id}/resource-metadata")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_project_resource_metadata(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.get_project_resource_metadata: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_project(hub, ctx, p_id, **kwargs):
    """Get project Get project with a given id Performs GET /iaas/api/projects/{id}


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/projects/{id}")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_project(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.get_project: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_projects(hub, ctx, **kwargs):
    """Get projects Get all projects Performs GET /iaas/api/projects


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param string orderBy: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. A set of
      operators and functions are defined for use:
      Operators: eq, ne, gt, ge, lt, le, and, or, not.
      Functions:
      bool substringof(string p0, string p1)
      bool endswith(string p0, string p1)
      bool startswith(string p0, string p1)
      int length(string p0)
      int indexof(string p0, string p1)
      string replace(string p0, string find, string replace)
      string substring(string p0, int pos)
      string substring(string p0, int pos, int length)
      string tolower(string p0)
      string toupper(string p0)
      string trim(string p0)
      string concat(string p0, string p1)
    """

    try:

        hub.log.debug("GET /iaas/api/projects")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_projects(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.get_projects: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def handle_get_project_zone_assignments(hub, ctx, p_id, **kwargs):
    """Get project zone assignments Get all zones assigned to a project by pages Performs GET /iaas/api/projects/{id}/zones


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/projects/{id}/zones")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.handle_get_project_zone_assignments(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.handle_get_project_zone_assignments: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_project_resource_metadata(hub, ctx, p_id, **kwargs):
    """Update project resource metadata Update project resource metadata by a given project id Performs PATCH /iaas/api/projects/{id}/resource-metadata


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array tags: (optional in body) A list of keys and optional values to be applied to compute resources
      provisioned in a project
    """

    try:

        hub.log.debug("PATCH /iaas/api/projects/{id}/resource-metadata")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.update_project_resource_metadata(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.update_project_resource_metadata: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_project(hub, ctx, p_id, name, **kwargs):
    """Update project Update project Performs PATCH /iaas/api/projects/{id}


    :param string p_id: (required in path) The ID of the project.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean validatePrincipals: (optional in query) If true, a limit of 20 principals is enforced. Additionally each
      principal is validated in the Identity provider and important rules
      for group email formats are enforced.
    :param string description: (optional in body) A human-friendly description.
    :param array administrators: (optional in body) List of administrator users associated with the project. Only
      administrators can manage projects configuration.
    :param array members: (optional in body) List of member users associated with the project.
    :param array viewers: (optional in body) List of viewer users associated with the project.
    :param array supervisors: (optional in body) List of supervisor users associated with the project.
    :param array zoneAssignmentConfigurations: (optional in body) List of configurations for zone assignment to a project.
    :param object constraints: (optional in body) List of storage, network and extensibility constraints to be applied
      when provisioning through this project.
    :param integer operationTimeout: (optional in body) The timeout that should be used for Blueprint operations and
      Provisioning tasks. The timeout is in seconds
    :param string machineNamingTemplate: (optional in body) The naming template to be used for machines provisioned in this
      project
    :param boolean sharedResources: (optional in body) Specifies whether the resources in this projects are shared or not. If
      not set default will be used.
    :param string placementPolicy: (optional in body) Placement policy for the project. Determines how a zone will be
      selected for provisioning. DEFAULT, SPREAD or SPREAD_MEMORY.
    :param object customProperties: (optional in body) The project custom properties which are added to all requests in this
      project
    """

    try:

        hub.log.debug("PATCH /iaas/api/projects/{id}")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "administrators" in kwargs:
            hub.log.debug(f"Got kwarg 'administrators' = {kwargs['administrators']}")
            body["administrators"] = kwargs.get("administrators")
            del kwargs["administrators"]
        if "members" in kwargs:
            hub.log.debug(f"Got kwarg 'members' = {kwargs['members']}")
            body["members"] = kwargs.get("members")
            del kwargs["members"]
        if "viewers" in kwargs:
            hub.log.debug(f"Got kwarg 'viewers' = {kwargs['viewers']}")
            body["viewers"] = kwargs.get("viewers")
            del kwargs["viewers"]
        if "supervisors" in kwargs:
            hub.log.debug(f"Got kwarg 'supervisors' = {kwargs['supervisors']}")
            body["supervisors"] = kwargs.get("supervisors")
            del kwargs["supervisors"]
        if "zoneAssignmentConfigurations" in kwargs:
            hub.log.debug(
                f"Got kwarg 'zoneAssignmentConfigurations' = {kwargs['zoneAssignmentConfigurations']}"
            )
            body["zoneAssignmentConfigurations"] = kwargs.get(
                "zoneAssignmentConfigurations"
            )
            del kwargs["zoneAssignmentConfigurations"]
        if "constraints" in kwargs:
            hub.log.debug(f"Got kwarg 'constraints' = {kwargs['constraints']}")
            body["constraints"] = kwargs.get("constraints")
            del kwargs["constraints"]
        if "operationTimeout" in kwargs:
            hub.log.debug(
                f"Got kwarg 'operationTimeout' = {kwargs['operationTimeout']}"
            )
            body["operationTimeout"] = kwargs.get("operationTimeout")
            del kwargs["operationTimeout"]
        if "machineNamingTemplate" in kwargs:
            hub.log.debug(
                f"Got kwarg 'machineNamingTemplate' = {kwargs['machineNamingTemplate']}"
            )
            body["machineNamingTemplate"] = kwargs.get("machineNamingTemplate")
            del kwargs["machineNamingTemplate"]
        if "sharedResources" in kwargs:
            hub.log.debug(f"Got kwarg 'sharedResources' = {kwargs['sharedResources']}")
            body["sharedResources"] = kwargs.get("sharedResources")
            del kwargs["sharedResources"]
        if "placementPolicy" in kwargs:
            hub.log.debug(f"Got kwarg 'placementPolicy' = {kwargs['placementPolicy']}")
            body["placementPolicy"] = kwargs.get("placementPolicy")
            del kwargs["placementPolicy"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]

        ret = api.update_project(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.update_project: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_project_zone_assignments(hub, ctx, p_id, **kwargs):
    """Update all zone assignments to a project Update all zone assignments to a project asynchronously Performs PUT /iaas/api/projects/{id}/zones


    :param string p_id: (required in path) The ID of the project.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array zoneAssignmentSpecifications: (optional in body) List of configurations for zone assignment to a project
    """

    try:

        hub.log.debug("PUT /iaas/api/projects/{id}/zones")

        api = ProjectApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "zoneAssignmentSpecifications" in kwargs:
            hub.log.debug(
                f"Got kwarg 'zoneAssignmentSpecifications' = {kwargs['zoneAssignmentSpecifications']}"
            )
            body["zoneAssignmentSpecifications"] = kwargs.get(
                "zoneAssignmentSpecifications"
            )
            del kwargs["zoneAssignmentSpecifications"]

        ret = api.update_project_zone_assignments(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProjectApi.update_project_zone_assignments: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
