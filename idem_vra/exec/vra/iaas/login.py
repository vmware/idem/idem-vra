from idem_vra.client.vra_iaas_lib.api import LoginApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def retrieve_auth_token(hub, ctx, refreshToken, **kwargs):
    """Retrieve AuthToken for local csp users Retrieve AuthToken for local csp users.
      When accessing other endpoints the Bearer authentication scheme and the
      received token must be provided in the Authorization request header field
      as follows:
      Authorization: Bearer {token} Performs POST /iaas/api/login


    :param string refreshToken: (required in body) Refresh token obtained from the UI
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/login")

        api = LoginApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["refreshToken"] = refreshToken

        ret = api.retrieve_auth_token(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking LoginApi.retrieve_auth_token: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
