from idem_vra.client.vra_iaas_lib.api import CertificatesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_certificate_info(hub, ctx, p_id, **kwargs):
    """Get certificate info Get certificate info Performs GET /iaas/api/certificates/{id}


    :param string p_id: (required in path) The ID of the EndpointCreationTaskState
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/certificates/{id}")

        api = CertificatesApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_certificate_info(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CertificatesApi.get_certificate_info: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
