from idem_vra.client.vra_iaas_lib.api import FabricVSphereDatastoreApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_v_sphere_datastore(hub, ctx, p_id, **kwargs):
    """Get fabric vSphere datastore Get fabric vSphere datastore with a given id Performs GET /iaas/api/fabric-vsphere-datastores/{id}


    :param string p_id: (required in path) The ID of the Fabric vSphere Datastore.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-vsphere-datastores/{id}")

        api = FabricVSphereDatastoreApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_fabric_v_sphere_datastore(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricVSphereDatastoreApi.get_fabric_v_sphere_datastore: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_fabric_v_sphere_datastores(hub, ctx, **kwargs):
    """Get fabric vSphere datastores Get all fabric vSphere datastores. Performs GET /iaas/api/fabric-vsphere-datastores


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-vsphere-datastores")

        api = FabricVSphereDatastoreApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_fabric_v_sphere_datastores(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricVSphereDatastoreApi.get_fabric_v_sphere_datastores: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_fabric_vsphere_datastore(hub, ctx, p_id, **kwargs):
    """Update Fabric vSphere Datastore. Update Fabric vSphere Datastore. Only tag updates are supported. Performs PATCH /iaas/api/fabric-vsphere-datastores/{id}


    :param string p_id: (required in path) The ID of the Fabric Datastore.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array tags: (optional in body) A set of tag keys and optional values that were set on this resource
      instance.
    :param integer maximumAllowedStorageAllocationPercent: (optional in body) What percent of the total available storage on the datastore will be
      used for disk provisioning.This value can be more than 100. e.g. If
      the datastore has 100gb of storage and this value is set to 80, then
      VMware Aria Automation will act as if this datastore has only 80gb. If
      it is 120, then VMware Aria Automation will act as if this datastore
      has 120g thus allowing 20gb overallocation.
    :param integer allocatedNonDiskStorageSpaceBytes: (optional in body) What byte amount is the space occupied by items that are NOT disks on
      the data store.This property is NOT calculated or updated by VMware
      Aria Automation. It is a static config propertypopulated by the
      customer if it is needed (e.g. in the case of a big content library).
    """

    try:

        hub.log.debug("PATCH /iaas/api/fabric-vsphere-datastores/{id}")

        api = FabricVSphereDatastoreApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "maximumAllowedStorageAllocationPercent" in kwargs:
            hub.log.debug(
                f"Got kwarg 'maximumAllowedStorageAllocationPercent' = {kwargs['maximumAllowedStorageAllocationPercent']}"
            )
            body["maximumAllowedStorageAllocationPercent"] = kwargs.get(
                "maximumAllowedStorageAllocationPercent"
            )
            del kwargs["maximumAllowedStorageAllocationPercent"]
        if "allocatedNonDiskStorageSpaceBytes" in kwargs:
            hub.log.debug(
                f"Got kwarg 'allocatedNonDiskStorageSpaceBytes' = {kwargs['allocatedNonDiskStorageSpaceBytes']}"
            )
            body["allocatedNonDiskStorageSpaceBytes"] = kwargs.get(
                "allocatedNonDiskStorageSpaceBytes"
            )
            del kwargs["allocatedNonDiskStorageSpaceBytes"]

        ret = api.update_fabric_vsphere_datastore(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricVSphereDatastoreApi.update_fabric_vsphere_datastore: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
