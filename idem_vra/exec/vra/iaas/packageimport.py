from idem_vra.client.vra_iaas_lib.api import PackageImportApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_package_metadata(hub, ctx, p_id, q_TusResumable, **kwargs):
    """Get package metadata. This API implements the TUS RFC: https://github.com/tus/tus-resumable-upload-
      protocol/blob/main/protocol.md Performs HEAD /iaas/api/integrations-ipam/package-import/{id}


    :param string p_id: (required in path) IPAM package id
    :param string q_TusResumable: (required in header) Tus version
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("HEAD /iaas/api/integrations-ipam/package-import/{id}")

        api = PackageImportApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_package_metadata(id=p_id, tus__resumable=q_TusResumable, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PackageImportApi.get_package_metadata: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_tus_option(hub, ctx, **kwargs):
    """Get Tus options Tus options as headers: Tus-Extension, Tus-Max-Size, Tus-Version.
      This API implements the TUS RFC: https://github.com/tus/tus-resumable-upload-
      protocol/blob/main/protocol.md Performs OPTIONS /iaas/api/integrations-ipam/package-import


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("OPTIONS /iaas/api/integrations-ipam/package-import")

        api = PackageImportApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_tus_option(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PackageImportApi.get_tus_option: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def import_package_http_method_override(
    hub,
    ctx,
    p_id,
    q_TusResumable,
    q_UploadOffset,
    q_ContentType,
    q_XHTTPMethodOverride,
    **kwargs,
):
    """Import package Import IPAM package on chunks of specified size with additional http method
      override.
      This API implements the TUS RFC: https://github.com/tus/tus-resumable-upload-
      protocol/blob/main/protocol.md#x-http-method-override Performs POST /iaas/api/integrations-ipam/package-import/{id}


    :param string p_id: (required in path) IPAM package id
    :param string q_TusResumable: (required in header) Tus version
    :param string q_UploadOffset: (required in header) Upload offset
    :param string q_ContentType: (required in header) Content type
    :param string q_XHTTPMethodOverride: (required in header) The http method to be used on the server instead of the actual method
      of the request. This header usage is part of the specfication for
      resumable uploads - https://tus.io/protocols/resumable-upload
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/integrations-ipam/package-import/{id}")

        api = PackageImportApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.import_package_http_method_override(
            id=p_id,
            tus__resumable=q_TusResumable,
            upload__offset=q_UploadOffset,
            content__type=q_ContentType,
            x__h_t_t_p__method__override=q_XHTTPMethodOverride,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PackageImportApi.import_package_http_method_override: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def import_package(
    hub, ctx, p_id, q_TusResumable, q_UploadOffset, q_ContentType, **kwargs
):
    """Import package Import IPAM package on chunks of specified size.
      This API implements the TUS RFC: https://github.com/tus/tus-resumable-upload-
      protocol/blob/main/protocol.md Performs PATCH /iaas/api/integrations-ipam/package-import/{id}


    :param string p_id: (required in path) IPAM package id
    :param string q_TusResumable: (required in header) Tus version
    :param string q_UploadOffset: (required in header) Upload offset
    :param string q_ContentType: (required in header) Content type
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("PATCH /iaas/api/integrations-ipam/package-import/{id}")

        api = PackageImportApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.import_package(
            id=p_id,
            tus__resumable=q_TusResumable,
            upload__offset=q_UploadOffset,
            content__type=q_ContentType,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PackageImportApi.import_package: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def reserve_package(hub, ctx, q_TusResumable, q_UploadLength, **kwargs):
    """Reserve package This operation has two purposes:
      Make initial request for importing package. Location of the new package is
      returned as a response header if body is not provided.
      Finalize the import when all batches are sent to the server if bundleIdis
      provided or make the complete import if compressedBundle is provided
      This API implements the TUS RFC: https://github.com/tus/tus-resumable-upload-
      protocol/blob/main/protocol.md Performs POST /iaas/api/integrations-ipam/package-import


    :param string q_TusResumable: (required in header) Tus version
    :param string q_UploadLength: (required in header) Length of file in bytes
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array compressedBundle: (optional in body)
    :param string bundleId: (optional in body)
    :param string option: (optional in body)
    :param object properties: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/integrations-ipam/package-import")

        api = PackageImportApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "compressedBundle" in kwargs:
            hub.log.debug(
                f"Got kwarg 'compressedBundle' = {kwargs['compressedBundle']}"
            )
            body["compressedBundle"] = kwargs.get("compressedBundle")
            del kwargs["compressedBundle"]
        if "bundleId" in kwargs:
            hub.log.debug(f"Got kwarg 'bundleId' = {kwargs['bundleId']}")
            body["bundleId"] = kwargs.get("bundleId")
            del kwargs["bundleId"]
        if "option" in kwargs:
            hub.log.debug(f"Got kwarg 'option' = {kwargs['option']}")
            body["option"] = kwargs.get("option")
            del kwargs["option"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]

        ret = api.reserve_package(
            body, tus__resumable=q_TusResumable, upload__length=q_UploadLength, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PackageImportApi.reserve_package: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
