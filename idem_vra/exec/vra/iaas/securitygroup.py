from idem_vra.client.vra_iaas_lib.api import SecurityGroupApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def change_security_groups(hub, ctx, p_id, id, _links, **kwargs):
    """Change security groups for a vSphere machine Change security groups for a vSphere machine network interfaces. Securing group
      that is part of the same deployment can be added or removed for a machine
      network interface. Performs POST /iaas/api/machines/{id}/operations/change-security-groups


    :param string p_id: (required in path) The id of the vSphere machine.
    :param string id: (required in body) The id of this resource instance
    :param object _links: (required in body) hrefs links
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string createdAt: (optional in body) Date when the entity was created. The date is in ISO 8601 and UTC.
    :param string updatedAt: (optional in body) Date when the entity was last updated. The date is ISO 8601 and UTC.
    :param string owner: (optional in body) Email of the user or display name of the group that owns the entity.
    :param string ownerType: (optional in body) Type of a owner(user/ad_group) that owns the entity.
    :param string orgId: (optional in body) The id of the organization this entity belongs to.
    :param string name: (optional in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string description: (optional in body) A human-friendly description.
    :param array networkInterfaceSpecifications: (optional in body) A set of network interface controller specifications for this machine.
      If not specified, then no reconfiguration will be performed.
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/change-security-groups")

        api = SecurityGroupApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["id"] = id
        body["_links"] = _links

        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "owner" in kwargs:
            hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
            body["owner"] = kwargs.get("owner")
            del kwargs["owner"]
        if "ownerType" in kwargs:
            hub.log.debug(f"Got kwarg 'ownerType' = {kwargs['ownerType']}")
            body["ownerType"] = kwargs.get("ownerType")
            del kwargs["ownerType"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "networkInterfaceSpecifications" in kwargs:
            hub.log.debug(
                f"Got kwarg 'networkInterfaceSpecifications' = {kwargs['networkInterfaceSpecifications']}"
            )
            body["networkInterfaceSpecifications"] = kwargs.get(
                "networkInterfaceSpecifications"
            )
            del kwargs["networkInterfaceSpecifications"]

        ret = api.change_security_groups(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecurityGroupApi.change_security_groups: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_on_demand_security_group(hub, ctx, name, projectId, **kwargs):
    """Create on-demand security group Provision a new on-demand security group Performs POST /iaas/api/security-groups


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param string description: (optional in body) A human-friendly description.
    :param array rules: (optional in body) List of security rules.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    """

    try:

        hub.log.debug("POST /iaas/api/security-groups")

        api = SecurityGroupApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId

        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "rules" in kwargs:
            hub.log.debug(f"Got kwarg 'rules' = {kwargs['rules']}")
            body["rules"] = kwargs.get("rules")
            del kwargs["rules"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.create_on_demand_security_group(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecurityGroupApi.create_on_demand_security_group: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_security_group(hub, ctx, p_id, **kwargs):
    """Delete an on-demand security group Delete an on-demand security group with a given id Performs DELETE /iaas/api/security-groups/{id}


    :param string p_id: (required in path) The ID of the security group.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this security group. Use with caution as
      force deleting may cause inconsistencies between the cloud provider
      and VMware Aria Automation.
    """

    try:

        hub.log.debug("DELETE /iaas/api/security-groups/{id}")

        api = SecurityGroupApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_security_group(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecurityGroupApi.delete_security_group: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_security_group(hub, ctx, p_id, **kwargs):
    """Get security group Get security group with a given id Performs GET /iaas/api/security-groups/{id}


    :param string p_id: (required in path) The ID of the security group.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/security-groups/{id}")

        api = SecurityGroupApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_security_group(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecurityGroupApi.get_security_group: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_security_groups(hub, ctx, **kwargs):
    """Get security groups Get all security groups Performs GET /iaas/api/security-groups


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    """

    try:

        hub.log.debug("GET /iaas/api/security-groups")

        api = SecurityGroupApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_security_groups(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecurityGroupApi.get_security_groups: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def reconfigure_security_group(hub, ctx, p_id, name, projectId, **kwargs):
    """Reconfigure operation for security group Day-2 reconfigure operation for new security groups provisioned by VMware Aria
      Automation. This is not supported for existing security groups Performs POST /iaas/api/security-groups/{id}/operations/reconfigure


    :param string p_id: (required in path) The ID of the security group.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param string description: (optional in body) A human-friendly description.
    :param array rules: (optional in body) List of security rules.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    """

    try:

        hub.log.debug("POST /iaas/api/security-groups/{id}/operations/reconfigure")

        api = SecurityGroupApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId

        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "rules" in kwargs:
            hub.log.debug(f"Got kwarg 'rules' = {kwargs['rules']}")
            body["rules"] = kwargs.get("rules")
            del kwargs["rules"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.reconfigure_security_group(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecurityGroupApi.reconfigure_security_group: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_security_group(hub, ctx, p_id, **kwargs):
    """Update security group. Update security group. Only tag updates are supported. Performs PATCH /iaas/api/security-groups/{id}


    :param string p_id: (required in path) The ID of the security group.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    """

    try:

        hub.log.debug("PATCH /iaas/api/security-groups/{id}")

        api = SecurityGroupApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]

        ret = api.update_security_group(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SecurityGroupApi.update_security_group: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
