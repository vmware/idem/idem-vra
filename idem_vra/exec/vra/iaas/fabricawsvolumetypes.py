from idem_vra.client.vra_iaas_lib.api import FabricAWSVolumeTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_fabric_aws_volume_types(hub, ctx, **kwargs):
    """Get fabric AWS volume types Get all fabric AWS volume types. Performs GET /iaas/api/fabric-aws-volume-types


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/fabric-aws-volume-types")

        api = FabricAWSVolumeTypesApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_fabric_aws_volume_types(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FabricAWSVolumeTypesApi.get_fabric_aws_volume_types: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
