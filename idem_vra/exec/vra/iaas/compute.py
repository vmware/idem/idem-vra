from idem_vra.client.vra_iaas_lib.api import ComputeApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def attach_machine_disk(hub, ctx, p_id, blockDeviceId, **kwargs):
    """Attach machine disk Attach a disk to a machine. Performs POST /iaas/api/machines/{id}/disks


    :param string p_id: (required in path) The ID of the machine.
    :param string blockDeviceId: (required in body) The id of the existing block device
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string name: (optional in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string description: (optional in body) A human-friendly description.
    :param string scsiController: (optional in body) Deprecated: The SCSI controller to be assigned
    :param string unitNumber: (optional in body) Deprecated: The Unit Number to be assigned
    :param object diskAttachmentProperties: (optional in body) Disk Attachment specific properties
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/disks")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["blockDeviceId"] = blockDeviceId

        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "scsiController" in kwargs:
            hub.log.debug(f"Got kwarg 'scsiController' = {kwargs['scsiController']}")
            body["scsiController"] = kwargs.get("scsiController")
            del kwargs["scsiController"]
        if "unitNumber" in kwargs:
            hub.log.debug(f"Got kwarg 'unitNumber' = {kwargs['unitNumber']}")
            body["unitNumber"] = kwargs.get("unitNumber")
            del kwargs["unitNumber"]
        if "diskAttachmentProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'diskAttachmentProperties' = {kwargs['diskAttachmentProperties']}"
            )
            body["diskAttachmentProperties"] = kwargs.get("diskAttachmentProperties")
            del kwargs["diskAttachmentProperties"]

        ret = api.attach_machine_disk(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.attach_machine_disk: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def change_security_groups(hub, ctx, p_id, id, _links, **kwargs):
    """Change security groups for a vSphere machine Change security groups for a vSphere machine network interfaces. Securing group
      that is part of the same deployment can be added or removed for a machine
      network interface. Performs POST /iaas/api/machines/{id}/operations/change-security-groups


    :param string p_id: (required in path) The id of the vSphere machine.
    :param string id: (required in body) The id of this resource instance
    :param object _links: (required in body) hrefs links
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string createdAt: (optional in body) Date when the entity was created. The date is in ISO 8601 and UTC.
    :param string updatedAt: (optional in body) Date when the entity was last updated. The date is ISO 8601 and UTC.
    :param string owner: (optional in body) Email of the user or display name of the group that owns the entity.
    :param string ownerType: (optional in body) Type of a owner(user/ad_group) that owns the entity.
    :param string orgId: (optional in body) The id of the organization this entity belongs to.
    :param string name: (optional in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string description: (optional in body) A human-friendly description.
    :param array networkInterfaceSpecifications: (optional in body) A set of network interface controller specifications for this machine.
      If not specified, then no reconfiguration will be performed.
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/change-security-groups")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["id"] = id
        body["_links"] = _links

        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "owner" in kwargs:
            hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
            body["owner"] = kwargs.get("owner")
            del kwargs["owner"]
        if "ownerType" in kwargs:
            hub.log.debug(f"Got kwarg 'ownerType' = {kwargs['ownerType']}")
            body["ownerType"] = kwargs.get("ownerType")
            del kwargs["ownerType"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "networkInterfaceSpecifications" in kwargs:
            hub.log.debug(
                f"Got kwarg 'networkInterfaceSpecifications' = {kwargs['networkInterfaceSpecifications']}"
            )
            body["networkInterfaceSpecifications"] = kwargs.get(
                "networkInterfaceSpecifications"
            )
            del kwargs["networkInterfaceSpecifications"]

        ret = api.change_security_groups(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.change_security_groups: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_machine_snapshot(hub, ctx, p_id, id, _links, **kwargs):
    """Create snapshot operation for machine Second day create snapshot operation for machine Performs POST /iaas/api/machines/{id}/operations/snapshots


    :param string p_id: (required in path) The id of the Machine.
    :param string id: (required in body) The id of this resource instance
    :param object _links: (required in body) hrefs links
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string createdAt: (optional in body) Date when the entity was created. The date is in ISO 8601 and UTC.
    :param string updatedAt: (optional in body) Date when the entity was last updated. The date is ISO 8601 and UTC.
    :param string owner: (optional in body) Email of the user or display name of the group that owns the entity.
    :param string ownerType: (optional in body) Type of a owner(user/ad_group) that owns the entity.
    :param string orgId: (optional in body) The id of the organization this entity belongs to.
    :param string name: (optional in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string description: (optional in body) A human-friendly description.
    :param boolean snapshotMemory: (optional in body) Captures the full state of a running virtual machine, including the
      memory.
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the snapshot.
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/snapshots")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["id"] = id
        body["_links"] = _links

        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "owner" in kwargs:
            hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
            body["owner"] = kwargs.get("owner")
            del kwargs["owner"]
        if "ownerType" in kwargs:
            hub.log.debug(f"Got kwarg 'ownerType' = {kwargs['ownerType']}")
            body["ownerType"] = kwargs.get("ownerType")
            del kwargs["ownerType"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "snapshotMemory" in kwargs:
            hub.log.debug(f"Got kwarg 'snapshotMemory' = {kwargs['snapshotMemory']}")
            body["snapshotMemory"] = kwargs.get("snapshotMemory")
            del kwargs["snapshotMemory"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]

        ret = api.create_machine_snapshot(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.create_machine_snapshot: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_machine(
    hub, ctx, name, projectId, flavor, flavorRef, image, imageRef, **kwargs
):
    """Create machine  Performs POST /iaas/api/machines


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string flavor: (required in body) Flavor of machine instance.
    :param string flavorRef: (required in body) Provider specific flavor reference. Valid if no flavor property is
      provided
    :param string image: (required in body) Type of image used for this machine.
    :param string imageRef: (required in body) Direct image reference used for this machine (name, path, location,
      uri, etc.). Valid if no image property is provided
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    :param string description: (optional in body) Describes machine within the scope of your organization and is not
      propagated to the cloud
    :param array nics: (optional in body) A set of network interface controller specifications for this machine.
      If not specified, then a default network connection will be created.
    :param array disks: (optional in body) A set of disk specifications for this machine.
    :param Any bootConfig: (optional in body)
    :param Any bootConfigSettings: (optional in body)
    :param integer machineCount: (optional in body) Number of machines to provision - default 1.
    :param array constraints: (optional in body) Constraints that are used to drive placement policies for the virtual
      machine that is produced from this specification. Constraint
      expressions are matched against tags on existing placement targets.
    :param array imageDiskConstraints: (optional in body) Constraints that are used to drive placement policies for the image
      disk. Constraint expressions are matched against tags on existing
      placement targets.
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    :param Any remoteAccess: (optional in body)
    :param Any saltConfiguration: (optional in body)
    """

    try:

        hub.log.debug("POST /iaas/api/machines")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId
        body["flavor"] = flavor
        body["flavorRef"] = flavorRef
        body["image"] = image
        body["imageRef"] = imageRef

        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "nics" in kwargs:
            hub.log.debug(f"Got kwarg 'nics' = {kwargs['nics']}")
            body["nics"] = kwargs.get("nics")
            del kwargs["nics"]
        if "disks" in kwargs:
            hub.log.debug(f"Got kwarg 'disks' = {kwargs['disks']}")
            body["disks"] = kwargs.get("disks")
            del kwargs["disks"]
        if "bootConfig" in kwargs:
            hub.log.debug(f"Got kwarg 'bootConfig' = {kwargs['bootConfig']}")
            body["bootConfig"] = kwargs.get("bootConfig")
            del kwargs["bootConfig"]
        if "bootConfigSettings" in kwargs:
            hub.log.debug(
                f"Got kwarg 'bootConfigSettings' = {kwargs['bootConfigSettings']}"
            )
            body["bootConfigSettings"] = kwargs.get("bootConfigSettings")
            del kwargs["bootConfigSettings"]
        if "machineCount" in kwargs:
            hub.log.debug(f"Got kwarg 'machineCount' = {kwargs['machineCount']}")
            body["machineCount"] = kwargs.get("machineCount")
            del kwargs["machineCount"]
        if "constraints" in kwargs:
            hub.log.debug(f"Got kwarg 'constraints' = {kwargs['constraints']}")
            body["constraints"] = kwargs.get("constraints")
            del kwargs["constraints"]
        if "imageDiskConstraints" in kwargs:
            hub.log.debug(
                f"Got kwarg 'imageDiskConstraints' = {kwargs['imageDiskConstraints']}"
            )
            body["imageDiskConstraints"] = kwargs.get("imageDiskConstraints")
            del kwargs["imageDiskConstraints"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "remoteAccess" in kwargs:
            hub.log.debug(f"Got kwarg 'remoteAccess' = {kwargs['remoteAccess']}")
            body["remoteAccess"] = kwargs.get("remoteAccess")
            del kwargs["remoteAccess"]
        if "saltConfiguration" in kwargs:
            hub.log.debug(
                f"Got kwarg 'saltConfiguration' = {kwargs['saltConfiguration']}"
            )
            body["saltConfiguration"] = kwargs.get("saltConfiguration")
            del kwargs["saltConfiguration"]

        ret = api.create_machine(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.create_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_machine_disk(hub, ctx, p_id, p_diskId, **kwargs):
    """Delete machine disk Remove a disk from a given machine. Performs DELETE /iaas/api/machines/{id}/disks/{diskId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_diskId: (required in path) The ID of the disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/machines/{id}/disks/{diskId}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_machine_disk(id=p_id, disk_id=p_diskId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.delete_machine_disk: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_machine_snapshot(hub, ctx, p_id, p_snapshotId, **kwargs):
    """Delete snapshot operation for machine Second day delete snapshot operation for machine Performs DELETE /iaas/api/machines/{id}/snapshots/{snapshotId}


    :param string p_id: (required in path) The id of the Machine.
    :param string p_snapshotId: (required in path) Snapshot id to delete.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/machines/{id}/snapshots/{snapshotId}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_machine_snapshot(id=p_id, snapshot_id=p_snapshotId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.delete_machine_snapshot: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_machine(hub, ctx, p_id, **kwargs):
    """Delete Machine Delete Machine with a given id Performs DELETE /iaas/api/machines/{id}


    :param string p_id: (required in path) The ID of the machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this machine. Use with caution as force
      deleting may cause inconsistencies between the cloud provider and
      VMware Aria Automation.
    """

    try:

        hub.log.debug("DELETE /iaas/api/machines/{id}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.delete_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_machine_disk(hub, ctx, p_id, p_diskId, **kwargs):
    """Get machine disk Get disk with a given id for specific machine Performs GET /iaas/api/machines/{id}/disks/{diskId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_diskId: (required in path) The ID of the disk.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/machines/{id}/disks/{diskId}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_machine_disk(id=p_id, disk_id=p_diskId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.get_machine_disk: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_machine_disks(hub, ctx, p_id, **kwargs):
    """Get machine disks Get all machine disks Performs GET /iaas/api/machines/{id}/disks


    :param string p_id: (required in path) The ID of the machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/machines/{id}/disks")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_machine_disks(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.get_machine_disks: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_machine_network_interface(hub, ctx, p_id, p_networkId, **kwargs):
    """Get machine network interface Get network interface with a given id for specific machine Performs GET /iaas/api/machines/{id}/network-interfaces/{networkId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_networkId: (required in path) The ID of the network interface.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/machines/{id}/network-interfaces/{networkId}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_machine_network_interface(
            id=p_id, network_id=p_networkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.get_machine_network_interface: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_machine_snapshot(hub, ctx, p_id, p_snapshotId, **kwargs):
    """Get machine snapshot Get snapshot with a given id for specific machine Performs GET /iaas/api/machines/{id}/snapshots/{snapshotId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_snapshotId: (required in path) The ID of the snapshot.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/machines/{id}/snapshots/{snapshotId}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_machine_snapshot(id=p_id, snapshot_id=p_snapshotId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.get_machine_snapshot: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_machine_snapshots(hub, ctx, p_id, **kwargs):
    """Get machine snapshots information Get machine snapshots information Performs GET /iaas/api/machines/{id}/snapshots


    :param string p_id: (required in path) The ID of the machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/machines/{id}/snapshots")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_machine_snapshots(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.get_machine_snapshots: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_machine(hub, ctx, p_id, **kwargs):
    """Get machine Get machine with a given id Performs GET /iaas/api/machines/{id}


    :param string p_id: (required in path) The ID of the machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string select: (optional in query) Select a subset of properties to include in the response.
    """

    try:

        hub.log.debug("GET /iaas/api/machines/{id}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.get_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_machines(hub, ctx, **kwargs):
    """Get machines Get all machines Performs GET /iaas/api/machines


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param integer top: (optional in query) Number of records you want to get.
    :param integer skip: (optional in query) Number of records you want to skip.
    :param boolean count: (optional in query) Flag which when specified, regardless of the assigned value, shows the
      total number of records. If the collection has a filter it shows the
      number of records matching the filter.
    :param string select: (optional in query) Select a subset of properties to include in the response.
    :param string filter: (optional in query) Filter the results by a specified predicate expression. Operators: eq,
      ne, and, or.
    :param boolean skipOperationLinks: (optional in query) If set to true will not return operation links.
    """

    try:

        hub.log.debug("GET /iaas/api/machines")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_machines(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.get_machines: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_machine_network_interface(hub, ctx, p_id, p_networkId, **kwargs):
    """Patch machine network interface Patch network interface with a given id for specific machine. Only name,
      description, IPv4 address and custom property updates are supported. The change
      to name and IPv4 address will not propagate to cloud endpoint for provisioned
      machines. Performs PATCH /iaas/api/machines/{id}/network-interfaces/{networkId}


    :param string p_id: (required in path) The ID of the machine.
    :param string p_networkId: (required in path) The ID of the network interface.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) Describes the network interface of the machine within the scope of
      your organization and is not propagated to the cloud
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the machine.
      Internal custom properties (for example, prefixed with: "__") can not
      be updated.
    :param string name: (optional in body) Network interface name used during machine network interface
      provisioning. This property only takes effect if it is set before
      machine provisioning starts. The change will not propagate to cloud
      endpoint for provisioned machines.
    :param string address: (optional in body) Set IPv4 address for the machine network interface. The change will
      not propagate to cloud endpoint for provisioned machines.
    """

    try:

        hub.log.debug("PATCH /iaas/api/machines/{id}/network-interfaces/{networkId}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "address" in kwargs:
            hub.log.debug(f"Got kwarg 'address' = {kwargs['address']}")
            body["address"] = kwargs.get("address")
            del kwargs["address"]

        ret = api.patch_machine_network_interface(
            body, id=p_id, network_id=p_networkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.patch_machine_network_interface: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def power_off_machine(hub, ctx, p_id, **kwargs):
    """Power-off operation for machine Second day power-off operation for machine Performs POST /iaas/api/machines/{id}/operations/power-off


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/power-off")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.power_off_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.power_off_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def power_on_machine(hub, ctx, p_id, **kwargs):
    """Power-on operation for machine Second day power-on operation for machine Performs POST /iaas/api/machines/{id}/operations/power-on


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/power-on")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.power_on_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.power_on_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def reboot_machine(hub, ctx, p_id, **kwargs):
    """Reboot operation for machine Second day reboot operation for machine Performs POST /iaas/api/machines/{id}/operations/reboot


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/reboot")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.reboot_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.reboot_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def reset_machine(hub, ctx, p_id, **kwargs):
    """Reset operation for machine Second day reset operation for machine Performs POST /iaas/api/machines/{id}/operations/reset


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/reset")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.reset_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.reset_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def resize_machine(hub, ctx, p_id, **kwargs):
    """Resize operation for machine Second day resize operation for machine Performs POST /iaas/api/machines/{id}/operations/resize


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string flavorName: (optional in query) The desired flavor to resize the Machine.
    :param string cpuCount: (optional in query) The desired number of CPUs to resize the
    :param string memoryInMB: (optional in query) The desired memory in MBs to resize the Machine
    :param string coreCount: (optional in query) The desired number of cores per socket to resize the Machine
    :param boolean rebootMachine: (optional in query) Only applicable for vSphere VMs with the CPU Hot Add or Memory Hot
      Plug options enabled. If set to false, VM is resized without reboot.
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/resize")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.resize_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.resize_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def restart_machine(hub, ctx, p_id, **kwargs):
    """Restart operation for machine Second day restart operation for machine Performs POST /iaas/api/machines/{id}/operations/restart


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/restart")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.restart_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.restart_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def revert_machine_snapshot(hub, ctx, p_id, p_snapshotId, **kwargs):
    """Revert snapshot operation for machine Second day revert snapshot operation for machine Performs POST /iaas/api/machines/{id}/operations/revert/{snapshotId}


    :param string p_id: (required in path) The id of the Machine.
    :param string p_snapshotId: (required in path) Snapshot id to revert.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/revert/{snapshotId}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.revert_machine_snapshot(id=p_id, snapshot_id=p_snapshotId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.revert_machine_snapshot: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def shutdown_machine(hub, ctx, p_id, **kwargs):
    """Shut down operation for machine Second day shut down operation machine Performs POST /iaas/api/machines/{id}/operations/shutdown


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/shutdown")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.shutdown_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.shutdown_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def suspend_machine(hub, ctx, p_id, **kwargs):
    """Suspend operation for machine Second day suspend operation for machine Performs POST /iaas/api/machines/{id}/operations/suspend


    :param string p_id: (required in path) The id of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/suspend")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.suspend_machine(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.suspend_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def unregister(hub, ctx, p_id, **kwargs):
    """Unregister a vSphere provisioned machine Unregister a vSphere provisioned machine Performs POST /iaas/api/machines/{id}/operations/unregister


    :param string p_id: (required in path) The id of the vSphere machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/machines/{id}/operations/unregister")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.unregister(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ComputeApi.unregister: {err}")
        return ExecReturn(result=False, comment=str(err))


async def update_machine(hub, ctx, p_id, **kwargs):
    """Update machine. Update machine. Only description, tag, custom property and bootConfig updates
      are supported. Please note that all existing tags, assigned to this machine,
      that are not implicitly added in the Patch body, will be unassigned from this
      machine!All other properties in the MachineSpecification body are ignored. Performs PATCH /iaas/api/machines/{id}


    :param string p_id: (required in path) The ID of the Machine.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) Describes machine within the scope of your organization and is not
      propagated to the cloud
    :param array tags: (optional in body) A set of tag keys and optional values that should be set on any
      resource that is produced from this specification.
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the machine.
      Internal custom properties (for example, prefixed with: "__") are
      discarded.
    :param Any bootConfig: (optional in body)
    """

    try:

        hub.log.debug("PATCH /iaas/api/machines/{id}")

        api = ComputeApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "tags" in kwargs:
            hub.log.debug(f"Got kwarg 'tags' = {kwargs['tags']}")
            body["tags"] = kwargs.get("tags")
            del kwargs["tags"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "bootConfig" in kwargs:
            hub.log.debug(f"Got kwarg 'bootConfig' = {kwargs['bootConfig']}")
            body["bootConfig"] = kwargs.get("bootConfig")
            del kwargs["bootConfig"]

        ret = api.update_machine(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeApi.update_machine: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
