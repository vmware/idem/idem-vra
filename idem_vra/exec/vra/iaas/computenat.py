from idem_vra.client.vra_iaas_lib.api import ComputeNatApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_compute_nat(hub, ctx, name, projectId, gateway, natRules, **kwargs):
    """Create a Compute Nat Create a new Compute Nat. Performs POST /iaas/api/compute-nats


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param string projectId: (required in body) The id of the project the current user belongs to.
    :param string gateway: (required in body) Id of the Compute Gateway to which the Compute Nat resource will be
      attached
    :param array natRules: (required in body) List of NAT Rules
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string deploymentId: (optional in body) The id of the deployment that is associated with this resource
    :param object customProperties: (optional in body) Additional custom properties that may be used to extend this resource.
    """

    try:

        hub.log.debug("POST /iaas/api/compute-nats")

        api = ComputeNatApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["projectId"] = projectId
        body["gateway"] = gateway
        body["natRules"] = natRules

        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]

        ret = api.create_compute_nat(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeNatApi.create_compute_nat: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_compute_nat(hub, ctx, p_id, **kwargs):
    """Delete a compute nat Delete compute nat with a given id Performs DELETE /iaas/api/compute-nats/{id}


    :param string p_id: (required in path) The ID of the compute nat resource.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param boolean forceDelete: (optional in query) Controls whether this is a force delete operation. If true, best
      effort is made for deleting this nat. Use with caution as force
      deleting may cause inconsistencies between the cloud provider and
      VMware Aria Automation.
    """

    try:

        hub.log.debug("DELETE /iaas/api/compute-nats/{id}")

        api = ComputeNatApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_compute_nat(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeNatApi.delete_compute_nat: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_compute_nat(hub, ctx, p_id, **kwargs):
    """Get a Compute Nat Get Compute Nat with a given id Performs GET /iaas/api/compute-nats/{id}


    :param string p_id: (required in path) The ID of the Compute Nat resource.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/compute-nats/{id}")

        api = ComputeNatApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_compute_nat(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeNatApi.get_compute_nat: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_compute_nats(hub, ctx, **kwargs):
    """Get Compute Nats Get all Compute Nats Performs GET /iaas/api/compute-nats


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/compute-nats")

        api = ComputeNatApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_compute_nats(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeNatApi.get_compute_nats: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def reconfigure_nat(hub, ctx, p_id, natRules, **kwargs):
    """Reconfigure operation for nat Day-2 reconfigure operation for nat Performs POST /iaas/api/compute-nats/{id}/operations/reconfigure


    :param string p_id: (required in path) The ID of the Compute Nat
    :param array natRules: (required in body) List of NAT rules to be applied on this Compute Nat.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("POST /iaas/api/compute-nats/{id}/operations/reconfigure")

        api = ComputeNatApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["natRules"] = natRules

        ret = api.reconfigure_nat(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ComputeNatApi.reconfigure_nat: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
