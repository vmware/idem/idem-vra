from idem_vra.client.vra_iaas_lib.api import ImageProfileApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_image_profile(
    hub, ctx, name, imageMapping, regionId, externalRegionId, cloudAccountId, **kwargs
):
    """Create image profile Create image profile Performs POST /iaas/api/image-profiles


    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param object imageMapping: (required in body) Image mapping defined for the corresponding region.
    :param string regionId: (required in body) The id of the region for which this profile is created
    :param string externalRegionId: (required in body) The id of the externalRegion for which this profile is created
    :param string cloudAccountId: (required in body) The id of the externalRegion for which this profile is created
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    """

    try:

        hub.log.debug("POST /iaas/api/image-profiles")

        api = ImageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["imageMapping"] = imageMapping
        body["regionId"] = regionId
        body["externalRegionId"] = externalRegionId
        body["cloudAccountId"] = cloudAccountId

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]

        ret = api.create_image_profile(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ImageProfileApi.create_image_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_image_profile(hub, ctx, p_id, **kwargs):
    """Delete image profile Delete image profile with a given id Performs DELETE /iaas/api/image-profiles/{id}


    :param string p_id: (required in path) The ID of the image.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("DELETE /iaas/api/image-profiles/{id}")

        api = ImageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.delete_image_profile(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ImageProfileApi.delete_image_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_image_profile(hub, ctx, p_id, **kwargs):
    """Get image profile Get image profile with a given id Performs GET /iaas/api/image-profiles/{id}


    :param string p_id: (required in path) The ID of the image.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/image-profiles/{id}")

        api = ImageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_image_profile(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ImageProfileApi.get_image_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_image_profiles(hub, ctx, **kwargs):
    """Get image profile Get all image profiles Performs GET /iaas/api/image-profiles


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    """

    try:

        hub.log.debug("GET /iaas/api/image-profiles")

        api = ImageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        ret = api.get_image_profiles(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ImageProfileApi.get_image_profiles: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_image_profile(hub, ctx, p_id, name, imageMapping, **kwargs):
    """Update image profile Update image profile Performs PATCH /iaas/api/image-profiles/{id}


    :param string p_id: (required in path) The ID of the image.
    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.
    :param object imageMapping: (required in body) Image mapping defined for the corresponding region.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about
    :param string description: (optional in body) A human-friendly description.
    """

    try:

        hub.log.debug("PATCH /iaas/api/image-profiles/{id}")

        api = ImageProfileApi(hub.clients["idem_vra.client.vra_iaas_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2021-07-15"

        body = {}
        body["name"] = name
        body["imageMapping"] = imageMapping

        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]

        ret = api.update_image_profile(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ImageProfileApi.update_image_profile: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
