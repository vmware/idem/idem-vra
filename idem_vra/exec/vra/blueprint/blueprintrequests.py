from idem_vra.client.vra_blueprint_lib.api import BlueprintRequestsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def cancel_blueprint_request_using_post(hub, ctx, p_requestId, **kwargs):
    """Cancel request  Performs POST /blueprint/api/blueprint-requests/{requestId}/actions/cancel


    :param string p_requestId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "POST /blueprint/api/blueprint-requests/{requestId}/actions/cancel"
        )

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.cancel_blueprint_request_using_post(request_id=p_requestId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.cancel_blueprint_request_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create_blueprint_request_using_post(hub, ctx, **kwargs):
    """Creates a blueprint request  Performs POST /blueprint/api/blueprint-requests


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string id: (optional in body) Object ID
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param string deploymentId: (optional in body) Existing deployment Id
    :param string requestTrackerId: (optional in body) Request tracker Id
    :param string deploymentName: (optional in body) Name for the new deployment
    :param string reason: (optional in body) Reason for requesting a blueprint
    :param string description: (optional in body) Description for the new request
    :param boolean plan: (optional in body) Plan only without affecting existing deployment, if set to true.
      Default value is false. Cannot be combined with other flags like
      simulate.
    :param boolean destroy: (optional in body) Destroy existing deployment, if it set to true. Default value is
      false.
    :param boolean ignoreDeleteFailures: (optional in body) Ignore delete failures in blueprint request
    :param boolean simulate: (optional in body) Simulate blueprint request with providers, if set to true. Default
      value is false. Cannot be combined with other flags like Plan.
    :param string blueprintId: (optional in body) Blueprint Id
    :param string blueprintVersion: (optional in body) Blueprint version
    :param string content: (optional in body) Blueprint YAML content
    :param object inputs: (optional in body) Blueprint request inputs
    :param string status: (optional in body) Status
    :param string failureMessage: (optional in body) Failure message
    :param array validationMessages: (optional in body) Validation messages
    :param string cancelRequestedAt: (optional in body) Cancel request time
    :param string cancelRequestedBy: (optional in body) Cancel requested by
    :param string flowId: (optional in body) Flow Id
    :param string flowExecutionId: (optional in body) Flow execution Id
    :param array targetResources: (optional in body) Target resources
    """

    try:

        hub.log.debug("POST /blueprint/api/blueprint-requests")

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "requestTrackerId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'requestTrackerId' = {kwargs['requestTrackerId']}"
            )
            body["requestTrackerId"] = kwargs.get("requestTrackerId")
            del kwargs["requestTrackerId"]
        if "deploymentName" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentName' = {kwargs['deploymentName']}")
            body["deploymentName"] = kwargs.get("deploymentName")
            del kwargs["deploymentName"]
        if "reason" in kwargs:
            hub.log.debug(f"Got kwarg 'reason' = {kwargs['reason']}")
            body["reason"] = kwargs.get("reason")
            del kwargs["reason"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "plan" in kwargs:
            hub.log.debug(f"Got kwarg 'plan' = {kwargs['plan']}")
            body["plan"] = kwargs.get("plan")
            del kwargs["plan"]
        if "destroy" in kwargs:
            hub.log.debug(f"Got kwarg 'destroy' = {kwargs['destroy']}")
            body["destroy"] = kwargs.get("destroy")
            del kwargs["destroy"]
        if "ignoreDeleteFailures" in kwargs:
            hub.log.debug(
                f"Got kwarg 'ignoreDeleteFailures' = {kwargs['ignoreDeleteFailures']}"
            )
            body["ignoreDeleteFailures"] = kwargs.get("ignoreDeleteFailures")
            del kwargs["ignoreDeleteFailures"]
        if "simulate" in kwargs:
            hub.log.debug(f"Got kwarg 'simulate' = {kwargs['simulate']}")
            body["simulate"] = kwargs.get("simulate")
            del kwargs["simulate"]
        if "blueprintId" in kwargs:
            hub.log.debug(f"Got kwarg 'blueprintId' = {kwargs['blueprintId']}")
            body["blueprintId"] = kwargs.get("blueprintId")
            del kwargs["blueprintId"]
        if "blueprintVersion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'blueprintVersion' = {kwargs['blueprintVersion']}"
            )
            body["blueprintVersion"] = kwargs.get("blueprintVersion")
            del kwargs["blueprintVersion"]
        if "content" in kwargs:
            hub.log.debug(f"Got kwarg 'content' = {kwargs['content']}")
            body["content"] = kwargs.get("content")
            del kwargs["content"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "failureMessage" in kwargs:
            hub.log.debug(f"Got kwarg 'failureMessage' = {kwargs['failureMessage']}")
            body["failureMessage"] = kwargs.get("failureMessage")
            del kwargs["failureMessage"]
        if "validationMessages" in kwargs:
            hub.log.debug(
                f"Got kwarg 'validationMessages' = {kwargs['validationMessages']}"
            )
            body["validationMessages"] = kwargs.get("validationMessages")
            del kwargs["validationMessages"]
        if "cancelRequestedAt" in kwargs:
            hub.log.debug(
                f"Got kwarg 'cancelRequestedAt' = {kwargs['cancelRequestedAt']}"
            )
            body["cancelRequestedAt"] = kwargs.get("cancelRequestedAt")
            del kwargs["cancelRequestedAt"]
        if "cancelRequestedBy" in kwargs:
            hub.log.debug(
                f"Got kwarg 'cancelRequestedBy' = {kwargs['cancelRequestedBy']}"
            )
            body["cancelRequestedBy"] = kwargs.get("cancelRequestedBy")
            del kwargs["cancelRequestedBy"]
        if "flowId" in kwargs:
            hub.log.debug(f"Got kwarg 'flowId' = {kwargs['flowId']}")
            body["flowId"] = kwargs.get("flowId")
            del kwargs["flowId"]
        if "flowExecutionId" in kwargs:
            hub.log.debug(f"Got kwarg 'flowExecutionId' = {kwargs['flowExecutionId']}")
            body["flowExecutionId"] = kwargs.get("flowExecutionId")
            del kwargs["flowExecutionId"]
        if "targetResources" in kwargs:
            hub.log.debug(f"Got kwarg 'targetResources' = {kwargs['targetResources']}")
            body["targetResources"] = kwargs.get("targetResources")
            del kwargs["targetResources"]

        ret = api.create_blueprint_request_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.create_blueprint_request_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_blueprint_request_using_delete(hub, ctx, p_requestId, **kwargs):
    """Deletes request  Performs DELETE /blueprint/api/blueprint-requests/{requestId}


    :param string p_requestId: (required in path)
    :param boolean force: (optional in query) Force delete if not in final stage
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("DELETE /blueprint/api/blueprint-requests/{requestId}")

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.delete_blueprint_request_using_delete(
            request_id=p_requestId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.delete_blueprint_request_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_request_execution_using_get(hub, ctx, p_requestId, **kwargs):
    """Returns request execution details  Performs GET /blueprint/api/blueprint-requests/{requestId}/execution


    :param string p_requestId: (required in path)
    :param boolean expand: (optional in query) Detailed execution
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprint-requests/{requestId}/execution")

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_blueprint_request_execution_using_get(
            request_id=p_requestId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.get_blueprint_request_execution_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_request_using_get(hub, ctx, p_requestId, **kwargs):
    """Returns blueprint request details  Performs GET /blueprint/api/blueprint-requests/{requestId}


    :param string p_requestId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprint-requests/{requestId}")

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_blueprint_request_using_get(request_id=p_requestId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.get_blueprint_request_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_resources_plan_using_get1(hub, ctx, p_requestId, **kwargs):
    """Returns request plan  Performs GET /blueprint/api/blueprint-requests/{requestId}/plan


    :param string p_requestId: (required in path)
    :param boolean expand: (optional in query) Detailed plan
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprint-requests/{requestId}/plan")

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_blueprint_resources_plan_using_get1(
            request_id=p_requestId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.get_blueprint_resources_plan_using_get1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_blueprint_resources_plan_using_get(hub, ctx, p_requestId, **kwargs):
    """Returns request resources plan  Performs GET /blueprint/api/blueprint-requests/{requestId}/resources-plan


    :param string p_requestId: (required in path)
    :param boolean expand: (optional in query) Detailed plan
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug(
            "GET /blueprint/api/blueprint-requests/{requestId}/resources-plan"
        )

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_blueprint_resources_plan_using_get(
            request_id=p_requestId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.get_blueprint_resources_plan_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_blueprint_requests_using_get(hub, ctx, **kwargs):
    """Lists blueprint requests  Performs GET /blueprint/api/blueprint-requests


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param array select: (optional in query) Fields to include in content.
    :param string deploymentId: (optional in query) Deployment Id filter
    :param boolean includePlan: (optional in query) Plan Requests filter
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy.
    """

    try:

        hub.log.debug("GET /blueprint/api/blueprint-requests")

        api = BlueprintRequestsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.list_blueprint_requests_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintRequestsApi.list_blueprint_requests_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
