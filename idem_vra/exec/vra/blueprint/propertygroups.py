from idem_vra.client.vra_blueprint_lib.api import PropertyGroupsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_property_group_using_post(hub, ctx, **kwargs):
    """Creates a property group  Performs POST /properties/api/property-groups


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string type: (optional in body) Property group type
    :param string id: (optional in body) Object ID
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param string name: (optional in body) Property group name
    :param string displayName: (optional in body) Property group display name
    :param string description: (optional in body) Property group description
    :param object properties: (optional in body) Properties
    """

    try:

        hub.log.debug("POST /properties/api/property-groups")

        api = PropertyGroupsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "displayName" in kwargs:
            hub.log.debug(f"Got kwarg 'displayName' = {kwargs['displayName']}")
            body["displayName"] = kwargs.get("displayName")
            del kwargs["displayName"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]

        ret = api.create_property_group_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyGroupsApi.create_property_group_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_property_group_using_delete(hub, ctx, p_propertyGroupId, **kwargs):
    """Deletes a property group  Performs DELETE /properties/api/property-groups/{propertyGroupId}


    :param string p_propertyGroupId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("DELETE /properties/api/property-groups/{propertyGroupId}")

        api = PropertyGroupsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.delete_property_group_using_delete(
            property_group_id=p_propertyGroupId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyGroupsApi.delete_property_group_using_delete: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_property_group_using_get(hub, ctx, p_propertyGroupId, **kwargs):
    """Returns property group details  Performs GET /properties/api/property-groups/{propertyGroupId}


    :param string p_propertyGroupId: (required in path)
    :param array select: (optional in query) Fields to include in content.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    """

    try:

        hub.log.debug("GET /properties/api/property-groups/{propertyGroupId}")

        api = PropertyGroupsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.get_property_group_using_get(
            property_group_id=p_propertyGroupId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyGroupsApi.get_property_group_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_property_groups_using_get(hub, ctx, **kwargs):
    """Lists Property Groups  Performs GET /properties/api/property-groups


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param array select: (optional in query) Fields to include.
    :param string name: (optional in query) Filter by name
    :param string type: (optional in query) Filter by type
    :param string search: (optional in query) Search by name and description
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is descending on updatedAt. Sorting is supported on fields
      createdAt, updatedAt, createdBy, updatedBy, name, type.
    """

    try:

        hub.log.debug("GET /properties/api/property-groups")

        api = PropertyGroupsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        ret = api.list_property_groups_using_get(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyGroupsApi.list_property_groups_using_get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_property_group_using_put(hub, ctx, p_propertyGroupId, **kwargs):
    """Updates a property group  Performs PUT /properties/api/property-groups/{propertyGroupId}


    :param string p_propertyGroupId: (required in path)
    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string type: (optional in body) Property group type
    :param string id: (optional in body) Object ID
    :param string createdAt: (optional in body) Created time
    :param string createdBy: (optional in body) Created by
    :param string updatedAt: (optional in body) Updated time
    :param string updatedBy: (optional in body) Updated by
    :param string orgId: (optional in body) Org ID
    :param string projectId: (optional in body) Project ID
    :param string projectName: (optional in body) Project Name
    :param string name: (optional in body) Property group name
    :param string displayName: (optional in body) Property group display name
    :param string description: (optional in body) Property group description
    :param object properties: (optional in body) Properties
    """

    try:

        hub.log.debug("PUT /properties/api/property-groups/{propertyGroupId}")

        api = PropertyGroupsApi(hub.clients["idem_vra.client.vra_blueprint_lib.api"])
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "updatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedAt' = {kwargs['updatedAt']}")
            body["updatedAt"] = kwargs.get("updatedAt")
            del kwargs["updatedAt"]
        if "updatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedBy' = {kwargs['updatedBy']}")
            body["updatedBy"] = kwargs.get("updatedBy")
            del kwargs["updatedBy"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "projectName" in kwargs:
            hub.log.debug(f"Got kwarg 'projectName' = {kwargs['projectName']}")
            body["projectName"] = kwargs.get("projectName")
            del kwargs["projectName"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "displayName" in kwargs:
            hub.log.debug(f"Got kwarg 'displayName' = {kwargs['displayName']}")
            body["displayName"] = kwargs.get("displayName")
            del kwargs["displayName"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]

        ret = api.update_property_group_using_put(
            body, property_group_id=p_propertyGroupId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PropertyGroupsApi.update_property_group_using_put: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
