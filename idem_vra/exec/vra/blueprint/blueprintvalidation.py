from idem_vra.client.vra_blueprint_lib.api import BlueprintValidationApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def validate_blueprint_using_post(hub, ctx, **kwargs):
    """Validates a blueprint  Performs POST /blueprint/api/blueprint-validation


    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about
    :param string blueprintId: (optional in body) Blueprint Id
    :param string blueprintVersion: (optional in body) Blueprint Version
    :param string content: (optional in body) Blueprint YAML content
    :param string projectId: (optional in body) Project Id
    :param object inputs: (optional in body) Blueprint request inputs
    """

    try:

        hub.log.debug("POST /blueprint/api/blueprint-validation")

        api = BlueprintValidationApi(
            hub.clients["idem_vra.client.vra_blueprint_lib.api"]
        )
        if "api_version" not in kwargs:
            kwargs["api_version"] = "2019-09-12"

        body = {}

        if "blueprintId" in kwargs:
            hub.log.debug(f"Got kwarg 'blueprintId' = {kwargs['blueprintId']}")
            body["blueprintId"] = kwargs.get("blueprintId")
            del kwargs["blueprintId"]
        if "blueprintVersion" in kwargs:
            hub.log.debug(
                f"Got kwarg 'blueprintVersion' = {kwargs['blueprintVersion']}"
            )
            body["blueprintVersion"] = kwargs.get("blueprintVersion")
            del kwargs["blueprintVersion"]
        if "content" in kwargs:
            hub.log.debug(f"Got kwarg 'content' = {kwargs['content']}")
            body["content"] = kwargs.get("content")
            del kwargs["content"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]

        ret = api.validate_blueprint_using_post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BlueprintValidationApi.validate_blueprint_using_post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
