from idem_vra.client.vra_cmx_lib.api import NamespacesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create3(hub, ctx, **kwargs):
    """Create a K8S Namespace Create a K8S Namespace Performs POST /cmx/api/resources/k8s/namespaces


    :param string authCredentialsLink: (optional in body)
    :param string clusterId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string externalLink: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param boolean persisted: (optional in body)
    :param string projectId: (optional in body)
    :param boolean registered: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string zoneProjectAssignmentId: (optional in body)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/k8s/namespaces")

        api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "authCredentialsLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
            )
            body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
            del kwargs["authCredentialsLink"]
        if "clusterId" in kwargs:
            hub.log.debug(f"Got kwarg 'clusterId' = {kwargs['clusterId']}")
            body["clusterId"] = kwargs.get("clusterId")
            del kwargs["clusterId"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "externalLink" in kwargs:
            hub.log.debug(f"Got kwarg 'externalLink' = {kwargs['externalLink']}")
            body["externalLink"] = kwargs.get("externalLink")
            del kwargs["externalLink"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "installerId" in kwargs:
            hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
            body["installerId"] = kwargs.get("installerId")
            del kwargs["installerId"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "owner" in kwargs:
            hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
            body["owner"] = kwargs.get("owner")
            del kwargs["owner"]
        if "persisted" in kwargs:
            hub.log.debug(f"Got kwarg 'persisted' = {kwargs['persisted']}")
            body["persisted"] = kwargs.get("persisted")
            del kwargs["persisted"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "registered" in kwargs:
            hub.log.debug(f"Got kwarg 'registered' = {kwargs['registered']}")
            body["registered"] = kwargs.get("registered")
            del kwargs["registered"]
        if "shared" in kwargs:
            hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
            body["shared"] = kwargs.get("shared")
            del kwargs["shared"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "updatedMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
            body["updatedMillis"] = kwargs.get("updatedMillis")
            del kwargs["updatedMillis"]
        if "zoneProjectAssignmentId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'zoneProjectAssignmentId' = {kwargs['zoneProjectAssignmentId']}"
            )
            body["zoneProjectAssignmentId"] = kwargs.get("zoneProjectAssignmentId")
            del kwargs["zoneProjectAssignmentId"]

        ret = api.create3(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking NamespacesApi.create3: {err}")
        return ExecReturn(result=False, comment=str(err))


async def delete1(hub, ctx, p_id, **kwargs):
    """Delete a K8S Namespace Delete a K8S Namespace by id Performs DELETE /cmx/api/resources/k8s/namespaces/{id}


    :param string p_id: (required in path)
    :param boolean destroy: (optional in query)
    """

    try:

        hub.log.debug("DELETE /cmx/api/resources/k8s/namespaces/{id}")

        api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.delete1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking NamespacesApi.delete1: {err}")
        return ExecReturn(result=False, comment=str(err))


async def get2(hub, ctx, p_id, **kwargs):
    """Get a K8S Namespace by id Get a K8S Namespace by id Performs GET /cmx/api/resources/k8s/namespaces/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/k8s/namespaces/{id}")

        api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get2(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking NamespacesApi.get2: {err}")
        return ExecReturn(result=False, comment=str(err))


async def get_kube_config(hub, ctx, p_id, **kwargs):
    """Get KubeConfig for a K8S Namespace Get KubeConfig for a K8S Namespace by providing a K8S Namespace id Performs GET /cmx/api/resources/k8s/namespaces/{id}/kube-config


    :param string p_id: (required in path)
    :param boolean ignoreTMC: (optional in query)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/k8s/namespaces/{id}/kube-config")

        api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_kube_config(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NamespacesApi.get_kube_config: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list6(hub, ctx, **kwargs):
    """Get All K8S Namespaces Get a list of all K8S Namespaces Performs GET /cmx/api/resources/k8s/namespaces


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/k8s/namespaces")

        api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.list6(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking NamespacesApi.list6: {err}")
        return ExecReturn(result=False, comment=str(err))


async def register2(hub, ctx, p_id, **kwargs):
    """Onboard existing K8S Namespace Onboard existing K8S Namespace by providing namespace id and entity Performs PUT /cmx/api/resources/k8s/namespaces/{id}/register


    :param string p_id: (required in path)
    :param string authCredentialsLink: (optional in body)
    :param string clusterId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string externalLink: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param boolean persisted: (optional in body)
    :param string projectId: (optional in body)
    :param boolean registered: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string zoneProjectAssignmentId: (optional in body)
    """

    try:

        hub.log.debug("PUT /cmx/api/resources/k8s/namespaces/{id}/register")

        api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "authCredentialsLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
            )
            body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
            del kwargs["authCredentialsLink"]
        if "clusterId" in kwargs:
            hub.log.debug(f"Got kwarg 'clusterId' = {kwargs['clusterId']}")
            body["clusterId"] = kwargs.get("clusterId")
            del kwargs["clusterId"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "externalLink" in kwargs:
            hub.log.debug(f"Got kwarg 'externalLink' = {kwargs['externalLink']}")
            body["externalLink"] = kwargs.get("externalLink")
            del kwargs["externalLink"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "installerId" in kwargs:
            hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
            body["installerId"] = kwargs.get("installerId")
            del kwargs["installerId"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "owner" in kwargs:
            hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
            body["owner"] = kwargs.get("owner")
            del kwargs["owner"]
        if "persisted" in kwargs:
            hub.log.debug(f"Got kwarg 'persisted' = {kwargs['persisted']}")
            body["persisted"] = kwargs.get("persisted")
            del kwargs["persisted"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "registered" in kwargs:
            hub.log.debug(f"Got kwarg 'registered' = {kwargs['registered']}")
            body["registered"] = kwargs.get("registered")
            del kwargs["registered"]
        if "shared" in kwargs:
            hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
            body["shared"] = kwargs.get("shared")
            del kwargs["shared"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "updatedMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
            body["updatedMillis"] = kwargs.get("updatedMillis")
            del kwargs["updatedMillis"]
        if "zoneProjectAssignmentId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'zoneProjectAssignmentId' = {kwargs['zoneProjectAssignmentId']}"
            )
            body["zoneProjectAssignmentId"] = kwargs.get("zoneProjectAssignmentId")
            del kwargs["zoneProjectAssignmentId"]

        ret = api.register2(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NamespacesApi.register2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update(hub, ctx, p_id, **kwargs):
    """Update a K8S Namespace description or project Update a K8S Namespace description or project by providing namespace id and
      Namespace entity Performs PUT /cmx/api/resources/k8s/namespaces/{id}


    :param string p_id: (required in path)
    :param string authCredentialsLink: (optional in body)
    :param string clusterId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object customProperties: (optional in body)
    :param string description: (optional in body)
    :param string externalLink: (optional in body)
    :param string id: (optional in body)
    :param string installerId: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string owner: (optional in body)
    :param boolean persisted: (optional in body)
    :param string projectId: (optional in body)
    :param boolean registered: (optional in body)
    :param boolean shared: (optional in body)
    :param string status: (optional in body)
    :param integer updatedMillis: (optional in body)
    :param string zoneProjectAssignmentId: (optional in body)
    """

    try:

        hub.log.debug("PUT /cmx/api/resources/k8s/namespaces/{id}")

        api = NamespacesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "authCredentialsLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'authCredentialsLink' = {kwargs['authCredentialsLink']}"
            )
            body["authCredentialsLink"] = kwargs.get("authCredentialsLink")
            del kwargs["authCredentialsLink"]
        if "clusterId" in kwargs:
            hub.log.debug(f"Got kwarg 'clusterId' = {kwargs['clusterId']}")
            body["clusterId"] = kwargs.get("clusterId")
            del kwargs["clusterId"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "externalLink" in kwargs:
            hub.log.debug(f"Got kwarg 'externalLink' = {kwargs['externalLink']}")
            body["externalLink"] = kwargs.get("externalLink")
            del kwargs["externalLink"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "installerId" in kwargs:
            hub.log.debug(f"Got kwarg 'installerId' = {kwargs['installerId']}")
            body["installerId"] = kwargs.get("installerId")
            del kwargs["installerId"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "owner" in kwargs:
            hub.log.debug(f"Got kwarg 'owner' = {kwargs['owner']}")
            body["owner"] = kwargs.get("owner")
            del kwargs["owner"]
        if "persisted" in kwargs:
            hub.log.debug(f"Got kwarg 'persisted' = {kwargs['persisted']}")
            body["persisted"] = kwargs.get("persisted")
            del kwargs["persisted"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "registered" in kwargs:
            hub.log.debug(f"Got kwarg 'registered' = {kwargs['registered']}")
            body["registered"] = kwargs.get("registered")
            del kwargs["registered"]
        if "shared" in kwargs:
            hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
            body["shared"] = kwargs.get("shared")
            del kwargs["shared"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "updatedMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
            body["updatedMillis"] = kwargs.get("updatedMillis")
            del kwargs["updatedMillis"]
        if "zoneProjectAssignmentId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'zoneProjectAssignmentId' = {kwargs['zoneProjectAssignmentId']}"
            )
            body["zoneProjectAssignmentId"] = kwargs.get("zoneProjectAssignmentId")
            del kwargs["zoneProjectAssignmentId"]

        ret = api.update(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking NamespacesApi.update: {err}")
        return ExecReturn(result=False, comment=str(err))
