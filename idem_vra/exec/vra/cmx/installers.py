from idem_vra.client.vra_cmx_lib.api import InstallersApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_installer(hub, ctx, p_id, **kwargs):
    """Find a Kubernetes Installer by id Retrieve a K8S Installer by id Performs GET /cmx/api/resources/installers/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/installers/{id}")

        api = InstallersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_installer(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking InstallersApi.get_installer: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_kubernetes_properties(hub, ctx, p_id, **kwargs):
    """Find installation properties by installer id Retrieve installation properties by id Performs GET /cmx/api/resources/installers/{id}/properties


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/installers/{id}/properties")

        api = InstallersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_kubernetes_properties(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking InstallersApi.get_kubernetes_properties: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def install(hub, ctx, p_id, **kwargs):
    """Install a K8S Installer Install a K8S Installer Performs POST /cmx/api/resources/installers/{id}/install


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/installers/{id}/install")

        api = InstallersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.install(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking InstallersApi.install: {err}")
        return ExecReturn(result=False, comment=str(err))


async def uninstall(hub, ctx, p_id, **kwargs):
    """Uninstall a K8S Installer Uninstall a K8S Installer Performs POST /cmx/api/resources/installers/{id}/uninstall


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/installers/{id}/uninstall")

        api = InstallersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.uninstall(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking InstallersApi.uninstall: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
