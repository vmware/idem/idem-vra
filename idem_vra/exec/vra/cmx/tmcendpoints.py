from idem_vra.client.vra_cmx_lib.api import TMCEndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_request_tracker(hub, ctx, **kwargs):
    """Create a request tracker for endpoint creation Create a request tracker for a TMC endpoint by providing the endpoint
      configuration Performs POST /cmx/api/resources/tmc/endpoints/request-tracker


    :param Any customProperties: (optional in body)
    :param Any endpointProperties: (optional in body)
    :param string endpointType: (optional in body)
    :param string name: (optional in body)
    :param string requestType: (optional in body)
    :param string resourceLink: (optional in body)
    :param string resourceReference: (optional in body)
    :param string taskLink: (optional in body)
    :param string taskReference: (optional in body)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/tmc/endpoints/request-tracker")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "customProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'customProperties' = {kwargs['customProperties']}"
            )
            body["customProperties"] = kwargs.get("customProperties")
            del kwargs["customProperties"]
        if "endpointProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'endpointProperties' = {kwargs['endpointProperties']}"
            )
            body["endpointProperties"] = kwargs.get("endpointProperties")
            del kwargs["endpointProperties"]
        if "endpointType" in kwargs:
            hub.log.debug(f"Got kwarg 'endpointType' = {kwargs['endpointType']}")
            body["endpointType"] = kwargs.get("endpointType")
            del kwargs["endpointType"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "requestType" in kwargs:
            hub.log.debug(f"Got kwarg 'requestType' = {kwargs['requestType']}")
            body["requestType"] = kwargs.get("requestType")
            del kwargs["requestType"]
        if "resourceLink" in kwargs:
            hub.log.debug(f"Got kwarg 'resourceLink' = {kwargs['resourceLink']}")
            body["resourceLink"] = kwargs.get("resourceLink")
            del kwargs["resourceLink"]
        if "resourceReference" in kwargs:
            hub.log.debug(
                f"Got kwarg 'resourceReference' = {kwargs['resourceReference']}"
            )
            body["resourceReference"] = kwargs.get("resourceReference")
            del kwargs["resourceReference"]
        if "taskLink" in kwargs:
            hub.log.debug(f"Got kwarg 'taskLink' = {kwargs['taskLink']}")
            body["taskLink"] = kwargs.get("taskLink")
            del kwargs["taskLink"]
        if "taskReference" in kwargs:
            hub.log.debug(f"Got kwarg 'taskReference' = {kwargs['taskReference']}")
            body["taskReference"] = kwargs.get("taskReference")
            del kwargs["taskReference"]

        ret = api.create_request_tracker(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TMCEndpointsApi.create_request_tracker: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create(hub, ctx, **kwargs):
    """Performs POST /cmx/api/resources/tmc/endpoints


    :param string apiEndpoint: (optional in body)
    :param string apiToken: (optional in body)
    :param string defaultClusterGroupName: (optional in body)
    :param string defaultWorkspaceName: (optional in body)
    :param string description: (optional in body)
    :param string documentSelfLink: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/tmc/endpoints")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "apiEndpoint" in kwargs:
            hub.log.debug(f"Got kwarg 'apiEndpoint' = {kwargs['apiEndpoint']}")
            body["apiEndpoint"] = kwargs.get("apiEndpoint")
            del kwargs["apiEndpoint"]
        if "apiToken" in kwargs:
            hub.log.debug(f"Got kwarg 'apiToken' = {kwargs['apiToken']}")
            body["apiToken"] = kwargs.get("apiToken")
            del kwargs["apiToken"]
        if "defaultClusterGroupName" in kwargs:
            hub.log.debug(
                f"Got kwarg 'defaultClusterGroupName' = {kwargs['defaultClusterGroupName']}"
            )
            body["defaultClusterGroupName"] = kwargs.get("defaultClusterGroupName")
            del kwargs["defaultClusterGroupName"]
        if "defaultWorkspaceName" in kwargs:
            hub.log.debug(
                f"Got kwarg 'defaultWorkspaceName' = {kwargs['defaultWorkspaceName']}"
            )
            body["defaultWorkspaceName"] = kwargs.get("defaultWorkspaceName")
            del kwargs["defaultWorkspaceName"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "documentSelfLink" in kwargs:
            hub.log.debug(
                f"Got kwarg 'documentSelfLink' = {kwargs['documentSelfLink']}"
            )
            body["documentSelfLink"] = kwargs.get("documentSelfLink")
            del kwargs["documentSelfLink"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]

        ret = api.create(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TMCEndpointsApi.create: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_cluster_groups_by_endpoint(hub, ctx, p_id, **kwargs):
    """Get all cluster groups for TMC endpoint Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting
      with the provided cluster group name. If cluster group name is not provided,
      then all cluster groups are returned Performs GET /cmx/api/resources/tmc/endpoints/{id}/clustergroups


    :param string p_id: (required in path)
    :param Any searchScope.name: (optional in query) Scope search to the specified name supports globbing default ().
    :param Any sortBy: (optional in query) Sort Order.
    :param Any query: (optional in query) TQL query string.
    :param Any pagination.offset: (optional in query) Offset at which to start returning records.
    :param Any pagination.size: (optional in query) Number of records to return.
    :param Any includeTotalCount: (optional in query) Include total count.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/tmc/endpoints/{id}/clustergroups")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_cluster_groups_by_endpoint(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TMCEndpointsApi.get_cluster_groups_by_endpoint: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_cluster_groups_by_request_tracker(hub, ctx, q_requestTrackerId, **kwargs):
    """Get all cluster groups for TMC endpoint Get cluster groups for a TMC endpoint by provided TMC endpoint id and starting
      with the provided cluster group name. If cluster group name is not provided,
      then all cluster groups are returned Performs GET /cmx/api/resources/tmc/endpoints/clustergroups


    :param Any q_requestTrackerId: (required in query)
    :param Any searchScope.name: (optional in query) Scope search to the specified name supports globbing default ().
    :param Any sortBy: (optional in query) Sort Order.
    :param Any query: (optional in query) TQL query string.
    :param Any pagination.offset: (optional in query) Offset at which to start returning records.
    :param Any pagination.size: (optional in query) Number of records to return.
    :param Any includeTotalCount: (optional in query) Include total count.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/tmc/endpoints/clustergroups")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_cluster_groups_by_request_tracker(
            request_tracker_id=q_requestTrackerId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TMCEndpointsApi.get_cluster_groups_by_request_tracker: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_tmc_cli_binaries(hub, ctx, **kwargs):
    """Get Tmc CLI binaries Get Tanzu CLI binaries for Mac, Linux, Windows Performs GET /cmx/api/resources/tmc/endpoints/tanzu-cli-binaries"""

    try:

        hub.log.debug("GET /cmx/api/resources/tmc/endpoints/tanzu-cli-binaries")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_tmc_cli_binaries(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TMCEndpointsApi.get_tmc_cli_binaries: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_workspaces_by_endpoint(hub, ctx, p_id, **kwargs):
    """Get all workspace names list for TMC endpoint Get workspace names list for a TMC endpoint by provided TMC endpoint id and
      starting with the default workspace name. If workspace name is not provided,
      then all workspaces are returned Performs GET /cmx/api/resources/tmc/endpoints/{id}/workspaces


    :param string p_id: (required in path)
    :param Any searchScope.name: (optional in query) Scope search to the specified name supports globbing default ().
    :param Any sortBy: (optional in query) Sort Order.
    :param Any query: (optional in query) TQL query string.
    :param Any pagination.offset: (optional in query) Offset at which to start returning records.
    :param Any pagination.size: (optional in query) Number of records to return.
    :param Any includeTotalCount: (optional in query) Include total count.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/tmc/endpoints/{id}/workspaces")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_workspaces_by_endpoint(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TMCEndpointsApi.get_workspaces_by_endpoint: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_workspaces_by_request_tracker(hub, ctx, q_requestTrackerId, **kwargs):
    """Get all workspace names list for TMC endpoint Get workspace names list for a TMC endpoint by provided TMC endpoint id and
      starting with the default workspace name. If workspace name is not provided,
      then all workspaces are returned Performs GET /cmx/api/resources/tmc/endpoints/workspaces


    :param Any q_requestTrackerId: (required in query)
    :param Any searchScope.name: (optional in query) Scope search to the specified name supports globbing default ().
    :param Any sortBy: (optional in query) Sort Order.
    :param Any query: (optional in query) TQL query string.
    :param Any pagination.offset: (optional in query) Offset at which to start returning records.
    :param Any pagination.size: (optional in query) Number of records to return.
    :param Any includeTotalCount: (optional in query) Include total count.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/tmc/endpoints/workspaces")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_workspaces_by_request_tracker(
            request_tracker_id=q_requestTrackerId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking TMCEndpointsApi.get_workspaces_by_request_tracker: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list(hub, ctx, q_pageable, **kwargs):
    """Performs GET /cmx/api/resources/tmc/endpoints


    :param None q_pageable: (required in query)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/tmc/endpoints")

        api = TMCEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.list(pageable=q_pageable, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking TMCEndpointsApi.list: {err}")
        return ExecReturn(result=False, comment=str(err))
