from idem_vra.client.vra_cmx_lib.api import VCenterEndpointsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_storage_classes1(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get all storage classes identifiers for a vCenter endpoint Get all storage classes defined in all managed supervisor clusters in a
      particular vCenter instance. vCenter instance is identified by endpoint
      SelfLink Id Performs GET /cmx/api/resources/vcenter/endpoints/{endpointSelfLinkId}/storage-classes


    :param string p_endpointSelfLinkId: (required in path)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/vcenter/endpoints/{endpointSelfLinkId}/storage-classes"
        )

        api = VCenterEndpointsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_storage_classes1(
            endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking VCenterEndpointsApi.get_storage_classes1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
