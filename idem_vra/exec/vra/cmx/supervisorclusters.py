from idem_vra.client.vra_cmx_lib.api import SupervisorClustersApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def existing_tanzu_clusters(hub, ctx, p_selfLinkId, **kwargs):
    """Get all existing tanzu kubernetes clusters on a supervisor cluster. Get all existing tanzu kubernetes clusters on a supervisor cluster. Performs GET /cmx/api/resources/supervisor-clusters/{selfLinkId}/tanzu-clusters-namespace


    :param string p_selfLinkId: (required in path)
    :param string searchTerm: (optional in query)
    :param string registered: (optional in query)
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/supervisor-clusters/{selfLinkId}/tanzu-clusters-namespace"
        )

        api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.existing_tanzu_clusters(self_link_id=p_selfLinkId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SupervisorClustersApi.existing_tanzu_clusters: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_cluster1(hub, ctx, p_endpointSelfLinkId, p_moref, **kwargs):
    """Find a Supervisor Cluster by vSphere moref and vSphere endpoint id Retrieve a Supervisor Cluster by vSphere moref and id from the endpoint self
      link of the vSphere endpoint this cluster is associated to Performs GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}/cluster/{moref}


    :param string p_endpointSelfLinkId: (required in path)
    :param string p_moref: (required in path)
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}/cluster/{moref}"
        )

        api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_cluster1(
            endpoint_self_link_id=p_endpointSelfLinkId, moref=p_moref, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SupervisorClustersApi.get_cluster1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_cluster(hub, ctx, p_clusterSelfLinkId, **kwargs):
    """Find a Supervisor Cluster by the id from documentSelfLink Retrieve a Supervisor Cluster by id from documentSelfLink Performs GET /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}


    :param string p_clusterSelfLinkId: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}")

        api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get_cluster(cluster_self_link_id=p_clusterSelfLinkId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SupervisorClustersApi.get_cluster: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list2(hub, ctx, **kwargs):
    """Get all managed Supervisor Clusters Get all managed Supervisor Clusters Performs GET /cmx/api/resources/supervisor-clusters


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/supervisor-clusters")

        api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.list2(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SupervisorClustersApi.list2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_clusters_on_endpoint(hub, ctx, p_endpointSelfLinkId, **kwargs):
    """Get all Supervisor Clusters on a vSphere endpoint Get all Supervisor Clusters on a vSphere endpoint by provided id from the
      endpoint self link Performs GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}


    :param string p_endpointSelfLinkId: (required in path)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string registered: (optional in query)
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug(
            "GET /cmx/api/resources/supervisor-clusters/endpoint/{endpointSelfLinkId}"
        )

        api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.list_clusters_on_endpoint(
            endpoint_self_link_id=p_endpointSelfLinkId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SupervisorClustersApi.list_clusters_on_endpoint: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def register1(hub, ctx, p_clusterSelfLinkId, **kwargs):
    """Make a Supervisor Cluster a managed entity A valid document self link id shall be provided. Performs PUT /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}/register


    :param string p_clusterSelfLinkId: (required in path)
    """

    try:

        hub.log.debug(
            "PUT /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}/register"
        )

        api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.register1(cluster_self_link_id=p_clusterSelfLinkId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SupervisorClustersApi.register1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def unregister(hub, ctx, p_clusterSelfLinkId, **kwargs):
    """Make a Supervisor Cluster an unmanaged entity A valid document self link id shall be provided. Performs DELETE /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}


    :param string p_clusterSelfLinkId: (required in path)
    """

    try:

        hub.log.debug(
            "DELETE /cmx/api/resources/supervisor-clusters/{clusterSelfLinkId}"
        )

        api = SupervisorClustersApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.unregister(cluster_self_link_id=p_clusterSelfLinkId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking SupervisorClustersApi.unregister: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
