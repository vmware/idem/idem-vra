from idem_vra.client.vra_cmx_lib.api import CapabilityTagsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def search_tags(hub, ctx, **kwargs):
    """Search for capability tags Search for capability tags using an OData filter. For example, use the
      following filter to retrieve the tag location:somewhere: $filter=(key eq
      location) Performs GET /cmx/api/resources/tags


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/tags")

        api = CapabilityTagsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.search_tags(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CapabilityTagsApi.search_tags: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
