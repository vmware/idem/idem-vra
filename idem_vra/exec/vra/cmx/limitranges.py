from idem_vra.client.vra_cmx_lib.api import LimitRangesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get1(hub, ctx, p_id, **kwargs):
    """Get a K8S LimitRange by id Get a K8S LimitRange by id Performs GET /cmx/api/resources/limit-ranges/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/limit-ranges/{id}")

        api = LimitRangesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking LimitRangesApi.get1: {err}")
        return ExecReturn(result=False, comment=str(err))


async def list5(hub, ctx, **kwargs):
    """Get All K8S LimitRanges Get a list of all K8S LimitRanges Performs GET /cmx/api/resources/limit-ranges


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/limit-ranges")

        api = LimitRangesApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.list5(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking LimitRangesApi.list5: {err}")
        return ExecReturn(result=False, comment=str(err))
