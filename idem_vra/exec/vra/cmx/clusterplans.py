from idem_vra.client.vra_cmx_lib.api import ClusterPlansApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create4(hub, ctx, **kwargs):
    """Create a cluster plan The body shall contain a cluster plan entity. Performs POST /cmx/api/resources/cluster-plans


    :param string cloudAccountSelfLinkId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object definition: (optional in body) The definition varies depending on the type of cluster plan. Example
      shown below is for cluster plans of type TANZU_CLUSTER_PLAN. In that
      case the definition is equivalent to the spec of a Tanzu Kubernetes
      cluster in JSON format. Here is a documentation
      https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-
      tanzu/GUID-B1034373-8C38-4FE2-9517-345BF7271A1E.html
      example: {"spec":{"distribution":{"version":"1.20"},"topology":{"contr
      olPlane":{"count":1,"class":"best-effort-xsmall","storageClass":"vsan-
      default-storage-policy"},"workers":{"count":1,"class":"best-effort-
      xsmall","storageClass":"vsan-default-storage-
      policy"}},"settings":{"storage":{"defaultClass":"","classes":[]}}}}
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string type: (optional in body)
    :param integer updatedMillis: (optional in body)
    """

    try:

        hub.log.debug("POST /cmx/api/resources/cluster-plans")

        api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "cloudAccountSelfLinkId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'cloudAccountSelfLinkId' = {kwargs['cloudAccountSelfLinkId']}"
            )
            body["cloudAccountSelfLinkId"] = kwargs.get("cloudAccountSelfLinkId")
            del kwargs["cloudAccountSelfLinkId"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "definition" in kwargs:
            hub.log.debug(f"Got kwarg 'definition' = {kwargs['definition']}")
            body["definition"] = kwargs.get("definition")
            del kwargs["definition"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "updatedMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
            body["updatedMillis"] = kwargs.get("updatedMillis")
            del kwargs["updatedMillis"]

        ret = api.create4(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ClusterPlansApi.create4: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete2(hub, ctx, p_id, **kwargs):
    """Delete a Cluster Plan by Id Delete a Cluster Plan identified by id string Performs DELETE /cmx/api/resources/cluster-plans/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("DELETE /cmx/api/resources/cluster-plans/{id}")

        api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.delete2(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ClusterPlansApi.delete2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get3(hub, ctx, p_id, **kwargs):
    """Get a Cluster Plan by Id Get a Cluster Plan by Id Performs GET /cmx/api/resources/cluster-plans/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /cmx/api/resources/cluster-plans/{id}")

        api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.get3(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ClusterPlansApi.get3: {err}")
        return ExecReturn(result=False, comment=str(err))


async def search_cluster_plan_aggregations(hub, ctx, q_groupBy, **kwargs):
    """Search for cluster plan aggregations Search for cluster plan aggregations by name and cloud account document self
      link id. Performs GET /cmx/api/resources/cluster-plans/aggregation


    :param string q_groupBy: (required in query)
    :param string name: (optional in query)
    :param string cloudAccountSelfLinkId: (optional in query)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/cluster-plans/aggregation")

        api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.search_cluster_plan_aggregations(group_by=q_groupBy, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ClusterPlansApi.search_cluster_plan_aggregations: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def search_cluster_plans(hub, ctx, **kwargs):
    """Search a cluster plan instance Search a cluster plan by name and cloud account document self link id. Performs GET /cmx/api/resources/cluster-plans


    :param string name: (optional in query)
    :param string cloudAccountSelfLinkId: (optional in query)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending.
    """

    try:

        hub.log.debug("GET /cmx/api/resources/cluster-plans")

        api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        ret = api.search_cluster_plans(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ClusterPlansApi.search_cluster_plans: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update2(hub, ctx, p_id, **kwargs):
    """Update a cluster plan The body shall contains the cluster plan mutation entity. Performs PUT /cmx/api/resources/cluster-plans/{id}


    :param string p_id: (required in path)
    :param string cloudAccountSelfLinkId: (optional in body)
    :param integer createdMillis: (optional in body)
    :param object definition: (optional in body) The definition varies depending on the type of cluster plan. Example
      shown below is for cluster plans of type TANZU_CLUSTER_PLAN. In that
      case the definition is equivalent to the spec of a Tanzu Kubernetes
      cluster in JSON format. Here is a documentation
      https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-
      tanzu/GUID-B1034373-8C38-4FE2-9517-345BF7271A1E.html
      example: {"spec":{"distribution":{"version":"1.20"},"topology":{"contr
      olPlane":{"count":1,"class":"best-effort-xsmall","storageClass":"vsan-
      default-storage-policy"},"workers":{"count":1,"class":"best-effort-
      xsmall","storageClass":"vsan-default-storage-
      policy"}},"settings":{"storage":{"defaultClass":"","classes":[]}}}}
    :param string description: (optional in body)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string orgId: (optional in body)
    :param string type: (optional in body)
    :param integer updatedMillis: (optional in body)
    """

    try:

        hub.log.debug("PUT /cmx/api/resources/cluster-plans/{id}")

        api = ClusterPlansApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "cloudAccountSelfLinkId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'cloudAccountSelfLinkId' = {kwargs['cloudAccountSelfLinkId']}"
            )
            body["cloudAccountSelfLinkId"] = kwargs.get("cloudAccountSelfLinkId")
            del kwargs["cloudAccountSelfLinkId"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "definition" in kwargs:
            hub.log.debug(f"Got kwarg 'definition' = {kwargs['definition']}")
            body["definition"] = kwargs.get("definition")
            del kwargs["definition"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "updatedMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'updatedMillis' = {kwargs['updatedMillis']}")
            body["updatedMillis"] = kwargs.get("updatedMillis")
            del kwargs["updatedMillis"]

        ret = api.update2(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ClusterPlansApi.update2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
