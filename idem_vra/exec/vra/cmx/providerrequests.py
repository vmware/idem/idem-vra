from idem_vra.client.vra_cmx_lib.api import ProviderRequestsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def patch(hub, ctx, **kwargs):
    """Handles unfinished requests Handles unfinished requests that are stopped because of service shutdown or a
      service outage. Performs PATCH /cmx/api/requests/blueprint/blueprint-provider-request


    :param string actionName: (optional in body)
    :param string id: (optional in body)
    :param string operation: (optional in body)
    :param string requestId: (optional in body)
    :param string resourceType: (optional in body)
    """

    try:

        hub.log.debug("PATCH /cmx/api/requests/blueprint/blueprint-provider-request")

        api = ProviderRequestsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "actionName" in kwargs:
            hub.log.debug(f"Got kwarg 'actionName' = {kwargs['actionName']}")
            body["actionName"] = kwargs.get("actionName")
            del kwargs["actionName"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "operation" in kwargs:
            hub.log.debug(f"Got kwarg 'operation' = {kwargs['operation']}")
            body["operation"] = kwargs.get("operation")
            del kwargs["operation"]
        if "requestId" in kwargs:
            hub.log.debug(f"Got kwarg 'requestId' = {kwargs['requestId']}")
            body["requestId"] = kwargs.get("requestId")
            del kwargs["requestId"]
        if "resourceType" in kwargs:
            hub.log.debug(f"Got kwarg 'resourceType' = {kwargs['resourceType']}")
            body["resourceType"] = kwargs.get("resourceType")
            del kwargs["resourceType"]

        ret = api.patch(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProviderRequestsApi.patch: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def post(hub, ctx, **kwargs):
    """Handle blueprint resources requests Handles blueprint requests for resource operations - allocation, provisioning,
      removal Performs POST /cmx/api/requests/blueprint/blueprint-provider-request


    :param object actionInputProperties: (optional in body)
    :param string actionName: (optional in body)
    :param object allocationResource: (optional in body)
    :param string callbackUrl: (optional in body)
    :param string deploymentId: (optional in body)
    :param string deploymentName: (optional in body)
    :param string operation: (optional in body)
    :param integer operationTimeoutSeconds: (optional in body)
    :param string projectId: (optional in body)
    :param string requestId: (optional in body)
    :param string resourceLink: (optional in body)
    :param object resourceModifiedProperties: (optional in body)
    :param string resourceName: (optional in body)
    :param object resourceProperties: (optional in body)
    :param string resourceRequestId: (optional in body)
    :param string resourceType: (optional in body)
    :param array tenantLinks: (optional in body)
    """

    try:

        hub.log.debug("POST /cmx/api/requests/blueprint/blueprint-provider-request")

        api = ProviderRequestsApi(hub.clients["idem_vra.client.vra_cmx_lib.api"])

        body = {}

        if "actionInputProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'actionInputProperties' = {kwargs['actionInputProperties']}"
            )
            body["actionInputProperties"] = kwargs.get("actionInputProperties")
            del kwargs["actionInputProperties"]
        if "actionName" in kwargs:
            hub.log.debug(f"Got kwarg 'actionName' = {kwargs['actionName']}")
            body["actionName"] = kwargs.get("actionName")
            del kwargs["actionName"]
        if "allocationResource" in kwargs:
            hub.log.debug(
                f"Got kwarg 'allocationResource' = {kwargs['allocationResource']}"
            )
            body["allocationResource"] = kwargs.get("allocationResource")
            del kwargs["allocationResource"]
        if "callbackUrl" in kwargs:
            hub.log.debug(f"Got kwarg 'callbackUrl' = {kwargs['callbackUrl']}")
            body["callbackUrl"] = kwargs.get("callbackUrl")
            del kwargs["callbackUrl"]
        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "deploymentName" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentName' = {kwargs['deploymentName']}")
            body["deploymentName"] = kwargs.get("deploymentName")
            del kwargs["deploymentName"]
        if "operation" in kwargs:
            hub.log.debug(f"Got kwarg 'operation' = {kwargs['operation']}")
            body["operation"] = kwargs.get("operation")
            del kwargs["operation"]
        if "operationTimeoutSeconds" in kwargs:
            hub.log.debug(
                f"Got kwarg 'operationTimeoutSeconds' = {kwargs['operationTimeoutSeconds']}"
            )
            body["operationTimeoutSeconds"] = kwargs.get("operationTimeoutSeconds")
            del kwargs["operationTimeoutSeconds"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "requestId" in kwargs:
            hub.log.debug(f"Got kwarg 'requestId' = {kwargs['requestId']}")
            body["requestId"] = kwargs.get("requestId")
            del kwargs["requestId"]
        if "resourceLink" in kwargs:
            hub.log.debug(f"Got kwarg 'resourceLink' = {kwargs['resourceLink']}")
            body["resourceLink"] = kwargs.get("resourceLink")
            del kwargs["resourceLink"]
        if "resourceModifiedProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'resourceModifiedProperties' = {kwargs['resourceModifiedProperties']}"
            )
            body["resourceModifiedProperties"] = kwargs.get(
                "resourceModifiedProperties"
            )
            del kwargs["resourceModifiedProperties"]
        if "resourceName" in kwargs:
            hub.log.debug(f"Got kwarg 'resourceName' = {kwargs['resourceName']}")
            body["resourceName"] = kwargs.get("resourceName")
            del kwargs["resourceName"]
        if "resourceProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'resourceProperties' = {kwargs['resourceProperties']}"
            )
            body["resourceProperties"] = kwargs.get("resourceProperties")
            del kwargs["resourceProperties"]
        if "resourceRequestId" in kwargs:
            hub.log.debug(
                f"Got kwarg 'resourceRequestId' = {kwargs['resourceRequestId']}"
            )
            body["resourceRequestId"] = kwargs.get("resourceRequestId")
            del kwargs["resourceRequestId"]
        if "resourceType" in kwargs:
            hub.log.debug(f"Got kwarg 'resourceType' = {kwargs['resourceType']}")
            body["resourceType"] = kwargs.get("resourceType")
            del kwargs["resourceType"]
        if "tenantLinks" in kwargs:
            hub.log.debug(f"Got kwarg 'tenantLinks' = {kwargs['tenantLinks']}")
            body["tenantLinks"] = kwargs.get("tenantLinks")
            del kwargs["tenantLinks"]

        ret = api.post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ProviderRequestsApi.post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
