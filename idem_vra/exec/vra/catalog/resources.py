from idem_vra.client.vra_catalog_lib.api import ResourcesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_resource(hub, ctx, name, type, **kwargs):
    """Create a new resource. Returns the resource request response. Performs POST /deployment/api/resources


    :param string name: (required in body) Name of the resource
    :param string type: (required in body) Type of the resource
    :param string deploymentId: (optional in body) Resource deployment id
    :param string projectId: (optional in body) Resource project id
    :param object properties: (optional in body) Resource properties
    """

    try:

        hub.log.debug("POST /deployment/api/resources")

        api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}
        body["name"] = name
        body["type"] = type

        if "deploymentId" in kwargs:
            hub.log.debug(f"Got kwarg 'deploymentId' = {kwargs['deploymentId']}")
            body["deploymentId"] = kwargs.get("deploymentId")
            del kwargs["deploymentId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]

        ret = api.create_resource(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourcesApi.create_resource: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_by_id(hub, ctx, p_resourceId, **kwargs):
    """Fetch a specific resource. Returns the resource with the supplied ID. Performs GET /deployment/api/resources/{resourceId}


    :param string p_resourceId: (required in path) Resource ID
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      project, deployment, currentRequest
    """

    try:

        hub.log.debug("GET /deployment/api/resources/{resourceId}")

        api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_by_id(resource_id=p_resourceId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourcesApi.get_resource_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_filter_by_id(hub, ctx, p_filterId, **kwargs):
    """Returns the Resource filter with the supplied ID.  Performs GET /deployment/api/resources/filters/{filterId}


    :param string p_filterId: (required in path) Filter Id
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string search: (optional in query) Search string for filters
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array resourceTypes: (optional in query) A comma-separated list. Only Resources with these types will be
      included in the results.
    :param array origin: (optional in query) A comma-separated list. Only Resources with the origin provided will
      be included in the results.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/resources/filters/{filterId}")

        api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_filter_by_id(filter_id=p_filterId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourcesApi.get_resource_filter_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_filters(hub, ctx, **kwargs):
    """Returns the Resource filters in context of given user.  Performs GET /deployment/api/resources/filters


    :param array resourceTypes: (optional in query) A comma-separated list. Only Resources with these types will be
      included in the results.
    """

    try:

        hub.log.debug("GET /deployment/api/resources/filters")

        api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_filters(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourcesApi.get_resource_filters: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resources(hub, ctx, **kwargs):
    """Fetch all resources. Returns a paginated list of resources. Performs GET /deployment/api/resources


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array resourceTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      resourceType Names.
    :param array tags: (optional in query) A comma-separated list. Results must be associated with one of these
      tags
    :param array cloudTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      cloud Types
    :param array cloudAccounts: (optional in query) A comma-separated list. Results must be associated with one of these
      cloud accounts.
    :param string search: (optional in query) Given string should be part of a searchable field in one of the
      resources.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      project, deployment, currentRequest
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/resources")

        api = ResourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resources(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourcesApi.get_resources: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
