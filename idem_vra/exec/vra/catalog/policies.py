from idem_vra.client.vra_catalog_lib.api import PoliciesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_policy1(hub, ctx, typeId, **kwargs):
    """Creates a new policy, updates an existing policy, or trigger a policy dry-run. Create a new policy or update an existing policy based on request body and
      validate its fields according to business rules or dry-run an existing policy
      to rehearse actual policy effect on application. Performs POST /policy/api/policies


    :param string typeId: (required in body) The policy type ID.
    :param string dryRun: (optional in query) Dry-run an existing policy
    :param boolean validationOnly: (optional in query) For a dry run that will do policy validation only instead of creating
      or updating a policy
    :param string id: (optional in body) The policy ID.
    :param string name: (optional in body) The policy name.
    :param string description: (optional in body) The policy description.
    :param string enforcementType: (optional in body) Defines enforcement type for policy. Default enforcement type is HARD.
    :param string orgId: (optional in body) The ID of the organization to which the policy belongs.
    :param string projectId: (optional in body) For project-scoped policies, the ID of the project to which the policy
      belongs.
    :param Any scopeCriteria: (optional in body)
    :param object definition: (optional in body)
    :param object definitionLegend: (optional in body)
    :param Any criteria: (optional in body)
    :param string createdAt: (optional in body) Policy creation timestamp.
    :param string createdBy: (optional in body) Policy author.
    :param string lastUpdatedAt: (optional in body) Most recent policy update timestamp.
    :param string lastUpdatedBy: (optional in body) Most recent policy editor.
    :param Any statistics: (optional in body)
    """

    try:

        hub.log.debug("POST /policy/api/policies")

        api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}
        body["typeId"] = typeId

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "enforcementType" in kwargs:
            hub.log.debug(f"Got kwarg 'enforcementType' = {kwargs['enforcementType']}")
            body["enforcementType"] = kwargs.get("enforcementType")
            del kwargs["enforcementType"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "scopeCriteria" in kwargs:
            hub.log.debug(f"Got kwarg 'scopeCriteria' = {kwargs['scopeCriteria']}")
            body["scopeCriteria"] = kwargs.get("scopeCriteria")
            del kwargs["scopeCriteria"]
        if "definition" in kwargs:
            hub.log.debug(f"Got kwarg 'definition' = {kwargs['definition']}")
            body["definition"] = kwargs.get("definition")
            del kwargs["definition"]
        if "definitionLegend" in kwargs:
            hub.log.debug(
                f"Got kwarg 'definitionLegend' = {kwargs['definitionLegend']}"
            )
            body["definitionLegend"] = kwargs.get("definitionLegend")
            del kwargs["definitionLegend"]
        if "criteria" in kwargs:
            hub.log.debug(f"Got kwarg 'criteria' = {kwargs['criteria']}")
            body["criteria"] = kwargs.get("criteria")
            del kwargs["criteria"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "lastUpdatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
            body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
            del kwargs["lastUpdatedAt"]
        if "lastUpdatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedBy' = {kwargs['lastUpdatedBy']}")
            body["lastUpdatedBy"] = kwargs.get("lastUpdatedBy")
            del kwargs["lastUpdatedBy"]
        if "statistics" in kwargs:
            hub.log.debug(f"Got kwarg 'statistics' = {kwargs['statistics']}")
            body["statistics"] = kwargs.get("statistics")
            del kwargs["statistics"]

        ret = api.create_policy1(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PoliciesApi.create_policy1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_policy1(hub, ctx, p_id, **kwargs):
    """Delete a policy Delete a specified policy corresponding to its unique id. Performs DELETE /policy/api/policies/{id}


    :param string p_id: (required in path) Policy ID
    """

    try:

        hub.log.debug("DELETE /policy/api/policies/{id}")

        api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.delete_policy1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PoliciesApi.delete_policy1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_policies1(hub, ctx, **kwargs):
    """Returns a paginated list of policies. Find all the policies associated with current org. Performs GET /policy/api/policies


    :param boolean expandDefinition: (optional in query) Retrieves policy definition information for each returned policy.
    :param boolean computeStats: (optional in query)
    :param string search: (optional in query) Matches will start with this string in their name or have this string
      somewhere in their description.
    :param string typeId: (optional in query)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /policy/api/policies")

        api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_policies1(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PoliciesApi.get_policies1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_policy1(hub, ctx, p_id, **kwargs):
    """Returns a specified policy. Find a specific policy based on the input policy id. Performs GET /policy/api/policies/{id}


    :param string p_id: (required in path) Policy ID
    :param boolean computeStats: (optional in query)
    :param boolean expandDefinition: (optional in query)
    """

    try:

        hub.log.debug("GET /policy/api/policies/{id}")

        api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_policy1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PoliciesApi.get_policy1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_policy_limit_usage(hub, ctx, **kwargs):
    """Returns a policy usage metric Returns a total number of policies, policy limit and flag which tells if limit
    reached in the org. Performs GET /policy/api/policies/limit-usage


    """

    try:

        hub.log.debug("GET /policy/api/policies/limit-usage")

        api = PoliciesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_policy_limit_usage(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PoliciesApi.get_policy_limit_usage: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
