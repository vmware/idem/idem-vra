from idem_vra.client.vra_catalog_lib.api import AboutApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_about_page(hub, ctx, **kwargs):
    """Get about page The page contains information about the supported API versions and the latest
    API version. The version parameter is optional but highly recommended.
    If you do not specify explicitly an exact version, you will be calling the
    latest supported API version.
    Here is an example of a call which specifies the exact version you are using:
    GET /catalog/api/items?apiVersion=2020-09-20 Performs GET /catalog/api/about


    """

    try:

        hub.log.debug("GET /catalog/api/about")

        api = AboutApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_about_page(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking AboutApi.get_about_page: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
