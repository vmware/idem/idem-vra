from idem_vra.client.vra_catalog_lib.api import BillingMetricsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_billing_metrics(hub, ctx, **kwargs):
    """Get Billing Metrics. Get billing metrics for current org. Performs GET /deployment/api/billing-metrics"""

    try:

        hub.log.debug("GET /deployment/api/billing-metrics")

        api = BillingMetricsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_billing_metrics(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking BillingMetricsApi.get_billing_metrics: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
