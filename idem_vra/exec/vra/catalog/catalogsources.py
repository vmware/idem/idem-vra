from idem_vra.client.vra_catalog_lib.api import CatalogSourcesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete2(hub, ctx, p_sourceId, **kwargs):
    """Delete catalog source. Deletes the catalog source with the supplied ID. Performs DELETE /catalog/api/admin/sources/{sourceId}


    :param string p_sourceId: (required in path) Catalog source ID
    """

    try:

        hub.log.debug("DELETE /catalog/api/admin/sources/{sourceId}")

        api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.delete2(source_id=p_sourceId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogSourcesApi.delete2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_content_source_limit_usage(hub, ctx, **kwargs):
    """Returns a content source usage metric Returns content source usage metrics including total number of existing content
    sources, content source limit and a flag specifying whether the content source
    creation limit has been reached for given org. Performs GET /catalog/api/admin/sources/limit-usage


    """

    try:

        hub.log.debug("GET /catalog/api/admin/sources/limit-usage")

        api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_content_source_limit_usage(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogSourcesApi.get_content_source_limit_usage: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_page(hub, ctx, **kwargs):
    """Fetch catalog sources. Returns a paginated list of catalog sources. Performs GET /catalog/api/admin/sources


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string search: (optional in query) Matches will have this string in their name or description.
    :param string projectId: (optional in query) Find sources which contains items that can be requested in the given
      projectId
    :param string typeId: (optional in query) Find sources which contains items that can be requested in the given
      typeId
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /catalog/api/admin/sources")

        api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_page(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogSourcesApi.get_page: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get(hub, ctx, p_sourceId, **kwargs):
    """Fetch a specific catalog source for the given ID. Returns the catalog source with the supplied ID. Performs GET /catalog/api/admin/sources/{sourceId}


    :param string p_sourceId: (required in path) Catalog source ID
    """

    try:

        hub.log.debug("GET /catalog/api/admin/sources/{sourceId}")

        api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get(source_id=p_sourceId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking CatalogSourcesApi.get: {err}")
        return ExecReturn(result=False, comment=str(err))


async def post(hub, ctx, name, typeId, config, **kwargs):
    """Create or update a catalog source. Creating or updating also imports (or re-
      imports) the associated catalog items. Creates a new catalog source or updates an existing catalog source based on the
      request body and imports catalog items from it. Performs POST /catalog/api/admin/sources


    :param string name: (required in body) Catalog Source name
    :param string typeId: (required in body) Type of source, e.g. blueprint, CFT... etc
    :param object config: (required in body)
    :param boolean validationOnly: (optional in query) If true, the source will not be created. It returns the number of
      items belonging to the source. The request will still return an error
      code if the source is invalid.
    :param string id: (optional in body) Catalog Source id
    :param string description: (optional in body) Catalog Source description
    :param string createdAt: (optional in body) Creation time
    :param string createdBy: (optional in body) Created By
    :param string lastUpdatedAt: (optional in body) Update time
    :param string lastUpdatedBy: (optional in body) Updated By
    :param integer itemsImported: (optional in body) Number of items imported.
    :param integer itemsFound: (optional in body) Number of items found
    :param string lastImportStartedAt: (optional in body) Last import start time
    :param string lastImportCompletedAt: (optional in body) Last import completion time
    :param array lastImportErrors: (optional in body) Last import error(s)
    :param string projectId: (optional in body) Project id where the source belongs
    :param boolean global: (optional in body) Global flag indicating that all the items can be requested across all
      projects.
    :param string iconId: (optional in body) Default Icon Id
    """

    try:

        hub.log.debug("POST /catalog/api/admin/sources")

        api = CatalogSourcesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}
        body["name"] = name
        body["typeId"] = typeId
        body["config"] = config

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "lastUpdatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
            body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
            del kwargs["lastUpdatedAt"]
        if "lastUpdatedBy" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedBy' = {kwargs['lastUpdatedBy']}")
            body["lastUpdatedBy"] = kwargs.get("lastUpdatedBy")
            del kwargs["lastUpdatedBy"]
        if "itemsImported" in kwargs:
            hub.log.debug(f"Got kwarg 'itemsImported' = {kwargs['itemsImported']}")
            body["itemsImported"] = kwargs.get("itemsImported")
            del kwargs["itemsImported"]
        if "itemsFound" in kwargs:
            hub.log.debug(f"Got kwarg 'itemsFound' = {kwargs['itemsFound']}")
            body["itemsFound"] = kwargs.get("itemsFound")
            del kwargs["itemsFound"]
        if "lastImportStartedAt" in kwargs:
            hub.log.debug(
                f"Got kwarg 'lastImportStartedAt' = {kwargs['lastImportStartedAt']}"
            )
            body["lastImportStartedAt"] = kwargs.get("lastImportStartedAt")
            del kwargs["lastImportStartedAt"]
        if "lastImportCompletedAt" in kwargs:
            hub.log.debug(
                f"Got kwarg 'lastImportCompletedAt' = {kwargs['lastImportCompletedAt']}"
            )
            body["lastImportCompletedAt"] = kwargs.get("lastImportCompletedAt")
            del kwargs["lastImportCompletedAt"]
        if "lastImportErrors" in kwargs:
            hub.log.debug(
                f"Got kwarg 'lastImportErrors' = {kwargs['lastImportErrors']}"
            )
            body["lastImportErrors"] = kwargs.get("lastImportErrors")
            del kwargs["lastImportErrors"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "global" in kwargs:
            hub.log.debug(f"Got kwarg 'global' = {kwargs['global']}")
            body["global"] = kwargs.get("global")
            del kwargs["global"]
        if "iconId" in kwargs:
            hub.log.debug(f"Got kwarg 'iconId' = {kwargs['iconId']}")
            body["iconId"] = kwargs.get("iconId")
            del kwargs["iconId"]

        ret = api.post(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogSourcesApi.post: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
