from idem_vra.client.vra_catalog_lib.api import UserEventsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_user_events(hub, ctx, p_deploymentId, **kwargs):
    """Get user events for deployment. Returns paged user events for given deployment ID. User events represent:
      Create, Day2, Approval or vRO User Interaction Performs GET /deployment/api/deployments/{deploymentId}/userEvents


    :param string p_deploymentId: (required in path) Deployment ID
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/{deploymentId}/userEvents")

        api = UserEventsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_user_events(deployment_id=p_deploymentId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking UserEventsApi.get_user_events: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
