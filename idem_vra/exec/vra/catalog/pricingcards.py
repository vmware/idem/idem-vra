from idem_vra.client.vra_catalog_lib.api import PricingCardsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_policy(hub, ctx, name, **kwargs):
    """Create a new pricing card Create a new pricing card based on request body and validate its field
      according to business rules. Performs POST /price/api/private/pricing-cards


    :param string name: (required in body)
    :param string id: (optional in body)
    :param string description: (optional in body)
    :param string orgId: (optional in body)
    :param Any fixedPrice: (optional in body)
    :param string createdAt: (optional in body)
    :param string lastUpdatedAt: (optional in body)
    :param string createdBy: (optional in body)
    :param string chargeModel: (optional in body)
    :param array meteringItems: (optional in body)
    :param array namedMeteringItems: (optional in body)
    :param array tagBasedMeteringItems: (optional in body)
    :param array oneTimeMeteringItems: (optional in body)
    :param array tagBasedOneTimeMeteringItems: (optional in body)
    :param array tagBasedRateFactorItems: (optional in body)
    :param Any pricingCardAssignmentInfo: (optional in body)
    """

    try:

        hub.log.debug("POST /price/api/private/pricing-cards")

        api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}
        body["name"] = name

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "fixedPrice" in kwargs:
            hub.log.debug(f"Got kwarg 'fixedPrice' = {kwargs['fixedPrice']}")
            body["fixedPrice"] = kwargs.get("fixedPrice")
            del kwargs["fixedPrice"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "lastUpdatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
            body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
            del kwargs["lastUpdatedAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "chargeModel" in kwargs:
            hub.log.debug(f"Got kwarg 'chargeModel' = {kwargs['chargeModel']}")
            body["chargeModel"] = kwargs.get("chargeModel")
            del kwargs["chargeModel"]
        if "meteringItems" in kwargs:
            hub.log.debug(f"Got kwarg 'meteringItems' = {kwargs['meteringItems']}")
            body["meteringItems"] = kwargs.get("meteringItems")
            del kwargs["meteringItems"]
        if "namedMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'namedMeteringItems' = {kwargs['namedMeteringItems']}"
            )
            body["namedMeteringItems"] = kwargs.get("namedMeteringItems")
            del kwargs["namedMeteringItems"]
        if "tagBasedMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'tagBasedMeteringItems' = {kwargs['tagBasedMeteringItems']}"
            )
            body["tagBasedMeteringItems"] = kwargs.get("tagBasedMeteringItems")
            del kwargs["tagBasedMeteringItems"]
        if "oneTimeMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'oneTimeMeteringItems' = {kwargs['oneTimeMeteringItems']}"
            )
            body["oneTimeMeteringItems"] = kwargs.get("oneTimeMeteringItems")
            del kwargs["oneTimeMeteringItems"]
        if "tagBasedOneTimeMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'tagBasedOneTimeMeteringItems' = {kwargs['tagBasedOneTimeMeteringItems']}"
            )
            body["tagBasedOneTimeMeteringItems"] = kwargs.get(
                "tagBasedOneTimeMeteringItems"
            )
            del kwargs["tagBasedOneTimeMeteringItems"]
        if "tagBasedRateFactorItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'tagBasedRateFactorItems' = {kwargs['tagBasedRateFactorItems']}"
            )
            body["tagBasedRateFactorItems"] = kwargs.get("tagBasedRateFactorItems")
            del kwargs["tagBasedRateFactorItems"]
        if "pricingCardAssignmentInfo" in kwargs:
            hub.log.debug(
                f"Got kwarg 'pricingCardAssignmentInfo' = {kwargs['pricingCardAssignmentInfo']}"
            )
            body["pricingCardAssignmentInfo"] = kwargs.get("pricingCardAssignmentInfo")
            del kwargs["pricingCardAssignmentInfo"]

        ret = api.create_policy(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardsApi.create_policy: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_policy(hub, ctx, p_id, **kwargs):
    """Delete the pricing card with specified Id Deletes the pricing card with the specified id Performs DELETE /price/api/private/pricing-cards/{id}


    :param string p_id: (required in path) pricing card Id
    """

    try:

        hub.log.debug("DELETE /price/api/private/pricing-cards/{id}")

        api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.delete_policy(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardsApi.delete_policy: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_policies(hub, ctx, **kwargs):
    """Fetch all pricing cards for private-policy cloud Returns a paginated list of pricing cards Performs GET /price/api/private/pricing-cards


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string search: (optional in query) Search by name and description
    :param boolean expandPricingCard: (optional in query) Whether or not returns detailed pricing card for each result.
    :param boolean expandAssignmentInfo: (optional in query) Whether or not returns count of assignments.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /price/api/private/pricing-cards")

        api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_policies(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardsApi.get_policies: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_policy(hub, ctx, p_id, **kwargs):
    """Find the pricing card with specified Id Returns the pricing card with the specified id Performs GET /price/api/private/pricing-cards/{id}


    :param string p_id: (required in path) pricing card Id
    """

    try:

        hub.log.debug("GET /price/api/private/pricing-cards/{id}")

        api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_policy(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardsApi.get_policy: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_policy(hub, ctx, p_id, name, **kwargs):
    """Update the pricing card Updates the pricing card with the specified Id Performs PUT /price/api/private/pricing-cards/{id}


    :param string p_id: (required in path) pricing card Id
    :param string name: (required in body)
    :param string id: (optional in body)
    :param string description: (optional in body)
    :param string orgId: (optional in body)
    :param Any fixedPrice: (optional in body)
    :param string createdAt: (optional in body)
    :param string lastUpdatedAt: (optional in body)
    :param string createdBy: (optional in body)
    :param string chargeModel: (optional in body)
    :param array meteringItems: (optional in body)
    :param array namedMeteringItems: (optional in body)
    :param array tagBasedMeteringItems: (optional in body)
    :param array oneTimeMeteringItems: (optional in body)
    :param array tagBasedOneTimeMeteringItems: (optional in body)
    :param array tagBasedRateFactorItems: (optional in body)
    :param Any pricingCardAssignmentInfo: (optional in body)
    """

    try:

        hub.log.debug("PUT /price/api/private/pricing-cards/{id}")

        api = PricingCardsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}
        body["name"] = name

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "fixedPrice" in kwargs:
            hub.log.debug(f"Got kwarg 'fixedPrice' = {kwargs['fixedPrice']}")
            body["fixedPrice"] = kwargs.get("fixedPrice")
            del kwargs["fixedPrice"]
        if "createdAt" in kwargs:
            hub.log.debug(f"Got kwarg 'createdAt' = {kwargs['createdAt']}")
            body["createdAt"] = kwargs.get("createdAt")
            del kwargs["createdAt"]
        if "lastUpdatedAt" in kwargs:
            hub.log.debug(f"Got kwarg 'lastUpdatedAt' = {kwargs['lastUpdatedAt']}")
            body["lastUpdatedAt"] = kwargs.get("lastUpdatedAt")
            del kwargs["lastUpdatedAt"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "chargeModel" in kwargs:
            hub.log.debug(f"Got kwarg 'chargeModel' = {kwargs['chargeModel']}")
            body["chargeModel"] = kwargs.get("chargeModel")
            del kwargs["chargeModel"]
        if "meteringItems" in kwargs:
            hub.log.debug(f"Got kwarg 'meteringItems' = {kwargs['meteringItems']}")
            body["meteringItems"] = kwargs.get("meteringItems")
            del kwargs["meteringItems"]
        if "namedMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'namedMeteringItems' = {kwargs['namedMeteringItems']}"
            )
            body["namedMeteringItems"] = kwargs.get("namedMeteringItems")
            del kwargs["namedMeteringItems"]
        if "tagBasedMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'tagBasedMeteringItems' = {kwargs['tagBasedMeteringItems']}"
            )
            body["tagBasedMeteringItems"] = kwargs.get("tagBasedMeteringItems")
            del kwargs["tagBasedMeteringItems"]
        if "oneTimeMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'oneTimeMeteringItems' = {kwargs['oneTimeMeteringItems']}"
            )
            body["oneTimeMeteringItems"] = kwargs.get("oneTimeMeteringItems")
            del kwargs["oneTimeMeteringItems"]
        if "tagBasedOneTimeMeteringItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'tagBasedOneTimeMeteringItems' = {kwargs['tagBasedOneTimeMeteringItems']}"
            )
            body["tagBasedOneTimeMeteringItems"] = kwargs.get(
                "tagBasedOneTimeMeteringItems"
            )
            del kwargs["tagBasedOneTimeMeteringItems"]
        if "tagBasedRateFactorItems" in kwargs:
            hub.log.debug(
                f"Got kwarg 'tagBasedRateFactorItems' = {kwargs['tagBasedRateFactorItems']}"
            )
            body["tagBasedRateFactorItems"] = kwargs.get("tagBasedRateFactorItems")
            del kwargs["tagBasedRateFactorItems"]
        if "pricingCardAssignmentInfo" in kwargs:
            hub.log.debug(
                f"Got kwarg 'pricingCardAssignmentInfo' = {kwargs['pricingCardAssignmentInfo']}"
            )
            body["pricingCardAssignmentInfo"] = kwargs.get("pricingCardAssignmentInfo")
            del kwargs["pricingCardAssignmentInfo"]

        ret = api.update_policy(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PricingCardsApi.update_policy: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
