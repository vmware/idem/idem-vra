from idem_vra.client.vra_catalog_lib.api import PolicyDecisionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_decision_by_id(hub, ctx, p_id, **kwargs):
    """Returns a policy decision by id. Find a specific policy decision based on the input policy decision id. Performs GET /policy/api/policyDecisions/{id}


    :param string p_id: (required in path) Policy decision Id
    """

    try:

        hub.log.debug("GET /policy/api/policyDecisions/{id}")

        api = PolicyDecisionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_decision_by_id(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PolicyDecisionsApi.get_decision_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_decisions(hub, ctx, **kwargs):
    """Fetch a list of policy decisions. Returns a paginated list of policy decisions. If a dryRunId is provided, the
      return value has a field indicating whether the dry run is complete. Performs GET /policy/api/policyDecisions


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string dryRunId: (optional in query)
    :param string projectId: (optional in query) Matches will only include decisions with this project ID
    :param string policyTypeId: (optional in query) Matches will only include policies of this type
    :param string search: (optional in query) Matches will start with this string in their policy name or target
      name or have this string somewhere in their description.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /policy/api/policyDecisions")

        api = PolicyDecisionsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_decisions(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PolicyDecisionsApi.get_decisions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
