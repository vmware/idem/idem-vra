from idem_vra.client.vra_catalog_lib.api import PerspectiveSyncApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def sync_perspective_group(hub, ctx, **kwargs):
    """On demand Perspective Sync To do on demand perspective sync for within the given org Performs POST /price/api/cloudhealth/perspective-sync"""

    try:

        hub.log.debug("POST /price/api/cloudhealth/perspective-sync")

        api = PerspectiveSyncApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.sync_perspective_group(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking PerspectiveSyncApi.sync_perspective_group: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
