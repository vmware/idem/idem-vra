from idem_vra.client.vra_catalog_lib.api import DeploymentsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def check_deployment_name_exists(hub, ctx, q_name, **kwargs):
    """Check if a deployment exists. Returns OK if a deployment with the supplied name exists. Performs GET /deployment/api/deployments/names


    :param string q_name: (required in query) Deployment name
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/names")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.check_deployment_name_exists(name=q_name, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.check_deployment_name_exists: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def check_deployment_name(hub, ctx, p_name, **kwargs):
    """Check if a deployment exists. Returns OK if a deployment with the supplied name exists. Performs GET /deployment/api/deployments/names/{name}


    :param string p_name: (required in path) Deployment name
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/names/{name}")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.check_deployment_name(name=p_name, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.check_deployment_name: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_deployment(hub, ctx, p_deploymentId, **kwargs):
    """Delete a deployment. Effectively triggers a Delete Day2 operation Deletes the deployment with the supplied ID, cleans up the associated resources
      from the Cloud Provider. Performs DELETE /deployment/api/deployments/{deploymentId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param boolean forceDelete: (optional in query) Guarantees the deployments deletion and all related resources from
      VMware Aria Automation. In some situations, this may leave provisioned
      infrastructure resources behind. Please ensure you remove them
      manually in such cases.
    """

    try:

        hub.log.debug("DELETE /deployment/api/deployments/{deploymentId}")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.delete_deployment(deployment_id=p_deploymentId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.delete_deployment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_resource(hub, ctx, p_deploymentId, p_resourceId, **kwargs):
    """Delete resource associated with a deployment. Effectively triggers a Delete
      Day2 operation. Deletes the resource with the specified ID and attempts to delete resource from
      the Cloud Provider. Performs DELETE /deployment/api/deployments/{deploymentId}/resources/{resourceId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    """

    try:

        hub.log.debug(
            "DELETE /deployment/api/deployments/{deploymentId}/resources/{resourceId}"
        )

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.delete_resource(
            deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.delete_resource: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployment_by_id1(hub, ctx, p_deploymentId, **kwargs):
    """Fetch a specific deployment. Returns the deployment with the supplied ID. Performs GET /deployment/api/deployments/{deploymentId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param boolean expandResources: (optional in query) The resources field of the deployment will be retrieved.
    :param boolean expandLastRequest: (optional in query) Expands deployment last request.
    :param boolean expandProject: (optional in query) The project field of the deployment will be retrieved.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects.
      resources option returns resources with all properties. Ex.
      blueprint, project
    :param boolean deleted: (optional in query) Retrieves the deployment, includes soft-deleted deployments that have
      not yet been completely deleted.
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/{deploymentId}")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_by_id1(deployment_id=p_deploymentId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_deployment_by_id1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployment_expense_history_by_id(hub, ctx, p_deploymentId, **kwargs):
    """Fetch a specific deployments expense history. Returns the deployment expense history with the supplied ID. Performs GET /deployment/api/deployments/{deploymentId}/expense-history


    :param string p_deploymentId: (required in path) Deployment ID
    :param string from: (optional in query) The timestamp from when history is requested. Should be of ISO_INSTANT
      format.
    :param string to: (optional in query) The timestamp until when history is requested. Should be of
      ISO_INSTANT format.
    :param string interval: (optional in query) The interval of the expense history. Should be one of daily, weekly or
      monthly.
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/{deploymentId}/expense-history")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_expense_history_by_id(
            deployment_id=p_deploymentId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_deployment_expense_history_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployment_filter_by_id(hub, ctx, p_filterId, **kwargs):
    """Returns the Deployment filter with the supplied ID.  Performs GET /deployment/api/deployments/filters/{filterId}


    :param string p_filterId: (required in path) Filter Id
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string search: (optional in query) Search string for filters
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/filters/{filterId}")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_filter_by_id(filter_id=p_filterId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_deployment_filter_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployment_filters(hub, ctx, **kwargs):
    """Returns the Deployment filters in context of given user.  Performs GET /deployment/api/deployments/filters"""

    try:

        hub.log.debug("GET /deployment/api/deployments/filters")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_filters(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_deployment_filters: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployment_resources(hub, ctx, p_deploymentId, **kwargs):
    """Fetch resources associated with a deployment. Returns a paginated list of resources corresponding to the deployment with the
      supplied ID. Performs GET /deployment/api/deployments/{deploymentId}/resources


    :param string p_deploymentId: (required in path) Deployment ID
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param array names: (optional in query) Results must have exactly these resource names.
    :param array resourceTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      resourceType Names.
    :param array tags: (optional in query) A comma-separated list. Results must be associated with one of these
      tags
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      currentRequest
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/deployments/{deploymentId}/resources")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployment_resources(deployment_id=p_deploymentId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_deployment_resources: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployments1(hub, ctx, **kwargs):
    """Fetch all deployments. Returns a paginated list of deployments. Performs GET /deployment/api/deployments


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param array ids: (optional in query) A comma-separated list. Only deployments with these IDs will be
      included in the results.
    :param string search: (optional in query) Given string should either be part of a searchable field in a
      deployment or one of deployments resources.
    :param string name: (optional in query) Results must have exactly this name.
    :param array status: (optional in query) A comma-separated list. Results must be associated with one of these
      statuses.
    :param array projects: (optional in query) A comma-separated list. Results must be associated with one of these
      project IDs.
    :param array resourceTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      resourceType Names.
    :param array cloudAccounts: (optional in query) A comma-separated list. Results must be associated with one of these
      cloud accounts.
    :param array cloudTypes: (optional in query) A comma-separated list. Results must be associated with one of these
      endpoint Types
    :param array requestedBy: (optional in query) A comma-separated list. Results must be associated with one of these
      requesters
    :param array ownedBy: (optional in query) A comma-separated list. Results must be associated with one of these
      owners
    :param array tags: (optional in query) A comma-separated list. Results must be associated with one of these
      tags
    :param None createdAt: (optional in query) Comma-separated start and end dates where start date or end date is
      optional (e.g. [2020-12-01T08:00:00.000Z,2020-12-11T23:59:00.000Z],
      [2020-11-03T08:00:00.000Z,], [,2020-11-08T08:00:00.000Z]
    :param None expiresAt: (optional in query) Comma-separated start and end dates for the interval
    :param None lastUpdatedAt: (optional in query) Comma-separated start and end dates where start date or end date is
      optional (e.g. [2020-12-01T08:00:00.000Z,2020-12-11T23:59:00.000Z],
      [2020-11-03T08:00:00.000Z,], [,2020-11-08T08:00:00.000Z]
    :param boolean expandResources: (optional in query) The resources field of each resulting deployment will be retrieved.
    :param boolean expandLastRequest: (optional in query) Expands deployment last request.
    :param boolean expandProject: (optional in query) The project field of each resulting deployment will be retrieved.
    :param array expand: (optional in query) The expanded details of the requested comma separated objects.
      resources option returns resources with all properties. Ex.
      blueprint, project
    :param array lastRequestStatus: (optional in query) A comma-separated list of last request statuses. Allowed values are:
      ABORTED, APPROVAL_PENDING, APPROVAL_REJECTED, FAILED, INPROGRESS,
      PENDING and SUCCESSFUL. Results must be associated with one of these
      last request statuses.
    :param boolean deleted: (optional in query) Retrieves only soft-deleted deployments that have not yet been
      completely deleted.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /deployment/api/deployments")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployments1(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_deployments1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_deployments_for_project(hub, ctx, p_projectId, **kwargs):
    """Returns a count of deployments using the project.  Performs GET /deployment/api/projects/{projectId}/deployment-count


    :param string p_projectId: (required in path) Project ID
    """

    try:

        hub.log.debug("GET /deployment/api/projects/{projectId}/deployment-count")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_deployments_for_project(project_id=p_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_deployments_for_project: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_by_id1(hub, ctx, p_deploymentId, p_resourceId, **kwargs):
    """Fetch resource associated with a deployment. Returns the resource with the specified ID that is correlated with the supplied
      deployment. Performs GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string p_resourceId: (required in path) Resource ID
    :param array expand: (optional in query) The expanded details of the requested comma separated objects. Ex.
      currentRequest
    """

    try:

        hub.log.debug(
            "GET /deployment/api/deployments/{deploymentId}/resources/{resourceId}"
        )

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_resource_by_id1(
            deployment_id=p_deploymentId, resource_id=p_resourceId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.get_resource_by_id1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch_deployment(hub, ctx, p_deploymentId, **kwargs):
    """Update deployment. Updates the deployment with the supplied ID. Performs PATCH /deployment/api/deployments/{deploymentId}


    :param string p_deploymentId: (required in path) Deployment ID
    :param string name: (optional in body) New name of the deployment
    :param string description: (optional in body) New description of the deployment
    :param string iconId: (optional in body) New iconid of the deployment
    """

    try:

        hub.log.debug("PATCH /deployment/api/deployments/{deploymentId}")

        api = DeploymentsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "iconId" in kwargs:
            hub.log.debug(f"Got kwarg 'iconId' = {kwargs['iconId']}")
            body["iconId"] = kwargs.get("iconId")
            del kwargs["iconId"]

        ret = api.patch_deployment(body, deployment_id=p_deploymentId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentsApi.patch_deployment: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
