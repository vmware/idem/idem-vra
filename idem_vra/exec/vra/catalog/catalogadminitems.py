from idem_vra.client.vra_catalog_lib.api import CatalogAdminItemsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_catalog_item1(hub, ctx, p_id, **kwargs):
    """Find a catalog item with specified id. Returns the catalog item with the specified id. Performs GET /catalog/api/admin/items/{id}


    :param string p_id: (required in path) Catalog item id
    :param boolean includeSource: (optional in query) Includes the Content Source metadata for the Catalog Item
    """

    try:

        hub.log.debug("GET /catalog/api/admin/items/{id}")

        api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_catalog_item1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogAdminItemsApi.get_catalog_item1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_catalog_items1(hub, ctx, **kwargs):
    """Fetch a list of catalog items. Returns a paginated list of catalog items. Performs GET /catalog/api/admin/items


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param string search: (optional in query) Matches will have this string somewhere in their name or description.
    :param array sourceIds: (optional in query) A list of Content Source IDs. Results will be from one of these
      sources.
    :param array types: (optional in query) A list of Catalog Item Type IDs. Results will be one of these types.
    :param string projectId: (optional in query) Matches which can be requested within project with the given projectId
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /catalog/api/admin/items")

        api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_catalog_items1(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogAdminItemsApi.get_catalog_items1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_version_by_id1(hub, ctx, p_id, p_versionId, **kwargs):
    """Fetch a detailed catalog item version. Returns a detailed catalog item version. Performs GET /catalog/api/admin/items/{id}/versions/{versionId}


    :param string p_id: (required in path) Catalog Item ID
    :param string p_versionId: (required in path) Catalog Item Version ID
    """

    try:

        hub.log.debug("GET /catalog/api/admin/items/{id}/versions/{versionId}")

        api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_version_by_id1(id=p_id, version_id=p_versionId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogAdminItemsApi.get_version_by_id1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_versions1(hub, ctx, p_id, **kwargs):
    """Fetch a list of catalog items with versions. Returns a paginated list of catalog item versions. Performs GET /catalog/api/admin/items/{id}/versions


    :param string p_id: (required in path) Catalog Item ID
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /catalog/api/admin/items/{id}/versions")

        api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_versions1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogAdminItemsApi.get_versions1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_catalog_item_version(hub, ctx, p_id, p_versionId, **kwargs):
    """Patch the catalog item version with form id. Patch the catalog item version with form id. Nullify the formId if the formId
      is an empty string. Performs PATCH /catalog/api/admin/items/{id}/versions/{versionId}


    :param string p_id: (required in path) The unique id of catalog item to update.
    :param string p_versionId: (required in path) The name of catalog item version to update.
    :param string formId: (optional in body) form id
    """

    try:

        hub.log.debug("PATCH /catalog/api/admin/items/{id}/versions/{versionId}")

        api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "formId" in kwargs:
            hub.log.debug(f"Got kwarg 'formId' = {kwargs['formId']}")
            body["formId"] = kwargs.get("formId")
            del kwargs["formId"]

        ret = api.update_catalog_item_version(
            body, id=p_id, version_id=p_versionId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogAdminItemsApi.update_catalog_item_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update_catalog_item(hub, ctx, p_id, **kwargs):
    """Set an icon or request limit to a catalog item. Updates a catalog item with specified icon id or request limit. Performs PATCH /catalog/api/admin/items/{id}


    :param string p_id: (required in path) The unique id of item to update.
    :param string iconId: (optional in body) icon id
    :param integer bulkRequestLimit: (optional in body) Max number of instances that can be requested at a time
    :param string formId: (optional in body) form id
    """

    try:

        hub.log.debug("PATCH /catalog/api/admin/items/{id}")

        api = CatalogAdminItemsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}

        if "iconId" in kwargs:
            hub.log.debug(f"Got kwarg 'iconId' = {kwargs['iconId']}")
            body["iconId"] = kwargs.get("iconId")
            del kwargs["iconId"]
        if "bulkRequestLimit" in kwargs:
            hub.log.debug(
                f"Got kwarg 'bulkRequestLimit' = {kwargs['bulkRequestLimit']}"
            )
            body["bulkRequestLimit"] = kwargs.get("bulkRequestLimit")
            del kwargs["bulkRequestLimit"]
        if "formId" in kwargs:
            hub.log.debug(f"Got kwarg 'formId' = {kwargs['formId']}")
            body["formId"] = kwargs.get("formId")
            del kwargs["formId"]

        ret = api.update_catalog_item(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogAdminItemsApi.update_catalog_item: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
