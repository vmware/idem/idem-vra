from idem_vra.client.vra_catalog_lib.api import IconsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def delete1(hub, ctx, p_id, **kwargs):
    """Delete an icon Delete an existing icon by its unique id. Performs DELETE /icon/api/icons/{id}


    :param string p_id: (required in path) Icon id
    """

    try:

        hub.log.debug("DELETE /icon/api/icons/{id}")

        api = IconsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.delete1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking IconsApi.delete1: {err}")
        return ExecReturn(result=False, comment=str(err))


async def download(hub, ctx, p_id, **kwargs):
    """Download an icon Download an existing icon by its unique id. Performs GET /icon/api/icons/{id}


    :param string p_id: (required in path) Icon id
    """

    try:

        hub.log.debug("GET /icon/api/icons/{id}")

        api = IconsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.download(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking IconsApi.download: {err}")
        return ExecReturn(result=False, comment=str(err))


async def upload(hub, ctx, file, **kwargs):
    """Upload an icon Create an icon. Performs POST /icon/api/icons


    :param string file: (required in body) Icon file
    """

    try:

        hub.log.debug("POST /icon/api/icons")

        api = IconsApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        body = {}
        body["file"] = file

        ret = api.upload(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking IconsApi.upload: {err}")
        return ExecReturn(result=False, comment=str(err))
