from idem_vra.client.vra_catalog_lib.api import CatalogItemTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_type_by_id(hub, ctx, p_id, **kwargs):
    """Fetch catalog item type associated with the specified ID. Returns the Catalog Item Type with the specified ID. Performs GET /catalog/api/types/{id}


    :param string p_id: (required in path) Catalog Type ID
    """

    try:

        hub.log.debug("GET /catalog/api/types/{id}")

        api = CatalogItemTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_type_by_id(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemTypesApi.get_type_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_types1(hub, ctx, **kwargs):
    """Find all Catalog Item Types. Returns a paginated list of all available Catalog Item Types. Performs GET /catalog/api/types


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param integer top: (optional in query) Number of records you want
    :param integer skip: (optional in query) Number of records you want to skip
    :param string orderby: (optional in query) Sorting criteria in the format: property (ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /catalog/api/types")

        api = CatalogItemTypesApi(hub.clients["idem_vra.client.vra_catalog_lib.api"])

        ret = api.get_types1(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CatalogItemTypesApi.get_types1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
