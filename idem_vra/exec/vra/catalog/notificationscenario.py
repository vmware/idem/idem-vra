from idem_vra.client.vra_catalog_lib.api import NotificationScenarioApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_scenario(hub, ctx, p_id, **kwargs):
    """Retrieves notification scenarios Retrieves notification scenarios Performs GET /notification/api/scenarios/{id}


    :param string p_id: (required in path) Notification scenario ID
    :param boolean expandBody: (optional in query) The body field of each scenario will be retrieved.
    """

    try:

        hub.log.debug("GET /notification/api/scenarios/{id}")

        api = NotificationScenarioApi(
            hub.clients["idem_vra.client.vra_catalog_lib.api"]
        )

        ret = api.get_scenario(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking NotificationScenarioApi.get_scenario: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
