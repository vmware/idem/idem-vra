from idem_vra.client.vra_abx_lib.api import ActionConstantsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create1(hub, ctx, **kwargs):
    """Create an action constant Creates a new action constant Performs POST /abx/api/resources/action-secrets


    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param object value: (optional in body)
    :param boolean encrypted: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/action-secrets")

        api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "value" in kwargs:
            hub.log.debug(f"Got kwarg 'value' = {kwargs['value']}")
            body["value"] = kwargs.get("value")
            del kwargs["value"]
        if "encrypted" in kwargs:
            hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
            body["encrypted"] = kwargs.get("encrypted")
            del kwargs["encrypted"]

        ret = api.create1(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionConstantsApi.create1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete2(hub, ctx, p_id, **kwargs):
    """Delete an action constant Deletes an action constant Performs DELETE /abx/api/resources/action-secrets/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("DELETE /abx/api/resources/action-secrets/{id}")

        api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.delete2(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionConstantsApi.delete2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all1(hub, ctx, **kwargs):
    """Fetch all action constants Retrieves all action constants for given organization Performs GET /abx/api/resources/action-secrets


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/action-secrets")

        api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all1(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionConstantsApi.get_all1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_by_id1(hub, ctx, p_id, **kwargs):
    """Fetch action constant Retrieves action constant by given id Performs GET /abx/api/resources/action-secrets/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /abx/api/resources/action-secrets/{id}")

        api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_by_id1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionConstantsApi.get_by_id1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update1(hub, ctx, p_id, **kwargs):
    """Update an action constant Updates an existing action constant Performs PUT /abx/api/resources/action-secrets/{id}


    :param string p_id: (required in path)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param object value: (optional in body)
    :param boolean encrypted: (optional in body)
    """

    try:

        hub.log.debug("PUT /abx/api/resources/action-secrets/{id}")

        api = ActionConstantsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "value" in kwargs:
            hub.log.debug(f"Got kwarg 'value' = {kwargs['value']}")
            body["value"] = kwargs.get("value")
            del kwargs["value"]
        if "encrypted" in kwargs:
            hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
            body["encrypted"] = kwargs.get("encrypted")
            del kwargs["encrypted"]

        ret = api.update1(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionConstantsApi.update1: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
