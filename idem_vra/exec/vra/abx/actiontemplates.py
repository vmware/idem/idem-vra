from idem_vra.client.vra_abx_lib.api import ActionTemplatesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_all(hub, ctx, **kwargs):
    """Fetch all action templates Retrieves all action templates Performs GET /abx/api/resources/action-templates


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/action-templates")

        api = ActionTemplatesApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionTemplatesApi.get_all: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_by_id(hub, ctx, p_id, **kwargs):
    """Fetch an action template by its ID Retrieves an action template entity with a specific ID Performs GET /abx/api/resources/action-templates/{id}


    :param string p_id: (required in path) ID of the action template
    """

    try:

        hub.log.debug("GET /abx/api/resources/action-templates/{id}")

        api = ActionTemplatesApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_by_id(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionTemplatesApi.get_by_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
