from idem_vra.client.vra_abx_lib.api import ActionRunsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def cancel_action_run(hub, ctx, p_id, **kwargs):
    """Cancel an action run Cancels an action run which is currently being executed Performs PATCH /abx/api/resources/action-runs/{id}/cancel


    :param string p_id: (required in path) ID of the action run
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param string runState: (optional in body)
    :param string actionId: (optional in body)
    :param string actionType: (optional in body)
    :param object inputs: (optional in body)
    :param object outputs: (optional in body)
    :param string errorMessage: (optional in body)
    :param string logs: (optional in body)
    :param string triggeredBy: (optional in body)
    :param integer endTimeMillis: (optional in body)
    :param integer startTimeMillis: (optional in body)
    :param string runtime: (optional in body)
    :param string provider: (optional in body)
    :param string source: (optional in body)
    :param integer timeoutSeconds: (optional in body)
    :param string projectId: (optional in body)
    :param string runProjectId: (optional in body)
    :param boolean scalable: (optional in body)
    :param string actionVersionId: (optional in body)
    :param string actionVersionName: (optional in body)
    :param object configuration: (optional in body)
    :param boolean system: (optional in body)
    """

    try:

        hub.log.debug("PATCH /abx/api/resources/action-runs/{id}/cancel")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "runState" in kwargs:
            hub.log.debug(f"Got kwarg 'runState' = {kwargs['runState']}")
            body["runState"] = kwargs.get("runState")
            del kwargs["runState"]
        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "actionType" in kwargs:
            hub.log.debug(f"Got kwarg 'actionType' = {kwargs['actionType']}")
            body["actionType"] = kwargs.get("actionType")
            del kwargs["actionType"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "outputs" in kwargs:
            hub.log.debug(f"Got kwarg 'outputs' = {kwargs['outputs']}")
            body["outputs"] = kwargs.get("outputs")
            del kwargs["outputs"]
        if "errorMessage" in kwargs:
            hub.log.debug(f"Got kwarg 'errorMessage' = {kwargs['errorMessage']}")
            body["errorMessage"] = kwargs.get("errorMessage")
            del kwargs["errorMessage"]
        if "logs" in kwargs:
            hub.log.debug(f"Got kwarg 'logs' = {kwargs['logs']}")
            body["logs"] = kwargs.get("logs")
            del kwargs["logs"]
        if "triggeredBy" in kwargs:
            hub.log.debug(f"Got kwarg 'triggeredBy' = {kwargs['triggeredBy']}")
            body["triggeredBy"] = kwargs.get("triggeredBy")
            del kwargs["triggeredBy"]
        if "endTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'endTimeMillis' = {kwargs['endTimeMillis']}")
            body["endTimeMillis"] = kwargs.get("endTimeMillis")
            del kwargs["endTimeMillis"]
        if "startTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'startTimeMillis' = {kwargs['startTimeMillis']}")
            body["startTimeMillis"] = kwargs.get("startTimeMillis")
            del kwargs["startTimeMillis"]
        if "runtime" in kwargs:
            hub.log.debug(f"Got kwarg 'runtime' = {kwargs['runtime']}")
            body["runtime"] = kwargs.get("runtime")
            del kwargs["runtime"]
        if "provider" in kwargs:
            hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
            body["provider"] = kwargs.get("provider")
            del kwargs["provider"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "timeoutSeconds" in kwargs:
            hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
            body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
            del kwargs["timeoutSeconds"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "runProjectId" in kwargs:
            hub.log.debug(f"Got kwarg 'runProjectId' = {kwargs['runProjectId']}")
            body["runProjectId"] = kwargs.get("runProjectId")
            del kwargs["runProjectId"]
        if "scalable" in kwargs:
            hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
            body["scalable"] = kwargs.get("scalable")
            del kwargs["scalable"]
        if "actionVersionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionVersionId' = {kwargs['actionVersionId']}")
            body["actionVersionId"] = kwargs.get("actionVersionId")
            del kwargs["actionVersionId"]
        if "actionVersionName" in kwargs:
            hub.log.debug(
                f"Got kwarg 'actionVersionName' = {kwargs['actionVersionName']}"
            )
            body["actionVersionName"] = kwargs.get("actionVersionName")
            del kwargs["actionVersionName"]
        if "configuration" in kwargs:
            hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
            body["configuration"] = kwargs.get("configuration")
            del kwargs["configuration"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]

        ret = api.cancel_action_run(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionRunsApi.cancel_action_run: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create2(hub, ctx, **kwargs):
    """Performs POST /abx/api/resources/action-runs


    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param string runState: (optional in body)
    :param string actionId: (optional in body)
    :param string actionType: (optional in body)
    :param object inputs: (optional in body)
    :param object outputs: (optional in body)
    :param string errorMessage: (optional in body)
    :param string logs: (optional in body)
    :param string triggeredBy: (optional in body)
    :param integer endTimeMillis: (optional in body)
    :param integer startTimeMillis: (optional in body)
    :param string runtime: (optional in body)
    :param string provider: (optional in body)
    :param string source: (optional in body)
    :param integer timeoutSeconds: (optional in body)
    :param string projectId: (optional in body)
    :param string runProjectId: (optional in body)
    :param boolean scalable: (optional in body)
    :param string actionVersionId: (optional in body)
    :param string actionVersionName: (optional in body)
    :param object configuration: (optional in body)
    :param boolean system: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/action-runs")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "runState" in kwargs:
            hub.log.debug(f"Got kwarg 'runState' = {kwargs['runState']}")
            body["runState"] = kwargs.get("runState")
            del kwargs["runState"]
        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "actionType" in kwargs:
            hub.log.debug(f"Got kwarg 'actionType' = {kwargs['actionType']}")
            body["actionType"] = kwargs.get("actionType")
            del kwargs["actionType"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "outputs" in kwargs:
            hub.log.debug(f"Got kwarg 'outputs' = {kwargs['outputs']}")
            body["outputs"] = kwargs.get("outputs")
            del kwargs["outputs"]
        if "errorMessage" in kwargs:
            hub.log.debug(f"Got kwarg 'errorMessage' = {kwargs['errorMessage']}")
            body["errorMessage"] = kwargs.get("errorMessage")
            del kwargs["errorMessage"]
        if "logs" in kwargs:
            hub.log.debug(f"Got kwarg 'logs' = {kwargs['logs']}")
            body["logs"] = kwargs.get("logs")
            del kwargs["logs"]
        if "triggeredBy" in kwargs:
            hub.log.debug(f"Got kwarg 'triggeredBy' = {kwargs['triggeredBy']}")
            body["triggeredBy"] = kwargs.get("triggeredBy")
            del kwargs["triggeredBy"]
        if "endTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'endTimeMillis' = {kwargs['endTimeMillis']}")
            body["endTimeMillis"] = kwargs.get("endTimeMillis")
            del kwargs["endTimeMillis"]
        if "startTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'startTimeMillis' = {kwargs['startTimeMillis']}")
            body["startTimeMillis"] = kwargs.get("startTimeMillis")
            del kwargs["startTimeMillis"]
        if "runtime" in kwargs:
            hub.log.debug(f"Got kwarg 'runtime' = {kwargs['runtime']}")
            body["runtime"] = kwargs.get("runtime")
            del kwargs["runtime"]
        if "provider" in kwargs:
            hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
            body["provider"] = kwargs.get("provider")
            del kwargs["provider"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "timeoutSeconds" in kwargs:
            hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
            body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
            del kwargs["timeoutSeconds"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "runProjectId" in kwargs:
            hub.log.debug(f"Got kwarg 'runProjectId' = {kwargs['runProjectId']}")
            body["runProjectId"] = kwargs.get("runProjectId")
            del kwargs["runProjectId"]
        if "scalable" in kwargs:
            hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
            body["scalable"] = kwargs.get("scalable")
            del kwargs["scalable"]
        if "actionVersionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionVersionId' = {kwargs['actionVersionId']}")
            body["actionVersionId"] = kwargs.get("actionVersionId")
            del kwargs["actionVersionId"]
        if "actionVersionName" in kwargs:
            hub.log.debug(
                f"Got kwarg 'actionVersionName' = {kwargs['actionVersionName']}"
            )
            body["actionVersionName"] = kwargs.get("actionVersionName")
            del kwargs["actionVersionName"]
        if "configuration" in kwargs:
            hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
            body["configuration"] = kwargs.get("configuration")
            del kwargs["configuration"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]

        ret = api.create2(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionRunsApi.create2: {err}")
        return ExecReturn(result=False, comment=str(err))


async def delete3(hub, ctx, **kwargs):
    """Delete multiple action runs Deletes multiple action runs with their specific ID Performs DELETE /abx/api/resources/action-runs"""

    try:

        hub.log.debug("DELETE /abx/api/resources/action-runs")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.delete3(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionRunsApi.delete3: {err}")
        return ExecReturn(result=False, comment=str(err))


async def delete4(hub, ctx, p_id, **kwargs):
    """Delete an action run Deletes an action run with a specific ID Performs DELETE /abx/api/resources/action-runs/{id}


    :param string p_id: (required in path) ID of the action run
    """

    try:

        hub.log.debug("DELETE /abx/api/resources/action-runs/{id}")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.delete4(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionRunsApi.delete4: {err}")
        return ExecReturn(result=False, comment=str(err))


async def get_action_runs_by_action_id(hub, ctx, p_id, **kwargs):
    """Fetch all action runs of an action Retrieves all action runs of an action Performs GET /abx/api/resources/actions/{id}/action-runs


    :param string p_id: (required in path) ID of the action
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/actions/{id}/action-runs")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_action_runs_by_action_id(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionRunsApi.get_action_runs_by_action_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all21(hub, ctx, q_projection, **kwargs):
    """Fetch all action runs Retrieves all action run entities Performs GET /abx/api/resources/action-runs


    :param boolean q_projection: (required in query)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/action-runs")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all21(projection=q_projection, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionRunsApi.get_all21: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_by_id2(hub, ctx, p_id, **kwargs):
    """Fetch an action run by its ID Retrieves an action run entity with a specific ID Performs GET /abx/api/resources/action-runs/{id}


    :param string p_id: (required in path) ID of the action run
    """

    try:

        hub.log.debug("GET /abx/api/resources/action-runs/{id}")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_by_id2(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionRunsApi.get_by_id2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def run(hub, ctx, p_id, **kwargs):
    """Trigger an action run Runs the specified action taking into consideration the supplied configuration Performs POST /abx/api/resources/actions/{id}/action-runs


    :param string p_id: (required in path) ID of the action
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param string runState: (optional in body)
    :param string actionId: (optional in body)
    :param string actionType: (optional in body)
    :param object inputs: (optional in body)
    :param object outputs: (optional in body)
    :param string errorMessage: (optional in body)
    :param string logs: (optional in body)
    :param string triggeredBy: (optional in body)
    :param integer endTimeMillis: (optional in body)
    :param integer startTimeMillis: (optional in body)
    :param string runtime: (optional in body)
    :param string provider: (optional in body)
    :param string source: (optional in body)
    :param integer timeoutSeconds: (optional in body)
    :param string projectId: (optional in body)
    :param string runProjectId: (optional in body)
    :param boolean scalable: (optional in body)
    :param string actionVersionId: (optional in body)
    :param string actionVersionName: (optional in body)
    :param object configuration: (optional in body)
    :param boolean system: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/actions/{id}/action-runs")

        api = ActionRunsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "runState" in kwargs:
            hub.log.debug(f"Got kwarg 'runState' = {kwargs['runState']}")
            body["runState"] = kwargs.get("runState")
            del kwargs["runState"]
        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "actionType" in kwargs:
            hub.log.debug(f"Got kwarg 'actionType' = {kwargs['actionType']}")
            body["actionType"] = kwargs.get("actionType")
            del kwargs["actionType"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "outputs" in kwargs:
            hub.log.debug(f"Got kwarg 'outputs' = {kwargs['outputs']}")
            body["outputs"] = kwargs.get("outputs")
            del kwargs["outputs"]
        if "errorMessage" in kwargs:
            hub.log.debug(f"Got kwarg 'errorMessage' = {kwargs['errorMessage']}")
            body["errorMessage"] = kwargs.get("errorMessage")
            del kwargs["errorMessage"]
        if "logs" in kwargs:
            hub.log.debug(f"Got kwarg 'logs' = {kwargs['logs']}")
            body["logs"] = kwargs.get("logs")
            del kwargs["logs"]
        if "triggeredBy" in kwargs:
            hub.log.debug(f"Got kwarg 'triggeredBy' = {kwargs['triggeredBy']}")
            body["triggeredBy"] = kwargs.get("triggeredBy")
            del kwargs["triggeredBy"]
        if "endTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'endTimeMillis' = {kwargs['endTimeMillis']}")
            body["endTimeMillis"] = kwargs.get("endTimeMillis")
            del kwargs["endTimeMillis"]
        if "startTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'startTimeMillis' = {kwargs['startTimeMillis']}")
            body["startTimeMillis"] = kwargs.get("startTimeMillis")
            del kwargs["startTimeMillis"]
        if "runtime" in kwargs:
            hub.log.debug(f"Got kwarg 'runtime' = {kwargs['runtime']}")
            body["runtime"] = kwargs.get("runtime")
            del kwargs["runtime"]
        if "provider" in kwargs:
            hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
            body["provider"] = kwargs.get("provider")
            del kwargs["provider"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "timeoutSeconds" in kwargs:
            hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
            body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
            del kwargs["timeoutSeconds"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "runProjectId" in kwargs:
            hub.log.debug(f"Got kwarg 'runProjectId' = {kwargs['runProjectId']}")
            body["runProjectId"] = kwargs.get("runProjectId")
            del kwargs["runProjectId"]
        if "scalable" in kwargs:
            hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
            body["scalable"] = kwargs.get("scalable")
            del kwargs["scalable"]
        if "actionVersionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionVersionId' = {kwargs['actionVersionId']}")
            body["actionVersionId"] = kwargs.get("actionVersionId")
            del kwargs["actionVersionId"]
        if "actionVersionName" in kwargs:
            hub.log.debug(
                f"Got kwarg 'actionVersionName' = {kwargs['actionVersionName']}"
            )
            body["actionVersionName"] = kwargs.get("actionVersionName")
            del kwargs["actionVersionName"]
        if "configuration" in kwargs:
            hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
            body["configuration"] = kwargs.get("configuration")
            del kwargs["configuration"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]

        ret = api.run(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionRunsApi.run: {err}")
        return ExecReturn(result=False, comment=str(err))
