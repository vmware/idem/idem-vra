from idem_vra.client.vra_abx_lib.api import EndpointsControllerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_all2(hub, ctx, **kwargs):
    """Proxy to fetch provisioning endpoints Retrieves provisioning endpoints for given organization Performs GET /abx/api/provisioning/endpoints


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/provisioning/endpoints")

        api = EndpointsControllerApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all2(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsControllerApi.get_all2: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get(hub, ctx, p_id, **kwargs):
    """Proxy to fetch a provisioning endpoint by self link Retrieves provisioning endpoints for given organization Performs GET /abx/api/provisioning/endpoints/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /abx/api/provisioning/endpoints/{id}")

        api = EndpointsControllerApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking EndpointsControllerApi.get: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
