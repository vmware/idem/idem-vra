from idem_vra.client.vra_abx_lib.api import ActionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_action_version(hub, ctx, p_id, q_projectId, **kwargs):
    """Create a version for an action Creates a new version for the specified action Performs POST /abx/api/resources/actions/{id}/versions


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param string description: (optional in body)
    :param string actionId: (optional in body)
    :param string projectId: (optional in body)
    :param string createdBy: (optional in body)
    :param boolean released: (optional in body)
    :param Any action: (optional in body)
    :param string contentId: (optional in body)
    :param string gitCommitId: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/actions/{id}/versions")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "createdBy" in kwargs:
            hub.log.debug(f"Got kwarg 'createdBy' = {kwargs['createdBy']}")
            body["createdBy"] = kwargs.get("createdBy")
            del kwargs["createdBy"]
        if "released" in kwargs:
            hub.log.debug(f"Got kwarg 'released' = {kwargs['released']}")
            body["released"] = kwargs.get("released")
            del kwargs["released"]
        if "action" in kwargs:
            hub.log.debug(f"Got kwarg 'action' = {kwargs['action']}")
            body["action"] = kwargs.get("action")
            del kwargs["action"]
        if "contentId" in kwargs:
            hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
            body["contentId"] = kwargs.get("contentId")
            del kwargs["contentId"]
        if "gitCommitId" in kwargs:
            hub.log.debug(f"Got kwarg 'gitCommitId' = {kwargs['gitCommitId']}")
            body["gitCommitId"] = kwargs.get("gitCommitId")
            del kwargs["gitCommitId"]

        ret = api.create_action_version(body, id=p_id, project_id=q_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.create_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def create(hub, ctx, name, runtime, entrypoint, actionType, orgId, **kwargs):
    """Create an action Creates a new action Performs POST /abx/api/resources/actions


    :param string name: (required in body)
    :param string runtime: (required in body)
    :param string entrypoint: (required in body)
    :param string actionType: (required in body)
    :param string orgId: (required in body)
    :param object metadata: (optional in body)
    :param string source: (optional in body)
    :param string description: (optional in body)
    :param object inputs: (optional in body)
    :param integer memoryInMB: (optional in body)
    :param integer prePolyglotMemoryLimitInMB: (optional in body)
    :param boolean showMemoryAlert: (optional in body)
    :param integer timeoutSeconds: (optional in body)
    :param string dependencies: (optional in body)
    :param array compressedContent: (optional in body)
    :param string provider: (optional in body)
    :param string contentId: (optional in body)
    :param object configuration: (optional in body)
    :param boolean system: (optional in body)
    :param boolean shared: (optional in body)
    :param boolean scalable: (optional in body)
    :param boolean asyncDeployed: (optional in body)
    :param string id: (optional in body)
    :param string projectId: (optional in body)
    :param string selfLink: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/actions")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}
        body["name"] = name
        body["runtime"] = runtime
        body["entrypoint"] = entrypoint
        body["actionType"] = actionType
        body["orgId"] = orgId

        if "metadata" in kwargs:
            hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
            body["metadata"] = kwargs.get("metadata")
            del kwargs["metadata"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "memoryInMB" in kwargs:
            hub.log.debug(f"Got kwarg 'memoryInMB' = {kwargs['memoryInMB']}")
            body["memoryInMB"] = kwargs.get("memoryInMB")
            del kwargs["memoryInMB"]
        if "prePolyglotMemoryLimitInMB" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prePolyglotMemoryLimitInMB' = {kwargs['prePolyglotMemoryLimitInMB']}"
            )
            body["prePolyglotMemoryLimitInMB"] = kwargs.get(
                "prePolyglotMemoryLimitInMB"
            )
            del kwargs["prePolyglotMemoryLimitInMB"]
        if "showMemoryAlert" in kwargs:
            hub.log.debug(f"Got kwarg 'showMemoryAlert' = {kwargs['showMemoryAlert']}")
            body["showMemoryAlert"] = kwargs.get("showMemoryAlert")
            del kwargs["showMemoryAlert"]
        if "timeoutSeconds" in kwargs:
            hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
            body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
            del kwargs["timeoutSeconds"]
        if "dependencies" in kwargs:
            hub.log.debug(f"Got kwarg 'dependencies' = {kwargs['dependencies']}")
            body["dependencies"] = kwargs.get("dependencies")
            del kwargs["dependencies"]
        if "compressedContent" in kwargs:
            hub.log.debug(
                f"Got kwarg 'compressedContent' = {kwargs['compressedContent']}"
            )
            body["compressedContent"] = kwargs.get("compressedContent")
            del kwargs["compressedContent"]
        if "provider" in kwargs:
            hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
            body["provider"] = kwargs.get("provider")
            del kwargs["provider"]
        if "contentId" in kwargs:
            hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
            body["contentId"] = kwargs.get("contentId")
            del kwargs["contentId"]
        if "configuration" in kwargs:
            hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
            body["configuration"] = kwargs.get("configuration")
            del kwargs["configuration"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]
        if "shared" in kwargs:
            hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
            body["shared"] = kwargs.get("shared")
            del kwargs["shared"]
        if "scalable" in kwargs:
            hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
            body["scalable"] = kwargs.get("scalable")
            del kwargs["scalable"]
        if "asyncDeployed" in kwargs:
            hub.log.debug(f"Got kwarg 'asyncDeployed' = {kwargs['asyncDeployed']}")
            body["asyncDeployed"] = kwargs.get("asyncDeployed")
            del kwargs["asyncDeployed"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "selfLink" in kwargs:
            hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
            body["selfLink"] = kwargs.get("selfLink")
            del kwargs["selfLink"]

        ret = api.create(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.create: {err}")
        return ExecReturn(result=False, comment=str(err))


async def delete1(hub, ctx, p_id, **kwargs):
    """Delete an action Deletes an action with a specific ID Performs DELETE /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    :param boolean force: (optional in query)
    """

    try:

        hub.log.debug("DELETE /abx/api/resources/actions/{id}")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.delete1(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.delete1: {err}")
        return ExecReturn(result=False, comment=str(err))


async def delete_action_version(hub, ctx, p_id, p_versionId, q_projectId, **kwargs):
    """Delete an action version Deletes an action version Performs DELETE /abx/api/resources/actions/{id}/versions/{versionId}


    :param string p_id: (required in path) ID of the action
    :param string p_versionId: (required in path) ID of the action version
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    """

    try:

        hub.log.debug("DELETE /abx/api/resources/actions/{id}/versions/{versionId}")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.delete_action_version(
            id=p_id, version_id=p_versionId, project_id=q_projectId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.delete_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete(hub, ctx, **kwargs):
    """Delete multiple actions by their id and projectId Deletes actions with a specific ID and projectId Performs DELETE /abx/api/resources/actions"""

    try:

        hub.log.debug("DELETE /abx/api/resources/actions")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.delete(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.delete: {err}")
        return ExecReturn(result=False, comment=str(err))


async def export(hub, ctx, actions, **kwargs):
    """Export actions Exports the specified actions as a zip bundle Performs POST /abx/api/resources/actions/export


    :param array actions: (required in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/actions/export")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}
        body["actions"] = actions

        ret = api.export(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.export: {err}")
        return ExecReturn(result=False, comment=str(err))


async def get_action_runs_by_action_id(hub, ctx, p_id, **kwargs):
    """Fetch all action runs of an action Retrieves all action runs of an action Performs GET /abx/api/resources/actions/{id}/action-runs


    :param string p_id: (required in path) ID of the action
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/actions/{id}/action-runs")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_action_runs_by_action_id(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.get_action_runs_by_action_id: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all11(hub, ctx, **kwargs):
    """Fetch all actions Retrieves all action entities Performs GET /abx/api/resources/actions


    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    :param boolean projection: (optional in query)
    :param string nameCriteria: (optional in query)
    """

    try:

        hub.log.debug("GET /abx/api/resources/actions")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all11(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.get_all11: {err}")
        return ExecReturn(result=False, comment=str(err))


async def get_all_flows_using_action(hub, ctx, p_id, **kwargs):
    """Fetch all action flows using an action Retrieves a list of all action flows which are using the specified action Performs GET /abx/api/resources/actions/{id}/flows


    :param string p_id: (required in path) ID of the action
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/actions/{id}/flows")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all_flows_using_action(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.get_all_flows_using_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_all_versions(hub, ctx, p_id, q_projectId, **kwargs):
    """Fetch all versions of an action Retrieves all created versions for an action Performs GET /abx/api/resources/actions/{id}/versions


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/actions/{id}/versions")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_all_versions(id=p_id, project_id=q_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.get_all_versions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_by_id11(hub, ctx, p_id, q_groupByProject, q_allProjects, **kwargs):
    """Fetch an action by its ID Retrieves an action entity with a specific ID Performs GET /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param boolean q_groupByProject: (required in query)
    :param boolean q_allProjects: (required in query)
    :param string projectId: (optional in query) Project ID of action (required for non-system actions)
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /abx/api/resources/actions/{id}")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.get_by_id11(
            id=p_id,
            group_by_project=q_groupByProject,
            all_projects=q_allProjects,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.get_by_id11: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def import_actions(hub, ctx, **kwargs):
    """Import an action bundle Imports an action bundle and saves all actions Performs POST /abx/api/resources/actions/import


    :param string bundleId: (optional in body)
    :param array compressedBundle: (optional in body)
    :param string option: (optional in body)
    :param string projectId: (optional in body)
    :param boolean system: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/actions/import")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "bundleId" in kwargs:
            hub.log.debug(f"Got kwarg 'bundleId' = {kwargs['bundleId']}")
            body["bundleId"] = kwargs.get("bundleId")
            del kwargs["bundleId"]
        if "compressedBundle" in kwargs:
            hub.log.debug(
                f"Got kwarg 'compressedBundle' = {kwargs['compressedBundle']}"
            )
            body["compressedBundle"] = kwargs.get("compressedBundle")
            del kwargs["compressedBundle"]
        if "option" in kwargs:
            hub.log.debug(f"Got kwarg 'option' = {kwargs['option']}")
            body["option"] = kwargs.get("option")
            del kwargs["option"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]

        ret = api.import_actions(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.import_actions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def patch(hub, ctx, p_id, name, runtime, entrypoint, actionType, orgId, **kwargs):
    """Patch an action Updates the state of the action based only on the supplied properties Performs PATCH /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param string name: (required in body)
    :param string runtime: (required in body)
    :param string entrypoint: (required in body)
    :param string actionType: (required in body)
    :param string orgId: (required in body)
    :param object metadata: (optional in body)
    :param string source: (optional in body)
    :param string description: (optional in body)
    :param object inputs: (optional in body)
    :param integer memoryInMB: (optional in body)
    :param integer prePolyglotMemoryLimitInMB: (optional in body)
    :param boolean showMemoryAlert: (optional in body)
    :param integer timeoutSeconds: (optional in body)
    :param string dependencies: (optional in body)
    :param array compressedContent: (optional in body)
    :param string provider: (optional in body)
    :param string contentId: (optional in body)
    :param object configuration: (optional in body)
    :param boolean system: (optional in body)
    :param boolean shared: (optional in body)
    :param boolean scalable: (optional in body)
    :param boolean asyncDeployed: (optional in body)
    :param string id: (optional in body)
    :param string projectId: (optional in body)
    :param string selfLink: (optional in body)
    """

    try:

        hub.log.debug("PATCH /abx/api/resources/actions/{id}")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}
        body["name"] = name
        body["runtime"] = runtime
        body["entrypoint"] = entrypoint
        body["actionType"] = actionType
        body["orgId"] = orgId

        if "metadata" in kwargs:
            hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
            body["metadata"] = kwargs.get("metadata")
            del kwargs["metadata"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "memoryInMB" in kwargs:
            hub.log.debug(f"Got kwarg 'memoryInMB' = {kwargs['memoryInMB']}")
            body["memoryInMB"] = kwargs.get("memoryInMB")
            del kwargs["memoryInMB"]
        if "prePolyglotMemoryLimitInMB" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prePolyglotMemoryLimitInMB' = {kwargs['prePolyglotMemoryLimitInMB']}"
            )
            body["prePolyglotMemoryLimitInMB"] = kwargs.get(
                "prePolyglotMemoryLimitInMB"
            )
            del kwargs["prePolyglotMemoryLimitInMB"]
        if "showMemoryAlert" in kwargs:
            hub.log.debug(f"Got kwarg 'showMemoryAlert' = {kwargs['showMemoryAlert']}")
            body["showMemoryAlert"] = kwargs.get("showMemoryAlert")
            del kwargs["showMemoryAlert"]
        if "timeoutSeconds" in kwargs:
            hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
            body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
            del kwargs["timeoutSeconds"]
        if "dependencies" in kwargs:
            hub.log.debug(f"Got kwarg 'dependencies' = {kwargs['dependencies']}")
            body["dependencies"] = kwargs.get("dependencies")
            del kwargs["dependencies"]
        if "compressedContent" in kwargs:
            hub.log.debug(
                f"Got kwarg 'compressedContent' = {kwargs['compressedContent']}"
            )
            body["compressedContent"] = kwargs.get("compressedContent")
            del kwargs["compressedContent"]
        if "provider" in kwargs:
            hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
            body["provider"] = kwargs.get("provider")
            del kwargs["provider"]
        if "contentId" in kwargs:
            hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
            body["contentId"] = kwargs.get("contentId")
            del kwargs["contentId"]
        if "configuration" in kwargs:
            hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
            body["configuration"] = kwargs.get("configuration")
            del kwargs["configuration"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]
        if "shared" in kwargs:
            hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
            body["shared"] = kwargs.get("shared")
            del kwargs["shared"]
        if "scalable" in kwargs:
            hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
            body["scalable"] = kwargs.get("scalable")
            del kwargs["scalable"]
        if "asyncDeployed" in kwargs:
            hub.log.debug(f"Got kwarg 'asyncDeployed' = {kwargs['asyncDeployed']}")
            body["asyncDeployed"] = kwargs.get("asyncDeployed")
            del kwargs["asyncDeployed"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "selfLink" in kwargs:
            hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
            body["selfLink"] = kwargs.get("selfLink")
            del kwargs["selfLink"]

        ret = api.patch(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.patch: {err}")
        return ExecReturn(result=False, comment=str(err))


async def release_action_version(hub, ctx, p_id, q_projectId, version, **kwargs):
    """Mark an action version as released Marks an exisiting version of an action as released Performs PUT /abx/api/resources/actions/{id}/release


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    :param string version: (required in body)
    """

    try:

        hub.log.debug("PUT /abx/api/resources/actions/{id}/release")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}
        body["version"] = version

        ret = api.release_action_version(
            body, id=p_id, project_id=q_projectId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.release_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def restore_action_version(hub, ctx, p_id, p_versionId, q_projectId, **kwargs):
    """Restore the action state based on a specified version Change the current action state to the state specified in the version Performs PUT /abx/api/resources/actions/{id}/versions/{versionId}/restore


    :param string p_id: (required in path) ID of the action
    :param string p_versionId: (required in path) ID of the action version
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    """

    try:

        hub.log.debug(
            "PUT /abx/api/resources/actions/{id}/versions/{versionId}/restore"
        )

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.restore_action_version(
            id=p_id, version_id=p_versionId, project_id=q_projectId, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.restore_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def run(hub, ctx, p_id, **kwargs):
    """Trigger an action run Runs the specified action taking into consideration the supplied configuration Performs POST /abx/api/resources/actions/{id}/action-runs


    :param string p_id: (required in path) ID of the action
    :param string id: (optional in body)
    :param string name: (optional in body)
    :param integer createdMillis: (optional in body)
    :param string orgId: (optional in body)
    :param string runState: (optional in body)
    :param string actionId: (optional in body)
    :param string actionType: (optional in body)
    :param object inputs: (optional in body)
    :param object outputs: (optional in body)
    :param string errorMessage: (optional in body)
    :param string logs: (optional in body)
    :param string triggeredBy: (optional in body)
    :param integer endTimeMillis: (optional in body)
    :param integer startTimeMillis: (optional in body)
    :param string runtime: (optional in body)
    :param string provider: (optional in body)
    :param string source: (optional in body)
    :param integer timeoutSeconds: (optional in body)
    :param string projectId: (optional in body)
    :param string runProjectId: (optional in body)
    :param boolean scalable: (optional in body)
    :param string actionVersionId: (optional in body)
    :param string actionVersionName: (optional in body)
    :param object configuration: (optional in body)
    :param boolean system: (optional in body)
    """

    try:

        hub.log.debug("POST /abx/api/resources/actions/{id}/action-runs")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "createdMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'createdMillis' = {kwargs['createdMillis']}")
            body["createdMillis"] = kwargs.get("createdMillis")
            del kwargs["createdMillis"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "runState" in kwargs:
            hub.log.debug(f"Got kwarg 'runState' = {kwargs['runState']}")
            body["runState"] = kwargs.get("runState")
            del kwargs["runState"]
        if "actionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionId' = {kwargs['actionId']}")
            body["actionId"] = kwargs.get("actionId")
            del kwargs["actionId"]
        if "actionType" in kwargs:
            hub.log.debug(f"Got kwarg 'actionType' = {kwargs['actionType']}")
            body["actionType"] = kwargs.get("actionType")
            del kwargs["actionType"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "outputs" in kwargs:
            hub.log.debug(f"Got kwarg 'outputs' = {kwargs['outputs']}")
            body["outputs"] = kwargs.get("outputs")
            del kwargs["outputs"]
        if "errorMessage" in kwargs:
            hub.log.debug(f"Got kwarg 'errorMessage' = {kwargs['errorMessage']}")
            body["errorMessage"] = kwargs.get("errorMessage")
            del kwargs["errorMessage"]
        if "logs" in kwargs:
            hub.log.debug(f"Got kwarg 'logs' = {kwargs['logs']}")
            body["logs"] = kwargs.get("logs")
            del kwargs["logs"]
        if "triggeredBy" in kwargs:
            hub.log.debug(f"Got kwarg 'triggeredBy' = {kwargs['triggeredBy']}")
            body["triggeredBy"] = kwargs.get("triggeredBy")
            del kwargs["triggeredBy"]
        if "endTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'endTimeMillis' = {kwargs['endTimeMillis']}")
            body["endTimeMillis"] = kwargs.get("endTimeMillis")
            del kwargs["endTimeMillis"]
        if "startTimeMillis" in kwargs:
            hub.log.debug(f"Got kwarg 'startTimeMillis' = {kwargs['startTimeMillis']}")
            body["startTimeMillis"] = kwargs.get("startTimeMillis")
            del kwargs["startTimeMillis"]
        if "runtime" in kwargs:
            hub.log.debug(f"Got kwarg 'runtime' = {kwargs['runtime']}")
            body["runtime"] = kwargs.get("runtime")
            del kwargs["runtime"]
        if "provider" in kwargs:
            hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
            body["provider"] = kwargs.get("provider")
            del kwargs["provider"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "timeoutSeconds" in kwargs:
            hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
            body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
            del kwargs["timeoutSeconds"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "runProjectId" in kwargs:
            hub.log.debug(f"Got kwarg 'runProjectId' = {kwargs['runProjectId']}")
            body["runProjectId"] = kwargs.get("runProjectId")
            del kwargs["runProjectId"]
        if "scalable" in kwargs:
            hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
            body["scalable"] = kwargs.get("scalable")
            del kwargs["scalable"]
        if "actionVersionId" in kwargs:
            hub.log.debug(f"Got kwarg 'actionVersionId' = {kwargs['actionVersionId']}")
            body["actionVersionId"] = kwargs.get("actionVersionId")
            del kwargs["actionVersionId"]
        if "actionVersionName" in kwargs:
            hub.log.debug(
                f"Got kwarg 'actionVersionName' = {kwargs['actionVersionName']}"
            )
            body["actionVersionName"] = kwargs.get("actionVersionName")
            del kwargs["actionVersionName"]
        if "configuration" in kwargs:
            hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
            body["configuration"] = kwargs.get("configuration")
            del kwargs["configuration"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]

        ret = api.run(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.run: {err}")
        return ExecReturn(result=False, comment=str(err))


async def unrelease_action_version(hub, ctx, p_id, q_projectId, **kwargs):
    """Mark an action version as not released Marks an actions released version as not released Performs DELETE /abx/api/resources/actions/{id}/release


    :param string p_id: (required in path) ID of the action
    :param string q_projectId: (required in query) Project ID of action (required for non-system actions)
    """

    try:

        hub.log.debug("DELETE /abx/api/resources/actions/{id}/release")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        ret = api.unrelease_action_version(id=p_id, project_id=q_projectId, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ActionsApi.unrelease_action_version: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def update(
    hub, ctx, p_id, name, runtime, entrypoint, actionType, orgId, **kwargs
):
    """Update an action Updates the action with the new supplied state Performs PUT /abx/api/resources/actions/{id}


    :param string p_id: (required in path) ID of the action
    :param string name: (required in body)
    :param string runtime: (required in body)
    :param string entrypoint: (required in body)
    :param string actionType: (required in body)
    :param string orgId: (required in body)
    :param object metadata: (optional in body)
    :param string source: (optional in body)
    :param string description: (optional in body)
    :param object inputs: (optional in body)
    :param integer memoryInMB: (optional in body)
    :param integer prePolyglotMemoryLimitInMB: (optional in body)
    :param boolean showMemoryAlert: (optional in body)
    :param integer timeoutSeconds: (optional in body)
    :param string dependencies: (optional in body)
    :param array compressedContent: (optional in body)
    :param string provider: (optional in body)
    :param string contentId: (optional in body)
    :param object configuration: (optional in body)
    :param boolean system: (optional in body)
    :param boolean shared: (optional in body)
    :param boolean scalable: (optional in body)
    :param boolean asyncDeployed: (optional in body)
    :param string id: (optional in body)
    :param string projectId: (optional in body)
    :param string selfLink: (optional in body)
    """

    try:

        hub.log.debug("PUT /abx/api/resources/actions/{id}")

        api = ActionsApi(hub.clients["idem_vra.client.vra_abx_lib.api"])

        body = {}
        body["name"] = name
        body["runtime"] = runtime
        body["entrypoint"] = entrypoint
        body["actionType"] = actionType
        body["orgId"] = orgId

        if "metadata" in kwargs:
            hub.log.debug(f"Got kwarg 'metadata' = {kwargs['metadata']}")
            body["metadata"] = kwargs.get("metadata")
            del kwargs["metadata"]
        if "source" in kwargs:
            hub.log.debug(f"Got kwarg 'source' = {kwargs['source']}")
            body["source"] = kwargs.get("source")
            del kwargs["source"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "inputs" in kwargs:
            hub.log.debug(f"Got kwarg 'inputs' = {kwargs['inputs']}")
            body["inputs"] = kwargs.get("inputs")
            del kwargs["inputs"]
        if "memoryInMB" in kwargs:
            hub.log.debug(f"Got kwarg 'memoryInMB' = {kwargs['memoryInMB']}")
            body["memoryInMB"] = kwargs.get("memoryInMB")
            del kwargs["memoryInMB"]
        if "prePolyglotMemoryLimitInMB" in kwargs:
            hub.log.debug(
                f"Got kwarg 'prePolyglotMemoryLimitInMB' = {kwargs['prePolyglotMemoryLimitInMB']}"
            )
            body["prePolyglotMemoryLimitInMB"] = kwargs.get(
                "prePolyglotMemoryLimitInMB"
            )
            del kwargs["prePolyglotMemoryLimitInMB"]
        if "showMemoryAlert" in kwargs:
            hub.log.debug(f"Got kwarg 'showMemoryAlert' = {kwargs['showMemoryAlert']}")
            body["showMemoryAlert"] = kwargs.get("showMemoryAlert")
            del kwargs["showMemoryAlert"]
        if "timeoutSeconds" in kwargs:
            hub.log.debug(f"Got kwarg 'timeoutSeconds' = {kwargs['timeoutSeconds']}")
            body["timeoutSeconds"] = kwargs.get("timeoutSeconds")
            del kwargs["timeoutSeconds"]
        if "dependencies" in kwargs:
            hub.log.debug(f"Got kwarg 'dependencies' = {kwargs['dependencies']}")
            body["dependencies"] = kwargs.get("dependencies")
            del kwargs["dependencies"]
        if "compressedContent" in kwargs:
            hub.log.debug(
                f"Got kwarg 'compressedContent' = {kwargs['compressedContent']}"
            )
            body["compressedContent"] = kwargs.get("compressedContent")
            del kwargs["compressedContent"]
        if "provider" in kwargs:
            hub.log.debug(f"Got kwarg 'provider' = {kwargs['provider']}")
            body["provider"] = kwargs.get("provider")
            del kwargs["provider"]
        if "contentId" in kwargs:
            hub.log.debug(f"Got kwarg 'contentId' = {kwargs['contentId']}")
            body["contentId"] = kwargs.get("contentId")
            del kwargs["contentId"]
        if "configuration" in kwargs:
            hub.log.debug(f"Got kwarg 'configuration' = {kwargs['configuration']}")
            body["configuration"] = kwargs.get("configuration")
            del kwargs["configuration"]
        if "system" in kwargs:
            hub.log.debug(f"Got kwarg 'system' = {kwargs['system']}")
            body["system"] = kwargs.get("system")
            del kwargs["system"]
        if "shared" in kwargs:
            hub.log.debug(f"Got kwarg 'shared' = {kwargs['shared']}")
            body["shared"] = kwargs.get("shared")
            del kwargs["shared"]
        if "scalable" in kwargs:
            hub.log.debug(f"Got kwarg 'scalable' = {kwargs['scalable']}")
            body["scalable"] = kwargs.get("scalable")
            del kwargs["scalable"]
        if "asyncDeployed" in kwargs:
            hub.log.debug(f"Got kwarg 'asyncDeployed' = {kwargs['asyncDeployed']}")
            body["asyncDeployed"] = kwargs.get("asyncDeployed")
            del kwargs["asyncDeployed"]
        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "selfLink" in kwargs:
            hub.log.debug(f"Got kwarg 'selfLink' = {kwargs['selfLink']}")
            body["selfLink"] = kwargs.get("selfLink")
            del kwargs["selfLink"]

        ret = api.update(body, id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking ActionsApi.update: {err}")
        return ExecReturn(result=False, comment=str(err))
