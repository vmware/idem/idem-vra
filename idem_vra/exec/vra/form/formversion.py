from idem_vra.client.vra_form_lib.api import FormVersionApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create(hub, ctx, **kwargs):
    """Create form definition version  Performs POST /form-service/api/forms/versions


    :param string sourceType: (optional in body)
    :param string sourceId: (optional in body)
    :param string formType: (optional in body)
    :param string name: (optional in body)
    :param string description: (optional in body)
    :param string changeLog: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/versions")

        api = FormVersionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "sourceType" in kwargs:
            hub.log.debug(f"Got kwarg 'sourceType' = {kwargs['sourceType']}")
            body["sourceType"] = kwargs.get("sourceType")
            del kwargs["sourceType"]
        if "sourceId" in kwargs:
            hub.log.debug(f"Got kwarg 'sourceId' = {kwargs['sourceId']}")
            body["sourceId"] = kwargs.get("sourceId")
            del kwargs["sourceId"]
        if "formType" in kwargs:
            hub.log.debug(f"Got kwarg 'formType' = {kwargs['formType']}")
            body["formType"] = kwargs.get("formType")
            del kwargs["formType"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "changeLog" in kwargs:
            hub.log.debug(f"Got kwarg 'changeLog' = {kwargs['changeLog']}")
            body["changeLog"] = kwargs.get("changeLog")
            del kwargs["changeLog"]

        ret = api.create(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking FormVersionApi.create: {err}")
        return ExecReturn(result=False, comment=str(err))


async def get_all(hub, ctx, q_sourceType, q_sourceId, q_formType, **kwargs):
    """Get form definition versions  Performs GET /form-service/api/forms/versions


    :param string q_sourceType: (required in query) The form source type. It can be com.vmw.vro.workflow or
      resource.action.
    :param string q_sourceId: (required in query) The form source id
    :param string q_formType: (required in query) The form type. It can be requestForm.
    :param integer page: (optional in query) Zero-based page index (0..N)
    :param integer size: (optional in query) The size of the page to be returned
    :param array sort: (optional in query) Sorting criteria in the format: property,(ascdesc). Default sort
      order is ascending. Multiple sort criteria are supported.
    """

    try:

        hub.log.debug("GET /form-service/api/forms/versions")

        api = FormVersionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_all(
            source_type=q_sourceType,
            source_id=q_sourceId,
            form_type=q_formType,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormVersionApi.get_all: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get(hub, ctx, p_id, **kwargs):
    """Get form definition version by id  Performs GET /form-service/api/forms/versions/{id}


    :param string p_id: (required in path) Form definition version identifier
    """

    try:

        hub.log.debug("GET /form-service/api/forms/versions/{id}")

        api = FormVersionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(f"Exception occurred while invoking FormVersionApi.get: {err}")
        return ExecReturn(result=False, comment=str(err))


async def restore(hub, ctx, p_id, **kwargs):
    """Restore form definition version  Performs PATCH /form-service/api/forms/versions/{id}/restore


    :param string p_id: (required in path) Form definition version identifier
    """

    try:

        hub.log.debug("PATCH /form-service/api/forms/versions/{id}/restore")

        api = FormVersionApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.restore(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormVersionApi.restore: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
