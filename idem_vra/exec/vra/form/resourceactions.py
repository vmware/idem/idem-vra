from idem_vra.client.vra_form_lib.api import ResourceActionsApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_or_update_resource_action(hub, ctx, **kwargs):
    """Create resource action.  Performs POST /form-service/api/custom/resource-actions


    :param string id: (optional in body)
    :param string name: (optional in body)
    :param string displayName: (optional in body)
    :param string description: (optional in body)
    :param string providerName: (optional in body)
    :param string resourceType: (optional in body)
    :param string status: (optional in body)
    :param string projectId: (optional in body)
    :param string orgId: (optional in body)
    :param Any runnableItem: (optional in body)
    :param Any formDefinition: (optional in body)
    :param Any criteria: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/custom/resource-actions")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "displayName" in kwargs:
            hub.log.debug(f"Got kwarg 'displayName' = {kwargs['displayName']}")
            body["displayName"] = kwargs.get("displayName")
            del kwargs["displayName"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "providerName" in kwargs:
            hub.log.debug(f"Got kwarg 'providerName' = {kwargs['providerName']}")
            body["providerName"] = kwargs.get("providerName")
            del kwargs["providerName"]
        if "resourceType" in kwargs:
            hub.log.debug(f"Got kwarg 'resourceType' = {kwargs['resourceType']}")
            body["resourceType"] = kwargs.get("resourceType")
            del kwargs["resourceType"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "runnableItem" in kwargs:
            hub.log.debug(f"Got kwarg 'runnableItem' = {kwargs['runnableItem']}")
            body["runnableItem"] = kwargs.get("runnableItem")
            del kwargs["runnableItem"]
        if "formDefinition" in kwargs:
            hub.log.debug(f"Got kwarg 'formDefinition' = {kwargs['formDefinition']}")
            body["formDefinition"] = kwargs.get("formDefinition")
            del kwargs["formDefinition"]
        if "criteria" in kwargs:
            hub.log.debug(f"Got kwarg 'criteria' = {kwargs['criteria']}")
            body["criteria"] = kwargs.get("criteria")
            del kwargs["criteria"]

        ret = api.create_or_update_resource_action(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.create_or_update_resource_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_resource_action(hub, ctx, p_id, **kwargs):
    """Delete resource action.  Performs DELETE /form-service/api/custom/resource-actions/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("DELETE /form-service/api/custom/resource-actions/{id}")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.delete_resource_action(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.delete_resource_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_action_form(hub, ctx, p_id, **kwargs):
    """Get resource actions form  Performs GET /form-service/api/custom/resource-actions/{id}/form


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /form-service/api/custom/resource-actions/{id}/form")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_action_form(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.get_action_form: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_action_runnable_item(hub, ctx, p_id, **kwargs):
    """Get resource action runnable item.  Performs GET /form-service/api/custom/resource-actions/{id}/runnable-item


    :param string p_id: (required in path)
    :param boolean includeCrResourceActions: (optional in query)
    """

    try:

        hub.log.debug(
            "GET /form-service/api/custom/resource-actions/{id}/runnable-item"
        )

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_action_runnable_item(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.get_action_runnable_item: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_binding_action(hub, ctx, q_resourceType, q_externalType, **kwargs):
    """Get binding action for given action resource type and external input type.  Performs GET /form-service/api/custom/resource-actions/binding-action


    :param string q_resourceType: (required in query) The action resource type
    :param string q_externalType: (required in query) The external input type
    """

    try:

        hub.log.debug("GET /form-service/api/custom/resource-actions/binding-action")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_binding_action(
            resource_type=q_resourceType, external_type=q_externalType, **kwargs
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.get_binding_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_action_form_data(hub, ctx, p_id, **kwargs):
    """Returns resolved binding values based on the properties of the resource for
      given resource operation  Performs POST /form-service/api/custom/resource-actions/{id}/form-data


    :param string p_id: (required in path) Resource operation id
    :param string projectId: (optional in query) The id of the project
    """

    try:

        hub.log.debug("POST /form-service/api/custom/resource-actions/{id}/form-data")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_resource_action_form_data(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.get_resource_action_form_data: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_resource_action(hub, ctx, p_id, **kwargs):
    """Get resource action.  Performs GET /form-service/api/custom/resource-actions/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /form-service/api/custom/resource-actions/{id}")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_resource_action(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.get_resource_action: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_actions(hub, ctx, **kwargs):
    """List resource actions.  Performs GET /form-service/api/custom/resource-actions


    :param string runnableId: (optional in query)
    :param None page: (optional in query)
    :param integer skip: (optional in query) Number of records you want to skip.
    """

    try:

        hub.log.debug("GET /form-service/api/custom/resource-actions")

        api = ResourceActionsApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.list_actions(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking ResourceActionsApi.list_actions: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
