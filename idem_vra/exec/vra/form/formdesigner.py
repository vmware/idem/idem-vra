from idem_vra.client.vra_form_lib.api import FormDesignerApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def fetch_customized_form(hub, ctx, **kwargs):
    """Fetch request form or generate one for a given library schema. Fetch a request form from the database, according to the search parameters.
      Generate one according to the JsonSchema if there isnt a persisted form. Performs POST /form-service/api/forms/designer/request


    :param string sourceType: (optional in query) The request source type
    :param string sourceId: (optional in query) The request source id
    :param string formType: (optional in query) The form type
    :param string formId: (optional in query) The request form id, used to find a form from a provider
    :param string type: (optional in body)
    :param boolean encrypted: (optional in body)
    :param boolean additionalProperties: (optional in body)
    :param string title: (optional in body)
    :param string description: (optional in body)
    :param boolean writeOnly: (optional in body)
    :param boolean readOnly: (optional in body)
    :param array allOf: (optional in body)
    :param array anyOf: (optional in body)
    :param array oneOf: (optional in body)
    :param Any not: (optional in body)
    :param Any items: (optional in body)
    :param boolean uniqueItems: (optional in body)
    :param integer maxItems: (optional in body)
    :param integer minItems: (optional in body)
    :param number maximum: (optional in body)
    :param number exclusiveMaximum: (optional in body)
    :param number minimum: (optional in body)
    :param number exclusiveMinimum: (optional in body)
    :param object properties: (optional in body)
    :param array required: (optional in body)
    :param integer maxProperties: (optional in body)
    :param integer minProperties: (optional in body)
    :param object patternProperties: (optional in body)
    :param integer maxLength: (optional in body)
    :param integer minLength: (optional in body)
    :param string pattern: (optional in body)
    :param string format: (optional in body)
    :param string formatMinimum: (optional in body)
    :param string formatMaximum: (optional in body)
    :param array enum: (optional in body)
    :param Any const: (optional in body)
    :param Any default: (optional in body)
    :param string $ref: (optional in body)
    :param string $data: (optional in body)
    :param string $dynamicDefault: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/designer/request")

        api = FormDesignerApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "encrypted" in kwargs:
            hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
            body["encrypted"] = kwargs.get("encrypted")
            del kwargs["encrypted"]
        if "additionalProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'additionalProperties' = {kwargs['additionalProperties']}"
            )
            body["additionalProperties"] = kwargs.get("additionalProperties")
            del kwargs["additionalProperties"]
        if "title" in kwargs:
            hub.log.debug(f"Got kwarg 'title' = {kwargs['title']}")
            body["title"] = kwargs.get("title")
            del kwargs["title"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "writeOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'writeOnly' = {kwargs['writeOnly']}")
            body["writeOnly"] = kwargs.get("writeOnly")
            del kwargs["writeOnly"]
        if "readOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'readOnly' = {kwargs['readOnly']}")
            body["readOnly"] = kwargs.get("readOnly")
            del kwargs["readOnly"]
        if "allOf" in kwargs:
            hub.log.debug(f"Got kwarg 'allOf' = {kwargs['allOf']}")
            body["allOf"] = kwargs.get("allOf")
            del kwargs["allOf"]
        if "anyOf" in kwargs:
            hub.log.debug(f"Got kwarg 'anyOf' = {kwargs['anyOf']}")
            body["anyOf"] = kwargs.get("anyOf")
            del kwargs["anyOf"]
        if "oneOf" in kwargs:
            hub.log.debug(f"Got kwarg 'oneOf' = {kwargs['oneOf']}")
            body["oneOf"] = kwargs.get("oneOf")
            del kwargs["oneOf"]
        if "not" in kwargs:
            hub.log.debug(f"Got kwarg 'not' = {kwargs['not']}")
            body["not"] = kwargs.get("not")
            del kwargs["not"]
        if "items" in kwargs:
            hub.log.debug(f"Got kwarg 'items' = {kwargs['items']}")
            body["items"] = kwargs.get("items")
            del kwargs["items"]
        if "uniqueItems" in kwargs:
            hub.log.debug(f"Got kwarg 'uniqueItems' = {kwargs['uniqueItems']}")
            body["uniqueItems"] = kwargs.get("uniqueItems")
            del kwargs["uniqueItems"]
        if "maxItems" in kwargs:
            hub.log.debug(f"Got kwarg 'maxItems' = {kwargs['maxItems']}")
            body["maxItems"] = kwargs.get("maxItems")
            del kwargs["maxItems"]
        if "minItems" in kwargs:
            hub.log.debug(f"Got kwarg 'minItems' = {kwargs['minItems']}")
            body["minItems"] = kwargs.get("minItems")
            del kwargs["minItems"]
        if "maximum" in kwargs:
            hub.log.debug(f"Got kwarg 'maximum' = {kwargs['maximum']}")
            body["maximum"] = kwargs.get("maximum")
            del kwargs["maximum"]
        if "exclusiveMaximum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMaximum' = {kwargs['exclusiveMaximum']}"
            )
            body["exclusiveMaximum"] = kwargs.get("exclusiveMaximum")
            del kwargs["exclusiveMaximum"]
        if "minimum" in kwargs:
            hub.log.debug(f"Got kwarg 'minimum' = {kwargs['minimum']}")
            body["minimum"] = kwargs.get("minimum")
            del kwargs["minimum"]
        if "exclusiveMinimum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMinimum' = {kwargs['exclusiveMinimum']}"
            )
            body["exclusiveMinimum"] = kwargs.get("exclusiveMinimum")
            del kwargs["exclusiveMinimum"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]
        if "required" in kwargs:
            hub.log.debug(f"Got kwarg 'required' = {kwargs['required']}")
            body["required"] = kwargs.get("required")
            del kwargs["required"]
        if "maxProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'maxProperties' = {kwargs['maxProperties']}")
            body["maxProperties"] = kwargs.get("maxProperties")
            del kwargs["maxProperties"]
        if "minProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'minProperties' = {kwargs['minProperties']}")
            body["minProperties"] = kwargs.get("minProperties")
            del kwargs["minProperties"]
        if "patternProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'patternProperties' = {kwargs['patternProperties']}"
            )
            body["patternProperties"] = kwargs.get("patternProperties")
            del kwargs["patternProperties"]
        if "maxLength" in kwargs:
            hub.log.debug(f"Got kwarg 'maxLength' = {kwargs['maxLength']}")
            body["maxLength"] = kwargs.get("maxLength")
            del kwargs["maxLength"]
        if "minLength" in kwargs:
            hub.log.debug(f"Got kwarg 'minLength' = {kwargs['minLength']}")
            body["minLength"] = kwargs.get("minLength")
            del kwargs["minLength"]
        if "pattern" in kwargs:
            hub.log.debug(f"Got kwarg 'pattern' = {kwargs['pattern']}")
            body["pattern"] = kwargs.get("pattern")
            del kwargs["pattern"]
        if "format" in kwargs:
            hub.log.debug(f"Got kwarg 'format' = {kwargs['format']}")
            body["format"] = kwargs.get("format")
            del kwargs["format"]
        if "formatMinimum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMinimum' = {kwargs['formatMinimum']}")
            body["formatMinimum"] = kwargs.get("formatMinimum")
            del kwargs["formatMinimum"]
        if "formatMaximum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMaximum' = {kwargs['formatMaximum']}")
            body["formatMaximum"] = kwargs.get("formatMaximum")
            del kwargs["formatMaximum"]
        if "enum" in kwargs:
            hub.log.debug(f"Got kwarg 'enum' = {kwargs['enum']}")
            body["enum"] = kwargs.get("enum")
            del kwargs["enum"]
        if "const" in kwargs:
            hub.log.debug(f"Got kwarg 'const' = {kwargs['const']}")
            body["const"] = kwargs.get("const")
            del kwargs["const"]
        if "default" in kwargs:
            hub.log.debug(f"Got kwarg 'default' = {kwargs['default']}")
            body["default"] = kwargs.get("default")
            del kwargs["default"]
        if "$ref" in kwargs:
            hub.log.debug(f"Got kwarg '$ref' = {kwargs['$ref']}")
            body["$ref"] = kwargs.get("$ref")
            del kwargs["$ref"]
        if "$data" in kwargs:
            hub.log.debug(f"Got kwarg '$data' = {kwargs['$data']}")
            body["$data"] = kwargs.get("$data")
            del kwargs["$data"]
        if "$dynamicDefault" in kwargs:
            hub.log.debug(f"Got kwarg '$dynamicDefault' = {kwargs['$dynamicDefault']}")
            body["$dynamicDefault"] = kwargs.get("$dynamicDefault")
            del kwargs["$dynamicDefault"]

        ret = api.fetch_customized_form(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDesignerApi.fetch_customized_form: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def generate_elements_runnable_item(hub, ctx, id, type, **kwargs):
    """Generate designer elements for a given runnable item.  Performs POST /form-service/api/forms/designer/runnable-item-elements


    :param string id: (required in body)
    :param string type: (required in body)
    :param string externalType: (optional in query) External type
    :param string name: (optional in body)
    :param string description: (optional in body)
    :param string projectId: (optional in body)
    :param array inputParameters: (optional in body)
    :param array outputParameters: (optional in body)
    :param string endpointLink: (optional in body)
    :param array inputBindings: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/designer/runnable-item-elements")

        api = FormDesignerApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}
        body["id"] = id
        body["type"] = type

        if "name" in kwargs:
            hub.log.debug(f"Got kwarg 'name' = {kwargs['name']}")
            body["name"] = kwargs.get("name")
            del kwargs["name"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "inputParameters" in kwargs:
            hub.log.debug(f"Got kwarg 'inputParameters' = {kwargs['inputParameters']}")
            body["inputParameters"] = kwargs.get("inputParameters")
            del kwargs["inputParameters"]
        if "outputParameters" in kwargs:
            hub.log.debug(
                f"Got kwarg 'outputParameters' = {kwargs['outputParameters']}"
            )
            body["outputParameters"] = kwargs.get("outputParameters")
            del kwargs["outputParameters"]
        if "endpointLink" in kwargs:
            hub.log.debug(f"Got kwarg 'endpointLink' = {kwargs['endpointLink']}")
            body["endpointLink"] = kwargs.get("endpointLink")
            del kwargs["endpointLink"]
        if "inputBindings" in kwargs:
            hub.log.debug(f"Got kwarg 'inputBindings' = {kwargs['inputBindings']}")
            body["inputBindings"] = kwargs.get("inputBindings")
            del kwargs["inputBindings"]

        ret = api.generate_elements_runnable_item(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDesignerApi.generate_elements_runnable_item: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def generate_elements(hub, ctx, **kwargs):
    """Generate designer elements for a given request schema.  Performs POST /form-service/api/forms/designer/elements


    :param string sourceId: (optional in query) The request source id
    :param string sourceType: (optional in query) The request source type
    :param string type: (optional in body)
    :param boolean encrypted: (optional in body)
    :param boolean additionalProperties: (optional in body)
    :param string title: (optional in body)
    :param string description: (optional in body)
    :param boolean writeOnly: (optional in body)
    :param boolean readOnly: (optional in body)
    :param array allOf: (optional in body)
    :param array anyOf: (optional in body)
    :param array oneOf: (optional in body)
    :param Any not: (optional in body)
    :param Any items: (optional in body)
    :param boolean uniqueItems: (optional in body)
    :param integer maxItems: (optional in body)
    :param integer minItems: (optional in body)
    :param number maximum: (optional in body)
    :param number exclusiveMaximum: (optional in body)
    :param number minimum: (optional in body)
    :param number exclusiveMinimum: (optional in body)
    :param object properties: (optional in body)
    :param array required: (optional in body)
    :param integer maxProperties: (optional in body)
    :param integer minProperties: (optional in body)
    :param object patternProperties: (optional in body)
    :param integer maxLength: (optional in body)
    :param integer minLength: (optional in body)
    :param string pattern: (optional in body)
    :param string format: (optional in body)
    :param string formatMinimum: (optional in body)
    :param string formatMaximum: (optional in body)
    :param array enum: (optional in body)
    :param Any const: (optional in body)
    :param Any default: (optional in body)
    :param string $ref: (optional in body)
    :param string $data: (optional in body)
    :param string $dynamicDefault: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/designer/elements")

        api = FormDesignerApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "type" in kwargs:
            hub.log.debug(f"Got kwarg 'type' = {kwargs['type']}")
            body["type"] = kwargs.get("type")
            del kwargs["type"]
        if "encrypted" in kwargs:
            hub.log.debug(f"Got kwarg 'encrypted' = {kwargs['encrypted']}")
            body["encrypted"] = kwargs.get("encrypted")
            del kwargs["encrypted"]
        if "additionalProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'additionalProperties' = {kwargs['additionalProperties']}"
            )
            body["additionalProperties"] = kwargs.get("additionalProperties")
            del kwargs["additionalProperties"]
        if "title" in kwargs:
            hub.log.debug(f"Got kwarg 'title' = {kwargs['title']}")
            body["title"] = kwargs.get("title")
            del kwargs["title"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "writeOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'writeOnly' = {kwargs['writeOnly']}")
            body["writeOnly"] = kwargs.get("writeOnly")
            del kwargs["writeOnly"]
        if "readOnly" in kwargs:
            hub.log.debug(f"Got kwarg 'readOnly' = {kwargs['readOnly']}")
            body["readOnly"] = kwargs.get("readOnly")
            del kwargs["readOnly"]
        if "allOf" in kwargs:
            hub.log.debug(f"Got kwarg 'allOf' = {kwargs['allOf']}")
            body["allOf"] = kwargs.get("allOf")
            del kwargs["allOf"]
        if "anyOf" in kwargs:
            hub.log.debug(f"Got kwarg 'anyOf' = {kwargs['anyOf']}")
            body["anyOf"] = kwargs.get("anyOf")
            del kwargs["anyOf"]
        if "oneOf" in kwargs:
            hub.log.debug(f"Got kwarg 'oneOf' = {kwargs['oneOf']}")
            body["oneOf"] = kwargs.get("oneOf")
            del kwargs["oneOf"]
        if "not" in kwargs:
            hub.log.debug(f"Got kwarg 'not' = {kwargs['not']}")
            body["not"] = kwargs.get("not")
            del kwargs["not"]
        if "items" in kwargs:
            hub.log.debug(f"Got kwarg 'items' = {kwargs['items']}")
            body["items"] = kwargs.get("items")
            del kwargs["items"]
        if "uniqueItems" in kwargs:
            hub.log.debug(f"Got kwarg 'uniqueItems' = {kwargs['uniqueItems']}")
            body["uniqueItems"] = kwargs.get("uniqueItems")
            del kwargs["uniqueItems"]
        if "maxItems" in kwargs:
            hub.log.debug(f"Got kwarg 'maxItems' = {kwargs['maxItems']}")
            body["maxItems"] = kwargs.get("maxItems")
            del kwargs["maxItems"]
        if "minItems" in kwargs:
            hub.log.debug(f"Got kwarg 'minItems' = {kwargs['minItems']}")
            body["minItems"] = kwargs.get("minItems")
            del kwargs["minItems"]
        if "maximum" in kwargs:
            hub.log.debug(f"Got kwarg 'maximum' = {kwargs['maximum']}")
            body["maximum"] = kwargs.get("maximum")
            del kwargs["maximum"]
        if "exclusiveMaximum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMaximum' = {kwargs['exclusiveMaximum']}"
            )
            body["exclusiveMaximum"] = kwargs.get("exclusiveMaximum")
            del kwargs["exclusiveMaximum"]
        if "minimum" in kwargs:
            hub.log.debug(f"Got kwarg 'minimum' = {kwargs['minimum']}")
            body["minimum"] = kwargs.get("minimum")
            del kwargs["minimum"]
        if "exclusiveMinimum" in kwargs:
            hub.log.debug(
                f"Got kwarg 'exclusiveMinimum' = {kwargs['exclusiveMinimum']}"
            )
            body["exclusiveMinimum"] = kwargs.get("exclusiveMinimum")
            del kwargs["exclusiveMinimum"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]
        if "required" in kwargs:
            hub.log.debug(f"Got kwarg 'required' = {kwargs['required']}")
            body["required"] = kwargs.get("required")
            del kwargs["required"]
        if "maxProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'maxProperties' = {kwargs['maxProperties']}")
            body["maxProperties"] = kwargs.get("maxProperties")
            del kwargs["maxProperties"]
        if "minProperties" in kwargs:
            hub.log.debug(f"Got kwarg 'minProperties' = {kwargs['minProperties']}")
            body["minProperties"] = kwargs.get("minProperties")
            del kwargs["minProperties"]
        if "patternProperties" in kwargs:
            hub.log.debug(
                f"Got kwarg 'patternProperties' = {kwargs['patternProperties']}"
            )
            body["patternProperties"] = kwargs.get("patternProperties")
            del kwargs["patternProperties"]
        if "maxLength" in kwargs:
            hub.log.debug(f"Got kwarg 'maxLength' = {kwargs['maxLength']}")
            body["maxLength"] = kwargs.get("maxLength")
            del kwargs["maxLength"]
        if "minLength" in kwargs:
            hub.log.debug(f"Got kwarg 'minLength' = {kwargs['minLength']}")
            body["minLength"] = kwargs.get("minLength")
            del kwargs["minLength"]
        if "pattern" in kwargs:
            hub.log.debug(f"Got kwarg 'pattern' = {kwargs['pattern']}")
            body["pattern"] = kwargs.get("pattern")
            del kwargs["pattern"]
        if "format" in kwargs:
            hub.log.debug(f"Got kwarg 'format' = {kwargs['format']}")
            body["format"] = kwargs.get("format")
            del kwargs["format"]
        if "formatMinimum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMinimum' = {kwargs['formatMinimum']}")
            body["formatMinimum"] = kwargs.get("formatMinimum")
            del kwargs["formatMinimum"]
        if "formatMaximum" in kwargs:
            hub.log.debug(f"Got kwarg 'formatMaximum' = {kwargs['formatMaximum']}")
            body["formatMaximum"] = kwargs.get("formatMaximum")
            del kwargs["formatMaximum"]
        if "enum" in kwargs:
            hub.log.debug(f"Got kwarg 'enum' = {kwargs['enum']}")
            body["enum"] = kwargs.get("enum")
            del kwargs["enum"]
        if "const" in kwargs:
            hub.log.debug(f"Got kwarg 'const' = {kwargs['const']}")
            body["const"] = kwargs.get("const")
            del kwargs["const"]
        if "default" in kwargs:
            hub.log.debug(f"Got kwarg 'default' = {kwargs['default']}")
            body["default"] = kwargs.get("default")
            del kwargs["default"]
        if "$ref" in kwargs:
            hub.log.debug(f"Got kwarg '$ref' = {kwargs['$ref']}")
            body["$ref"] = kwargs.get("$ref")
            del kwargs["$ref"]
        if "$data" in kwargs:
            hub.log.debug(f"Got kwarg '$data' = {kwargs['$data']}")
            body["$data"] = kwargs.get("$data")
            del kwargs["$data"]
        if "$dynamicDefault" in kwargs:
            hub.log.debug(f"Got kwarg '$dynamicDefault' = {kwargs['$dynamicDefault']}")
            body["$dynamicDefault"] = kwargs.get("$dynamicDefault")
            del kwargs["$dynamicDefault"]

        ret = api.generate_elements(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDesignerApi.generate_elements: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_external_value(hub, ctx, **kwargs):
    """Get a list of external values.  Performs POST /form-service/api/forms/designer/external-value/sources


    :param string filter: (optional in body)
    :param string externalSource: (optional in body)
    :param Any valueType: (optional in body)
    :param integer resultSize: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/forms/designer/external-value/sources")

        api = FormDesignerApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "filter" in kwargs:
            hub.log.debug(f"Got kwarg 'filter' = {kwargs['filter']}")
            body["filter"] = kwargs.get("filter")
            del kwargs["filter"]
        if "externalSource" in kwargs:
            hub.log.debug(f"Got kwarg 'externalSource' = {kwargs['externalSource']}")
            body["externalSource"] = kwargs.get("externalSource")
            del kwargs["externalSource"]
        if "valueType" in kwargs:
            hub.log.debug(f"Got kwarg 'valueType' = {kwargs['valueType']}")
            body["valueType"] = kwargs.get("valueType")
            del kwargs["valueType"]
        if "resultSize" in kwargs:
            hub.log.debug(f"Got kwarg 'resultSize' = {kwargs['resultSize']}")
            body["resultSize"] = kwargs.get("resultSize")
            del kwargs["resultSize"]

        ret = api.get_external_value(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking FormDesignerApi.get_external_value: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
