from idem_vra.client.vra_form_lib.api import DeploymentResourceTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def get_resource_type(hub, ctx, p_id, **kwargs):
    """Gets resource type by id. Get the resource type for the provided ID in the currently authorized
      organization Performs GET /form-service/api/deployment/resource-types/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("GET /form-service/api/deployment/resource-types/{id}")

        api = DeploymentResourceTypesApi(
            hub.clients["idem_vra.client.vra_form_lib.api"]
        )

        ret = api.get_resource_type(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentResourceTypesApi.get_resource_type: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_resource_types(hub, ctx, **kwargs):
    """List resource types. List all of the resource types in an organization Performs GET /form-service/api/deployment/resource-types


    :param None page: (optional in query)
    :param string search: (optional in query) Search by name and description
    """

    try:

        hub.log.debug("GET /form-service/api/deployment/resource-types")

        api = DeploymentResourceTypesApi(
            hub.clients["idem_vra.client.vra_form_lib.api"]
        )

        ret = api.list_resource_types(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking DeploymentResourceTypesApi.list_resource_types: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
