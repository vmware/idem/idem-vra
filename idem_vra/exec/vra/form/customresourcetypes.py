from idem_vra.client.vra_form_lib.api import CustomResourceTypesApi
from idem_vra.helpers.mapper import remap_response
from idem_vra.helpers.models import ExecReturn


async def create_or_update_custom_resource_type(hub, ctx, **kwargs):
    """Create custom resource type.  Performs POST /form-service/api/custom/resource-types


    :param string id: (optional in body)
    :param string displayName: (optional in body)
    :param string description: (optional in body)
    :param string resourceType: (optional in body)
    :param string externalType: (optional in body)
    :param string status: (optional in body)
    :param string orgId: (optional in body)
    :param string projectId: (optional in body)
    :param Any mainActions: (optional in body)
    :param array additionalActions: (optional in body)
    :param Any properties: (optional in body)
    :param string schemaType: (optional in body)
    :param string propertiesYaml: (optional in body)
    """

    try:

        hub.log.debug("POST /form-service/api/custom/resource-types")

        api = CustomResourceTypesApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        body = {}

        if "id" in kwargs:
            hub.log.debug(f"Got kwarg 'id' = {kwargs['id']}")
            body["id"] = kwargs.get("id")
            del kwargs["id"]
        if "displayName" in kwargs:
            hub.log.debug(f"Got kwarg 'displayName' = {kwargs['displayName']}")
            body["displayName"] = kwargs.get("displayName")
            del kwargs["displayName"]
        if "description" in kwargs:
            hub.log.debug(f"Got kwarg 'description' = {kwargs['description']}")
            body["description"] = kwargs.get("description")
            del kwargs["description"]
        if "resourceType" in kwargs:
            hub.log.debug(f"Got kwarg 'resourceType' = {kwargs['resourceType']}")
            body["resourceType"] = kwargs.get("resourceType")
            del kwargs["resourceType"]
        if "externalType" in kwargs:
            hub.log.debug(f"Got kwarg 'externalType' = {kwargs['externalType']}")
            body["externalType"] = kwargs.get("externalType")
            del kwargs["externalType"]
        if "status" in kwargs:
            hub.log.debug(f"Got kwarg 'status' = {kwargs['status']}")
            body["status"] = kwargs.get("status")
            del kwargs["status"]
        if "orgId" in kwargs:
            hub.log.debug(f"Got kwarg 'orgId' = {kwargs['orgId']}")
            body["orgId"] = kwargs.get("orgId")
            del kwargs["orgId"]
        if "projectId" in kwargs:
            hub.log.debug(f"Got kwarg 'projectId' = {kwargs['projectId']}")
            body["projectId"] = kwargs.get("projectId")
            del kwargs["projectId"]
        if "mainActions" in kwargs:
            hub.log.debug(f"Got kwarg 'mainActions' = {kwargs['mainActions']}")
            body["mainActions"] = kwargs.get("mainActions")
            del kwargs["mainActions"]
        if "additionalActions" in kwargs:
            hub.log.debug(
                f"Got kwarg 'additionalActions' = {kwargs['additionalActions']}"
            )
            body["additionalActions"] = kwargs.get("additionalActions")
            del kwargs["additionalActions"]
        if "properties" in kwargs:
            hub.log.debug(f"Got kwarg 'properties' = {kwargs['properties']}")
            body["properties"] = kwargs.get("properties")
            del kwargs["properties"]
        if "schemaType" in kwargs:
            hub.log.debug(f"Got kwarg 'schemaType' = {kwargs['schemaType']}")
            body["schemaType"] = kwargs.get("schemaType")
            del kwargs["schemaType"]
        if "propertiesYaml" in kwargs:
            hub.log.debug(f"Got kwarg 'propertiesYaml' = {kwargs['propertiesYaml']}")
            body["propertiesYaml"] = kwargs.get("propertiesYaml")
            del kwargs["propertiesYaml"]

        ret = api.create_or_update_custom_resource_type(body, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomResourceTypesApi.create_or_update_custom_resource_type: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def delete_custom_resource_type(hub, ctx, p_id, **kwargs):
    """Delete custom resource type.  Performs DELETE /form-service/api/custom/resource-types/{id}


    :param string p_id: (required in path)
    """

    try:

        hub.log.debug("DELETE /form-service/api/custom/resource-types/{id}")

        api = CustomResourceTypesApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.delete_custom_resource_type(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomResourceTypesApi.delete_custom_resource_type: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_custom_resource_type(hub, ctx, p_id, **kwargs):
    """Get custom resource type.  Performs GET /form-service/api/custom/resource-types/{id}


    :param string p_id: (required in path)
    :param string propertiesFormat: (optional in query)
    """

    try:

        hub.log.debug("GET /form-service/api/custom/resource-types/{id}")

        api = CustomResourceTypesApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_custom_resource_type(id=p_id, **kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomResourceTypesApi.get_custom_resource_type: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def get_r_esource_action_for_custom_resource(
    hub, ctx, p_resourceTypeId, p_resourceActionId, **kwargs
):
    """Get resource action for resource type.  Performs GET /form-service/api/custom/resource-types/{resourceTypeId}/resource-actions/{resourceActionId}


    :param string p_resourceTypeId: (required in path)
    :param string p_resourceActionId: (required in path)
    """

    try:

        hub.log.debug(
            "GET /form-service/api/custom/resource-types/{resourceTypeId}/resource-actions/{resourceActionId}"
        )

        api = CustomResourceTypesApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.get_r_esource_action_for_custom_resource(
            resource_type_id=p_resourceTypeId,
            resource_action_id=p_resourceActionId,
            **kwargs,
        )

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomResourceTypesApi.get_r_esource_action_for_custom_resource: {err}"
        )
        return ExecReturn(result=False, comment=str(err))


async def list_custom_resource_types(hub, ctx, **kwargs):
    """List custom resource types.  Performs GET /form-service/api/custom/resource-types


    :param string propertiesFormat: (optional in query)
    :param string runnableId: (optional in query)
    :param None page: (optional in query)
    """

    try:

        hub.log.debug("GET /form-service/api/custom/resource-types")

        api = CustomResourceTypesApi(hub.clients["idem_vra.client.vra_form_lib.api"])

        ret = api.list_custom_resource_types(**kwargs)

        return ExecReturn(result=True, ret=remap_response(ret))

    except Exception as err:
        hub.log.error(
            f"Exception occurred while invoking CustomResourceTypesApi.list_custom_resource_types: {err}"
        )
        return ExecReturn(result=False, comment=str(err))
