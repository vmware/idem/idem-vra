import asyncio
import base64
import os
import re
import time
import urllib.request
from datetime import datetime
from typing import Any
from typing import Dict

import jwt

import idem_vra.client.vra_abx_lib as vra_abx_lib
import idem_vra.client.vra_blueprint_lib as vra_blueprint_lib
import idem_vra.client.vra_catalog_lib as vra_catalog_lib
import idem_vra.client.vra_cmx_lib as vra_cmx_lib
import idem_vra.client.vra_form_lib as vra_form_lib
import idem_vra.client.vra_iaas_lib as vra_iaas_lib
import idem_vra.client.vra_identity_lib as vra_identity_lib
import idem_vra.client.vra_pipeline_lib as vra_pipeline_lib
import idem_vra.client.vra_platform_lib as vra_platform_lib
import idem_vra.client.vra_rbac_lib as vra_rbac_lib

PUBLIC_VRA_URL = "https://api.mgmt.cloud.vmware.com"

lock = asyncio.Lock()


async def gather(hub, profiles) -> Dict[str, Any]:
    """
    Given a refresh token, retrieve an access token.
    Any extra parameters will be saved as part of the profile.

    Example:

    .. code-block:: yaml
        vra:
          profile_name:
            refresh_token: <refresh token or API key>
            # optional configuration
            vra_url: https://api.mgmt.cloud.vmware.com
    """

    sub_profiles = {}

    # Use the profiles from an acct_file
    selected_profile = hub.OPT.idem.get("acct_profile", hub.acct.DEFAULT)
    if selected_profile in profiles.get("vra", {}):
        sub_profiles[selected_profile] = profiles.get("vra", {}).get(selected_profile)
    else:
        raise Exception(
            f'Unknown profile: {selected_profile}. Please define a profile "{selected_profile}" in your credentials file'
        )

    async with lock:

        if not hasattr(hub, "profiles"):
            hub.profiles = {}

        for profile, ctx in sub_profiles.items():

            if hub.profiles.get(profile) and not hub.acct.vra.token.token_will_expire(
                profile
            ):
                hub.log.debug(f"Reusing existing context for profile: {profile}")
                sub_profiles[profile] = hub.profiles.get(profile)
                continue

            try:

                # Fall back on to the public vRA url
                ctx["vra_url"] = ctx.get("vra_url", PUBLIC_VRA_URL)

                extras = hub.tool.vra.config.get_extras(ctx)

                hub.log.debug(f"connecting to vra with profile: {profile}")
                iaas_client = ClientFactory(hub, ctx, vra_iaas_lib, extras)

                # Login
                # Use the iaas_client to get the access token

                loginApi = vra_iaas_lib.LoginApi(iaas_client.get_client())
                auth_response = loginApi.retrieve_auth_token(
                    {
                        "refreshToken": iaas_client.get_config().get_api_key_with_prefix(
                            "refreshToken"
                        )
                    }
                )
                ctx["access_token"] = auth_response.token

                jwt_decoded = jwt.decode(
                    ctx["access_token"], options={"verify_signature": False}
                )
                ctx["orgId"] = jwt_decoded.get("context_name", "unknown")

                if not hub.acct.vra.token.is_cloud(ctx["vra_url"]):
                    jsonVersion = await hub.tool.vra.session.request(
                        ctx=ctx, method="GET", path="config.json", headers={}
                    )
                    if jsonVersion and jsonVersion["applicationVersion"]:
                        ctx["vra_version"] = base64.b64decode(
                            jsonVersion["applicationVersion"]
                        ).decode("utf-8")
                        match = re.search(r"\d+\.\d+\.\d+", ctx["vra_version"])
                        if match:
                            ctx["vra_version"] = match.group()

                        hub.log.debug(f"Appliance version: {ctx.vra_version}")
                    ctx["is_cloud"] = False
                else:
                    ctx["is_cloud"] = True

                # store the token in the context
                # EOF Login

                iaas_client.update_config(ctx["access_token"])

                catalog_client = ClientFactory(hub, ctx, vra_catalog_lib, extras)
                catalog_client.update_config(ctx["access_token"])

                blueprint_client = ClientFactory(hub, ctx, vra_blueprint_lib, extras)
                blueprint_client.update_config(ctx["access_token"])

                cmx_client = ClientFactory(hub, ctx, vra_cmx_lib, extras)
                cmx_client.update_config(ctx["access_token"])

                abx_client = ClientFactory(hub, ctx, vra_abx_lib, extras)
                abx_client.update_config(ctx["access_token"])

                pipeline_client = ClientFactory(hub, ctx, vra_pipeline_lib, extras)
                pipeline_client.update_config(ctx["access_token"])

                rbac_client = ClientFactory(hub, ctx, vra_rbac_lib, extras)
                rbac_client.update_config(ctx["access_token"])

                platform_client = ClientFactory(hub, ctx, vra_platform_lib, extras)
                platform_client.update_config(ctx["access_token"])

                form_client = ClientFactory(hub, ctx, vra_form_lib, extras)
                form_client.update_config(ctx["access_token"])

                ictx = dict(ctx)
                ictx["vra_url"] = hub.acct.vra.token.get_csp_base(ctx["vra_url"])

                identity_client = ClientFactory(hub, ictx, vra_identity_lib, extras)
                identity_client.update_config(ctx["access_token"])

                hub.clients = {
                    "idem_vra.client.vra_iaas_lib.api": iaas_client.get_client(),
                    "idem_vra.client.vra_catalog_lib.api": catalog_client.get_client(),
                    "idem_vra.client.vra_blueprint_lib.api": blueprint_client.get_client(),
                    "idem_vra.client.vra_cmx_lib.api": cmx_client.get_client(),
                    "idem_vra.client.vra_abx_lib.api": abx_client.get_client(),
                    "idem_vra.client.vra_pipeline_lib.api": pipeline_client.get_client(),
                    "idem_vra.client.vra_rbac_lib.api": rbac_client.get_client(),
                    "idem_vra.client.vra_platform_lib.api": platform_client.get_client(),
                    "idem_vra.client.vra_form_lib.api": form_client.get_client(),
                    "idem_vra.client.vra_identity_lib.api": identity_client.get_client(),
                }

                sub_profiles[profile] = ctx
                hub.profiles[profile] = ctx
                hub.log.debug(f"connected to vra with profile: {profile}")

            except Exception as e:
                hub.log.error(f"{e.__class__.__name__}: {e}")
                continue

    return sub_profiles


class ClientFactory:
    def __init__(self, hub, ctx, lib, extras):

        self.hub = hub
        self.lib = lib
        self.ctx = ctx

        self.config = self.lib.Configuration()
        self.config.host = self.ctx["vra_url"]
        self.config.debug = (
            extras.get("debug-clients", False) or hub.OPT.idem.debug_clients
        )
        self.config.verify_ssl = False
        proxies = urllib.request.getproxies()
        no_proxy = os.environ.get("no_proxy")

        if "https" in proxies:
            self.config.proxy = proxies.get("https")
        elif "http" in proxies:
            self.config.proxy = proxies.get("http")
        else:
            self.config.proxy = None

        # make no_proxy override
        if no_proxy and no_proxy in self.ctx["vra_url"]:
            self.config.proxy = None

        self.api_client = self.lib.ApiClient(self.config)
        self.config.api_key["refreshToken"] = self.ctx["refresh_token"]

    def get_config(self):
        return self.config

    def get_client(self):
        return self.api_client

    def update_config(self, access_token):
        self.config.api_key["Authorization"] = access_token
        self.config.api_key_prefix["Authorization"] = "Bearer"


def token_will_expire(hub, profile, threshold_in_minutes=10):
    access_token = hub.profiles.get(profile).get("access_token")
    now = int(time.time())
    threshold = threshold_in_minutes * 60
    if not access_token:
        hub.log.debug("Token not found. Assuming acquiring an initial token")
        return True

    decoded_token = jwt.decode(access_token, options={"verify_signature": False})
    token_expiration = int(decoded_token.get("exp", now))
    token_is_expiring = (now + threshold) > token_expiration
    if token_is_expiring:
        hub.log.debug(
            f"Token will expire soon: {datetime.fromtimestamp(token_expiration).isoformat()}"
        )

    return token_is_expiring


def is_cloud(hub, url):
    VRA_CLOUD_API_ENDPOINT = os.getenv("VRA_CLOUD_API_ENDPOINT", None)

    if VRA_CLOUD_API_ENDPOINT:
        return url.endswith(VRA_CLOUD_API_ENDPOINT)
    else:
        return url.endswith("cloud.vmware.com")


def get_csp_base(hub, url):

    VRA_CLOUD_CSP_ENDPOINT = os.getenv("VRA_CLOUD_CSP_ENDPOINT", None)

    # override CSP endpoint if defined in the environment
    if VRA_CLOUD_CSP_ENDPOINT and hub.acct.vra.token.is_cloud(url):
        return f"https://{VRA_CLOUD_CSP_ENDPOINT}"

    # return CSP endpoint if we are in cloud
    elif hub.acct.vra.token.is_cloud(url):
        return "https://console.cloud.vmware.com"

    # return the on-prem endpoint
    else:
        return url
