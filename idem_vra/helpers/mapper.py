import imp
import re
import textwrap
from collections.abc import MutableMapping
from copy import deepcopy
from functools import lru_cache

from jsonpath_ng.ext import parse


TO_BE_REMOVED_MARK = "To Be Removed"


def remap_response(response):
    """Convert response object into a serializable structure"""
    if not response:
        return response

    # dicts and lists passthrough
    if isinstance(response, dict) or isinstance(response, list):
        return response

    return to_good_dict(response)


def to_good_dict(o):
    """Returns the model properties as a dict with correct object names"""

    def val_to_dict(val):
        if hasattr(val, "to_dict"):
            return to_good_dict(val)
        elif isinstance(val, list):
            return list(
                map(lambda x: to_good_dict(x) if hasattr(x, "to_dict") else x, val)
            )
        else:
            return val

    result = {}
    o_map = o.attribute_map

    for attr, _ in o.swagger_types.items():
        value = getattr(o, attr)
        if isinstance(value, list):
            result[o_map[attr]] = list(
                map(lambda x: to_good_dict(x) if hasattr(x, "to_dict") else x, value)
            )
        elif hasattr(value, "to_dict"):
            result[o_map[attr]] = to_good_dict(value)
        elif isinstance(value, dict):
            result[o_map[attr]] = dict(
                map(lambda item: (item[0], val_to_dict(item[1])), value.items())
            )
        else:
            result[o_map[attr]] = value

    return result


def delete_keys_from_dict(dictionary, keys):
    """Deletes a set of keys from nested dictionaries"""
    if not callable(getattr(dictionary, "items", None)):
        return dictionary

    keys_set = set(keys)

    modified_dict = {}
    for key, value in dictionary.items():
        if key not in keys_set:
            if isinstance(value, MutableMapping):
                modified_dict[key] = delete_keys_from_dict(value, keys_set)
            elif isinstance(value, list):
                modified_dict[key] = [
                    delete_keys_from_dict(list_entry, keys_set) for list_entry in value
                ]
            else:
                modified_dict[
                    key
                ] = value  # or copy.deepcopy(value) if a copy is desired for non-dicts.

    return modified_dict


def delete_values_from_dict(dictionary, values):
    """Deletes a set of keys from nested dictionaries by matching their values"""
    if not callable(getattr(dictionary, "items", None)):
        return dictionary

    values_set = set(values)

    modified_dict = {}
    for key, value in dictionary.items():
        if isinstance(value, str) and value in values_set:
            continue
        if isinstance(value, MutableMapping):
            modified_dict[key] = delete_values_from_dict(value, values_set)
        elif isinstance(value, list):
            modified_dict[key] = [
                delete_values_from_dict(list_entry, values_set) for list_entry in value
            ]
        else:
            modified_dict[
                key
            ] = value  # or copy.deepcopy(value) if a copy is desired for non-dicts.

    return modified_dict


def mark_values_in_dict(dictionary, paths, mark):
    """Mark values from a dictionary that were matched using json path with a mark"""
    for path in paths:
        jsonpath_expression = cached_parse(path)
        for match in jsonpath_expression.find(dictionary):
            jsonpath_expression.update(dictionary, mark)

    return dictionary


def args_decoder(func):
    def wrapp(*args, **kwargs):
        args = decode_dict_values(args)
        kwargs = decode_dict_values(kwargs)
        return func(*args, **kwargs)

    return wrapp


def quote_curly(item):
    return item.replace("{", "%7B").replace("}", "%7D")


def unquote_curly(item):
    return item.replace("%7B", "{").replace("%7D", "}")


def encode_dict_values(dictionary):
    """Deletes a set of keys from nested dictionaries"""
    if not callable(getattr(dictionary, "items", None)):
        return dictionary

    modified_dict = {}
    for key, value in dictionary.items():
        if isinstance(value, MutableMapping):
            modified_dict[key] = encode_dict_values(value)
        elif isinstance(value, list):
            modified_dict[key] = [
                encode_dict_values(list_entry) for list_entry in value
            ]
        elif isinstance(value, dict):
            modified_dict[key] = dict(
                map(lambda item: (item[0], quote_curly(item[1])), value.items())
            )
        else:
            if isinstance(value, str):
                modified_dict[key] = quote_curly(value)
            else:
                modified_dict[key] = value
    return modified_dict


def decode_dict_values(dictionary):
    """Deletes a set of keys from nested dictionaries"""
    if not callable(getattr(dictionary, "items", None)):
        return dictionary

    modified_dict = {}
    for key, value in dictionary.items():
        if isinstance(value, MutableMapping):
            modified_dict[key] = decode_dict_values(value)
        elif isinstance(value, list):
            modified_dict[key] = [
                decode_dict_values(list_entry) for list_entry in value
            ]
        elif isinstance(value, dict):
            modified_dict[key] = dict(
                map(lambda item: (item[0], unquote_curly(item[1])), value.items())
            )
        else:
            modified_dict[key] = unquote_curly(str(value))

    return modified_dict


def omit_properties(dictionary, omit=[]):
    """Handle property omission during resource remapping

    Args:
        dictionary (dict): dictionary to process
        omit (list, optional): omission list. Defaults to [].

    Returns:
        dict: updated dictionary
    """

    dictionary = mark_values_in_dict(dictionary, omit, TO_BE_REMOVED_MARK)
    dictionary = delete_values_from_dict(dictionary, [TO_BE_REMOVED_MARK])
    return dictionary


def nested_set(dic, keys, value):
    """Insert a value in a nested address

    Args:
        dic (dict): dictionary to update
        keys (list): list of keys representing the address
        value (Any): value to insert
    """
    for key in keys[:-1]:
        if dic.get(key) == None:
            dic[key] = {}
        dic = dic.setdefault(key, {})
    dic[keys[-1]] = value


def resolve_nested_dict_values(source, dictionary=None):
    """Substitute values in a dictionary with their respective
    resolved values from a source.

    Args:
        source (dict): source dictionary
        dictionary (dict, optional): target dictionary. Defaults to None.

    Returns:
        dict: updated dictionary
    """
    if not dictionary:
        dictionary = deepcopy(source)

    modified_dict = {}
    for key, value in dictionary.items():
        if isinstance(value, str) and value.startswith("jsonpath:"):
            jsonpath_expression = cached_parse(value.replace("jsonpath:", "", 1))
            for match in jsonpath_expression.find(source):
                modified_dict[key] = match.value
        elif isinstance(value, MutableMapping):
            modified_dict[key] = resolve_nested_dict_values(source, value)
        elif isinstance(value, list):
            modified_dict[key] = [
                resolve_nested_dict_values(source, list_entry) for list_entry in value
            ]
        else:
            modified_dict[
                key
            ] = value  # or copy.deepcopy(value) if a copy is desired for non-dicts.

    return modified_dict


def resolve_template_string(source: dict, input: str) -> str:
    """Process any template expression in a string

    Args:
        source (dict): dictionary to use as source for any jsonpath matching
        input (str): template string

    Returns:
        str: resolved string
    """
    re_match = re.search(r"\$\{(.+)\}", input)
    if re_match:
        nested_expression = re_match.group(1)
        if nested_expression.startswith("jsonpath:"):
            jsonpath_expression = cached_parse(
                nested_expression.replace("jsonpath:", "", 1)
            )
            for match in jsonpath_expression.find(source):
                input = re.sub(re_match.re, match.value, input)
                break

    return input


async def add_properties(dictionary, add=[]):
    """Handle properties addition during resource remapping

    Args:
        dictionary (dict): dictionary to process
        add (list, optional): addition list. Defaults to [].

    Raises:
        Exception: unknown source schema
        Exception: wrong addition definition

    Returns:
        dict: updated dictionary
    """

    if len(add) == 0:
        return dictionary

    for addition in add:

        key = addition.get("key")
        value = addition.get("value")
        source = addition.get("source", "self")
        resolved_value = None

        # Assert addition
        if not key or not value:
            raise (f"Key and value are required for addition: {addition}")

        # Handle self sources
        if source == "self":

            # Process the result
            resolved_value = expand_value_expression(value, dictionary, dictionary)

        # Handle lambda sources
        elif source == "lambda":

            # Expand kwargs
            resolved_kwargs = resolve_nested_dict_values(
                dictionary, addition.get("kwargs", {})
            )

            # Call the lambda
            lambda_code = addition.get("lambda", "pass")
            lambda_result = run_lambda_function(lambda_code, resolved_kwargs)

            # Post-process the result from the lambda
            resolved_value = expand_value_expression(value, lambda_result, dictionary)

        # Handle callable sources
        elif callable(source):

            # Expand kwargs
            resolved_kwargs = resolve_nested_dict_values(
                dictionary, addition.get("kwargs", {})
            )

            # Call the callable
            res = await source(addition.get("ctx"), **resolved_kwargs)

            # Post-process the result from the callable
            resolved_value = expand_value_expression(
                value,
                res.get("ret", res) if getattr(res, "get", None) else res,
                dictionary,
            )

        # Handle unknown source schemas
        else:
            raise Exception(f"Unknown source schema: {source}")

        # Assert that the value is properly resolved
        if resolved_value == None:
            raise Exception(f'Could not resolve value for "{value}"')

        # Insert the resolved value at the proper address
        nested_set(dictionary, key.split("."), resolved_value)

    return dictionary


def expand_value_expression(value, dictionary_to_expand, dictionary_for_expressions):
    """Expand a value expression from a dictionary as source.

    Args:
        value (str): json path expression or a constant
        dictionary_to_expand (dict): dictionary to use as source for the expansion
        dictionary_for_expressions (dict): dictionary to use as source for nested expressions within the value
            expression, e.g. 'jsonpath:enabledRegions[?externalRegionId=="${jsonpath:externalRegionId}"].id'.
            The inner json path expression will be expanded from the 'dictionary_for_expression' argument.

    Returns:
        any: expanded value
    """
    resolved_value = None

    # Resolve a value from a json path
    if isinstance(value, str) and value.startswith("jsonpath:"):
        query = resolve_template_string(
            dictionary_for_expressions, value.replace("jsonpath:", "", 1)
        )
        jsonpath_expression = cached_parse(query)
        for match in jsonpath_expression.find(dictionary_to_expand):
            resolved_value = match.value
            break
        if resolved_value == None:
            resolved_value = TO_BE_REMOVED_MARK

    # Resolve value as a constant
    else:
        resolved_value = value

    return resolved_value


def run_lambda_function(code, kwargs):
    """Dynamically create a function from source code with the
    specified arguments and execute it, returning its return value.
    The source code is auto-indented to create a valid Python syntax.

    Args:
        code (str): source code
        kwargs (dict): function arguments

    Returns:
        any: function's return value
    """
    args = ", ".join(kwargs.keys())
    source = f"def lambda_function({args}):\n"
    source += textwrap.indent(code, 2 * " ")

    module = imp.new_module("lambda_functions")
    exec(source, module.__dict__)
    return_value = module.lambda_function(**kwargs)
    return return_value


@lru_cache(512)
def cached_parse(obj):
    return parse(obj)
