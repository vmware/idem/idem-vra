# Auto-generated virtuale state module

__contracts__ = ["resource"]

TREQ = {"present": {"require": []}, "absent": {"require": []}}


# ====================================
# State implementation
# ====================================
# Auto-generated virtuale state module

__contracts__ = ["resource"]

TREQ = {"present": {"require": []}, "absent": {"require": []}}


# ====================================
# State implementation
# ====================================
# Auto-generated virtuale state module

__contracts__ = ["resource"]

TREQ = {"present": {"require": []}, "absent": {"require": []}}


# ====================================
# State implementation
# ====================================
from idem_vra.helpers.models import StateReturn
from idem_vra.helpers.identityroles import *


async def present(
    hub,
    ctx,
    name: str,
    id: str,
    type: str,
    domain: str,
    orgRoleNames: list = [],
    svcRoleNames: list = [],
    **kwargs,
):
    """

    :param string name: (required) A human-friendly resource name used in APIs that support this option.

    :param string id: (required) An internal resource identifier used in APIs that support this option.

    :param string type: (required) The type of the identity role virtual resource (user or group) used to determine the required API calls.

    :param string domain: (required) A human-friendly resource domain used in APIs that support this option.

    :param list orgRoleNames: (required) A human-friendly list of organization role names used in APIs that support this
      option.

    :param list svcRoleNames: (required) A human-friendly list of service role names used in APIs that support this
      option.

    """
    try:

        orgId = ctx["acct"]["orgId"]

        # Enumerate the internal ids for all organization and service roles.
        # Cache the results to avoid duplicate calls for each virtual resource
        ##########################################################################
        if get_enforced_state("org_roles") is None:
            ctx["cache"] = {}

            # Get the internal organization role ids
            existing_org_roles = (
                await hub.exec.vra.identity.organizationcontroller.get_org_roles(
                    ctx, orgId, **kwargs
                )
            )
            for org_role in existing_org_roles.get("ret", {}).get("orgRolesData", []):
                org_role_name = org_role.get("name", "unknown")
                org_role_id = org_role.get("refLink", "unknown").rsplit("/", 1)[-1]
                ctx["cache"].update([(org_role_name, org_role_id)])

            # cache data
            post_present(hub, ctx, "org_roles")

        if get_enforced_state("svc_roles") is None:
            ctx["cache"] = {}

            # Get the internal service role ids
            existing_service_roles = await hub.exec.vra.identity.servicedefinitionv2controller.get_all_by_org_service_definitions(
                ctx, orgId, **kwargs
            )
            for service in existing_service_roles.get("ret", {}).get("results", []):
                for svc_role in service.get("serviceRoles", []):
                    svc_role_name = svc_role.get("name", "unknown")
                    svc_role_id = svc_role.get(
                        "serviceDefinitionLink", "unknown"
                    ).rsplit("/", 1)[-1]
                    ctx["cache"].update([(svc_role_name, svc_role_id)])

            post_present(hub, ctx, "svc_roles")
        ##########################################################################

        # Process Group Organization and Service Roles
        if type == "group":

            ret_group_id = None

            # Get the internal group id
            group_search_res = (
                await hub.exec.vra.identity.groupcontroller.search_groups(
                    ctx, name, **kwargs
                )
            )
            for group in group_search_res.get("ret", {}).get("results", []):
                if (
                    group.get("displayName") == name
                    and (
                        group.get("groupType") == "AD_GROUP"
                        or group.get("groupType") == "USER_GROUP"
                    )
                    and group.get("domain").lower() == domain.lower()
                ):
                    ret_group_id = group.get("id", "unknown")
                    ret_group_name = group.get("displayName", "unknown")
                    ret_group_domain = group.get("domain", "unknown")

            # The AD Group is present in Identity Manager
            if ret_group_id is not None:
                # Collect the group organization and service roles
                group_roles = await hub.exec.vra.identity.organizationgroupscontroller.get_organization_groups(
                    ctx, orgId, **kwargs
                )
                group_roles_count = len(group_roles.get("ret", {}).get("results", []))

                # The group already has some roles assigned
                if group_roles_count > 0:
                    for roles_data in group_roles.get("ret", {}).get("results", []):

                        hub.log.debug(f"The Group [{name}] has assigned roles!")

                        # Collect the organization role names
                        ret_group_org_role_names = []
                        for org_role in roles_data.get("organizationRoles", []):
                            org_role_name = org_role.get("name", "unknown")
                            ret_group_org_role_names.append(org_role_name)

                        # Collect the service role names
                        ret_group_svc_roles_names = []
                        for service in roles_data.get("serviceRoles", []):
                            svc_roles = service.get("serviceRoleNames", [])
                            for svc_role in svc_roles:
                                ret_group_svc_roles_names.append(svc_role)

                        if (set(orgRoleNames) != set(ret_group_org_role_names)) or (
                            set(svcRoleNames) != set(ret_group_svc_roles_names)
                        ):

                            # Delete the existing roles (organization and service)
                            kwargs["ids"] = [ret_group_id]
                            remove_groups_result = await hub.exec.vra.identity.organizationgroupscontroller.remove_groups_from_organization(
                                ctx, orgId, **kwargs
                            )
                            if remove_groups_result.get("result", False) == True:

                                del kwargs["ids"]

                                # Prepare to update the group roles
                                update_groups_result = (
                                    update_org_role_payload,
                                    update_group_svc_payload,
                                ) = prepare_group_roles_payload(
                                    orgRoleNames, svcRoleNames
                                )
                                if update_groups_result.get("result", False) == True:
                                    kwargs["organizationRoles"] = {
                                        "rolesToAdd": update_org_role_payload,
                                        "rolesToRemove": [],
                                    }
                                    kwargs["serviceRoles"] = update_group_svc_payload

                                    # Update the virtual resource group roles
                                    update_virtual_result = await hub.exec.vra.identity.organizationgroupscontroller.update_group_roles_on_organization(
                                        ctx, orgId, ret_group_id, **kwargs
                                    )
                                    if (
                                        update_virtual_result.get("result", False)
                                        == True
                                    ):
                                        del kwargs["organizationRoles"]
                                        del kwargs["serviceRoles"]

                                        return StateReturn(
                                            result=True,
                                            comment=f"Resource updated.",
                                            old=dict(
                                                id=ret_group_id,
                                                name=ret_group_name,
                                                domain=ret_group_domain,
                                                orgRoleNames=ret_group_org_role_names,
                                                svcRoleNames=ret_group_svc_roles_names,
                                            ),
                                            new=dict(
                                                id=id,
                                                name=name,
                                                domain=domain,
                                                orgRoleNames=orgRoleNames,
                                                svcRoleNames=svcRoleNames,
                                            ),
                                        )
                                    else:
                                        return StateReturn(
                                            result=False,
                                            comment=f"Errors during update virtual resource group roles.",
                                        )
                                else:
                                    return StateReturn(
                                        result=False,
                                        comment=f"Errors during update group roles.",
                                    )
                            else:
                                return StateReturn(
                                    result=False,
                                    comment=f"Errors during existing group roles removal.",
                                )
                        else:
                            # The group roles are already up-to date
                            return StateReturn(
                                result=True,
                                comment=f"Resource state is already up-to-date.",
                                old=dict(
                                    id=ret_group_id,
                                    name=ret_group_name,
                                    domain=ret_group_domain,
                                    orgRoleNames=orgRoleNames,
                                    svcRoleNames=svcRoleNames,
                                ),
                                new=dict(
                                    id=id,
                                    name=name,
                                    domain=domain,
                                    orgRoleNames=orgRoleNames,
                                    svcRoleNames=svcRoleNames,
                                ),
                            )

                else:
                    hub.log.error(f"The Group [{name}] has no assigned roles!")

                    # Prepare to create the group roles
                    (
                        create_org_role_payload,
                        create_group_svc_payload,
                    ) = prepare_group_roles_payload(orgRoleNames, svcRoleNames)

                    kwargs["organizationRoles"] = {
                        "rolesToAdd": create_org_role_payload,
                        "rolesToRemove": [],
                    }
                    kwargs["serviceRoles"] = create_group_svc_payload

                    # Create the virtual resource group roles
                    create_virtual_resource = await hub.exec.vra.identity.organizationgroupscontroller.update_group_roles_on_organization(
                        ctx, orgId, ret_group_id, **kwargs
                    )
                    if create_virtual_resource.get("result", False) == True:
                        del kwargs["organizationRoles"]
                        del kwargs["serviceRoles"]

                        return StateReturn(
                            result=True,
                            comment=f"Resource created.",
                            old=dict(
                                id=ret_group_id,
                                name=ret_group_name,
                                domain=ret_group_domain,
                                orgRoleNames=[],
                                svcRoleNames=[],
                            ),
                            new=dict(
                                id=id,
                                name=name,
                                domain=domain,
                                orgRoleNames=orgRoleNames,
                                svcRoleNames=svcRoleNames,
                            ),
                        )
                    else:
                        return StateReturn(
                            result=False,
                            comment=f"Errors during virtual resource creation.",
                        )
            else:
                hub.log.error(
                    f"The AD Group [{name}] is not available in Identity Manager..Skipping the roles assignment!"
                )
                return StateReturn(
                    result=False,
                    comment=f"Errors during identityrolestate enforcement.",
                    old={},
                    new={},
                )

        elif type == "user":

            ret_user_id = None

            # Get the user internal id
            user_search_res = (
                await hub.exec.vra.identity.organizationuserscontroller.search_users(
                    ctx, orgId, name, **kwargs
                )
            )
            for user in user_search_res.get("ret", {}).get("results", []):
                if (
                    user.get("user").get("acct") == name
                    and user.get("user").get("domain").lower() == domain.lower()
                ):
                    ret_user_id = user.get("user").get("userId", "unknown")
                    ret_user_name = user.get("user").get("acct", "unknown")
                    ret_user_domain = user.get("user").get("domain", "unknown")

            # The AD User is present in Identity Manager
            if ret_user_id:

                # Get the current user organization and service roles
                user_roles = await hub.exec.vra.identity.usercontroller.get_user_roles_on_org_with_group_info(
                    ctx, ret_user_id, orgId, **kwargs
                )
                user_org_roles = user_roles.get("ret", {}).get("organizationRoles", [])
                user_svc_roles = user_roles.get("ret", {}).get("serviceRoles", [])
                user_org_role_count = len(user_org_roles)
                user_svc_roles_count = len(user_svc_roles)

                # The user has an organization role assigned
                if user_org_role_count > 0:
                    hub.log.debug(
                        f"The User [{name}] has an assigned organization role!"
                    )
                    ret_user_org_role_names = []
                    for user_org_role in user_org_roles:
                        if user_org_role.get("membershipType", "unknown") == "DIRECT":
                            ret_user_org_role_names = [
                                user_org_role.get("name", "uknown")
                            ]
                else:
                    ret_user_org_role_names = []

                if user_svc_roles_count > 0:
                    hub.log.debug(f"The User [{name}] has some assigned service roles!")
                    ret_user_svc_roles_names = []
                    # Collect the user service role names
                    for service_roles in user_svc_roles:
                        for service_role in service_roles.get("serviceRoles", []):
                            if (
                                service_role.get("membershipType", "unknown")
                                == "DIRECT"
                            ):
                                ret_user_svc_roles_names.append(
                                    service_role.get("name", "unknown")
                                )
                else:
                    ret_user_svc_roles_names = []

                if user_org_role_count > 0 or user_svc_roles_count > 0:

                    if (set(orgRoleNames) != set(ret_user_org_role_names)) or (
                        set(svcRoleNames) != set(ret_user_svc_roles_names)
                    ):
                        userList, user_svc_payload = prepare_user_roles_payload(
                            hub,
                            ret_user_org_role_names,
                            ret_user_svc_roles_names,
                            ret_user_name,
                            "remove",
                        )
                        kwargs["servicesRolesPatchRequest"] = {
                            "serviceRolesPatchRequest": user_svc_payload
                        }

                        # Delete the existing user roles
                        patch_delete_result = await hub.exec.vra.identity.organizationcontroller.patch_org_roles(
                            ctx, orgId, userList, **kwargs
                        )
                        if patch_delete_result.get("result", False) == True:
                            del kwargs["servicesRolesPatchRequest"]

                            # Update the user roles
                            userList, user_svc_payload = prepare_user_roles_payload(
                                hub, orgRoleNames, svcRoleNames, ret_user_name, "add"
                            )
                            kwargs["servicesRolesPatchRequest"] = {
                                "serviceRolesPatchRequest": user_svc_payload
                            }

                            patch_update_result = await hub.exec.vra.identity.organizationcontroller.patch_org_roles(
                                ctx, orgId, userList, **kwargs
                            )
                            if patch_update_result.get("result", False) == True:
                                del kwargs["servicesRolesPatchRequest"]

                                return StateReturn(
                                    result=True,
                                    comment=f"Resource updated.",
                                    old=dict(
                                        id=ret_user_id,
                                        name=ret_user_name,
                                        domain=ret_user_domain,
                                        orgRoleNames=ret_user_org_role_names,
                                        svcRoleNames=ret_user_svc_roles_names,
                                    ),
                                    new=dict(
                                        id=id,
                                        name=name,
                                        domain=domain,
                                        orgRoleNames=orgRoleNames,
                                        svcRoleNames=svcRoleNames,
                                    ),
                                )
                            else:
                                return StateReturn(
                                    result=False,
                                    comment=f"Errors during patch_org_roles update.",
                                )
                        else:
                            return StateReturn(
                                result=False,
                                comment=f"Errors during patch_org_roles delete.",
                            )
                    else:
                        # The user roles are already up-to date
                        return StateReturn(
                            result=True,
                            comment=f"Resource state is already up-to-date.",
                            old=dict(
                                id=ret_user_id,
                                name=ret_user_name,
                                domain=ret_user_domain,
                                orgRoleNames=orgRoleNames,
                                svcRoleNames=svcRoleNames,
                            ),
                            new=dict(
                                id=id,
                                name=name,
                                domain=domain,
                                orgRoleNames=orgRoleNames,
                                svcRoleNames=svcRoleNames,
                            ),
                        )
                else:
                    hub.log.error(f"The User [{name}] has no assigned roles!")

                    # Create the user roles
                    userList, user_svc_payload = prepare_user_roles_payload(
                        hub, orgRoleNames, svcRoleNames, ret_user_name, "add"
                    )
                    kwargs["servicesRolesPatchRequest"] = {
                        "serviceRolesPatchRequest": user_svc_payload
                    }

                    patch_create_result = await hub.exec.vra.identity.organizationcontroller.patch_org_roles(
                        ctx, orgId, userList, **kwargs
                    )
                    if patch_create_result.get("result", False) == True:
                        del kwargs["servicesRolesPatchRequest"]

                        return StateReturn(
                            result=True,
                            comment=f"Resource created.",
                            old=dict(
                                id=ret_user_id,
                                name=ret_user_name,
                                domain=ret_user_domain,
                                orgRoleNames=[],
                                svcRoleNames=[],
                            ),
                            new=dict(
                                id=id,
                                name=name,
                                domain=domain,
                                orgRoleNames=orgRoleNames,
                                svcRoleNames=svcRoleNames,
                            ),
                        )
                    else:
                        return StateReturn(
                            result=False,
                            comment=f"Errors during patch_org_roles create.",
                        )
            else:
                hub.log.error(
                    "The AD user is not present in Identity Manger! Skipping.."
                )
        else:
            hub.log.error(
                f"Unsupported type [{type}]. Supported types:  [group, user]!"
            )

        return StateReturn(
            result=False,
            comment=f"Errors during identityrolestate enforcement.",
            old={},
            new={},
        )

    except Exception as error:
        hub.log.error("Error during enforcing present state: identityrolestate")
        hub.log.error(str(error))
        raise error


async def absent(
    hub,
    ctx,
    name: str,
    id: str,
    domain: str,
    orgRoleNames: list = [],
    svcRoleNames: list = [],
    **kwargs,
):
    pass


async def describe(hub, ctx):

    try:
        result = {}

        orgId = ctx["acct"]["orgId"]

        groups = await hub.exec.vra.identity.organizationgroupscontroller.get_organization_groups(
            ctx, orgId
        )
        for group in groups.get("ret", {}).get("results", []):
            ret_group_name = group.get("displayName", "unknown")
            ret_group_id = group.get("id", "unknown")
            ret_group_domain = group.get("domain", "unknown")
            ret_group_org_roles = group.get("organizationRoles", [])
            ret_group_svc_roles = group.get("serviceRoles", [])

            # Collect the group organization role names
            group_org_role_names = []
            for role in ret_group_org_roles:
                role_name = role.get("name", "unknown")
                group_org_role_names.append(role_name)

            # Collect the group service role names
            group_svc_roles_names = []
            for service in ret_group_svc_roles:
                svc_roles = service.get("serviceRoleNames", [])
                for role in svc_roles:
                    group_svc_roles_names.append(role)

            if len(group_svc_roles_names) > 0 or len(group_org_role_names):
                result[f"virtual-identityrole-group-{ret_group_id.split('-')[-1]}"] = {
                    "vra.virtual.identityrolestate.present": [
                        {"name": ret_group_name},
                        {"id": ret_group_id},
                        {"type": "group"},
                        {"domain": ret_group_domain},
                        {"orgRoleNames": group_org_role_names},
                        {"svcRoleNames": group_svc_roles_names},
                    ]
                }
            else:
                hub.log.error("Skipping groups with no roles assigned")

        users = await hub.exec.vra.identity.organizationuserscontroller.get_paginated_org_users_info(
            ctx, orgId
        )
        for user in users.get("ret", {}).get("results", []):
            ret_user_name = user["user"].get("username", "unknown")
            ret_user_id = user["user"].get("userId", "unknown")
            ret_user_domain = user["user"].get("domain", "unknown")
            ret_user_org_roles = user.get("organizationRoles", [])
            ret_user_svc_services = user.get("serviceRoles", [])

            # Collect the user organization role names
            user_org_role_names = []
            for role in ret_user_org_roles:
                role_name = role.get("name", "unknown")
                role_membership = role.get("membershipType", "unknown")
                if role_membership == "DIRECT":
                    user_org_role_names.append(role_name)

            # Collect the user service role names
            user_svc_roles_names = []
            for service in ret_user_svc_services:
                svc_roles = service.get("serviceRoles", [])
                for role in svc_roles:
                    role_membership = role.get("membershipType", "unknown")
                    if role_membership == "DIRECT":
                        role_name = role.get("name", "unknown")
                        user_svc_roles_names.append(role_name)

            if len(user_svc_roles_names) > 0 or len(user_org_role_names):
                result[f"virtual-identityrole-user-{ret_user_id.split('-')[-1]}"] = {
                    "vra.virtual.identityrolestate.present": [
                        {"name": ret_user_name},
                        {"id": ret_user_id},
                        {"domain": ret_user_domain},
                        {"type": "user"},
                        {"orgRoleNames": user_org_role_names},
                        {"svcRoleNames": user_svc_roles_names},
                    ]
                }
            else:
                hub.log.warning("Skipping users with no roles assigned!")

        return result

    except Exception as error:
        hub.log.error("Error during describe: identityrolestate")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    return False
