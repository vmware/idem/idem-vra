# Auto-generated virtuale state module

__contracts__ = ["resource"]

TREQ = {
    "present": {"require": ["vra.iaas.cloudaccount.present"]},
    "absent": {"require": []},
}


# ====================================
# State implementation
# ====================================
from idem_vra.helpers.models import StateReturn
from dict_tools import differ


async def present(
    hub,
    ctx,
    name: str,
    subnetId: str,
    ipv6Cidr: str = None,
    isDefault: bool = None,
    domain: str = None,
    defaultIpv6Gateway: str = None,
    dnsServerAddresses: dict = None,
    isPublic: bool = None,
    cidr: str = None,
    defaultGateway: str = None,
    tags: list[dict[str, str]] = [],
    dnsSearchDomains: dict = None,
    **kwargs,
):
    try:

        tags = tags or []

        # get existing fabric network
        existing = await hub.exec.vra.iaas.fabricnetwork.get_vsphere_fabric_network(
            ctx, p_id=subnetId
        )
        existing_tags = existing.ret.tags or []

        if ipv6Cidr is None:
            ipv6Cidr = existing.ret.ipv6Cidr
        if isDefault is None:
            isDefault = existing.ret.isDefault
        if domain is None:
            domain = existing.ret.domain
        if defaultIpv6Gateway is None:
            defaultIpv6Gateway = existing.ret.defaultIpv6Gateway
        if dnsServerAddresses is None:
            dnsServerAddresses = existing.ret.dnsServerAddresses
        if isPublic is None:
            isPublic = existing.ret.isPublic
        if cidr is None:
            cidr = existing.ret.cidr
        if defaultGateway is None:
            defaultGateway = existing.ret.defaultGateway
        if dnsSearchDomains is None:
            dnsSearchDomains = existing.ret.dnsSearchDomains

        existing_state = dict(
            ipv6Cidr=existing.ret.ipv6Cidr,
            isDefault=existing.ret.isDefault,
            domain=existing.ret.domain,
            defaultIpv6Gateway=existing.ret.defaultIpv6Gateway,
            dnsServerAddresses=existing.ret.dnsServerAddresses,
            isPublic=existing.ret.isPublic,
            cidr=existing.ret.cidr,
            defaultGateway=existing.ret.defaultGateway,
            dnsSearchDomains=existing.ret.dnsSearchDomains,
        )

        new_state = dict(
            ipv6Cidr=ipv6Cidr,
            isDefault=isDefault,
            domain=domain,
            defaultIpv6Gateway=defaultIpv6Gateway,
            dnsServerAddresses=dnsServerAddresses,
            isPublic=isPublic,
            cidr=cidr,
            defaultGateway=defaultGateway,
            dnsSearchDomains=dnsSearchDomains,
        )

        diff = differ.deep_diff(existing_state, new_state)

        # normalize the passed tags
        for tag in tags:
            if "key" not in tag:
                tag["key"] = ""
            if "value" not in tag:
                tag["value"] = ""

        # add tags that are passed but are not in the existing tags
        tags_to_add = [tag for tag in tags if tag not in existing_tags]

        if len(diff.keys()) > 0 or len(tags_to_add) > 0:

            # build full list of tags for the update
            full_tags_definition = existing_tags + tags_to_add

            # issue the update
            kwargs.update(new_state)
            update_result = (
                await hub.exec.vra.iaas.fabricnetwork.updatev_sphere_fabric_network(
                    ctx, p_id=subnetId, tags=full_tags_definition, **kwargs
                )
            )

            return StateReturn(
                result=True,
                comment=f"Resource state updated.",
                old=dict(
                    subnetId=existing.ret.id, tags=existing_tags, **existing_state
                ),
                new=dict(
                    subnetId=update_result.ret.id,
                    tags=update_result.ret.tags,
                    **new_state,
                ),
            )

        else:

            resource = dict(
                subnetId=existing.ret.id, tags=existing_tags, **existing_state
            )

            return StateReturn(
                result=True,
                comment=f"Resource state is already up-to-date.",
                old=resource,
                new=resource,
            )

    except Exception as error:
        hub.log.error("Error during enforcing present state: vspherefabricnetworkstate")
        hub.log.error(str(error))
        raise error


async def absent(
    hub, ctx, name: str, subnetId: str, tags: list[dict[str, str]] = [], **kwargs
):
    try:

        tags = tags or []

        # get existing fabric network
        existing = await hub.exec.vra.iaas.fabricnetwork.get_vsphere_fabric_network(
            ctx, p_id=subnetId
        )
        existing_tags = existing.ret.tags or []

        # Build the existing state only for reference
        existing_state = dict(
            ipv6Cidr=existing.ret.ipv6Cidr,
            isDefault=existing.ret.isDefault,
            domain=existing.ret.domain,
            defaultIpv6Gateway=existing.ret.defaultIpv6Gateway,
            dnsServerAddresses=existing.ret.dnsServerAddresses,
            isPublic=existing.ret.isPublic,
            cidr=existing.ret.cidr,
            defaultGateway=existing.ret.defaultGateway,
            dnsSearchDomains=existing.ret.dnsSearchDomains,
        )

        # The new state is intended to rest the values of all properties
        new_state = dict(
            ipv6Cidr="",
            isDefault=False,
            domain="",
            defaultIpv6Gateway="",
            dnsServerAddresses=[],
            isPublic=False,
            cidr="",
            defaultGateway="",
            dnsSearchDomains=[],
        )

        # normalize the passed tags
        for tag in tags:
            if "key" not in tag:
                tag["key"] = ""
            if "value" not in tag:
                tag["value"] = ""

        # remove tags that are not passed but are in the existing tags
        tags_to_remove = [tag for tag in existing_tags if tag in tags]

        # build full list of tags for the update
        full_tags_definition = [
            tag for tag in existing_tags if tag not in tags_to_remove
        ]

        # issue the update
        kwargs.update(new_state)
        update_result = (
            await hub.exec.vra.iaas.fabricnetwork.updatev_sphere_fabric_network(
                ctx, p_id=subnetId, tags=full_tags_definition, **kwargs
            )
        )

        return StateReturn(
            result=True,
            comment=f"Resource state updated.",
            old=dict(subnetId=existing.ret.id, tags=existing_tags, **existing_state),
            new=dict(
                subnetId=update_result.ret.id, tags=update_result.ret.tags, **new_state
            ),
        )

    except Exception as error:
        hub.log.error("Error during enforcing absent state: vspherefabricnetworkstate")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):

    try:

        result = {}

        # Leverage generated pagination to retrieve all available fabrics
        res = await paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            # Build result
            result[f"virtual-vsphere-{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.virtual.vspherefabricnetworkstate.present": [
                    dict(name=obj_name),
                    dict(subnetId=obj_id),
                    dict(ipv6Cidr=obj.get("ipv6Cidr")),
                    dict(isDefault=obj.get("isDefault")),
                    dict(domain=obj.get("domain")),
                    dict(defaultIpv6Gateway=obj.get("defaultIpv6Gateway")),
                    dict(dnsServerAddresses=obj.get("dnsServerAddresses")),
                    dict(isPublic=obj.get("isPublic")),
                    dict(cidr=obj.get("cidr")),
                    dict(defaultGateway=obj.get("defaultGateway")),
                    dict(tags=obj.get("tags")),
                    dict(dnsSearchDomains=obj.get("dnsSearchDomains")),
                ]
            }

        return result

    except Exception as error:
        hub.log.error("Error during describe: vspherefabricnetworkstate")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    return False


async def paginate_find(hub, ctx, **kwargs):
    """
    Paginate through all resources using their 'find' method.
    """
    res = await hub.exec.vra.iaas.fabricnetwork.get_vsphere_fabric_networks(
        ctx, **kwargs
    )

    numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
    totalElements = res.get("ret", {}).get("totalElements", 0)
    initialElements = numberOfElements
    if numberOfElements != totalElements and totalElements != 0:
        while initialElements < totalElements:
            hub.log.debug(
                f"Requesting vsphere fabricnetwork with offset={initialElements} out of {totalElements}"
            )
            pres = await hub.exec.vra.iaas.fabricnetwork.get_vsphere_fabric_networks(
                ctx, skip=initialElements, **kwargs
            )
            initialElements += pres.get("ret", {}).get("numberOfElements", 0)
            aggO = res.get("ret", {}).get("content", [])
            aggN = pres.get("ret", {}).get("content", [])
            res["ret"]["content"] = [*aggO, *aggN]
            res["ret"]["numberOfElements"] = initialElements

    return res
