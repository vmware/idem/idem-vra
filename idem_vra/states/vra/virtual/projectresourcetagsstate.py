# Auto-generated virtuale state module

__contracts__ = ["resource"]

TREQ = {
    "present": {"require": ["vra.iaas.project.present"]},
    "absent": {"require": ["vra.iaas.project.present"]},
}


# ====================================
# State implementation
# ====================================
from idem_vra.helpers.models import StateReturn


async def paginate_find(hub, ctx, **kwargs):
    res = await hub.exec.vra.iaas.project.get_projects(ctx, **kwargs)

    numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
    totalElements = res.get("ret", {}).get("totalElements", 0)
    initialElements = numberOfElements
    if numberOfElements != totalElements and totalElements != 0:
        while initialElements < totalElements:
            hub.log.debug(
                f"Requesting project with offset={initialElements} out of {totalElements}"
            )
            pres = await hub.exec.vra.iaas.project.get_projects(
                ctx, skip=initialElements, **kwargs
            )
            aggO = res.get("ret", {}).get("content", [])
            aggN = pres.get("ret", {}).get("content", [])
            res["ret"]["content"] = [*aggO, *aggN]
            initialElements += max(
                pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
            )
            res["ret"]["numberOfElements"] = initialElements

    return res


async def present(hub, ctx, name: str, tags: list[dict[str, str]] = [], **kwargs):
    try:

        tags = tags or []

        projects = await paginate_find(hub, ctx)
        project_id = None

        for project in projects.get("ret", {}).get("content", []):
            if project.get("name") == name:
                project_id = project.get("id")
                break

        if project_id is None:
            hub.log.error(f"Project {name} not found")
            raise Exception(f"Project {name} not found")

        existing_tags_request = (
            await hub.exec.vra.iaas.project.get_project_resource_metadata(
                ctx, p_id=project_id
            )
        )

        existing_tags = existing_tags_request.get("ret").get("tags") or []
        for tag in existing_tags:
            if "id" in tag:
                del tag["id"]

        for tag in tags:
            if "key" not in tag:
                tag["key"] = ""
            if "value" not in tag:
                tag["value"] = ""

        tags_to_add = [tag for tag in tags if tag not in existing_tags]

        if len(tags_to_add) > 0:
            update_result = (
                await hub.exec.vra.iaas.project.update_project_resource_metadata(
                    ctx, p_id=project_id, tags=tags_to_add
                )
            )

            for tag in update_result.ret.tags:
                if "id" in tag:
                    del tag["id"]

            return StateReturn(
                result=True,
                comment=f"Resource state updated.",
                old=dict(tags=existing_tags),
                new=dict(tags=update_result.ret.tags),
            )
        else:
            resource = dict(tags=existing_tags)

            return StateReturn(
                result=True,
                comment=f"Resource state is already up-to-date.",
                old=resource,
                new=resource,
            )

    except Exception as error:
        hub.log.error("Error during enforcing present state: projectresourcetagsstate")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, tags: list[dict[str, str]] = [], **kwargs):
    try:

        tags = tags or []

        projects = await paginate_find(hub, ctx)
        project_id = None

        for project in projects.get("ret", {}).get("content", []):
            if project.get("name") == name:
                project_id = project.get("id")
                break

        if project_id is None:
            hub.log.error(f"Project {name} not found")
            raise Exception(f"Project {name} not found")

        existing_tags_request = (
            await hub.exec.vra.iaas.project.get_project_resource_metadata(
                ctx, p_id=project_id
            )
        )

        existing_tags = existing_tags_request.get("ret").get("tags") or []
        for tag in existing_tags:
            if "id" in tag:
                del tag["id"]

        tags_to_remove = [tag for tag in existing_tags if tag in tags]

        if len(tags_to_remove) > 0:

            full_tags_definition = [
                tag for tag in existing_tags or [] if tag not in tags_to_remove
            ]

            # there's a weird behaviour of the update call we get error instead of tags=[]
            update_result = (
                await hub.exec.vra.iaas.project.update_project_resource_metadata(
                    ctx, p_id=project_id, tags=full_tags_definition
                )
            )

            for tag in update_result.ret.tags:
                if "id" in tag:
                    del tag["id"]

            return StateReturn(
                result=True,
                comment=f"Resource state updated.",
                old=dict(tags=existing_tags),
                new=dict(tags=update_result.ret.tags),
            )

        else:
            resource = dict(tags=existing_tags)

            return StateReturn(
                result=True,
                comment=f"Resource state is already up-to-date.",
                old=resource,
                new=resource,
            )

    except Exception as error:
        hub.log.error("Error during enforcing absent state: projectresourcetagsstate")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:

        result = {}
        res = await paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            # Build result

            existing_tags_request = (
                await hub.exec.vra.iaas.project.get_project_resource_metadata(
                    ctx, p_id=obj_id
                )
            )

            existing_tags = existing_tags_request.get("ret").get("tags") or []

            for tag in existing_tags:
                if "id" in tag:
                    del tag["id"]

            result[f"virtual-{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.virtual.projectresourcetagsstate.present": [
                    dict(name=obj_name),
                    dict(tags=existing_tags),
                ]
            }

        return result

    except Exception as error:
        hub.log.error("Error during describe: projectresourcetagsstate")
        hub.log.error(str(error))
        raise error
