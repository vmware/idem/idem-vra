from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(
    hub,
    ctx,
    name: str,
    runtime: Any,
    entrypoint: Any,
    actionType: Any,
    orgId: Any,
    **kwargs,
):

    """

    :param string name: (required in body)

    :param string runtime: (required in body)

    :param string entrypoint: (required in body)

    :param string actionType: (required in body)

    :param string orgId: (required in body)

    :param object metadata: (optional in body)

    :param string source: (optional in body)

    :param string description: (optional in body)

    :param object inputs: (optional in body)

    :param integer memoryInMB: (optional in body)

    :param integer prePolyglotMemoryLimitInMB: (optional in body)

    :param boolean showMemoryAlert: (optional in body)

    :param integer timeoutSeconds: (optional in body)

    :param string dependencies: (optional in body)

    :param array compressedContent: (optional in body)

    :param string provider: (optional in body)

    :param string contentId: (optional in body)

    :param object configuration: (optional in body)

    :param boolean system: (optional in body)

    :param boolean shared: (optional in body)

    :param boolean scalable: (optional in body)

    :param boolean asyncDeployed: (optional in body)

    :param string id: (optional in body)

    :param string projectId: (optional in body)

    :param string selfLink: (optional in body)

    """

    try:
        state = ActionsStateImpl(hub, ctx)
        return await state.present(
            hub, ctx, name, runtime, entrypoint, actionType, orgId, **kwargs
        )
    except Exception as error:
        hub.log.error("Error during enforcing present state: actions")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) ID of the action

    :param string projectId: (optional in query) Project ID of action (required for non-system actions)

    :param boolean force: (optional in query)

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = ActionsStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: actions")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = ActionsStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: actions")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = ActionsStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: actions")
        hub.log.error(str(error))
        raise error


class ActionsState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    @args_decoder
    async def present(
        self,
        hub,
        ctx,
        name: str,
        runtime: Any,
        entrypoint: Any,
        actionType: Any,
        orgId: Any,
        **kwargs,
    ):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource actions "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource actions "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for actions  p_id, name, runtime, entrypoint, actionType, orgId,
                # update_optional_for actions  metadata, source, description, inputs, memoryInMB, prePolyglotMemoryLimitInMB, showMemoryAlert, timeoutSeconds, dependencies, compressedContent, provider, contentId, configuration, system, shared, scalable, asyncDeployed, id, projectId, selfLink,
                p_id = s["id"]

                # Prevent update operation if update is denied
                if "u" in prohibit_ops:
                    remapped_resolved_resource = await self.remap_resource_structure(
                        hub, ctx, s
                    )
                    return StateReturn(
                        result=True,
                        comment=f'Update operation is denied for resource actions "{name}"',
                        old=remapped_resolved_resource,
                        new=remapped_resolved_resource,
                    )

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource actions does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s,
                        desired_state={
                            "id": p_id,
                            "name": name,
                            "runtime": runtime,
                            "entrypoint": entrypoint,
                            "actionType": actionType,
                            "orgId": orgId,
                        },
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="actions", name=name
                    )
                    return result
                else:
                    res = await hub.exec.vra.abx.actions.update(
                        ctx,
                        p_id,
                        name,
                        runtime,
                        entrypoint,
                        actionType,
                        orgId,
                        **kwargs,
                    )
                    if res.get("result", False) == True:
                        res["ret"] = await self.remap_resource_structure(
                            hub, ctx, res["ret"]
                        )
                        res["ret"]["resource_id"] = res["ret"]["id"]
                        return StateReturn(
                            result=True,
                            comment=f"Updated actions name={name} successfully.",
                            old=s,
                            new=res["ret"],
                        )
                    else:
                        hub.log.error(f"name={name} {res['comment']}")
                        return StateReturn(
                            result=False,
                            comment=f"Update of actions name={name} failed.",
                        )

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource actions "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False, comment=f"Resource actions does not support test"
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "name": name,
                    "runtime": runtime,
                    "entrypoint": entrypoint,
                    "actionType": actionType,
                    "orgId": orgId,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="actions", name=name
            )
            return result
        else:
            res = await hub.exec.vra.abx.actions.create(
                ctx, name, runtime, entrypoint, actionType, orgId, **kwargs
            )
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of actions name={name} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"name={name} {res['comment']}")
                return StateReturn(
                    result=False, comment=f"Creation of actions name={name} failed."
                )

    async def absent(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource actions "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource actions "{name}"',
                )

            delete_kwargs = {}

            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"actions with name={name} already exists")
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False, comment=f"Resource actions does not support test"
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="actions", name=name
                )
                return result
            else:
                res = await hub.exec.vra.abx.actions.delete1(ctx, **delete_kwargs)
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource actions with name={name} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource actions with name={name} deletion failed.",
                    )

        return StateReturn(
            result=True, comment=f"Resource actions with name={name} is already absent."
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-actions"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.abx.actions.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.abx.actions.get_all11(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting actions with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.abx.actions.get_all11(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "selfLink",
                "contentId",
                "createdMillis",
                "updatedMillis",
                "orgId",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "actions"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
import gzip
import base64
from idem_vra.helpers.models import StateReturn


class ActionsStateImpl(ActionsState):
    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.abx.actions.get_all11(ctx, **kwargs)

        total_pages = res["ret"]["totalPages"]
        page_list = list(range(1, total_pages + 1))
        for i in page_list:
            new_data = await hub.exec.vra.abx.actions.get_all11(ctx, page=i)
            for j in new_data["ret"]["content"]:
                res["ret"]["content"].append(j)
        return res

    async def present(
        self,
        hub,
        ctx,
        name: str,
        actionType: str,
        entryPoint: str,
        runtime: str,
        **kwargs,
    ):
        # Search function to check all inventory
        search_result = (await self.paginate_find(hub, ctx))["ret"]
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource actions "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource actions {name} already exists.",
                    old=s,
                    new=s,
                )

        orgId = ctx["acct"]["orgId"]
        payload = {
            "actionType": actionType,
            "entryPoint": entryPoint,
            "name": name,
            "orgId": orgId,
            "runtime": runtime,
        }
        # adding all extra parameters to payload
        for key, value in kwargs.items():
            payload[key] = value

        # checking if Bundle is part of sls payload or not
        if "bundle" in payload.keys():
            for bundle in payload["bundle"]:
                byte_encoded_data = hub.tool.binary_data.read_binary(bundle)
                compressed_data = gzip.compress(byte_encoded_data)
                base64_data = base64.b64encode(compressed_data).decode("utf-8")
                payload["compressedContent"] = base64_data

        res = await hub.exec.vra.rest.request(
            ctx, method="post", path="/abx/api/resources/actions", json=payload
        )
        res = await self.remap_resource_structure(hub, ctx, res)
        return StateReturn(
            result=True,
            comment=f"Creation of actions {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource actions "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s
        if resource:
            # it exists!
            hub.log.debug(f"actions with name = {resource.get('name')} already exists")
            await hub.exec.vra.abx.actions.delete1(
                ctx,
                resource["id"],
                resource["projectId"],
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )
        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):
        result = {}
        data = await self.paginate_find(hub, ctx)
        # Removing System ABX action from main payload
        res = [d for d in data["ret"]["content"] if not d["system"]]
        for i in res:
            if i.get("contentId", None) is not None:
                data = {
                    "actions": [
                        {
                            "id": i["id"],
                            "orgId": i["orgId"],
                            "projectId": i["projectId"],
                        }
                    ]
                }
                export_action = await hub.exec.vra.rest.request(
                    ctx,
                    method="post",
                    path="/abx/api/resources/actions/export",
                    json=data,
                )
                # Added dummy uri for this used case uri = 'file://'
                extracted_files = hub.tool.binary_data.write_binary(
                    uri="zip://",
                    byte_data=export_action["ret"],
                    extract_all_with_extension=".zip",
                )
                i["bundle"] = extracted_files

        for obj in res:
            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.abx.actions.present": props
            }

        return result
