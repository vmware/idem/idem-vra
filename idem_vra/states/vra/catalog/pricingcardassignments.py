from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, pricingCardId: Any, **kwargs):

    """

    :param string pricingCardId: (required in body) Pricing card id

    :param string id: (optional in body) Id of the pricingCardAssignment

    :param string orgId: (optional in body) OrgId of the pricingCardAssignment

    :param string entityId: (optional in body) Pricing card assigned entity id

    :param string entityType: (optional in body) Pricing card assigned entity type

    :param string createdAt: (optional in body) Creation time

    :param string lastUpdatedAt: (optional in body) Updated time

    :param string entityName: (optional in body) Pricing card assigned entity name

    :param string pricingCardName: (optional in body) Pricing card name

    """

    try:
        state = PricingcardassignmentsState(hub, ctx)
        return await state.present(hub, ctx, name, pricingCardId, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: pricingcardassignments")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, pricingCardName: Any, **kwargs):

    """

    :param string p_id: (required in path) pricing card Assignment Id

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = PricingcardassignmentsState(hub, ctx)
        return await state.absent(hub, ctx, name, pricingCardName, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: pricingcardassignments")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = PricingcardassignmentsState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: pricingcardassignments")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = PricingcardassignmentsState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: pricingcardassignments")
        hub.log.error(str(error))
        raise error


class PricingcardassignmentsState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = True

    @args_decoder
    async def present(self, hub, ctx, name: str, pricingCardId: Any, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if pricingCardName == s["pricingCardName"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource pricingcardassignments "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource pricingcardassignments "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for pricingcardassignments  p_id, pricingCardId,
                # update_optional_for pricingcardassignments  id, orgId, entityId, entityType, createdAt, lastUpdatedAt, entityName, pricingCardName,
                p_id = s["id"]

                # Prevent update operation if update is denied
                if "u" in prohibit_ops:
                    remapped_resolved_resource = await self.remap_resource_structure(
                        hub, ctx, s
                    )
                    return StateReturn(
                        result=True,
                        comment=f'Update operation is denied for resource pricingcardassignments "{name}"',
                        old=remapped_resolved_resource,
                        new=remapped_resolved_resource,
                    )

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource pricingcardassignments does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s,
                        desired_state={
                            "id": p_id,
                            "pricingCardId": pricingCardId,
                        },
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="pricingcardassignments", name=name
                    )
                    return result
                else:
                    res = await hub.exec.vra.catalog.pricingcardassignments.patch_metering_policy_assignment(
                        ctx, p_id, pricingCardId, **kwargs
                    )
                    if res.get("result", False) == True:
                        res["ret"] = await self.remap_resource_structure(
                            hub, ctx, res["ret"]
                        )
                        res["ret"]["resource_id"] = res["ret"]["id"]
                        return StateReturn(
                            result=True,
                            comment=f"Updated pricingcardassignments pricingCardName={pricingCardName} successfully.",
                            old=s,
                            new=res["ret"],
                        )
                    else:
                        hub.log.error(
                            f"pricingCardName={pricingCardName} {res['comment']}"
                        )
                        return StateReturn(
                            result=False,
                            comment=f"Update of pricingcardassignments pricingCardName={pricingCardName} failed.",
                        )

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource pricingcardassignments "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False,
                    comment=f"Resource pricingcardassignments does not support test",
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "pricingCardId": pricingCardId,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="pricingcardassignments", name=name
            )
            return result
        else:
            res = await hub.exec.vra.catalog.pricingcardassignments.create_metering_policy_assignment(
                ctx, pricingCardId, **kwargs
            )
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of pricingcardassignments pricingCardName={pricingCardName} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"pricingCardName={pricingCardName} {res['comment']}")
                return StateReturn(
                    result=False,
                    comment=f"Creation of pricingcardassignments pricingCardName={pricingCardName} failed.",
                )

    async def absent(self, hub, ctx, name: str, pricingCardName: Any, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if pricingCardName == s["pricingCardName"] and True:
                hub.log.info(
                    f'Found resource pricingcardassignments "{s["name"]}" due to existing resource pricingCardName={pricingCardName}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource pricingcardassignments "{name}"',
                )

            delete_kwargs = {}

            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(
                f"pricingcardassignments with pricingCardName={pricingCardName} already exists"
            )
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False,
                        comment=f"Resource pricingcardassignments does not support test",
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="pricingcardassignments", name=name
                )
                return result
            else:
                res = await hub.exec.vra.catalog.pricingcardassignments.delete_metering_policy_assignment(
                    ctx, **delete_kwargs
                )
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource pricingcardassignments with pricingCardName={pricingCardName} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource pricingcardassignments with pricingCardName={pricingCardName} deletion failed.",
                    )

        return StateReturn(
            result=True,
            comment=f"Resource pricingcardassignments with pricingCardName={pricingCardName} is already absent.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-pricingcardassignments"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.catalog.pricingcardassignments.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.catalog.pricingcardassignments.get_all_metering_policy_assignments(
            ctx, **kwargs
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting pricingcardassignments with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.catalog.pricingcardassignments.get_all_metering_policy_assignments(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "createdAt",
                "lastUpdatedAt",
                "pricingCardName",
                "entityName",
                "orgId",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "pricingcardassignments"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj
