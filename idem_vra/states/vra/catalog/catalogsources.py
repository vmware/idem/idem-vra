from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, typeId: Any, config: Any, **kwargs):

    """

    :param string name: (required in body) Catalog Source name

    :param string typeId: (required in body) Type of source, e.g. blueprint, CFT... etc

    :param object config: (required in body)

    :param boolean validationOnly: (optional in query) If true, the source will not be created. It returns the number of
      items belonging to the source. The request will still return an error
      code if the source is invalid.

    :param string id: (optional in body) Catalog Source id

    :param string description: (optional in body) Catalog Source description

    :param string createdAt: (optional in body) Creation time

    :param string createdBy: (optional in body) Created By

    :param string lastUpdatedAt: (optional in body) Update time

    :param string lastUpdatedBy: (optional in body) Updated By

    :param integer itemsImported: (optional in body) Number of items imported.

    :param integer itemsFound: (optional in body) Number of items found

    :param string lastImportStartedAt: (optional in body) Last import start time

    :param string lastImportCompletedAt: (optional in body) Last import completion time

    :param array lastImportErrors: (optional in body) Last import error(s)

    :param string projectId: (optional in body) Project id where the source belongs

    :param boolean global: (optional in body) Global flag indicating that all the items can be requested across all
      projects.

    :param string iconId: (optional in body) Default Icon Id

    """

    try:
        state = CatalogsourcesStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, typeId, config, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: catalogsources")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_sourceId: (required in path) Catalog source ID

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = CatalogsourcesStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: catalogsources")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = CatalogsourcesStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: catalogsources")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = CatalogsourcesStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: catalogsources")
        hub.log.error(str(error))
        raise error


class CatalogsourcesState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    @args_decoder
    async def present(self, hub, ctx, name: str, typeId: Any, config: Any, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource catalogsources "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource catalogsources "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for catalogsources  name, typeId, config,
                # update_optional_for catalogsources  validationOnly, id, description, createdAt, createdBy, lastUpdatedAt, lastUpdatedBy, itemsImported, itemsFound, lastImportStartedAt, lastImportCompletedAt, lastImportErrors, projectId, global, iconId,
                s["id"]

                # Prevent update operation if update is denied
                if "u" in prohibit_ops:
                    remapped_resolved_resource = await self.remap_resource_structure(
                        hub, ctx, s
                    )
                    return StateReturn(
                        result=True,
                        comment=f'Update operation is denied for resource catalogsources "{name}"',
                        old=remapped_resolved_resource,
                        new=remapped_resolved_resource,
                    )

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource catalogsources does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s,
                        desired_state={
                            "name": name,
                            "typeId": typeId,
                            "config": config,
                        },
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="catalogsources", name=name
                    )
                    return result
                else:
                    res = await hub.exec.vra.catalog.catalogsources.post(
                        ctx, name, typeId, config, **kwargs
                    )
                    if res.get("result", False) == True:
                        res["ret"] = await self.remap_resource_structure(
                            hub, ctx, res["ret"]
                        )
                        res["ret"]["resource_id"] = res["ret"]["id"]
                        return StateReturn(
                            result=True,
                            comment=f"Updated catalogsources name={name} successfully.",
                            old=s,
                            new=res["ret"],
                        )
                    else:
                        hub.log.error(f"name={name} {res['comment']}")
                        return StateReturn(
                            result=False,
                            comment=f"Update of catalogsources name={name} failed.",
                        )

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource catalogsources "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False,
                    comment=f"Resource catalogsources does not support test",
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "name": name,
                    "typeId": typeId,
                    "config": config,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="catalogsources", name=name
            )
            return result
        else:
            res = await hub.exec.vra.catalog.catalogsources.post(
                ctx, name, typeId, config, **kwargs
            )
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of catalogsources name={name} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"name={name} {res['comment']}")
                return StateReturn(
                    result=False,
                    comment=f"Creation of catalogsources name={name} failed.",
                )

    async def absent(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource catalogsources "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource catalogsources "{name}"',
                )

            delete_kwargs = {}

            delete_kwargs["p_sourceId"] = resource.get("sourceId")

            hub.log.debug(f"catalogsources with name={name} already exists")
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False,
                        comment=f"Resource catalogsources does not support test",
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="catalogsources", name=name
                )
                return result
            else:
                res = await hub.exec.vra.catalog.catalogsources.delete2(
                    ctx, **delete_kwargs
                )
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource catalogsources with name={name} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource catalogsources with name={name} deletion failed.",
                    )

        return StateReturn(
            result=True,
            comment=f"Resource catalogsources with name={name} is already absent.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-catalogsources"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.catalog.catalogsources.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.catalog.catalogsources.get_page(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting catalogsources with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.catalog.catalogsources.get_page(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "createdAt",
                "createdBy",
                "lastImportCompletedAt",
                "lastImportErrors",
                "lastImportStartedAt",
                "lastUpdatedAt",
                "lastUpdatedBy",
                "iconId",
                "itemsFound",
                "itemsImported",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "catalogsources"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
class CatalogsourcesStateImpl(CatalogsourcesState):
    def __init__(self, hub, ctx, **kwargs):
        super().__init__(hub, ctx)
        self.is_dry_run_supported = True

    async def present(self, hub, ctx, name: str, typeId: Any, config: Any, **kwargs):
        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource catalogsources "{s["name"]}" as resource "{name}" exists'
                )

                p_id = s["id"]

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource catalogsources does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s,
                        desired_state={
                            "name": name,
                            "type_id": typeId,
                            "config": config,
                        },
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="catalogsources", name=name
                    )
                    return result
                else:
                    res = (
                        await hub.exec.vra.catalog.catalogsources.post(
                            ctx, name, typeId, config, id=p_id, **kwargs
                        )
                    )["ret"]
                    res = await self.remap_resource_structure(hub, ctx, res)
                    res["resource_id"] = res["id"]

                return StateReturn(
                    result=True,
                    comment=f"Updated catalogsources name={name} successfully.",
                    old=s,
                    new=res,
                )
        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False,
                    comment=f"Resource catalogsources does not support test",
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )
            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "name": name,
                    "type_id": typeId,
                    "config": config,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="catalogsources", name=name
            )
            return result
        else:

            res = (
                await hub.exec.vra.catalog.catalogsources.post(
                    ctx, name, typeId, config, **kwargs
                )
            )["ret"]
            res = await self.remap_resource_structure(hub, ctx, res)
        return StateReturn(
            result=True,
            comment=f"Creation of catalogsources name={name} success.",
            old=None,
            new=res,
        )
