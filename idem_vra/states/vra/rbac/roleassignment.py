from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, principalId: Any, **kwargs):

    """

    :param string principalId: (required in body) The id of the principal this role assignment will be applicable for

    :param string apiVersion: (optional in query)

    :param string orgId: (optional in body) The id of the org this role assignment belongs to

    :param string projectId: (optional in body) The id of the project this role assignment belongs to

    :param string principalType: (optional in body) Principal type (user/group)

    :param array rolesToAdd: (optional in body) Ids of roles to add

    :param array rolesToRemove: (optional in body) Ids of roles to remove

    """

    try:
        state = RoleassignmentStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, principalId, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: roleassignment")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string principalId: (required in body) The id of the principal this role assignment will be applicable for

    :param string apiVersion: (optional in query)

    :param string orgId: (optional in body) The id of the org this role assignment belongs to

    :param string projectId: (optional in body) The id of the project this role assignment belongs to

    :param string principalType: (optional in body) Principal type (user/group)

    :param array rolesToAdd: (optional in body) Ids of roles to add

    :param array rolesToRemove: (optional in body) Ids of roles to remove

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = RoleassignmentStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: roleassignment")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = RoleassignmentStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: roleassignment")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = RoleassignmentStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: roleassignment")
        hub.log.error(str(error))
        raise error


class RoleassignmentState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    @args_decoder
    async def present(self, hub, ctx, name: str, principalId: Any, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource roleassignment "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource roleassignment "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for roleassignment  principalId,
                # update_optional_for roleassignment  apiVersion, orgId, projectId, principalType, rolesToAdd, rolesToRemove,
                s["id"]

                # Prevent update operation if update is denied
                if "u" in prohibit_ops:
                    remapped_resolved_resource = await self.remap_resource_structure(
                        hub, ctx, s
                    )
                    return StateReturn(
                        result=True,
                        comment=f'Update operation is denied for resource roleassignment "{name}"',
                        old=remapped_resolved_resource,
                        new=remapped_resolved_resource,
                    )

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource roleassignment does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s,
                        desired_state={
                            "principalId": principalId,
                        },
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="roleassignment", name=name
                    )
                    return result
                else:
                    res = (
                        await hub.exec.vra.rbac.roleassignment.update_role_assignments(
                            ctx, principalId, **kwargs
                        )
                    )
                    if res.get("result", False) == True:
                        res["ret"] = await self.remap_resource_structure(
                            hub, ctx, res["ret"]
                        )
                        res["ret"]["resource_id"] = res["ret"]["id"]
                        return StateReturn(
                            result=True,
                            comment=f"Updated roleassignment name={name} successfully.",
                            old=s,
                            new=res["ret"],
                        )
                    else:
                        hub.log.error(f"name={name} {res['comment']}")
                        return StateReturn(
                            result=False,
                            comment=f"Update of roleassignment name={name} failed.",
                        )

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource roleassignment "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False,
                    comment=f"Resource roleassignment does not support test",
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "principalId": principalId,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="roleassignment", name=name
            )
            return result
        else:
            res = await hub.exec.vra.rbac.roleassignment.update_role_assignments(
                ctx, principalId, **kwargs
            )
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of roleassignment name={name} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"name={name} {res['comment']}")
                return StateReturn(
                    result=False,
                    comment=f"Creation of roleassignment name={name} failed.",
                )

    async def absent(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource roleassignment "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource roleassignment "{name}"',
                )

            delete_kwargs = {}

            delete_kwargs["principalId"] = resource.get("principalId")

            hub.log.debug(f"roleassignment with name={name} already exists")
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False,
                        comment=f"Resource roleassignment does not support test",
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="roleassignment", name=name
                )
                return result
            else:
                res = await hub.exec.vra.rbac.roleassignment.update_role_assignments(
                    ctx, **delete_kwargs
                )
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource roleassignment with name={name} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource roleassignment with name={name} deletion failed.",
                    )

        return StateReturn(
            result=True,
            comment=f"Resource roleassignment with name={name} is already absent.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-roleassignment"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.rbac.roleassignment.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.rbac.roleassignment.get_role_assignments(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting roleassignment with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.rbac.roleassignment.get_role_assignments(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = None

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "roleassignment"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
class RoleassignmentStateImpl(RoleassignmentState):
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = True

    async def present(self, hub, ctx, name: str, principalId: str, **kwargs):

        roleId = kwargs.get("roleId")
        del kwargs["roleId"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if roleId == s["roleId"] and principalId == s["principalId"] and True:
                hub.log.info(
                    f"Returning resource roleassignment due to existing resource name={name}"
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource roleassignment name={name} already exists.",
                    old=s,
                    new=s,
                )

        if ctx.get("test", False):

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "principalId": principalId,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="roleassignment", name=name
            )
            return result
        else:
            kwargs["rolesToAdd"] = [roleId]
            res = (
                await hub.exec.vra.rbac.roleassignment.update_role_assignments(
                    ctx, principalId, **kwargs
                )
            )["ret"]
            res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of roleassignment name={name}, roleId={roleId}, principalId={principalId} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, principalId: str, **kwargs):

        roleId = kwargs.get("roleId")
        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if roleId == s["roleId"] and principalId == s["principalId"] and True:
                hub.log.info(
                    f"Found resource roleassignment due to existing resource name={name}, roleId={roleId}, principalId={principalId}"
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            delete_kwargs = {}
            delete_kwargs["rolesToRemove"] = [roleId]

            hub.log.debug(
                f"roleassignment with name={name}, roleId={roleId}, principalId={principalId} already exists"
            )

            if ctx.get("test", False):

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="roleassignment", name=name
                )
                return result
            else:
                await hub.exec.vra.rbac.roleassignment.update_role_assignments(
                    ctx, principalId, **delete_kwargs
                )

            return StateReturn(
                result=True,
                comment=f"Resource roleassignment with name={name}, roleId={roleId}, principalId={principalId} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource roleassignment with name={name}, roleId={roleId}, principalId={principalId} is already absent.",
            old=None,
            new=None,
        )
