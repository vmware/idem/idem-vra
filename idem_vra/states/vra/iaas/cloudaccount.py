from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": []}, "absent": {"require": ["vra.iaas.project.absent"]}}


async def present(
    hub,
    ctx,
    name: str,
    cloudAccountType: Any,
    cloudAccountProperties: Any,
    regions: Any,
    **kwargs,
):

    """

    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.

    :param string cloudAccountType: (required in body) Cloud account type

    :param object cloudAccountProperties: (required in body) Cloud Account specific properties supplied in as name value pairs

    :param array regions: (required in body) A set of regions to enable provisioning on.Refer to /iaas/api/cloud-
      accounts/region-enumeration.
      regionInfos is a required parameter for AWS, AZURE, GCP, VSPHERE,
      VMC, VCF cloud account types.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    :param string validateOnly: (optional in query) If provided, it only validates the credentials in the Cloud Account
      Specification, and cloud account will not be created.

    :param string description: (optional in body) A human-friendly description.

    :param string privateKeyId: (optional in body) Access key id or username to be used to authenticate with the cloud
      account

    :param string privateKey: (optional in body) Secret access key or password to be used to authenticate with the
      cloud account. In case of AAP pass a dummy value.

    :param array associatedCloudAccountIds: (optional in body) Cloud accounts to associate with this cloud account

    :param object associatedMobilityCloudAccountIds: (optional in body) Cloud Account IDs and directionalities create associations to other
      vSphere cloud accounts that can be used for workload mobility. ID
      refers to an associated cloud account, and directionality can be
      unidirectional or bidirectional. Only supported on vSphere cloud
      accounts.

    :param object customProperties: (optional in body) Additional custom properties that may be used to extend the Cloud
      Account. In case of AAP, provide environment property here.Example:
      "customProperties": {
      "environment": "aap"
      }

    :param boolean createDefaultZones: (optional in body) Create default cloud zones for the enabled regions.

    :param array tags: (optional in body) A set of tag keys and optional values to set on the Cloud Account

    :param Any certificateInfo: (optional in body)

    """

    try:
        state = CloudaccountStateImpl(hub, ctx)
        return await state.present(
            hub, ctx, name, cloudAccountType, cloudAccountProperties, regions, **kwargs
        )
    except Exception as error:
        hub.log.error("Error during enforcing present state: cloudaccount")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) The ID of the Cloud Account

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = CloudaccountStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: cloudaccount")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = CloudaccountStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: cloudaccount")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = CloudaccountStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: cloudaccount")
        hub.log.error(str(error))
        raise error


class CloudaccountState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    @args_decoder
    async def present(
        self,
        hub,
        ctx,
        name: str,
        cloudAccountType: Any,
        cloudAccountProperties: Any,
        regions: Any,
        **kwargs,
    ):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource cloudaccount "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Returning resource cloudaccount "{s["name"]}" due to existing resource name={name}'
                )

                return StateReturn(
                    result=True,
                    comment=f"Resource cloudaccount name={name} already exists.",
                    old=s,
                    new=s,
                )

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource cloudaccount "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False, comment=f"Resource cloudaccount does not support test"
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "name": name,
                    "cloudAccountType": cloudAccountType,
                    "cloudAccountProperties": cloudAccountProperties,
                    "regions": regions,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="cloudaccount", name=name
            )
            return result
        else:
            res = await hub.exec.vra.iaas.cloudaccount.create_cloud_account_async(
                ctx, name, cloudAccountType, cloudAccountProperties, regions, **kwargs
            )
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of cloudaccount name={name} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"name={name} {res['comment']}")
                return StateReturn(
                    result=False,
                    comment=f"Creation of cloudaccount name={name} failed.",
                )

    async def absent(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource cloudaccount "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource cloudaccount "{name}"',
                )

            delete_kwargs = {}

            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"cloudaccount with name={name} already exists")
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False,
                        comment=f"Resource cloudaccount does not support test",
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="cloudaccount", name=name
                )
                return result
            else:
                res = await hub.exec.vra.iaas.cloudaccount.delete_cloud_account(
                    ctx, **delete_kwargs
                )
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource cloudaccount with name={name} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource cloudaccount with name={name} deletion failed.",
                    )

        return StateReturn(
            result=True,
            comment=f"Resource cloudaccount with name={name} is already absent.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-cloudaccount"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.iaas.cloudaccount.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.iaas.cloudaccount.get_cloud_accounts(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting cloudaccount with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.iaas.cloudaccount.get_cloud_accounts(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [
                {
                    "key": "certificateInfo.certificate",
                    "value": "jsonpath:cloudAccountProperties.certificate",
                },
                {
                    "key": "regions",
                    "value": "jsonpath:$",
                    "source": "lambda",
                    "lambda": "return [{\n  'externalRegionId': region.get('externalRegionId'),\n  'name': region.get('name'),\n  'regionId': region.get('id')\n} for region in resource.get('enabledRegions')] if resource.get('enabledRegions') else []\n",
                    "kwargs": {"resource": "jsonpath:$"},
                },
                {
                    "key": "associatedCloudAccountIds",
                    "value": "jsonpath:$",
                    "source": hub.states.vra.iaas.cloudaccount.remap_associated_cloudaccount_ids,
                    "kwargs": {"resource": "jsonpath:$"},
                    "hub": hub,
                    "ctx": ctx,
                },
                {
                    "key": "privateKeyId",
                    "value": "jsonpath:cloudAccountProperties.privateKeyId",
                },
                {"key": "privateKey", "value": "<private key / password>"},
            ],
            "omit": [
                "orgId",
                "createdAt",
                "updatedAt",
                "owner",
                "ownerType",
                "_links",
                "healthy",
                "inMaintenanceMode",
                "customProperties",
                "cloudAccountProperties.privateKeyId",
                "cloudAccountProperties.certificate",
                "cloudAccountProperties.supportTagging",
                "cloudAccountProperties.supportDatastores",
                "enabledRegions",
                "tags[*].id",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "cloudaccount"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
import asyncio
from idem_vra.helpers.models import FunctionReturn


class CloudaccountStateImpl(CloudaccountState):
    def __init__(self, hub, ctx, **kwargs):
        super().__init__(hub, ctx)
        self.is_dry_run_supported = True

    async def present(
        self,
        hub,
        ctx,
        name: str,
        cloudAccountType: Any,
        cloudAccountProperties: Any,
        regions: Any,
        **kwargs,
    ):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        # ==================================
        # Existing cloud account
        # ==================================

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource cloudaccount "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Returning resource cloudaccount "{s["name"]}" due to existing resource name={name}'
                )

                return StateReturn(
                    result=True,
                    comment=f"Resource cloudaccount name={name} already exists.",
                    old=s,
                    new=s,
                )

        # ==================================
        # New cloud account
        # ==================================

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource cloudaccount "{name}"',
            )

        if ctx.get("test", False):
            # ==================================
            # Dry run
            # ==================================

            return StateReturn(
                result=True,
                comment=hub.tool.vra.state_utils.would_create_comment(
                    resource_type="cloudaccount", name=name
                ),
                old=None,
                new=hub.tool.vra.state_utils.generate_test_state(
                    enforced_state={},
                    desired_state={
                        "name": name,
                        "cloudAccountType": cloudAccountType,
                        "cloudAccountProperties": cloudAccountProperties,
                        "regions": regions,
                    },
                ),
            )

        else:
            # ==================================
            # Actual create
            # ==================================

            # Cloud account creation task
            res = await hub.exec.vra.iaas.cloudaccount.create_cloud_account_async(
                ctx, name, cloudAccountType, cloudAccountProperties, regions, **kwargs
            )

            if not res.result:
                return StateReturn(result=False, comment=res.comment)

            # Await the creation to complete
            create_status, error_or_account = await self.await_iaas_task(
                res.ret.get("selfLink")
            )
            if not create_status:
                return StateReturn(result=False, comment=error_or_account)

            # Read the newly created cloud account
            ca = await hub.exec.vra.iaas.cloudaccount.get_cloud_account(
                ctx, error_or_account
            )
            ca = await self.remap_resource_structure(hub, ctx, ca.ret)

            return StateReturn(
                result=True,
                comment=f"Creation of cloudaccount name={name} success.",
                old=None,
                new=ca,
            )

    async def await_iaas_task(
        self, tracker: str, max_retries: int = 50, sleep: int = 5
    ):
        """Await IaaS task to finish and return the result.

        Args:
            tracker (str): task link
            max_retries (int, optional): maximum retries for the polling. Defaults to 20.

        Returns:
            Tuple[bool, str]: task result
        """

        status = None
        task = None
        retry = 0
        final_statuses = ["FINISHED", "FAILED"]

        # Monitor task progress
        while retry <= max_retries:
            task = await self.hub.tool.vra.authed.get(self.ctx, tracker)
            status = task.get("status")
            if status in final_statuses:
                break
            retry += 1
            await asyncio.sleep(sleep)

        # Evaluate task result
        if status == "FINISHED":
            ca_id = task.get("resources")[0].split("/")[-1]
            return True, ca_id
        elif status == "FAILED":
            return False, task.get("message")
        else:
            return False, f"Max retries ({max_retries}) exhausted"


async def remap_associated_cloudaccount_ids(hub, ctx, resource):
    associated_account_ids = []
    associated_accounts = resource.get("_links", {}).get(
        "associated-cloud-accounts", None
    )

    # Output accociated cloud account ids only for vsphere cloud accounts
    if resource.get("cloudAccountType", None) == "vsphere" and associated_accounts:
        associated_account_ids = [
            account.split("/")[-1] for account in associated_accounts.get("hrefs", [])
        ]

    return FunctionReturn(ret=associated_account_ids)
