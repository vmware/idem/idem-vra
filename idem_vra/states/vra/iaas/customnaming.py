from hashlib import sha256

import semver

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": []}, "absent": {"require": []}}


async def present(hub, ctx, name: str, **kwargs):

    """

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    :param string id: (optional in body)

    :param string name: (optional in body)

    :param string description: (optional in body)

    :param array projects: (optional in body)

    :param array templates: (optional in body)

    """

    try:
        state = CustomnamingStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: customnaming")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) The ID of the custom name.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = CustomnamingStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: customnaming")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = CustomnamingStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: customnaming")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = CustomnamingStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: customnaming")
        hub.log.error(str(error))
        raise error


class CustomnamingState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    @args_decoder
    async def present(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.10.2") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.10.2 max:9.0.0 target:{ctx.acct.vra_version} for resource:customnaming"
            )
            return StateReturn(
                result=True,
                comment=f"State present is not available for resource customnaming.",
            )

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource customnaming "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource customnaming "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for customnaming
                # update_optional_for customnaming  apiVersion, id, name, description, projects, templates,
                s["id"]

                # Prevent update operation if update is denied
                if "u" in prohibit_ops:
                    remapped_resolved_resource = await self.remap_resource_structure(
                        hub, ctx, s
                    )
                    return StateReturn(
                        result=True,
                        comment=f'Update operation is denied for resource customnaming "{name}"',
                        old=remapped_resolved_resource,
                        new=remapped_resolved_resource,
                    )

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource customnaming does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s, desired_state={}
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="customnaming", name=name
                    )
                    return result
                else:
                    res = await hub.exec.vra.iaas.customnaming.update_custom_name(
                        ctx, **kwargs
                    )
                    if res.get("result", False) == True:
                        res["ret"] = await self.remap_resource_structure(
                            hub, ctx, res["ret"]
                        )
                        res["ret"]["resource_id"] = res["ret"]["id"]
                        return StateReturn(
                            result=True,
                            comment=f"Updated customnaming name={name} successfully.",
                            old=s,
                            new=res["ret"],
                        )
                    else:
                        hub.log.error(f"name={name} {res['comment']}")
                        return StateReturn(
                            result=False,
                            comment=f"Update of customnaming name={name} failed.",
                        )
        kwargs["name"] = name

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource customnaming "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False, comment=f"Resource customnaming does not support test"
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={}, desired_state={}
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="customnaming", name=name
            )
            return result
        else:
            res = await hub.exec.vra.iaas.customnaming.create_custom_name(ctx, **kwargs)
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of customnaming name={name} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"name={name} {res['comment']}")
                return StateReturn(
                    result=False,
                    comment=f"Creation of customnaming name={name} failed.",
                )

    async def absent(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.10.2") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.10.2 max:9.0.0 target:{ctx.acct.vra_version} for resource:customnaming"
            )
            return StateReturn(
                result=True,
                comment=f"State absent is not available for resource customnaming.",
            )

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource customnaming "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource customnaming "{name}"',
                )

            delete_kwargs = {}

            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"customnaming with name={name} already exists")
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False,
                        comment=f"Resource customnaming does not support test",
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="customnaming", name=name
                )
                return result
            else:
                res = await hub.exec.vra.iaas.customnaming.delete_customname(
                    ctx, **delete_kwargs
                )
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource customnaming with name={name} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource customnaming with name={name} deletion failed.",
                    )

        return StateReturn(
            result=True,
            comment=f"Resource customnaming with name={name} is already absent.",
        )

    async def describe(self, hub, ctx):
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.10.2") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.10.2 max:9.0.0 target:{ctx.acct.vra_version} for resource:customnaming"
            )
            return {}

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-customnaming"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.iaas.customnaming.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.iaas.customnaming.get_all_custom_names(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting customnaming with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.iaas.customnaming.get_all_custom_names(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [
                {
                    "key": "templates",
                    "value": "jsonpath:templates",
                    "source": hub.exec.vra.iaas.customnaming.get_custom_name,
                    "kwargs": {"p_id": "jsonpath:id"},
                    "hub": hub,
                    "ctx": ctx,
                },
            ],
            "omit": [
                "createdAt",
                "updatedAt",
                "createdBy",
                "updatedBy",
                "owner",
                "ownerType",
                "_links",
                "orgId",
                "templates[*].id",
                "templates[*].counters[*].id",
                "projects[*].id",
                "projects[*].orgId",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "customnaming"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
class CustomnamingStateImpl(CustomnamingState):
    @args_decoder
    async def present(self, hub, ctx, name: str, **kwargs):
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.10.2") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.10.2 max:9.0.0 target:{ctx.acct.vra_version} for resource:customnaming"
            )
            return StateReturn(
                result=True,
                comment=f"State present is not available for resource customnaming.",
            )

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource customnaming NOT SUPPORTED "{s["name"]}" as resource "{name}" exists'
                )

                return StateReturn(
                    result=True,
                    comment=f"Updating customnaming NOT SUPPORTED",
                    old=s,
                    new=s,
                )

        kwargs["name"] = name
        res = (await hub.exec.vra.iaas.customnaming.create_custom_name(ctx, **kwargs))[
            "ret"
        ]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of customnaming {name} success.",
            old=None,
            new=res,
        )
