from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {
    "present": {"require": ["vra.iaas.location.present"]},
    "absent": {"require": []},
}


async def present(hub, ctx, name: str, defaultItem: Any, regionId: Any, **kwargs):

    """

    :param string name: (required in body) A human-friendly name used as an identifier in APIs that support this
      option.

    :param boolean defaultItem: (required in body) Indicates if a storage profile is a default profile.

    :param string regionId: (required in body) The Id of the region that is associated with the storage profile.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    :param string description: (optional in body) A human-friendly description.

    :param boolean supportsEncryption: (optional in body) Indicates whether this storage profile supports encryption or not.

    :param array tags: (optional in body) A list of tags that represent the capabilities of this storage profile

    :param object diskProperties: (optional in body) Map of storage properties that are to be applied on disk while
      provisioning.

    :param object diskTargetProperties: (optional in body) Map of storage placements to know where the disk is provisioned.

    """

    try:
        state = StorageprofileState(hub, ctx)
        return await state.present(hub, ctx, name, defaultItem, regionId, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: storageprofile")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_id: (required in path) The ID of the storage profile.

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information refer to /iaas/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = StorageprofileState(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: storageprofile")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = StorageprofileState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: storageprofile")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = StorageprofileState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: storageprofile")
        hub.log.error(str(error))
        raise error


class StorageprofileState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = True

    @args_decoder
    async def present(
        self, hub, ctx, name: str, defaultItem: Any, regionId: Any, **kwargs
    ):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource storageprofile "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource storageprofile "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for storageprofile  p_id, name, defaultItem, regionId,
                # update_optional_for storageprofile  apiVersion, description, supportsEncryption, tags, diskProperties, diskTargetProperties,
                p_id = s["id"]

                # Prevent update operation if update is denied
                if "u" in prohibit_ops:
                    remapped_resolved_resource = await self.remap_resource_structure(
                        hub, ctx, s
                    )
                    return StateReturn(
                        result=True,
                        comment=f'Update operation is denied for resource storageprofile "{name}"',
                        old=remapped_resolved_resource,
                        new=remapped_resolved_resource,
                    )

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource storageprofile does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s,
                        desired_state={
                            "id": p_id,
                            "name": name,
                            "defaultItem": defaultItem,
                            "regionId": regionId,
                        },
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="storageprofile", name=name
                    )
                    return result
                else:
                    res = (
                        await hub.exec.vra.iaas.storageprofile.replace_storage_profile(
                            ctx, p_id, name, defaultItem, regionId, **kwargs
                        )
                    )
                    if res.get("result", False) == True:
                        res["ret"] = await self.remap_resource_structure(
                            hub, ctx, res["ret"]
                        )
                        res["ret"]["resource_id"] = res["ret"]["id"]
                        return StateReturn(
                            result=True,
                            comment=f"Updated storageprofile name={name} successfully.",
                            old=s,
                            new=res["ret"],
                        )
                    else:
                        hub.log.error(f"name={name} {res['comment']}")
                        return StateReturn(
                            result=False,
                            comment=f"Update of storageprofile name={name} failed.",
                        )

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource storageprofile "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False,
                    comment=f"Resource storageprofile does not support test",
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "name": name,
                    "defaultItem": defaultItem,
                    "regionId": regionId,
                },
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="storageprofile", name=name
            )
            return result
        else:
            res = await hub.exec.vra.iaas.storageprofile.create_storage_profile(
                ctx, name, defaultItem, regionId, **kwargs
            )
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of storageprofile name={name} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"name={name} {res['comment']}")
                return StateReturn(
                    result=False,
                    comment=f"Creation of storageprofile name={name} failed.",
                )

    async def absent(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource storageprofile "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource storageprofile "{name}"',
                )

            delete_kwargs = {}

            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"storageprofile with name={name} already exists")
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False,
                        comment=f"Resource storageprofile does not support test",
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="storageprofile", name=name
                )
                return result
            else:
                res = await hub.exec.vra.iaas.storageprofile.delete_storage_profile(
                    ctx, **delete_kwargs
                )
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource storageprofile with name={name} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource storageprofile with name={name} deletion failed.",
                    )

        return StateReturn(
            result=True,
            comment=f"Resource storageprofile with name={name} is already absent.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-storageprofile"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.iaas.storageprofile.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.iaas.storageprofile.get_storage_profiles(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting storageprofile with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.iaas.storageprofile.get_storage_profiles(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [
                {
                    "key": "regionId",
                    "value": "jsonpath:$",
                    "source": "lambda",
                    "lambda": "return resource.get('_links', {}).get('region', {}).get('href', {}).split(\"/\")[-1]\n",
                    "kwargs": {"resource": "jsonpath:$"},
                },
                {
                    "key": "diskTargetProperties",
                    "value": "jsonpath:$",
                    "source": "lambda",
                    "lambda": "datastoreId = resource.get('_links', {}).get('datastore', {}).get('href', '').split(\"/\")[-1]\ndatastoreId = None if datastoreId == '' else datastoreId\nstoragePolicyId = resource.get('_links', {}).get('storage-policy', {}).get('href', '').split(\"/\")[-1]\nstoragePolicyId = None if storagePolicyId == '' else storagePolicyId\nstorageAccountId = resource.get('_links', {}).get('storage-account', {}).get('href', '').split(\"/\")[-1]\nstorageAccountId = None if storageAccountId == '' else storageAccountId\nreturn {\"datastoreId\": datastoreId, \"storagePolicyId\": storagePolicyId, \"storageAccountId\": storageAccountId}\n",
                    "kwargs": {"resource": "jsonpath:$"},
                },
            ],
            "omit": [
                "orgId",
                "createdAt",
                "updatedAt",
                "owner",
                "ownerType",
                "tags[*].id",
                "externalRegionId",
                "cloudAccountId",
                "_links",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "storageprofile"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj
