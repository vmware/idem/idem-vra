from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {
    "present": {"require": ["vra.iaas.cloudaccount.present"]},
    "absent": {"require": []},
}


async def present(hub, ctx, name: str, externalId: Any, cloudAccountIds: Any, **kwargs):
    """
    :param string name: (required) name of the resource
    """

    try:
        state = FabricvspherestoragepoliciesState(hub, ctx)
        return await state.present(
            hub, ctx, name, externalId, cloudAccountIds, **kwargs
        )
    except Exception as error:
        hub.log.error(
            "Error during enforcing present state: fabricvspherestoragepolicies"
        )
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, externalId: Any, cloudAccountIds: Any, **kwargs):

    """
    :param string name: (required) name of the resource
    """
    try:
        state = FabricvspherestoragepoliciesState(hub, ctx)
        return await state.absent(hub, ctx, name, externalId, cloudAccountIds, **kwargs)
    except Exception as error:
        hub.log.error(
            "Error during enforcing absent state: fabricvspherestoragepolicies"
        )
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = FabricvspherestoragepoliciesState(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: fabricvspherestoragepolicies")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = FabricvspherestoragepoliciesState(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: fabricvspherestoragepolicies")
        hub.log.error(str(error))
        raise error


class FabricvspherestoragepoliciesState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = True

    async def present(
        self, hub, ctx, name: str, externalId: Any, cloudAccountIds: Any, **kwargs
    ):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        # Prevent read operation if read is denied
        if "r" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Read operation is denied for resource fabricvspherestoragepolicies "{name}"',
            )

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if (
                name == s["name"]
                and externalId == s["externalId"]
                and cloudAccountIds == s["cloudAccountIds"]
                and True
            ):
                hub.log.info(
                    f'Returning resource fabricvspherestoragepolicies "{s["name"]}" due to existing resource name={name}, externalId={externalId}, cloudAccountIds={cloudAccountIds}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource fabricvspherestoragepolicies name={name}, externalId={externalId}, cloudAccountIds={cloudAccountIds} already exists.",
                    old=s,
                    new=s,
                )

        return StateReturn(
            result=False,
            comment=f"Resource fabricvspherestoragepolicies is not present in the environment.",
        )

    async def absent(
        self, hub, ctx, name: str, externalId: Any, cloudAccountIds: Any, **kwargs
    ):
        return StateReturn(
            result=True,
            comment=f"State absent is not available for resource fabricvspherestoragepolicies.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-fabricvspherestoragepolicies"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"ro-{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.iaas.fabricvspherestoragepolicies.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.iaas.fabricvspherestoragepolicies.get_fabric_v_sphere_storage_policies(
            ctx, **kwargs
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting fabricvspherestoragepolicies with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.iaas.fabricvspherestoragepolicies.get_fabric_v_sphere_storage_policies(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "orgId",
                "createdAt",
                "updatedAt",
                "owner",
                "ownerType",
                "externalRegionId",
                "_links",
                "tags[*].id",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "fabricvspherestoragepolicies"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj
