from hashlib import sha256
from typing import Any

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, **kwargs):
    """
    :param string name: (required) name of the resource
    """

    try:
        state = GerriteventsStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: gerritevents")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """
    :param string name: (required) name of the resource
    """
    try:
        state = GerriteventsStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: gerritevents")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = GerriteventsStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: gerritevents")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = GerriteventsStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: gerritevents")
        hub.log.error(str(error))
        raise error


class GerriteventsState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    async def present(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        # Prevent read operation if read is denied
        if "r" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Read operation is denied for resource gerritevents "{name}"',
            )

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource gerritevents "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource gerritevents name={name} already exists.",
                    old=s,
                    new=s,
                )

        return StateReturn(
            result=False,
            comment=f"Resource gerritevents is not present in the environment.",
        )

    async def absent(self, hub, ctx, name: str, **kwargs):
        return StateReturn(
            result=True,
            comment=f"State absent is not available for resource gerritevents.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-gerritevents"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"ro-{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.pipeline.gerritevents.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.pipeline.triggers.get_all_gerrit_events_using_get(
            ctx, **kwargs
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting gerritevents with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.pipeline.triggers.get_all_gerrit_events_using_get(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = None

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "gerritevents"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
class GerriteventsStateImpl(GerriteventsState):
    async def present(self, hub, ctx, name: str, type: Any, value: Any, **kwargs):
        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for itm in search_result.get("links", []):
            s = search_result.documents.get(itm)
            if name == s.get("name", "") and True:
                hub.log.info(
                    f'Returning resource triggers "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource triggers {name} already exists.",
                    old=s,
                    new=s,
                )
        res = (
            await hub.exec.vra.pipeline.triggers.create_variable_using_post(
                ctx, name, type, value, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of triggers {name} success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):
        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for itm in search_result.get("links", []):
            s = search_result.documents.get(itm)
            if name == s.get("name", "") and True:
                hub.log.info(
                    f'Found resource triggers "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!
            delete_kwargs = {}
            delete_kwargs["p_id"] = resource.get("id")

            hub.log.debug(f"triggers with name = {resource.get('name')} already exists")
            await hub.exec.vra.pipeline.triggers.delete_variable_by_id_using_delete(
                ctx, **delete_kwargs
            )

            return StateReturn(
                result=True,
                comment=f"Resource with name = {resource.get('name')} deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name = {name} is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)
        for itm in res.get("ret", {}).get("links", []):

            # Keep track of name and id properties as they may get remapped
            obj = res.get("ret", {}).get("documents", {}).get(itm)
            obj_name = obj.get("name", "unknown")
            obj_id = obj.get("id", "unknown")

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.pipeline.triggers.present": props
            }

        return result
