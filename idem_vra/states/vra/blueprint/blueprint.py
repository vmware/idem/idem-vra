from hashlib import sha256

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import args_decoder
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, **kwargs):

    """

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about

    :param string id: (optional in body) Object ID

    :param string createdAt: (optional in body) Created time

    :param string createdBy: (optional in body) Created by

    :param string updatedAt: (optional in body) Updated time

    :param string updatedBy: (optional in body) Updated by

    :param string orgId: (optional in body) Org ID

    :param string projectId: (optional in body) Project ID

    :param string projectName: (optional in body) Project Name

    :param string selfLink: (optional in body) Blueprint self link

    :param string name: (optional in body) Blueprint name

    :param string description: (optional in body) Blueprint description

    :param string status: (optional in body) Blueprint status

    :param string content: (optional in body) Blueprint YAML content

    :param boolean valid: (optional in body) Validation result on update

    :param array validationMessages: (optional in body) Validation messages

    :param integer totalVersions: (optional in body) Total versions

    :param integer totalReleasedVersions: (optional in body) Total released versions

    :param boolean requestScopeOrg: (optional in body) Flag to indicate blueprint can be requested from any project in org

    :param string contentSourceId: (optional in body) Content source id

    :param string contentSourcePath: (optional in body) Content source path

    :param string contentSourceType: (optional in body) Content source type

    :param string contentSourceSyncStatus: (optional in body) Content source last sync status

    :param array contentSourceSyncMessages: (optional in body) Content source last sync messages

    :param string contentSourceSyncAt: (optional in body) Content source last sync time

    """

    try:
        state = BlueprintStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: blueprint")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """

    :param string p_blueprintId: (required in path)

    :param string apiVersion: (optional in query) The version of the API in yyyy-MM-dd format (UTC). For versioning
      information please refer to /blueprint/api/about

    """

    """
    :param string name: (required) name of the resource
    """
    try:
        state = BlueprintStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: blueprint")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = BlueprintStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: blueprint")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = BlueprintStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: blueprint")
        hub.log.error(str(error))
        raise error


class BlueprintState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    @args_decoder
    async def present(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:

                # Prevent read operation if read is denied
                if "r" in prohibit_ops:
                    return StateReturn(
                        result=False,
                        comment=f'Read operation is denied for resource blueprint "{name}"',
                    )

                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource blueprint "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for blueprint  p_blueprintId,
                # update_optional_for blueprint  apiVersion, id, createdAt, createdBy, updatedAt, updatedBy, orgId, projectId, projectName, selfLink, name, description, status, content, valid, validationMessages, totalVersions, totalReleasedVersions, requestScopeOrg, contentSourceId, contentSourcePath, contentSourceType, contentSourceSyncStatus, contentSourceSyncMessages, contentSourceSyncAt,
                s["id"]

                # Prevent update operation if update is denied
                if "u" in prohibit_ops:
                    remapped_resolved_resource = await self.remap_resource_structure(
                        hub, ctx, s
                    )
                    return StateReturn(
                        result=True,
                        comment=f'Update operation is denied for resource blueprint "{name}"',
                        old=remapped_resolved_resource,
                        new=remapped_resolved_resource,
                    )

                if ctx.get("test", False):

                    # Prevent dry run for resources with unsupported test
                    if not self.is_dry_run_supported:
                        return StateReturn(
                            result=False,
                            comment=f"Resource blueprint does not support test",
                        )

                    result = dict(
                        comment=[],
                        old_state=None,
                        new_state=None,
                        name=name,
                        result=True,
                    )
                    result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                        enforced_state=s,
                        desired_state={
                            "blueprintId": p_blueprintId,
                        },
                    )
                    result["comment"] = hub.tool.vra.state_utils.would_update_comment(
                        resource_type="blueprint", name=name
                    )
                    return result
                else:
                    res = await hub.exec.vra.blueprint.blueprint.update_blueprint_using_put(
                        ctx, p_blueprintId, **kwargs
                    )
                    if res.get("result", False) == True:
                        res["ret"] = await self.remap_resource_structure(
                            hub, ctx, res["ret"]
                        )
                        res["ret"]["resource_id"] = res["ret"]["id"]
                        return StateReturn(
                            result=True,
                            comment=f"Updated blueprint name={name} successfully.",
                            old=s,
                            new=res["ret"],
                        )
                    else:
                        hub.log.error(f"name={name} {res['comment']}")
                        return StateReturn(
                            result=False,
                            comment=f"Update of blueprint name={name} failed.",
                        )
        kwargs["name"] = name

        # Prevent create operation if create is denied
        if "c" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Create operation is denied for resource blueprint "{name}"',
            )

        if ctx.get("test", False):

            # Prevent dry run for resources with unsupported test
            if not self.is_dry_run_supported:
                return StateReturn(
                    result=False, comment=f"Resource blueprint does not support test"
                )

            result = dict(
                comment=[], old_state=None, new_state=None, name=name, result=True
            )

            result["new_state"] = hub.tool.vra.state_utils.generate_test_state(
                enforced_state={}, desired_state={}
            )
            result["comment"] = hub.tool.vra.state_utils.would_create_comment(
                resource_type="blueprint", name=name
            )
            return result
        else:
            res = await hub.exec.vra.blueprint.blueprint.create_blueprint_using_post(
                ctx, **kwargs
            )
            if res.get("result", False) == True:
                res["ret"] = await self.remap_resource_structure(hub, ctx, res["ret"])
                return StateReturn(
                    result=True,
                    comment=f"Creation of blueprint name={name} success.",
                    old=None,
                    new=res["ret"],
                )
            else:
                hub.log.error(f"name={name} {res['comment']}")
                return StateReturn(
                    result=False, comment=f"Creation of blueprint name={name} failed."
                )

    async def absent(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource blueprint "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            # Prevent delete operation if delete is denied
            if "d" in prohibit_ops:
                return StateReturn(
                    result=False,
                    comment=f'Delete operation is denied for resource blueprint "{name}"',
                )

            delete_kwargs = {}
            delete_kwargs["p_blueprintId"] = resource.get("id")

            hub.log.debug(f"blueprint with name={name} already exists")
            if ctx.get("test", False):

                # Prevent dry run for resources with unsupported test
                if not self.is_dry_run_supported:
                    return StateReturn(
                        result=False,
                        comment=f"Resource blueprint does not support test",
                    )

                result = dict(
                    comment=[], old_state=None, new_state=None, name=name, result=True
                )
                result["old_state"] = hub.tool.vra.state_utils.generate_test_state(
                    enforced_state=resource, desired_state={}
                )
                result["comment"] = hub.tool.vra.state_utils.would_delete_comment(
                    resource_type="blueprint", name=name
                )
                return result
            else:
                res = await hub.exec.vra.blueprint.blueprint.delete_blueprint_using_delete(
                    ctx, **delete_kwargs
                )
                if res.get("result", False) == True:
                    return StateReturn(
                        result=True,
                        comment=f"Resource blueprint with name={name} deleted.",
                        old=resource,
                        new=None,
                    )
                else:
                    return StateReturn(
                        result=False,
                        comment=f"Resource blueprint with name={name} deletion failed.",
                    )

        return StateReturn(
            result=True,
            comment=f"Resource blueprint with name={name} is already absent.",
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-blueprint"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.blueprint.blueprint.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.blueprint.blueprint.list_blueprints_using_get(
            ctx, **kwargs
        )

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting blueprint with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.blueprint.blueprint.list_blueprints_using_get(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "orgId",
                "createdAt",
                "updatedAt",
                "createdBy",
                "updatedBy",
                "projectName",
                "status",
                "valid",
                "validationMessages",
                "totalVersions",
                "totalReleasedVersions",
                "contentSourceId",
                "contentSourcePath",
                "contentSourceType",
                "contentSourceSyncStatus",
                "contentSourceSyncMessages",
                "contentSourceSyncAt",
                "selfLink",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "blueprint"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
from hashlib import sha256
from copy import deepcopy
import base64


class BlueprintStateImpl(BlueprintState):
    @args_decoder
    async def present(self, hub, ctx, name: str, versions: dict[str] = [], **kwargs):

        search_result = (
            await self.paginate_find(
                hub,
                ctx,
                select=[
                    "projectId",
                    "name",
                    "description",
                    "content",
                    "requestScopeOrg",
                    "totalVersions",
                ],
            )
        )["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(
                    f'Updating resource blueprint "{s["name"]}" as resource "{name}" exists'
                )
                # update_requires_for blueprint  p_blueprintId,
                # update_optional_for blueprint  apiVersion, id, createdAt, createdBy, updatedAt, updatedBy, orgId, projectId, projectName, selfLink, name, description, status, content, valid, validationMessages, totalVersions, totalReleasedVersions, requestScopeOrg, contentSourceId, contentSourcePath, contentSourceType, contentSourceSyncStatus, contentSourceSyncMessages, contentSourceSyncAt,
                p_blueprintId = s["id"]

                kwargs["name"] = name
                kwargs["content"] = base64.b64decode(
                    kwargs.get("content").encode("utf-8")
                ).decode("utf-8")

                if "resource_id" in kwargs:
                    del kwargs["resource_id"]

                res = (
                    await hub.exec.vra.blueprint.blueprint.update_blueprint_using_put(
                        ctx, p_blueprintId, **kwargs
                    )
                )["ret"]
                res = await self.remap_resource_structure(hub, ctx, res)

                res["resource_id"] = res["id"]

                return StateReturn(
                    result=True,
                    comment=f"Updated blueprint {name} successfully.",
                    old=s,
                    new=res,
                )
        kwargs["name"] = name
        if "resource_id" in kwargs:
            del kwargs["resource_id"]

        kwargs["content"] = base64.b64decode(
            kwargs.get("content").encode("utf-8")
        ).decode("utf-8")
        res = (
            await hub.exec.vra.blueprint.blueprint.create_blueprint_using_post(
                ctx, **kwargs
            )
        )["ret"]

        # Publish and release versions if there are any
        if versions:
            for version in versions:
                update_payload = deepcopy(kwargs)
                update_payload["content"] = base64.b64decode(
                    hub.tool.binary_data.uncompress_string(
                        version.get("content").encode("utf-8")
                    )
                ).decode("utf-8")
                await hub.exec.vra.blueprint.blueprint.update_blueprint_using_put(
                    ctx, res["id"], **update_payload
                )
                await hub.exec.vra.blueprint.blueprint.create_blueprint_version_using_post(
                    ctx,
                    res["id"],
                    version=version.get("version"),
                    description=version.get("description", ""),
                    changeLog=version.get("changeLog", ""),
                    release=version.get("release", False),
                )

            # restore the current draft
            if kwargs.get("content"):
                kwargs["content"] = base64.b64decode(
                    kwargs.get("content").encode("utf-8")
                ).decode("utf-8")
                await hub.exec.vra.blueprint.blueprint.update_blueprint_using_put(
                    ctx, res["id"], **kwargs
                )

        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of blueprint {name} success.",
            old=None,
            new=res,
        )

    async def describe(self, hub, ctx):

        result = {}
        res = await self.paginate_find(
            hub,
            ctx,
            select=[
                "projectId",
                "name",
                "description",
                "content",
                "requestScopeOrg",
                "totalVersions",
            ],
        )

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown"
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            # Get versions
            versions = []
            if obj.get("totalVersions", 0) > 0:
                versions_response = await hub.exec.vra.blueprint.blueprint.list_blueprint_versions_using_get(
                    ctx,
                    p_blueprintId=obj.get("id"),
                    select=[
                        "version",
                        "status",
                        "versionDescription",
                        "versionChangeLog",
                        "content",
                        "createdAt",
                    ],
                    orderby="createdAt asc",
                    size=200,
                )
                versions = [
                    {
                        "content": hub.tool.binary_data.compress_string(
                            v.get("content")
                        ),
                        "version": v.get("version"),
                        "description": v.get("versionDescription"),
                        "changeLog": v.get("versionChangeLog"),
                        "release": v.get("status") == "RELEASED",
                    }
                    for v in versions_response.ret.content
                ]

            obj["content"] = base64.b64encode(
                obj.get("content").encode("utf-8")
            ).decode("utf-8")
            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"versions": versions, "resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.blueprint.blueprint.present": props
            }

        return result
