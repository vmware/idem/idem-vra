from hashlib import sha256
from typing import Any

import semver

from idem_vra.helpers.mapper import add_properties
from idem_vra.helpers.mapper import encode_dict_values
from idem_vra.helpers.mapper import omit_properties
from idem_vra.helpers.models import StateReturn


__contracts__ = ["resource"]

TREQ = {"present": {"require": ["vra.iaas.project.present"]}, "absent": {"require": []}}


async def present(hub, ctx, name: str, **kwargs):
    """
    :param string name: (required) name of the resource
    """

    try:
        state = SecretsStateImpl(hub, ctx)
        return await state.present(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing present state: secrets")
        hub.log.error(str(error))
        raise error


async def absent(hub, ctx, name: str, **kwargs):

    """
    :param string name: (required) name of the resource
    """
    try:
        state = SecretsStateImpl(hub, ctx)
        return await state.absent(hub, ctx, name, **kwargs)
    except Exception as error:
        hub.log.error("Error during enforcing absent state: secrets")
        hub.log.error(str(error))
        raise error


async def describe(hub, ctx):
    try:
        state = SecretsStateImpl(hub, ctx)
        return await state.describe(hub, ctx)
    except Exception as error:
        hub.log.error("Error during describe: secrets")
        hub.log.error(str(error))
        raise error


def is_pending(hub, ret: dict, state: str = None, **pending_kwargs):
    try:
        state = SecretsStateImpl(hub, None)
        return state.is_pending(hub, ret, state, **pending_kwargs)
    except Exception as error:
        hub.log.error("Error during is_pending: secrets")
        hub.log.error(str(error))
        raise error


class SecretsState:
    def __init__(self, hub, ctx):
        self.hub = hub
        self.ctx = ctx
        self.is_dry_run_supported = False

    async def present(self, hub, ctx, name: str, **kwargs):

        prohibit_ops = ""
        if "__prohibit_ops" in kwargs:
            prohibit_ops = (kwargs.get("__prohibit_ops") or "").lower()
            del kwargs["__prohibit_ops"]
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.16.0") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.16.0 max:9.0.0 target:{ctx.acct.vra_version} for resource:secrets"
            )
            return StateReturn(
                result=True,
                comment=f"State present is not available for resource secrets.",
            )

        # Prevent read operation if read is denied
        if "r" in prohibit_ops:
            return StateReturn(
                result=False,
                comment=f'Read operation is denied for resource secrets "{name}"',
            )

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Returning resource secrets "{s["name"]}" due to existing resource name={name}'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                return StateReturn(
                    result=True,
                    comment=f"Resource secrets name={name} already exists.",
                    old=s,
                    new=s,
                )

        return StateReturn(
            result=False, comment=f"Resource secrets is not present in the environment."
        )

    async def absent(self, hub, ctx, name: str, **kwargs):
        return StateReturn(
            result=True, comment=f"State absent is not available for resource secrets."
        )

    async def describe(self, hub, ctx):
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.16.0") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.16.0 max:9.0.0 target:{ctx.acct.vra_version} for resource:secrets"
            )
            return {}

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown-secrets"
            # Santize object name to avoid parsing errors later on
            obj_name = obj_name.replace(":", "_")
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"ro-{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.platform.secrets.present": props
            }

        return result

    async def paginate_find(self, hub, ctx, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        res = await hub.exec.vra.platform.secrets.get_all_secrets_v2(ctx, **kwargs)

        numberOfElements = res.get("ret", {}).get("numberOfElements", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        initialElements = numberOfElements
        if numberOfElements != totalElements and totalElements != 0:
            while initialElements < totalElements:
                hub.log.debug(
                    f"Requesting secrets with offset={initialElements} out of {totalElements}"
                )
                pres = await hub.exec.vra.platform.secrets.get_all_secrets_v2(
                    ctx, skip=initialElements, **kwargs
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]
                initialElements += max(
                    pres.get("ret", {}).get("numberOfElements", 0), len(aggN)
                )
                res["ret"]["numberOfElements"] = initialElements

        return res

    def is_pending(self, hub, ret: dict, state: str = None, **pending_kwargs):
        """
        State reconciliation
        """
        hub.log.debug(f'Running is_pending for resource: {ret.get("__id__", None)}...')
        is_pending_result = False
        hub.log.debug(
            f'is_pending_result for resource "{ret.get("__id__", None)}": {is_pending_result}'
        )
        return is_pending_result

    async def remap_resource_structure(self, hub, ctx, obj: dict) -> dict:
        schema_mapper = {
            "add": [],
            "omit": [
                "createdAt",
                "createdBy",
                "updatedAt",
                "updatedBy",
                "orgId",
                "projectName",
                "projectId",
                "projectIds",
            ],
        }

        # Perform resource mapping by adding properties and omitting properties.
        # Property renaming is addition followed by omission.
        if schema_mapper:
            resource_name = "secrets"
            hub.log.debug(f"Remapping resource {resource_name}...")
            obj = await add_properties(obj, schema_mapper.get("add", []))
            obj = omit_properties(obj, schema_mapper.get("omit", []))
            obj = encode_dict_values(obj)

        return obj


# ====================================
# State override
# ====================================
class SecretsStateImpl(SecretsState):
    async def present(self, hub, ctx, name: str, value: Any, **kwargs):
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.16.0") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.16.0 max:9.0.0 target:{ctx.acct.vra_version} for resource:secrets"
            )
            return {}

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        for s in search_result.content:
            if name == s["name"] and True:
                s = await self.remap_resource_structure(hub, ctx, s)
                s["resource_id"] = s["id"]

                hub.log.info(f' Resource secret"{name}" exists')

                return StateReturn(
                    result=True, comment=f"Update not supported.", old=s, new=None
                )
        res = (
            await hub.exec.vra.platform.secrets.create_secret_v2(
                ctx, name, value, **kwargs
            )
        )["ret"]
        res = await self.remap_resource_structure(hub, ctx, res)

        return StateReturn(
            result=True,
            comment=f"Creation of secret '{name}' success.",
            old=None,
            new=res,
        )

    async def absent(self, hub, ctx, name: str, **kwargs):

        search_result = (await self.paginate_find(hub, ctx))["ret"]

        resource = None
        for s in search_result.content:
            if name == s["name"] and True:
                hub.log.info(
                    f'Found resource secret "{s["name"]}" due to existing resource "{name}"'
                )
                s = await self.remap_resource_structure(hub, ctx, s)
                resource = s

        if resource:
            # it exists!

            hub.log.debug(f"Secret with name '{resource.get('name')}' already exists")
            req = await hub.exec.vra.rest.request(
                ctx, path=f"/platform/api/secrets/{resource.get('id')}", method="delete"
            )
            hub.log.debug(f"vra.rest.request: {req}")

            return StateReturn(
                result=True,
                comment=f"Resource with name '{resource.get('name')}' deleted.",
                old=resource,
                new=None,
            )

        return StateReturn(
            result=True,
            comment=f"Resource with name '{name}' is already absent.",
            old=None,
            new=None,
        )

    async def describe(self, hub, ctx):
        # version check
        if "vra_version" in ctx.acct and (
            semver.compare(ctx.acct.vra_version, "8.16.0") == -1
            or semver.compare("9.0.0", ctx.acct.vra_version) == -1
        ):
            hub.log.warning(
                f"Unsupported version min:8.16.0 max:9.0.0 target:{ctx.acct.vra_version} for resource:secrets"
            )
            return {}

        result = {}
        res = await self.paginate_find(hub, ctx)

        for obj in res.get("ret", {}).get("content", []):

            # Keep track of name and id properties as they may get remapped
            obj_name = obj.get("name") or "unknown"
            obj_id = obj.get("id") or sha256(obj_name.encode()).hexdigest()

            # Add resource_id to any resource
            res = {"resource_id": obj_id}
            obj = {**obj, **res}

            obj["orgScoped"] = (
                False if obj.get("orgScoped") is None else obj["orgScoped"]
            )

            if obj.get("projectId", None) == None and obj.get("orgScoped") == False:
                # Add projectIds
                res_pIds = (await self.find_projectIds(hub, ctx, obj_id))["ret"]
                projectIds = [p.get("id", None) for p in res_pIds.get("content", None)]
                obj["projectIdsToAdd"] = projectIds
            elif obj.get("projectId", None) != None:
                obj["projectIdsToAdd"] = [obj.get("projectId")]

            obj = await self.remap_resource_structure(hub, ctx, obj)

            # Define props
            props = [{key: value} for key, value in obj.items()]

            # Build result
            result[f"{obj_name}-{obj_id.split('-')[-1]}"] = {
                "vra.platform.secrets.present": props
            }

        return result

    async def find_projectIds(self, hub, ctx, p_id, **kwargs):
        """
        Paginate through all resources using their 'find' method.
        """
        kwargs["size"] = 100
        res = await hub.exec.vra.platform.secrets.get_projects_linked_to_secret(
            ctx, p_id, **kwargs
        )

        pageNumber = res.get("ret", {}).get("pageable", {}).get("pageNumber", 0) + 1
        totalPages = res.get("ret", {}).get("totalPages", 0)
        totalElements = res.get("ret", {}).get("totalElements", 0)
        if pageNumber != totalPages and totalElements != 0:
            while pageNumber < totalPages:
                hub.log.debug(
                    f"Requesting projects with page={pageNumber} out of {totalPages}"
                )
                pres = (
                    await hub.exec.vra.platform.secrets.get_projects_linked_to_secret(
                        ctx, p_id, page=pageNumber, **kwargs
                    )
                )
                pageNumber = (
                    pres.get("ret", {}).get("pageable", {}).get("pageNumber", 0) + 1
                )
                aggO = res.get("ret", {}).get("content", [])
                aggN = pres.get("ret", {}).get("content", [])
                res["ret"]["content"] = [*aggO, *aggN]

        return res
