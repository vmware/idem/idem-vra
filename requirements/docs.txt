-r base.txt
sphinx==7.1.2
furo>=2022.9.29
sphinx-copybutton>=0.5.0
Sphinx-Substitution-Extensions>=2022.2.16
sphinx-notfound-page>=0.8.3
sphinx-design
